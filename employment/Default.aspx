﻿<%@ Page Title="Employment Law Attorney, Attorneys, Lawyer, Lawyers | Law Office of Marc E. Grossman, Upland, California" Language="C#" MasterPageFile="~/MasterPages/SiteMainInternal.master" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="employment_Default" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
<meta name="keywords" content="lawyer, attorney, law firm, law office, legal advice">
<meta name="description" content="Free information about employment law. Call 1-888-407-9068 for a free consultation with an Upland, California lawyer at the Law Offices of Marc E. Grossman.">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainLogo" Runat="Server">
    <img src="../images/teamofattorneys2.jpg" style="width: 800px; height: 244px"/>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="PageTitleH1" Runat="Server">
<h1 id="pageTitle">Upland Employment Law Attorneys</h1>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="MainContent" Runat="Server">
<p>Independent contractors are not treated the same as employees. While independent contractors have more freedom in their work, they also lack some of the protections enjoyed by traditional employees, such as workers' compensation and unemployment benefits. They are also responsible for paying their own taxes directly to the Internal Revenue Service from the first dollar, since their taxes are not withheld by the business that is paying them. If you have a question as to whether you should work as an independent contractor or as an employee, contact an experienced employment law attorney today to discuss your situation.</p>
        <h3>Frequently Asked Questions about Employment Law for the Employee</h3>
        <p>
          <strong>Q: What laws must employers follow when hiring new employees?</strong>
        </p>
        <p>
          <strong>A:</strong> A prospective employer must avoid any illegal discrimination based on race, national origin, gender, pregnancy, age, disability or religion during the hiring process. Employers should also be sure to protect the privacy rights of applicants by protecting confidential or private information provided by the applicant and by disclosing to the applicant any background or credit checks that the employer wishes to perform. Employers are required to follow all applicable documentation rules regarding immigration and take care not to discriminate against applicants over 40 because of their age.</p>
        <p>
          <strong>Q: Can employers monitor their employees' Internet usage or read their e-mails?</strong>
        </p>
        <p>
          <strong>A:</strong> The Supreme Court has found that employees have very limited rights to privacy in their employers' computer systems. Employers may monitor Web sites visited by their employees and may block their employees from visiting certain Web sites. Employers can also limit employees' Internet usage to business-related Web sites. If the employer has a company policy that its computer systems are to be used only for work-related activities, it may reprimand or punish an employee who used its equipment for personal purposes. E-mails are considered to be company property if they are sent using the company e-mail system, and many employers monitor or archive all incoming and outgoing e-mails sent through their systems.</p>
      <h2>Are you in need of a trusted employment law attorney?</h2>
      <p>The Law Offices of Marc E. Grossman can help. If you have been the victim of workplace <a href="/discrimination-harassment/">discrimination and harassment</a> or any other type of mistreatment, we are here for you. We believe you deserve a lawyer who will work hard to get you results. We want to make sure that you are happy with the service we provide, because we want you to turn to us whenever you have a legal matter to face.</p>
      <p>For more information about how our Upland, California, law firm can help you, <a href="/Forms/contact.aspx" rel="nofollow">E-mail us</a> or call us toll free at 1-888-407-9068 for a free initial consultation.</p>
      <h3>Employment Law, Employee - An Overview</h3>
      <p>Employment law covers the relationships between employers and their current, prospective and former employees. Both federal and state laws control various aspects of the employer-employee relationship, including each side's rights and obligations. Because of the complexity of the employment relationship, this area of law involves issues as diverse as discrimination, recordkeeping, taxation and workplace safety.</p>
      <p>There are also different types of employment relationships. Employment relationships can be based on a contract, or they can be "at-will." If the employment relationship is based on a valid contract entered into by the employer and the employee, the terms of that contract will govern the relationship. By contrast, an at-will employment arrangement can be terminated at any time, with or without reason, by either the employer (as long as the reason does not constitute illegal discrimination) or the employee.</p>
      <p>With all these factors to consider, it is clear why employment law is such a complex area.
	If you have an employment law concern, contact an employment lawyer at Law Office of Marc E. Grossman in Upland, California, who can provide sound advice and skilled representation in a range of workplace-related matters.
	</p>
      <h3>Federal Regulations on Employment Relationships</h3>
      <p>Numerous federal laws apply to employment nationwide. Some laws affect only employers over a certain size, while others have different restrictions. The following is a quick summary of the major federal employment laws:</p>
      <h3>Title VII of the Civil Rights Act of 1964, as amended:</h3>
      <ul>
        <li>Applies only to employers with 15 or more employees</li>
        <li>Prohibits employers from discriminating based on race, color, religion, sex, national origin or pregnancy</li>
      </ul>
      <h3>Americans with Disabilities Act (ADA):</h3>
      <ul>
        <li>Applies only to employers with 15 or more employees</li>
        <li>Defines a disability as a physical or mental impairment that substantially limits one or more major life activities</li>
        <li>Is designed to prohibit discrimination against workers with disabilities</li>
        <li>Provides that if an individual with a disability can perform the essential functions of the job, with reasonable accommodation, that person cannot be discriminated against on the basis of the disability</li>
      </ul>
      <h3>Age Discrimination in Employment Act (ADEA):</h3>
      <ul>
        <li>Applies only to employers with 20 or more employees</li>
        <li>Applies only to employees who are 40 years old or older</li>
        <li>Prevents employers from giving preferential treatment to younger workers to the exclusion of older workers when it comes to hiring, pay, benefits such as health insurance, job assignments and promotions</li>
        <li>Does not prevent an employer from favoring older employees over younger employees</li>
      </ul>
      <h3>Fair Labor Standards Act (FLSA):</h3>
      <ul>
        <li>Applies to businesses that gross $500,000 or more per year and to other specific types of businesses</li>
        <li>Provides that qualified employees who work more than 40 hours in a week should receive time-and-a-half pay for the overtime</li>
        <li>Does not provide regulation as to the number and duration of breaks an employer must allow, but individual states may do so</li>
        <li>Specifies minimum wage requirements</li>
      </ul>
      <h3>Family and Medical Leave Act (FMLA):</h3>
      <ul>
        <li>Applies only to employers with 50 or more employees within 75 miles of the workplace</li>
        <li>Applies only to employees who have worked for the employer for at least 12 months and 1,250 hours in the year preceding the leave</li>
        <li>Provides that employers must allow employees to take up to a 12-week unpaid leave of absence for qualified family and medical reasons</li>
        <li>Preserves qualified employees' positions for the duration of the leave</li>
        <li>Employees generally cannot be punished or demoted for taking valid FMLA leave</li>
      </ul>
      <h3>Employee Rights in the Workplace</h3>
      <p>All employees have basic rights in the workplace. Those rights include privacy and freedom from illegal discrimination. In addition to federal law, each state has enacted laws to protect the rights of workers. A job applicant also has certain rights even prior to being hired as an employee. Those rights include the right to be free from discrimination based on age, gender, race, national origin or religion during the hiring process.</p>
      <p>In most states, employees have a right to privacy in the workplace. This right to privacy can include one's personal possessions, including handbags or briefcases, and storage lockers accessible only by employees. Employees also have a right to privacy in their personal telephone conversations. Employees have very little privacy or right to privacy, however, in their messages on company e-mail and their Internet usage on the employer's computer system.</p>
      <p>There are certain pieces of information that an employer may not seek out concerning a potential applicant or an employee. An employer may not conduct a credit or background check of an employee or a prospective employee unless the employer notifies the employee or applicant in writing that it intends to do so and receives authorization to do so.</p>
      <p>In addition, most private employers may not require an employee or a prospective employee to submit to a polygraph (lie-detector test). There are very narrow exceptions to this rule if the employee is suspected of being involved in an incident that caused economic loss or injury to the employer or if the employee is being considered to drive an armored car, work for a security company, work with controlled substances or work in national security.</p>
      <h3>Conclusion</h3>
      <p>Employees have a variety of rights in the workplace, through both federal and state law. Employers, however, also have rights and protections under the law. It is important for both employers and employees to be aware of their legal rights and the duties they owe to each other.

	If you are an employee and you feel your rights have been violated by your employer, get in touch with an experienced employment law attorney at Law Office of Marc E. Grossman in Upland, California, to ensure that your rights are protected.
	</p>
      <p>Copyright ©2009
FindLaw, a Thomson Business</p>
      <p>DISCLAIMER: This site and any information contained herein are intended for informational purposes only and should not be construed as legal advice. Seek competent legal counsel for advice on any legal matter.</p>
</asp:Content>

