﻿<%@ Page Title="Upland Personal Injury Lawyer, Attorney | Law Offices of Marc Grossman" Language="C#" MasterPageFile="~/MasterPages/SiteMainInternal.master" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="personal_injury_Default" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
<meta name="keywords" content="lawyer, attorney, law firm, law office, legal advice, personal injury, car accident, brain injury, slip and fall inujries, premises liability, product liability, truck, crash, spinal cord injury, lawyer, attorney,">
<meta name="description" content="Do you need an experienced Upland Personal Injury attorney to handle your case? Call 1-888-407-9068 for a free consultation with an Upland, California lawyer at the Law Office of Marc E. Grossman that will fight for you.">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainLogo" Runat="Server">
<p style="text-align:center">
<a href="http://www.wefight4you.com" title="Personal Injury Lawyer">
    <img src="../images/teamofattorneys2.jpg" alt="Personal Injury Attorney" style="width: 800px; height: 244px" /></p></a>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="PageTitleH1" Runat="Server">
<h1 id="pageTitle">Upland Personal Injury Lawyers that know the Law</h1>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="MainContent" Runat="Server">
<h2>Injured? Know Your Rights</h2>
<p>If you or a loved one has been hurt in an accident, you want a <strong>personal injury lawyer</strong> who will fight for you. At the Law Office of Marc E. Grossman, we will do much more than that. We will stand by your side to see that you get the resolution that is right for you. We will be there for you when you need us with your personal injury claim.</p>
<h3>Free initial consultations are available</h3>
<p><a href="/Forms/contact.aspx" rel="nofollow">E-mail us</a> or call us toll free at 1-888-407-9068 to discuss your case with a trusted <a href="/attorneys/Default.aspx" title="personal injury attorney">personal injury attorney</a>.</p>
<p>During your initial consultation, we will spend a significant amount of time learning about you and analyzing your personal injury case. Whether you hire us or not we want to make certain that you get started down the right path.</p>
<h3>We've handled hundreds of personal injury claims</h3>
<p><a href="/Firm-Overview/">Our Personal injury law firm</a> is here to serve accident victims in Upland, California, and throughout the Inland Empire.</p>
<p>We are able to take on cases involving brain injury, spinal cord injury, broken bones, closed head injury, loss of limb, burns and any other type of personal injury. We take all cases, including:</p>
<ul>
   <li>Car Accidents</li>
   <li>Truck Crash Cases</li>
   <li><a href="/motorcycle-accidents/">Motorcycle Accidents</a></li>
   <li>Premises Liability</li>
   <li><a href="/dog-bites/">Dog Bites</a></li>
   <li>Slip and Fall Injuries</li>
   <li>Product Liability</li>
   <li><a href="/work-injury/">Work injury</a></li>
</ul>

<p>We also handle cases involving accidents that resulted in a <a href="/wrongful-death/">wrongful death</a>.</p>
<p>Customer service means a lot to us. We want you to know that when you choose our law firm to handle your case, we will be there for you every step of the way. At the outset, we will make certain you understand the process. We will let you know your options and the potential outcomes. We will do everything within our power to see that you get fair compensation.</p>
<p>For a free initial consultation about any personal injury case, <a href="/Forms/contact.aspx" rel="nofollow">e-mail us</a> or call us toll free at 1-888-407-9068.</p>
</asp:Content>

