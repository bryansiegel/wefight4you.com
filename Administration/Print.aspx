﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Print.aspx.cs" Inherits="Administration_Print" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>

    <p><a href="javascript:window.print()">Would You Like to Print?</a></p>
        <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" 
            DataSourceID="SqlDataSource1" EnableModelValidation="True">
            <Columns>
                <asp:BoundField DataField="case_type" HeaderText="case_type" 
                    SortExpression="case_type" />
                <asp:BoundField DataField="name" HeaderText="name" SortExpression="name" />
                <asp:BoundField DataField="filing_status" HeaderText="filing_status" 
                    SortExpression="filing_status" />
                <asp:BoundField DataField="staff" HeaderText="staff" SortExpression="staff" />
                <asp:BoundField DataField="case_actions" HeaderText="case_actions" 
                    SortExpression="case_actions" />
                <asp:BoundField DataField="fee" HeaderText="fee" SortExpression="fee" />
                <asp:BoundField DataField="priority" HeaderText="priority" 
                    SortExpression="priority" />
            </Columns>
        </asp:GridView>

        <asp:SqlDataSource ID="SqlDataSource1" runat="server" 
            ConnectionString="<%$ ConnectionStrings:casefilesConnectionString %>" 
            SelectCommand="SELECT [case_type], [name], [filing_status], [staff], [case_actions], [fee], [priority] FROM [casefiles]"></asp:SqlDataSource>
    </div>
    </form>
</body>
</html>
