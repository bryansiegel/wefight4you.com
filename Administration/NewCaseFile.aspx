﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="NewCaseFile.aspx.cs" Inherits="Administration_NewCaseFile" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    <h1>New Case File</h1>
        <p>
            <asp:FormView ID="FormView1" runat="server" DataKeyNames="id" 
                DataSourceID="SqlDataSource1" DefaultMode="Insert" EnableModelValidation="True" 
                Width="422px">
               
                <InsertItemTemplate>
                    Name:
                    <asp:TextBox ID="nameTextBox" runat="server" Text='<%# Bind("name") %>' />
                    <br />
                    Case Type:
                    <asp:TextBox ID="case_typeTextBox" runat="server" 
                        Text='<%# Bind("case_type") %>' />
                        <br />
                    Filing Status:
                    <asp:TextBox ID="filing_statusTextBox" runat="server" 
                        Text='<%# Bind("filing_status") %>' />
                    <br />
                    Notes:
                    <asp:TextBox ID="case_actionsTextBox" runat="server" 
                        Text='<%# Bind("case_actions") %>' />
                    <br />
                    Staff:
                    <asp:TextBox ID="staffTextBox" runat="server" Text='<%# Bind("staff") %>' />
                    <br />
                    fee:
                    <asp:DropDownList ID="DropDownList1" runat="server" 
                        SelectedValue='<%# Bind("fee") %>'>
                        <asp:ListItem>Contingency</asp:ListItem>
                        <asp:ListItem>Fee</asp:ListItem>
                        <asp:ListItem>Fee2</asp:ListItem>
                    </asp:DropDownList>
                    <br />
                    <asp:LinkButton ID="InsertButton" runat="server" CausesValidation="True" 
                        CommandName="Insert" Text="Insert" />
                    &nbsp;<asp:LinkButton ID="InsertCancelButton" runat="server" 
                        CausesValidation="False" CommandName="Cancel" Text="Cancel" />
                </InsertItemTemplate>
                <ItemTemplate>
                    <br />
                    case_id:
                    <asp:Label ID="case_idLabel" runat="server" Text='<%# Bind("case_id") %>' />
                    <br />
                    case_number:
                    <asp:Label ID="case_numberLabel" runat="server" 
                        Text='<%# Bind("case_number") %>' />
                    <br />
                    case_type:
                    <asp:Label ID="case_typeLabel" runat="server" Text='<%# Bind("case_type") %>' />
                    <br />
                    firstName:
                    <asp:Label ID="firstNameLabel" runat="server" Text='<%# Bind("firstName") %>' />
                    <br />
                    lastName:
                    <asp:Label ID="lastNameLabel" runat="server" Text='<%# Bind("lastName") %>' />
                    <br />
                    name:
                    <asp:Label ID="nameLabel" runat="server" Text='<%# Bind("name") %>' />
                    <br />
                    filing_status:
                    <asp:Label ID="filing_statusLabel" runat="server" 
                        Text='<%# Bind("filing_status") %>' />
                    <br />
                    case_actions:
                    <asp:Label ID="case_actionsLabel" runat="server" 
                        Text='<%# Bind("case_actions") %>' />
                    <br />
                    staff:
                    <asp:Label ID="staffLabel" runat="server" Text='<%# Bind("staff") %>' />
                    <br />
                    created_at:
                    <asp:Label ID="created_atLabel" runat="server" 
                        Text='<%# Bind("created_at") %>' />
                    <br />
                    updated_at:
                    <asp:Label ID="updated_atLabel" runat="server" 
                        Text='<%# Bind("updated_at") %>' />
                    <br />
                    active:
                    <asp:Label ID="activeLabel" runat="server" Text='<%# Bind("active") %>' />
                    <br />
                    fee:
                    <asp:Label ID="feeLabel" runat="server" Text='<%# Bind("fee") %>' />
                    <br />
                    priority:
                    <asp:Label ID="priorityLabel" runat="server" Text='<%# Bind("priority") %>' />
                    <br />
                    phoneNumber:
                    <asp:Label ID="phoneNumberLabel" runat="server" 
                        Text='<%# Bind("phoneNumber") %>' />
                    <br />
                    <asp:LinkButton ID="EditButton" runat="server" CausesValidation="False" 
                        CommandName="Edit" Text="Edit" />
                    &nbsp;<asp:LinkButton ID="DeleteButton" runat="server" CausesValidation="False" 
                        CommandName="Delete" Text="Delete" />
                    &nbsp;<asp:LinkButton ID="NewButton" runat="server" CausesValidation="False" 
                        CommandName="New" Text="New" />
                </ItemTemplate>
            </asp:FormView>
        </p>
        <asp:SqlDataSource ID="SqlDataSource1" runat="server" 
            ConnectionString="<%$ ConnectionStrings:casefilesProductionConnectionString %>" 
            OldValuesParameterFormatString="original_{0}" 
            SelectCommand="SELECT * FROM [casefiles]" 
            ConflictDetection="CompareAllValues" 
            DeleteCommand="DELETE FROM [casefiles] WHERE [id] = @original_id AND (([case_id] = @original_case_id) OR ([case_id] IS NULL AND @original_case_id IS NULL)) AND (([case_number] = @original_case_number) OR ([case_number] IS NULL AND @original_case_number IS NULL)) AND (([case_type] = @original_case_type) OR ([case_type] IS NULL AND @original_case_type IS NULL)) AND (([first_name] = @original_first_name) OR ([first_name] IS NULL AND @original_first_name IS NULL)) AND (([last_name] = @original_last_name) OR ([last_name] IS NULL AND @original_last_name IS NULL)) AND (([case_status] = @original_case_status) OR ([case_status] IS NULL AND @original_case_status IS NULL)) AND (([case_actions] = @original_case_actions) OR ([case_actions] IS NULL AND @original_case_actions IS NULL)) AND (([attorney] = @original_attorney) OR ([attorney] IS NULL AND @original_attorney IS NULL)) AND (([created_at] = @original_created_at) OR ([created_at] IS NULL AND @original_created_at IS NULL)) AND (([updated_at] = @original_updated_at) OR ([updated_at] IS NULL AND @original_updated_at IS NULL)) AND (([active] = @original_active) OR ([active] IS NULL AND @original_active IS NULL)) AND (([fee] = @original_fee) OR ([fee] IS NULL AND @original_fee IS NULL)) AND (([priority] = @original_priority) OR ([priority] IS NULL AND @original_priority IS NULL))" 
            InsertCommand="INSERT INTO [casefiles] ([case_id], [case_number], [case_type], [first_name], [last_name], [case_status], [case_actions], [attorney], [created_at], [updated_at], [active], [fee], [priority]) VALUES (@case_id, @case_number, @case_type, @first_name, @last_name, @case_status, @case_actions, @attorney, @created_at, @updated_at, @active, @fee, @priority)" 
            UpdateCommand="UPDATE [casefiles] SET [case_id] = @case_id, [case_number] = @case_number, [case_type] = @case_type, [first_name] = @first_name, [last_name] = @last_name, [case_status] = @case_status, [case_actions] = @case_actions, [attorney] = @attorney, [created_at] = @created_at, [updated_at] = @updated_at, [active] = @active, [fee] = @fee, [priority] = @priority WHERE [id] = @original_id AND (([case_id] = @original_case_id) OR ([case_id] IS NULL AND @original_case_id IS NULL)) AND (([case_number] = @original_case_number) OR ([case_number] IS NULL AND @original_case_number IS NULL)) AND (([case_type] = @original_case_type) OR ([case_type] IS NULL AND @original_case_type IS NULL)) AND (([first_name] = @original_first_name) OR ([first_name] IS NULL AND @original_first_name IS NULL)) AND (([last_name] = @original_last_name) OR ([last_name] IS NULL AND @original_last_name IS NULL)) AND (([case_status] = @original_case_status) OR ([case_status] IS NULL AND @original_case_status IS NULL)) AND (([case_actions] = @original_case_actions) OR ([case_actions] IS NULL AND @original_case_actions IS NULL)) AND (([attorney] = @original_attorney) OR ([attorney] IS NULL AND @original_attorney IS NULL)) AND (([created_at] = @original_created_at) OR ([created_at] IS NULL AND @original_created_at IS NULL)) AND (([updated_at] = @original_updated_at) OR ([updated_at] IS NULL AND @original_updated_at IS NULL)) AND (([active] = @original_active) OR ([active] IS NULL AND @original_active IS NULL)) AND (([fee] = @original_fee) OR ([fee] IS NULL AND @original_fee IS NULL)) AND (([priority] = @original_priority) OR ([priority] IS NULL AND @original_priority IS NULL))">
            <DeleteParameters>
                <asp:Parameter Name="original_id" Type="Int32" />
                <asp:Parameter Name="original_case_id" Type="String" />
                <asp:Parameter Name="original_case_number" Type="String" />
                <asp:Parameter Name="original_case_type" Type="String" />
                <asp:Parameter Name="original_first_name" Type="String" />
                <asp:Parameter Name="original_last_name" Type="String" />
                <asp:Parameter Name="original_case_status" Type="String" />
                <asp:Parameter Name="original_case_actions" Type="String" />
                <asp:Parameter Name="original_attorney" Type="String" />
                <asp:Parameter Name="original_created_at" Type="DateTime" />
                <asp:Parameter Name="original_updated_at" Type="DateTime" />
                <asp:Parameter Name="original_active" Type="Decimal" />
                <asp:Parameter Name="original_fee" Type="String" />
                <asp:Parameter Name="original_priority" Type="String" />
            </DeleteParameters>
            <InsertParameters>
                <asp:Parameter Name="case_id" Type="String" />
                <asp:Parameter Name="case_number" Type="String" />
                <asp:Parameter Name="case_type" Type="String" />
                <asp:Parameter Name="first_name" Type="String" />
                <asp:Parameter Name="last_name" Type="String" />
                <asp:Parameter Name="case_status" Type="String" />
                <asp:Parameter Name="case_actions" Type="String" />
                <asp:Parameter Name="attorney" Type="String" />
                <asp:Parameter Name="created_at" Type="DateTime" />
                <asp:Parameter Name="updated_at" Type="DateTime" />
                <asp:Parameter Name="active" Type="Decimal" />
                <asp:Parameter Name="fee" Type="String" />
                <asp:Parameter Name="priority" Type="String" />
            </InsertParameters>
            <UpdateParameters>
                <asp:Parameter Name="case_id" Type="String" />
                <asp:Parameter Name="case_number" Type="String" />
                <asp:Parameter Name="case_type" Type="String" />
                <asp:Parameter Name="first_name" Type="String" />
                <asp:Parameter Name="last_name" Type="String" />
                <asp:Parameter Name="case_status" Type="String" />
                <asp:Parameter Name="case_actions" Type="String" />
                <asp:Parameter Name="attorney" Type="String" />
                <asp:Parameter Name="created_at" Type="DateTime" />
                <asp:Parameter Name="updated_at" Type="DateTime" />
                <asp:Parameter Name="active" Type="Decimal" />
                <asp:Parameter Name="fee" Type="String" />
                <asp:Parameter Name="priority" Type="String" />
                <asp:Parameter Name="original_id" Type="Int32" />
                <asp:Parameter Name="original_case_id" Type="String" />
                <asp:Parameter Name="original_case_number" Type="String" />
                <asp:Parameter Name="original_case_type" Type="String" />
                <asp:Parameter Name="original_first_name" Type="String" />
                <asp:Parameter Name="original_last_name" Type="String" />
                <asp:Parameter Name="original_case_status" Type="String" />
                <asp:Parameter Name="original_case_actions" Type="String" />
                <asp:Parameter Name="original_attorney" Type="String" />
                <asp:Parameter Name="original_created_at" Type="DateTime" />
                <asp:Parameter Name="original_updated_at" Type="DateTime" />
                <asp:Parameter Name="original_active" Type="Decimal" />
                <asp:Parameter Name="original_fee" Type="String" />
                <asp:Parameter Name="original_priority" Type="String" />
            </UpdateParameters>
        </asp:SqlDataSource>
    </div>
    </form>
</body>
</html>
