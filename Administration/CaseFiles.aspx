﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="CaseFiles.aspx.cs" Inherits="Administration_grid" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <style type="text/css">
        .menu{width:auto;}
    .cases {margin-top:15px; padding-top:15px;}
    .header {width:auto; }
    .title {float:left;}
    .login {float:right;}

    </style>
</head>
<body>
    <form id="form1" runat="server">
    <telerik:RadScriptManager ID="RadScriptManager1" runat="server">
    </telerik:RadScriptManager>
    <div class="header">
    <div class="title">
        <h1 style="margin-bottom: 16px">Law Offices of Marc Grossman Casemanager</h1>
    </div>
    <div class="login">
        Hi <asp:LoginName ID="LoginName1" runat="server" /> | 
        <asp:LoginStatus ID="LoginStatus1" runat="server" />
    </div>
    </div>
    <br />
    <br />
    <br />
    
    <!--end head-->
    
    <br />
    <div class="menu">
        <telerik:RadMenu ID="RadMenu1" runat="server" Skin="Windows7" 
            EnableShadows="True" Font-Overline="False">
            <Items>
                <telerik:RadMenuItem runat="server" Text="New">
                    <Items>
                        <telerik:RadMenuItem runat="server" NavigateUrl="~/Administration/NewCaseFile.aspx" 
                            Text="Case File">
                        </telerik:RadMenuItem>
                        <telerik:RadMenuItem runat="server" Text="Legend" NavigateUrl="~/Administration/NewLegend.aspx">
                        </telerik:RadMenuItem>
                    </Items>
                </telerik:RadMenuItem>
                <telerik:RadMenuItem runat="server" Text="Archives"
                Navigateurl="~/Administration/Archives.aspx">
                </telerik:RadMenuItem>
                <telerik:RadMenuItem runat="server" Text="Legend" 
                    NavigateUrl="~/Administration/Legend.aspx">
                </telerik:RadMenuItem>
                <telerik:RadMenuItem runat="server" Text="Print" 
                    NavigateUrl="~/Administration/Print.aspx">
                </telerik:RadMenuItem>
            </Items>
        </telerik:RadMenu>
    </div>
    <br />
    <br />
    <div id="cases">
        <p>
        </p>
        <telerik:RadGrid ID="RadGrid1" runat="server" AllowAutomaticDeletes="True" 
            AllowAutomaticInserts="True" AllowAutomaticUpdates="True" 
            AllowFilteringByColumn="True" AllowPaging="True" AllowSorting="True" 
            DataSourceID="SqlDataSource1" GridLines="None" ShowGroupPanel="True" 
            ShowStatusBar="True" Skin="Windows7">
            <ClientSettings AllowDragToGroup="True">
                <Scrolling AllowScroll="True" UseStaticHeaders="True" />
            </ClientSettings>
<MasterTableView AutoGenerateColumns="False" CommandItemDisplay="Top" DataKeyNames="id" 
                DataSourceID="SqlDataSource1" EditMode="InPlace">
<CommandItemSettings ExportToPdfText="Export to Pdf"></CommandItemSettings>

    <Columns>
        <telerik:GridBoundColumn DataField="priority" HeaderText="Priority" 
            SortExpression="priority" UniqueName="priority">
        </telerik:GridBoundColumn>
        <telerik:GridBoundColumn DataField="id" DataType="System.Int32" HeaderText="id" 
            ReadOnly="True" SortExpression="id" UniqueName="id" Visible="False">
        </telerik:GridBoundColumn>
        <telerik:GridBoundColumn DataField="case_id" HeaderText="case_id" 
            SortExpression="case_id" UniqueName="case_id" Visible="False">
        </telerik:GridBoundColumn>
        <telerik:GridBoundColumn DataField="case_number" HeaderText="case_number" 
            SortExpression="case_number" UniqueName="case_number" Visible="False">
        </telerik:GridBoundColumn>
        <telerik:GridBoundColumn DataField="case_type" HeaderText="Case Type" 
            SortExpression="case_type" UniqueName="case_type">
        </telerik:GridBoundColumn>
        <telerik:GridBoundColumn DataField="first_name" HeaderText="first_name" 
            SortExpression="first_name" UniqueName="first_name" Visible="False">
        </telerik:GridBoundColumn>
        <telerik:GridBoundColumn DataField="last_name" HeaderText="Name" 
            SortExpression="last_name" UniqueName="last_name">
        </telerik:GridBoundColumn>
        <telerik:GridBoundColumn DataField="case_status" HeaderText="Case Status" 
            SortExpression="case_status" UniqueName="case_status">
        </telerik:GridBoundColumn>
        <telerik:GridBoundColumn DataField="case_actions" HeaderText="Notes" 
            SortExpression="case_actions" UniqueName="case_actions">
        </telerik:GridBoundColumn>
        <telerik:GridBoundColumn DataField="attorney" HeaderText="Staff" 
            SortExpression="attorney" UniqueName="attorney">
        </telerik:GridBoundColumn>
        <telerik:GridBoundColumn DataField="created_at" DataType="System.DateTime" 
            HeaderText="created_at" SortExpression="Created" UniqueName="created_at" 
            Visible="False">
        </telerik:GridBoundColumn>
        <telerik:GridBoundColumn DataField="updated_at" DataType="System.DateTime" 
            HeaderText="updated_at" SortExpression="Updated" UniqueName="updated_at">
        </telerik:GridBoundColumn>
        <telerik:GridBoundColumn DataField="active" DataType="System.Decimal" 
            HeaderText="active" SortExpression="active" UniqueName="active" Visible="False">
        </telerik:GridBoundColumn>
        <telerik:GridBoundColumn DataField="fee" HeaderText="Fee" SortExpression="fee" 
            UniqueName="fee">
        </telerik:GridBoundColumn>
        <telerik:GridButtonColumn CommandName="Select" Text="Select" 
            UniqueName="column1">
        </telerik:GridButtonColumn>
        <telerik:GridEditCommandColumn>
        </telerik:GridEditCommandColumn>
        <telerik:GridButtonColumn CommandName="Delete" Text="Delete" 
            UniqueName="column">
        </telerik:GridButtonColumn>
    </Columns>
</MasterTableView>

<HeaderContextMenu EnableImageSprites="True" CssClass="GridContextMenu GridContextMenu_Default"></HeaderContextMenu>
        </telerik:RadGrid>
        <asp:SqlDataSource ID="SqlDataSource1" runat="server" 
            ConflictDetection="CompareAllValues" 
            ConnectionString="<%$ ConnectionStrings:casefilesProductionConnectionString %>" 
            DeleteCommand="DELETE FROM [casefiles] WHERE [id] = @original_id AND (([case_id] = @original_case_id) OR ([case_id] IS NULL AND @original_case_id IS NULL)) AND (([case_number] = @original_case_number) OR ([case_number] IS NULL AND @original_case_number IS NULL)) AND (([case_type] = @original_case_type) OR ([case_type] IS NULL AND @original_case_type IS NULL)) AND (([first_name] = @original_first_name) OR ([first_name] IS NULL AND @original_first_name IS NULL)) AND (([last_name] = @original_last_name) OR ([last_name] IS NULL AND @original_last_name IS NULL)) AND (([case_status] = @original_case_status) OR ([case_status] IS NULL AND @original_case_status IS NULL)) AND (([case_actions] = @original_case_actions) OR ([case_actions] IS NULL AND @original_case_actions IS NULL)) AND (([attorney] = @original_attorney) OR ([attorney] IS NULL AND @original_attorney IS NULL)) AND (([created_at] = @original_created_at) OR ([created_at] IS NULL AND @original_created_at IS NULL)) AND (([updated_at] = @original_updated_at) OR ([updated_at] IS NULL AND @original_updated_at IS NULL)) AND (([active] = @original_active) OR ([active] IS NULL AND @original_active IS NULL)) AND (([fee] = @original_fee) OR ([fee] IS NULL AND @original_fee IS NULL)) AND (([priority] = @original_priority) OR ([priority] IS NULL AND @original_priority IS NULL))" 
            InsertCommand="INSERT INTO casefiles(case_id, case_number, case_type, first_name, last_name, case_status, case_actions, attorney, created_at, updated_at, fee, priority, id, active) VALUES (@case_id, @case_number, @case_type, @first_name, @last_name, @case_status, @case_actions, @attorney, @created_at, @updated_at, @fee, @priority,,)" 
            OldValuesParameterFormatString="original_{0}" 
            SelectCommand="SELECT * FROM [casefiles] WHERE ([active] = @active)" 
            
            UpdateCommand="UPDATE [casefiles] SET [case_id] = @case_id, [case_number] = @case_number, [case_type] = @case_type, [first_name] = @first_name, [last_name] = @last_name, [case_status] = @case_status, [case_actions] = @case_actions, [attorney] = @attorney, [created_at] = @created_at, [updated_at] = @updated_at, [active] = @active, [fee] = @fee, [priority] = @priority WHERE [id] = @original_id AND (([case_id] = @original_case_id) OR ([case_id] IS NULL AND @original_case_id IS NULL)) AND (([case_number] = @original_case_number) OR ([case_number] IS NULL AND @original_case_number IS NULL)) AND (([case_type] = @original_case_type) OR ([case_type] IS NULL AND @original_case_type IS NULL)) AND (([first_name] = @original_first_name) OR ([first_name] IS NULL AND @original_first_name IS NULL)) AND (([last_name] = @original_last_name) OR ([last_name] IS NULL AND @original_last_name IS NULL)) AND (([case_status] = @original_case_status) OR ([case_status] IS NULL AND @original_case_status IS NULL)) AND (([case_actions] = @original_case_actions) OR ([case_actions] IS NULL AND @original_case_actions IS NULL)) AND (([attorney] = @original_attorney) OR ([attorney] IS NULL AND @original_attorney IS NULL)) AND (([created_at] = @original_created_at) OR ([created_at] IS NULL AND @original_created_at IS NULL)) AND (([updated_at] = @original_updated_at) OR ([updated_at] IS NULL AND @original_updated_at IS NULL)) AND (([active] = @original_active) OR ([active] IS NULL AND @original_active IS NULL)) AND (([fee] = @original_fee) OR ([fee] IS NULL AND @original_fee IS NULL)) AND (([priority] = @original_priority) OR ([priority] IS NULL AND @original_priority IS NULL))">
            <DeleteParameters>
                <asp:Parameter Name="original_id" Type="Int32" />
                <asp:Parameter Name="original_case_id" Type="String" />
                <asp:Parameter Name="original_case_number" Type="String" />
                <asp:Parameter Name="original_case_type" Type="String" />
                <asp:Parameter Name="original_first_name" Type="String" />
                <asp:Parameter Name="original_last_name" Type="String" />
                <asp:Parameter Name="original_case_status" Type="String" />
                <asp:Parameter Name="original_case_actions" Type="String" />
                <asp:Parameter Name="original_attorney" Type="String" />
                <asp:Parameter Name="original_created_at" Type="DateTime" />
                <asp:Parameter Name="original_updated_at" Type="DateTime" />
                <asp:Parameter Name="original_active" Type="Decimal" />
                <asp:Parameter Name="original_fee" Type="String" />
                <asp:Parameter Name="original_priority" Type="String" />
            </DeleteParameters>
            <InsertParameters>
                <asp:Parameter Name="case_id" Type="String" />
                <asp:Parameter Name="case_number" Type="String" />
                <asp:Parameter Name="case_type" Type="String" />
                <asp:Parameter Name="first_name" Type="String" />
                <asp:Parameter Name="last_name" Type="String" />
                <asp:Parameter Name="case_status" Type="String" />
                <asp:Parameter Name="case_actions" Type="String" />
                <asp:Parameter Name="attorney" Type="String" />
                <asp:Parameter Name="created_at" Type="DateTime" />
                <asp:Parameter Name="updated_at" Type="DateTime" />
                <asp:Parameter Name="fee" Type="String" />
                <asp:Parameter Name="priority" Type="String" />
            </InsertParameters>
            <SelectParameters>
                <asp:Parameter DefaultValue="1" Name="active" Type="Decimal" />
            </SelectParameters>
            <UpdateParameters>
                <asp:Parameter Name="case_id" Type="String" />
                <asp:Parameter Name="case_number" Type="String" />
                <asp:Parameter Name="case_type" Type="String" />
                <asp:Parameter Name="first_name" Type="String" />
                <asp:Parameter Name="last_name" Type="String" />
                <asp:Parameter Name="case_status" Type="String" />
                <asp:Parameter Name="case_actions" Type="String" />
                <asp:Parameter Name="attorney" Type="String" />
                <asp:Parameter Name="created_at" Type="DateTime" />
                <asp:Parameter Name="updated_at" Type="DateTime" />
                <asp:Parameter Name="active" Type="Decimal" />
                <asp:Parameter Name="fee" Type="String" />
                <asp:Parameter Name="priority" Type="String" />
                <asp:Parameter Name="original_id" Type="Int32" />
                <asp:Parameter Name="original_case_id" Type="String" />
                <asp:Parameter Name="original_case_number" Type="String" />
                <asp:Parameter Name="original_case_type" Type="String" />
                <asp:Parameter Name="original_first_name" Type="String" />
                <asp:Parameter Name="original_last_name" Type="String" />
                <asp:Parameter Name="original_case_status" Type="String" />
                <asp:Parameter Name="original_case_actions" Type="String" />
                <asp:Parameter Name="original_attorney" Type="String" />
                <asp:Parameter Name="original_created_at" Type="DateTime" />
                <asp:Parameter Name="original_updated_at" Type="DateTime" />
                <asp:Parameter Name="original_active" Type="Decimal" />
                <asp:Parameter Name="original_fee" Type="String" />
                <asp:Parameter Name="original_priority" Type="String" />
            </UpdateParameters>
        </asp:SqlDataSource>
        <br />
    </div>
    <telerik:RadAjaxManager runat="server" RestoreOriginalRenderDelegate="false">
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="RadGrid1">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="RadGrid1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="SqlDataSource1">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="RadGrid1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
        </AjaxSettings>
    </telerik:RadAjaxManager>
    </form>
</body>
</html>
