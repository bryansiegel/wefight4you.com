﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Archives.aspx.cs" Inherits="Administration_Archives" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <telerik:RadScriptManager ID="RadScriptManager1" runat="server">
    </telerik:RadScriptManager>
    <div>
    <h1>Archives</h1>
        <telerik:RadGrid ID="RadGrid1" runat="server" AllowFilteringByColumn="True" 
            AllowPaging="True" AllowSorting="True" AutoGenerateEditColumn="True" 
            DataSourceID="SqlDataSource1" GridLines="None" ShowGroupPanel="True" 
            Skin="Windows7">
            <ClientSettings AllowDragToGroup="True">
                <Scrolling AllowScroll="True" UseStaticHeaders="True" />
            </ClientSettings>
<MasterTableView AutoGenerateColumns="False" DataSourceID="SqlDataSource1">
<CommandItemSettings ExportToPdfText="Export to Pdf"></CommandItemSettings>

<RowIndicatorColumn>
<HeaderStyle Width="20px"></HeaderStyle>
</RowIndicatorColumn>

<ExpandCollapseColumn>
<HeaderStyle Width="20px"></HeaderStyle>
</ExpandCollapseColumn>
    <Columns>
        <telerik:GridBoundColumn DataField="id" HeaderText="id" 
            SortExpression="id" UniqueName="id" DataType="System.Int32">
        </telerik:GridBoundColumn>
        <telerik:GridBoundColumn DataField="case_id" HeaderText="case_id" 
            SortExpression="case_id" UniqueName="case_id">
        </telerik:GridBoundColumn>
        <telerik:GridBoundColumn DataField="case_number" HeaderText="case_number" 
            SortExpression="case_number" UniqueName="case_number">
        </telerik:GridBoundColumn>
        <telerik:GridBoundColumn DataField="case_type" HeaderText="case_type" 
            SortExpression="case_type" UniqueName="case_type">
        </telerik:GridBoundColumn>
        <telerik:GridBoundColumn DataField="first_name" HeaderText="first_name" SortExpression="first_name" 
            UniqueName="first_name">
        </telerik:GridBoundColumn>
        <telerik:GridBoundColumn DataField="last_name" 
            HeaderText="last_name" SortExpression="last_name" UniqueName="last_name">
        </telerik:GridBoundColumn>
        <telerik:GridBoundColumn DataField="case_status" HeaderText="case_status" 
            SortExpression="case_status" UniqueName="case_status">
        </telerik:GridBoundColumn>
        <telerik:GridBoundColumn DataField="case_actions" HeaderText="case_actions" 
            SortExpression="case_actions" UniqueName="case_actions">
        </telerik:GridBoundColumn>
        <telerik:GridBoundColumn DataField="attorney" 
            HeaderText="attorney" SortExpression="attorney" UniqueName="attorney">
        </telerik:GridBoundColumn>
        <telerik:GridBoundColumn DataField="created_at" DataType="System.DateTime" 
            HeaderText="created_at" SortExpression="created_at" 
            UniqueName="created_at">
        </telerik:GridBoundColumn>
        <telerik:GridBoundColumn DataField="updated_at" DataType="System.DateTime" 
            HeaderText="updated_at" SortExpression="updated_at" UniqueName="updated_at">
        </telerik:GridBoundColumn>
        <telerik:GridBoundColumn DataField="active" DataType="System.Decimal" 
            HeaderText="active" SortExpression="active" UniqueName="active">
        </telerik:GridBoundColumn>
        <telerik:GridBoundColumn DataField="fee" HeaderText="fee" SortExpression="fee" 
            UniqueName="fee">
        </telerik:GridBoundColumn>
        <telerik:GridBoundColumn DataField="priority" HeaderText="priority" 
            SortExpression="priority" UniqueName="priority">
        </telerik:GridBoundColumn>
    </Columns>
</MasterTableView>

<HeaderContextMenu EnableImageSprites="True" CssClass="GridContextMenu GridContextMenu_Default"></HeaderContextMenu>
        </telerik:RadGrid>

        <asp:SqlDataSource ID="SqlDataSource1" runat="server" 
            ConnectionString="<%$ ConnectionStrings:archivesProductionConnectionString %>" 
            SelectCommand="SELECT * FROM [casefiles] WHERE ([active] = @active)">
            <SelectParameters>
                <asp:Parameter DefaultValue="0" Name="active" Type="Decimal" />
            </SelectParameters>
        </asp:SqlDataSource>
    </div>
    </form>
</body>
</html>
