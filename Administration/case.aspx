﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="case.aspx.cs" Inherits="Administration_case" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <asp:GridView ID="GridView1" runat="server" AllowPaging="True" 
            AllowSorting="True" AutoGenerateColumns="False" DataKeyNames="id" 
            DataSourceID="SqlDataSource1" EnableModelValidation="True">
            <Columns>
                <asp:CommandField ShowDeleteButton="True" ShowEditButton="True" 
                    ShowSelectButton="True" />
                <asp:BoundField DataField="id" HeaderText="id" InsertVisible="False" 
                    ReadOnly="True" SortExpression="id" />
                <asp:BoundField DataField="case_id" HeaderText="case_id" 
                    SortExpression="case_id" />
                <asp:BoundField DataField="case_number" HeaderText="case_number" 
                    SortExpression="case_number" />
                <asp:BoundField DataField="case_type" HeaderText="case_type" 
                    SortExpression="case_type" />
                <asp:BoundField DataField="first_name" HeaderText="first_name" 
                    SortExpression="first_name" />
                <asp:BoundField DataField="last_name" HeaderText="last_name" 
                    SortExpression="last_name" />
                <asp:BoundField DataField="case_status" HeaderText="case_status" 
                    SortExpression="case_status" />
                <asp:BoundField DataField="case_actions" HeaderText="case_actions" 
                    SortExpression="case_actions" />
                <asp:BoundField DataField="attorney" HeaderText="attorney" 
                    SortExpression="attorney" />
                <asp:BoundField DataField="created_at" HeaderText="created_at" 
                    SortExpression="created_at" />
                <asp:BoundField DataField="updated_at" HeaderText="updated_at" 
                    SortExpression="updated_at" />
                <asp:BoundField DataField="active" HeaderText="active" 
                    SortExpression="active" />
                <asp:BoundField DataField="fee" HeaderText="fee" SortExpression="fee" />
                <asp:BoundField DataField="priority" HeaderText="priority" 
                    SortExpression="priority" />
            </Columns>
        </asp:GridView>
        <asp:SqlDataSource ID="SqlDataSource1" runat="server" 
            ConflictDetection="CompareAllValues" 
            ConnectionString="<%$ ConnectionStrings:casefilesProductionConnectionString %>" 
            DeleteCommand="DELETE FROM [casefiles] WHERE [id] = @original_id AND (([case_id] = @original_case_id) OR ([case_id] IS NULL AND @original_case_id IS NULL)) AND (([case_number] = @original_case_number) OR ([case_number] IS NULL AND @original_case_number IS NULL)) AND (([case_type] = @original_case_type) OR ([case_type] IS NULL AND @original_case_type IS NULL)) AND (([first_name] = @original_first_name) OR ([first_name] IS NULL AND @original_first_name IS NULL)) AND (([last_name] = @original_last_name) OR ([last_name] IS NULL AND @original_last_name IS NULL)) AND (([case_status] = @original_case_status) OR ([case_status] IS NULL AND @original_case_status IS NULL)) AND (([case_actions] = @original_case_actions) OR ([case_actions] IS NULL AND @original_case_actions IS NULL)) AND (([attorney] = @original_attorney) OR ([attorney] IS NULL AND @original_attorney IS NULL)) AND (([created_at] = @original_created_at) OR ([created_at] IS NULL AND @original_created_at IS NULL)) AND (([updated_at] = @original_updated_at) OR ([updated_at] IS NULL AND @original_updated_at IS NULL)) AND (([active] = @original_active) OR ([active] IS NULL AND @original_active IS NULL)) AND (([fee] = @original_fee) OR ([fee] IS NULL AND @original_fee IS NULL)) AND (([priority] = @original_priority) OR ([priority] IS NULL AND @original_priority IS NULL))" 
            InsertCommand="INSERT INTO [casefiles] ([case_id], [case_number], [case_type], [first_name], [last_name], [case_status], [case_actions], [attorney], [created_at], [updated_at], [active], [fee], [priority]) VALUES (@case_id, @case_number, @case_type, @first_name, @last_name, @case_status, @case_actions, @attorney, @created_at, @updated_at, @active, @fee, @priority)" 
            OldValuesParameterFormatString="original_{0}" 
            SelectCommand="SELECT * FROM [casefiles] WHERE ([active] = @active)" 
            UpdateCommand="UPDATE [casefiles] SET [case_id] = @case_id, [case_number] = @case_number, [case_type] = @case_type, [first_name] = @first_name, [last_name] = @last_name, [case_status] = @case_status, [case_actions] = @case_actions, [attorney] = @attorney, [created_at] = @created_at, [updated_at] = @updated_at, [active] = @active, [fee] = @fee, [priority] = @priority WHERE [id] = @original_id AND (([case_id] = @original_case_id) OR ([case_id] IS NULL AND @original_case_id IS NULL)) AND (([case_number] = @original_case_number) OR ([case_number] IS NULL AND @original_case_number IS NULL)) AND (([case_type] = @original_case_type) OR ([case_type] IS NULL AND @original_case_type IS NULL)) AND (([first_name] = @original_first_name) OR ([first_name] IS NULL AND @original_first_name IS NULL)) AND (([last_name] = @original_last_name) OR ([last_name] IS NULL AND @original_last_name IS NULL)) AND (([case_status] = @original_case_status) OR ([case_status] IS NULL AND @original_case_status IS NULL)) AND (([case_actions] = @original_case_actions) OR ([case_actions] IS NULL AND @original_case_actions IS NULL)) AND (([attorney] = @original_attorney) OR ([attorney] IS NULL AND @original_attorney IS NULL)) AND (([created_at] = @original_created_at) OR ([created_at] IS NULL AND @original_created_at IS NULL)) AND (([updated_at] = @original_updated_at) OR ([updated_at] IS NULL AND @original_updated_at IS NULL)) AND (([active] = @original_active) OR ([active] IS NULL AND @original_active IS NULL)) AND (([fee] = @original_fee) OR ([fee] IS NULL AND @original_fee IS NULL)) AND (([priority] = @original_priority) OR ([priority] IS NULL AND @original_priority IS NULL))">
            <DeleteParameters>
                <asp:Parameter Name="original_id" Type="Int32" />
                <asp:Parameter Name="original_case_id" Type="String" />
                <asp:Parameter Name="original_case_number" Type="String" />
                <asp:Parameter Name="original_case_type" Type="String" />
                <asp:Parameter Name="original_first_name" Type="String" />
                <asp:Parameter Name="original_last_name" Type="String" />
                <asp:Parameter Name="original_case_status" Type="String" />
                <asp:Parameter Name="original_case_actions" Type="String" />
                <asp:Parameter Name="original_attorney" Type="String" />
                <asp:Parameter Name="original_created_at" Type="DateTime" />
                <asp:Parameter Name="original_updated_at" Type="DateTime" />
                <asp:Parameter Name="original_active" Type="Decimal" />
                <asp:Parameter Name="original_fee" Type="String" />
                <asp:Parameter Name="original_priority" Type="String" />
            </DeleteParameters>
            <InsertParameters>
                <asp:Parameter Name="case_id" Type="String" />
                <asp:Parameter Name="case_number" Type="String" />
                <asp:Parameter Name="case_type" Type="String" />
                <asp:Parameter Name="first_name" Type="String" />
                <asp:Parameter Name="last_name" Type="String" />
                <asp:Parameter Name="case_status" Type="String" />
                <asp:Parameter Name="case_actions" Type="String" />
                <asp:Parameter Name="attorney" Type="String" />
                <asp:Parameter Name="created_at" Type="DateTime" />
                <asp:Parameter Name="updated_at" Type="DateTime" />
                <asp:Parameter Name="active" Type="Decimal" />
                <asp:Parameter Name="fee" Type="String" />
                <asp:Parameter Name="priority" Type="String" />
            </InsertParameters>
            <SelectParameters>
                <asp:Parameter DefaultValue="1" Name="active" Type="Decimal" />
            </SelectParameters>
            <UpdateParameters>
                <asp:Parameter Name="case_id" Type="String" />
                <asp:Parameter Name="case_number" Type="String" />
                <asp:Parameter Name="case_type" Type="String" />
                <asp:Parameter Name="first_name" Type="String" />
                <asp:Parameter Name="last_name" Type="String" />
                <asp:Parameter Name="case_status" Type="String" />
                <asp:Parameter Name="case_actions" Type="String" />
                <asp:Parameter Name="attorney" Type="String" />
                <asp:Parameter Name="created_at" Type="DateTime" />
                <asp:Parameter Name="updated_at" Type="DateTime" />
                <asp:Parameter Name="active" Type="Decimal" />
                <asp:Parameter Name="fee" Type="String" />
                <asp:Parameter Name="priority" Type="String" />
                <asp:Parameter Name="original_id" Type="Int32" />
                <asp:Parameter Name="original_case_id" Type="String" />
                <asp:Parameter Name="original_case_number" Type="String" />
                <asp:Parameter Name="original_case_type" Type="String" />
                <asp:Parameter Name="original_first_name" Type="String" />
                <asp:Parameter Name="original_last_name" Type="String" />
                <asp:Parameter Name="original_case_status" Type="String" />
                <asp:Parameter Name="original_case_actions" Type="String" />
                <asp:Parameter Name="original_attorney" Type="String" />
                <asp:Parameter Name="original_created_at" Type="DateTime" />
                <asp:Parameter Name="original_updated_at" Type="DateTime" />
                <asp:Parameter Name="original_active" Type="Decimal" />
                <asp:Parameter Name="original_fee" Type="String" />
                <asp:Parameter Name="original_priority" Type="String" />
            </UpdateParameters>
        </asp:SqlDataSource>
    </div>
    </form>
</body>
</html>
