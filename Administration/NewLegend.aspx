﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="NewLegend.aspx.cs" Inherits="Administration_NewLegend" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    <h1>New Legend</h1>
        <asp:FormView ID="FormView1" runat="server" DataKeyNames="id" 
            DataSourceID="SqlDataSource1" DefaultMode="Insert" EnableModelValidation="True">
           
            <InsertItemTemplate>
                word:
                <asp:TextBox ID="wordTextBox" runat="server" Text='<%# Bind("word") %>' />
                <br />
                acronym:
                <asp:TextBox ID="acronymTextBox" runat="server" Text='<%# Bind("acronym") %>' />
                <br />
                <asp:LinkButton ID="InsertButton" runat="server" CausesValidation="True" 
                    CommandName="Insert" Text="Insert" />
                &nbsp;<asp:LinkButton ID="InsertCancelButton" runat="server" 
                    CausesValidation="False" CommandName="Cancel" Text="Cancel" />
            </InsertItemTemplate>
            <ItemTemplate>
                id:
                <asp:Label ID="idLabel" runat="server" Text='<%# Eval("id") %>' />
                <br />
                word:
                <asp:Label ID="wordLabel" runat="server" Text='<%# Bind("word") %>' />
                <br />
                acronym:
                <asp:Label ID="acronymLabel" runat="server" Text='<%# Bind("acronym") %>' />
                <br />
                created_at:
                <asp:Label ID="created_atLabel" runat="server" 
                    Text='<%# Bind("created_at") %>' />
                <br />
                updated_at:
                <asp:Label ID="updated_atLabel" runat="server" 
                    Text='<%# Bind("updated_at") %>' />
                <br />
                <asp:LinkButton ID="EditButton" runat="server" CausesValidation="False" 
                    CommandName="Edit" Text="Edit" />
                &nbsp;<asp:LinkButton ID="DeleteButton" runat="server" CausesValidation="False" 
                    CommandName="Delete" Text="Delete" />
                &nbsp;<asp:LinkButton ID="NewButton" runat="server" CausesValidation="False" 
                    CommandName="New" Text="New" />
            </ItemTemplate>
        </asp:FormView>

        <asp:SqlDataSource ID="SqlDataSource1" runat="server" 
            ConnectionString="<%$ ConnectionStrings:legendProductionConnectionString %>" 
            OldValuesParameterFormatString="original_{0}" 
            SelectCommand="SELECT * FROM [legends]" 
            ConflictDetection="CompareAllValues" 
            DeleteCommand="DELETE FROM [legends] WHERE [id] = @original_id AND (([word] = @original_word) OR ([word] IS NULL AND @original_word IS NULL)) AND (([acronym] = @original_acronym) OR ([acronym] IS NULL AND @original_acronym IS NULL)) AND (([created_at] = @original_created_at) OR ([created_at] IS NULL AND @original_created_at IS NULL)) AND (([updated_at] = @original_updated_at) OR ([updated_at] IS NULL AND @original_updated_at IS NULL))" 
            InsertCommand="INSERT INTO [legends] ([word], [acronym], [created_at], [updated_at]) VALUES (@word, @acronym, @created_at, @updated_at)" 
            UpdateCommand="UPDATE [legends] SET [word] = @word, [acronym] = @acronym, [created_at] = @created_at, [updated_at] = @updated_at WHERE [id] = @original_id AND (([word] = @original_word) OR ([word] IS NULL AND @original_word IS NULL)) AND (([acronym] = @original_acronym) OR ([acronym] IS NULL AND @original_acronym IS NULL)) AND (([created_at] = @original_created_at) OR ([created_at] IS NULL AND @original_created_at IS NULL)) AND (([updated_at] = @original_updated_at) OR ([updated_at] IS NULL AND @original_updated_at IS NULL))">
            <DeleteParameters>
                <asp:Parameter Name="original_id" Type="Int32" />
                <asp:Parameter Name="original_word" Type="String" />
                <asp:Parameter Name="original_acronym" Type="String" />
                <asp:Parameter Name="original_created_at" Type="DateTime" />
                <asp:Parameter Name="original_updated_at" Type="DateTime" />
            </DeleteParameters>
            <InsertParameters>
                <asp:Parameter Name="word" Type="String" />
                <asp:Parameter Name="acronym" Type="String" />
                <asp:Parameter Name="created_at" Type="DateTime" />
                <asp:Parameter Name="updated_at" Type="DateTime" />
            </InsertParameters>
            <UpdateParameters>
                <asp:Parameter Name="word" Type="String" />
                <asp:Parameter Name="acronym" Type="String" />
                <asp:Parameter Name="created_at" Type="DateTime" />
                <asp:Parameter Name="updated_at" Type="DateTime" />
                <asp:Parameter Name="original_id" Type="Int32" />
                <asp:Parameter Name="original_word" Type="String" />
                <asp:Parameter Name="original_acronym" Type="String" />
                <asp:Parameter Name="original_created_at" Type="DateTime" />
                <asp:Parameter Name="original_updated_at" Type="DateTime" />
            </UpdateParameters>
        </asp:SqlDataSource>
    </div>
    </form>
</body>
</html>
