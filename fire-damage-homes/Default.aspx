﻿<%@ Page Title="Fire Damage Lawsuit Attorney California | House Insurance Lawyer CA" Language="C#" MasterPageFile="~/MasterPages/Bankruptcy.master" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="fire_damage_homes_Default" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
<meta name="keywords" content="lawyer, attorney, law firm, law office, legal advice, fire damage, lawsuit, wild fire, insurance, insurance adjuster, bad faith, firestorm, mold, repair, restoration, underinsurance, agent, business loss, lawyer, attorney,">
<meta name="description" content="Fire Damage Attorney is on your side. Since 1998 we've helped those with fire damage homes get what they were entitled to. Contact the Law Offices of Marc Grossman today for a free intitial consultation.">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainLogo" Runat="Server">
    <img src="../images/i-prac-firedamagetohomes.jpg" 
        style="width: 802px; height: 296px"/>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="PageTitleH1" Runat="Server">
<h2 id="pageTitle">Fire Damage to Homes</h2>
    <p style="color:White; font-weight:bold; text-align:center">Serving Victims of the California Fires in the following locations</p>
<h3>La Crescenta, La Cañada-Flintridge, Altadena, Los Angeles, San Bernardino 
    County, San Gabriel Mountains</h3>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="MainContent" Runat="Server">
<h2>Fire Damage Claims</h2>
<p>Recently, California has seen the terrible damage that a fire can cause. When fire gets a hold of something, there is nothing that an ordinary citizen can do to stop it. After the damage has been done, people turn to their insurance companies for help rebuilding their lives. All too often, the insurance company simply turns its back and denies 
    fire damage claims. At the<a href="../Firm-Overview/Default.aspx"> Law Office of Marc E. Grossman</a>, we are here to see that insurance companies fulfill their promises to their customers.</p>
    <h3>Smart, honest fire damage claims representation</h3>
<p>Have you lost your home in a wild fire or any other type of fire? Have you turned to your insurance company for help, only to have them deny your claim?</p>
<p>We will stand by your side.</p>
    <div class="callOut">
<h3>We offer free initial consultations</h3>
<p><a href="/Forms/contact.aspx" rel="nofollow">E-mail us</a> or call us toll free at 1-888-407-9068 to schedule a meeting with a dedicated<a 
        href="../fire-insurance-claims/Default.aspx"> fire insurance claim lawyer</a>.</p>
<p>Let us talk to you about your fire damage case. We offer initial consultations to 
    victims of fire damage to their homes. We would like to get to know your situation and offer you guidance about what can be done to resolve it. We want to help.</p>
</div>
<h2>How to make sure your insurance company pays you what you are entitled.</h2>

<p>Insurance companies usually have significant resources at their disposal. At <a href="/Firm-Overview/">the Law Office of Marc E. Grossman</a>,  we have significant resources, too. We have earned our reputation by working hard to get results for all of our clients. Our dedication has earned us recognition. Our lead attorney, 
    <a href="../attorneys/marc-grossman.aspx">Marc E. Grossman</a>, was included on a list of the 66 most influential people in the region by the LA Times Inland Valley Voice. That is a reputation you can count on.</p>
<p>We can deal with insurance adjuster scams. We can address low-balling by the insurance company. 
    <b>We can help with issues involving mold, microscopic particles of ash and soot in your house and other less visible results of fire damage.</b> We know how to handle these cases. We know how to do what is right for you.</p>
<p>To learn more about how a trusted <a href="../fire-insurance-claims/Default.aspx">
    fire insuranace attorney</a> can help in your fire damage case, <a href="/Forms/contact.aspx" rel="nofollow">e-mail us</a> or call us toll free at 1-888-407-9068 for a free initial consultation.</p>
</asp:Content>

<asp:Content ID="Content5" runat="server" contentplaceholderid="leftNav">

								<li><a href="../fire-insurance-claims/why-you-should-hire-an-attorney.aspx" title="Why Hire an Attorney for a Fire Insurance Claim">Why Hire an Attorney for Fire Inusrance Claims</a></li>
<li><a href="../fire-insurance-claims/Default.aspx" title="Fire Insurance Claims">Fire Insurance Claims</a></li>
<li><a href="../fire-damage-homes/Default.aspx" title="Fire Damage Homes">Fire Damage Homes</a></li>

</asp:Content>
<asp:Content ID="Content6" runat="server" 
    contentplaceholderid="espanolNavigation">

					<li><a href="/espanol/danos-a-hogares-de-incendios.aspx">Casas de Daños de Incendio</a></li>
    <li><a href="/espanol/reclamaciones-de-seguros-de-incendios.aspx">reclamaciones de seguros de incendios</a></li>
					
</asp:Content>


<asp:Content ID="Content7" runat="server" contentplaceholderid="Toph1Page">
 Home Fire Damage Claim Denial? Attorneys that are on your Side
</asp:Content>



