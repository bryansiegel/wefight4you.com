﻿<%@ Page Title="Motorcycle Accident Lawyer Upland, CA" Language="C#" MasterPageFile="~/MasterPages/SiteMainInternal.master" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="motorcycle_accidents_Default" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
<meta name="keywords" content="lawyer, attorney, law firm, law office, legal advice, motorcycle accident, motorbike, crash, bike, wreck, helmet, lawyer, attorney," />
<meta name="description" content="Victim of a Car Accident? Need an Attorney? Contact the law offices of Marc Grossman for a free initial Consultation." />
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainLogo" Runat="Server">
<a href="http://www.wefight4you.com" title="Motorcycle Lawyer">
    <img src="../images/teamofattorneys2.jpg" alt="Motorcycle Accident Attorney" style="width: 800px; height: 244px"  /></a>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="PageTitleH1" Runat="Server">
<h1 id="pageTitle">Motorcycle Accident Attorney</h1>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="MainContent" Runat="Server">
<h2> Motorcycle Accident Attorney</h2>
<p>Motorcycle accidents frequently result in catastrophic personal injury or wrongful death. All too often, these accidents are the result of a car or truck driver who has failed to follow the appropriate safety precautions when sharing the road with a motorcyclist. At the Law Office of Marc E. Grossman, we believe that these negligent drivers should be held accountable for the pain and suffering they cause.</p>
<h3>Free initial consultations are available</h3>
<p>Call us toll free at 1-888-407-9068 to discuss your case with an experienced motorcycle accident lawyer.</p>
<p>When you turn to us for your initial consultation, we will spend time with you. We will find out about what you have been through. We will let you know your options so you can get started down the right path.</p>
<h3>Smart, honest motorcycle accident representation</h3>
<p><a href="/Firm-Overview/Default.aspx">Our law firm</a> is here to serve motorcycle accident victims in Upland, California, and throughout the Inland Empire.</p>
<p>We are a law firm that has earned a reputation for success. Over the years, we have appeared on CBS News as well as many local news programs. Our lead attorney, Marc E. Grossman, was included on a list of the 66 most influential people in the region by the LA Times Inland Valley Voice. We believe that we have earned our reputation for working hard and doing what is right for our clients.</p>
<p>Whether your motorcycle accident case involves severe head injury, loss of limb, broken bones, back injury or the wrongful death of a loved one, we are prepared to stand by your side. We will do more than simply fight for you; we will provide personalized attention all along the way. Working on your behalf, we want to get you results that matter.</p>
<p>To schedule a free initial consultation about your motorbike crash case, Call us toll free at 1-888-407-9068.</p>
</asp:Content>

