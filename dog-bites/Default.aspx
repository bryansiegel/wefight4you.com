﻿<%@ Page Title="Dog Bite Lawyer Riverside County, San Bernardino Premises Liability Attorney Inland Empire California CA" Language="C#" MasterPageFile="~/MasterPages/SiteMainInternal.master" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="dog_bites_Default" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
<meta name="keywords" content="lawyer, attorney, law firm, law office, legal advice, dog bite, premises liability, pit bull, animal attack, Rottweiler, laceration, disfigurement, child injury, lawyer, attorney,">
<meta name="description" content="Do you need an experienced attorney to handle your dog bite case? Call 1-888-407-9068 for a free consultation with an Upland, California lawyer at the Law Office of Marc E. Grossman.">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainLogo" Runat="Server">
    <img src="../images/i-prac-dogbites.jpg" style="width: 802px; height: 228px"/>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="PageTitleH1" Runat="Server">
<h1 id="pageTitle">Dog Bites</h1>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="MainContent" Runat="Server">
<h2>Upland Law Firm: Dog Bite Injuries</h2>
<p>Have you or a loved one been seriously hurt in an animal attack? These cases can cause serious damage, particularly when they involve injury to a child. The harm can be emotional, as well as physical. At the Law Office of Marc E. Grossman, we will not only fight to get you fair compensation, we will do much more. We will help you get closure. We will see you through this.</p>
<h3>We offer free initial consultations</h3>
<p><a href="/Forms/contact.aspx" rel="nofollow">E-mail us</a> or call us toll free at 1-888-407-9068 to schedule a meeting with a dedicated lawyer about your dog bite or premises liability issue.</p>
<p>Our initial consultations are comprehensive and personalized to your situation. We will take the time to find out about the impact a dog bite has had on your life. We will educate you about your options and explain how we can help.</p>
<h3>Smart, honest dog bite representation</h3>
<p><a href="/Firm-Overview/">Our law firm</a> is tightly rooted in the community we serve. We stand beside dog bite victims in Upland, California, and throughout the Inland Empire.</p>
<p>We pride ourselves on providing smart, honest representation. We believe that is the type of representation you deserve. Our job is not to run up your fees by dragging your case out. Our job is to get you the right results in the most efficient manner we can.</p>
<p>Dog bite injuries are not always caused by pit bulls, Rottweilers and large animals. Even smaller dogs can cause serious damage, especially when the victim is a child.</p>
<p>Whether your case involves disfigurement, skin lacerations or any other physical or emotional harm caused by a dog that was not properly controlled by its owner, we are here for you.</p>
<p>For more information about how an experienced attorney can help you, <a href="/Forms/contact.aspx" rel="nofollow">e-mail us</a> or call us toll free at 1-888-407-9068 for a free initial consultation about your dog bite injury.</p>
</asp:Content>

