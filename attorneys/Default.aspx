<%@ Page Title="Attorneys at the Law Offices of Marc Grossman, Upland California" Language="C#" MasterPageFile="~/MasterPages/SiteMainInternal.master" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="attorneys_Default" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
<meta name="keywords" content="lawyer, attorney, law firm, law office, legal advice,">
<meta name="description" content="Dedicated civil litigation and criminal defense attorney. Call 1-888-407-9068 for a free consultation with an Upland, California lawyer at the Law Offices of Marc E. Grossman.">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainLogo" Runat="Server">
<a href="http://www.wefight4you.com" title="California Lawyers">
    <img src="http://www.wefight4you.com/images/teamofattorneys2.jpg" alt="California Lawyers" height="244px" width="800px"/>
    </a>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="PageTitleH1" Runat="Server">
<h1 id="pageTitle">Meet the California Attorneys at the Law Offices of Marc Grossman</h1>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="MainContent" Runat="Server">
<div class="callOut" style="text-align:center"><h2>Our Attorneys</h2>
<a href="http://www.wefight4you.com/attorneys/marc-grossman.aspx">Marc Grossman</a><br />
<a href="http://www.wefight4you.com/attorneys/albert-d-antin.aspx">Albert D'Antin</a><br />
<a href="http://www.wefight4you.com/attorneys/lisa-robinson.aspx">Lisa Robinson</a><br />
<a href="http://www.wefight4you.com/attorneys/lawrence-gassner.aspx">Lawrence M. Gassner</a><br />
<a href="http://www.wefight4you.com/attorneys/beverly-gassner.aspx">Beverly Jean Gassner</a><br />
<a href="http://www.wefight4you.com/attorneys/stephen-gassner.aspx">Stephen I. Gassner</a><br />
<a href="http://www.wefight4you.com/attorneys/sam-wasserson.aspx">Sammuel R. Wasserson</a><br />
<a href="http://www.wefight4you.com/attorneys/brian-hannemann.aspx">Brian Hanneman</a><br />
<a href="http://www.wefight4you.com/attorneys/kenton-koszdin.aspx" rel="nofollow">Kenton Kozdin</a><br />
<a href="http://www.wefight4you.com/attorneys/paul-thomsen.aspx">Paul Thomsen</a><br />
<a href="http://www.wefight4you.com/attorneys/brandon-carr.aspx">Brandon Carr</a><br />
<a href="http://www.wefight4you.com/attorneys/rowel-manasan.aspx">Rowel Manasan</a><br />
<a href="http://www.wefight4you.com/attorneys/scott-mohney.aspx">Scott Mohney</a><br />
<a href="http://www.wefight4you.com/attorneys/mike-padgett.aspx" title="Mike Padgett Attorney At Law">Mike Padgett</a>

</div>
<p>Since our California Law firm was founded in 1998, we have been committed to providing quality legal services to people in Upland, California and the surrounding areas. When you turn to the Law Office of Marc E. Grossman for help, you can be assured that you will receive the smart, honest and effective representation that you deserve from your attorney.</p>
<p>Whether you are faced with a <a href="http://www.wefight4you.com/bankruptcy/Default.aspx" title="bankruptcy">bankruptcy</a>, <a href="http://www.wefight4you.com/divorce/Default.aspx" title="divorce">divorce</a>, <a href="http://www.wefight4you.com/insurance-bad-faith/Default.aspx" title="insurance bad faith attorney">insurance bad faith claim</a>, <a href="http://www.wefight4you.com/workers-compensation/Default.aspx" title="workers compensation">Workers Compensation</a>, personal injury case, a criminal defense matter, an employment discrimination case or some other legal dilemma, we are here to stand by your side.</p>
<p><a href="http://www.wefight4you.com/Forms/contact.aspx" rel="nofollow">E-mail us</a> or call us toll free at 1-888-407-9068 to schedule a free initial consultation with a trusted lawyer.</p>
</asp:Content>
