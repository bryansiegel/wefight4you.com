﻿<%@ Page Title="Albert D'Antin Attorney at Law - Law Offices of Marc Grossman" Language="C#" MasterPageFile="~/MasterPages/SiteMainInternal.master" AutoEventWireup="true" CodeFile="albert-d-antin.aspx.cs" Inherits="attorneys_albert_d_antin" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainLogo" Runat="Server">
<a href="http://www.wefight4you.com" title="California Lawyers">
<img src="http://www.wefight4you.com/images/teamofattorneys2.jpg" alt="California Lawyers" style="width: 800px; height: 244px"/>
</a>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="PageTitleH1" Runat="Server">
<h1 id="pageTitle">Albert D'Antin Attorney at Law</h1>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="MainContent" Runat="Server">
<p style="text-align:center">
    <img src="../images/albertdantinnew.jpg" style="width:400; height:349"/></p>
  <div class="callOut">
  <h3 style="text-align:center">Quick Facts</h3>
   <strong>Practice Areas:</strong> <a href="http://www.wefight4you.com/family-law/Default.aspx" title="Family Law">Family Law</a>, <a href="http://www.wefight4you.com/criminal-defense/Default.aspx" title="Criminal Defense">Criminal</a>, <a href="http://www.wefight4you.com/civil-litigation/Default.aspx" title="Civil Litigation">Civil</a>, <a href="http://www.wefight4you.com/Bankruptcy/Default.aspx" title="Bankruptcy">Bankruptcy</a><br />
<b>University of La Verne</b><br />
<b>American Bar Association</b><br />
<b>Can Practice in Federal Court</b></div>

<p>Marc Grossman and Albert first met when Albert opposed Marc Grossman in court during a challenging divorce trial.  They developed a mutual respect for each others zeal, aggressiveness and professionalism, and they decided to work together.  Albert  has been an active member of our firm ever since. </p>
<p>Prior to becoming a lawyer, Albert was a police officer in the San Gabriel Valley with a local agency for  13 years. The crime rate was high and there was a lot of activity.</p>
	<p>While working there Albert was a training officer for many new recruits, supervised the explorer program, and patrolled an active beat.  He worked many DUI check points, assisted in numerous search warrants, and made numerous narcotic and gang arrests. During his time he worked closely with the Detective Bureau helping to solve crimes. </p> 
<p>His work as a police officer has given him special insight into how the police work and their special culture.  Albert has been on the “inside” and has personal knowledge how police officers act and think and brings this personal experience and useful information for the benefit of all his clients who require representation in criminal cases.  </p>
<p>After passing the bar exam on the first try Albert retired from the force in 1998 and began his next career on the other side of the fence as a defense lawyer.   Albert has tried cases in family law, civil and criminal court as well as workers’ compensation where he is currently assigned.  Albert has tried criminal cases in DUI, Assault & Battery, Drugs, Domestic Violence, Sex cases, and Murder.  Felonies and Misdemeanor alike.  On the Family Law side he has tried cases relating to Custody, Visitation, Support and Modification,  Restraining Orders, Domestic Violence, and County Child Support matters.  In the Civil arena Albert has tried cases to a jury in Assault & Battery, Motor Vehicle Accidents, and Negligence and filed several appeals. </p>
<p>However, many other cases either settled prior to trial or by way of mediation saving clients thousands of dollars and quicker recoveries.</p>   
<p>Albert has a great reputation in the local courts where he is known by many attorneys and judges throughout the valley as an aggressive but professional and civil minded attorney.  Albert’s life experiences are those of the everyday common man/woman because he has worked his way up from impoverished circumstances in a large family to becoming a well respected attorney in his field.</p>
</asp:Content>

