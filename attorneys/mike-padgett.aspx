<%@ Page Title="Mike Padgett Attorney at the Law Offices of Marc Grossman" Language="C#" MasterPageFile="~/MasterPages/SiteMainInternal.master" AutoEventWireup="true" CodeFile="mike-padgett.aspx.cs" Inherits="attorneys_mike_padgett" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainLogo" Runat="Server">
<a href="http://www.wefight4you.com" title="California Lawyers">
<img src="http://www.wefight4you.com/images/teamofattorneys2.jpg" alt="California Lawyers" style="width: 800px; height: 244px"/>
</a>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="PageTitleH1" Runat="Server">
<h1 id="pageTitle">Mike Padgett Attorney at Law</h1>
<h2 id="pageTitle">Of Counsel</h2>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="MainContent" Runat="Server">
<p style="text-align:center">
    <img src="../images/mikepadgett.jpg" /></p>
  <div class="callOut">
  <h3 style="text-align:center">Quick Facts</h3>
<strong>Practice Areas:</strong> <a href="http://www.wefight4you.com/employment/Default.aspx">Employment Law</a><br />
<strong> Admissions: Supreme Court of California; United States District Court, Central District of             California.</strong><br />
<strong>Law School: Western State University College of Law.  Juris Doctor, 2005.</strong><br />
<strong>Witkin Award for Academic Excellence, Contracts and Advanced Criminal Procedure</strong><br />
<strong>Graduate and Undergraduate Education: California State University, Fullerton. Master of Arts,       Political Science, 1998.  California State University, Fullerton; Bachelor of Arts, Political Science, 1995</strong></div>

<p>Michael A. Padgett is Of Counsel to the Law Offices of Marc E. Grossman for its employment law cases.  He has successfully represented clients in all areas of employment law, including cases involving sexual harassment; discrimination based on disability, pregnancy, race, age, sexual orientation and other protected characteristics; retaliation; wage and hour violations; and wrongful termination.  Mr. Padgett attended evening law school classes while working full time during the day and is sympathetic to the struggles faced by employees.</p>

<p>Mr. Padgett is a member of the California Employment Lawyers Association and the Riverside County Bar Association.</p>


</asp:Content>


