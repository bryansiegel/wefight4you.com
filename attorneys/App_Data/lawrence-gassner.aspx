﻿<%@ Page Title="Lawrence M. Gassner Attorney At Law" Language="C#" MasterPageFile="~/MasterPages/SiteMainInternal.master" AutoEventWireup="true" CodeFile="lawrence-gassner.aspx.cs" Inherits="attorneys_App_Data_lawrence_gassner" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainLogo" Runat="Server">
<a href="http://www.wefight4you.com/" title="California Lawyers">
<img src="http://www.wefight4you.com/images/teamofattorneys2.jpg" alt="California Lawyers" style="width: 800px; height: 244px"/>
</a>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="PageTitleH1" Runat="Server">
<h1 id="pageTitle">Lawrence M. Gassner Attorney At Law</h1>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="MainContent" Runat="Server">
<strong>Holds the following degrees:</strong>

<p>B.S., 1954; M.S., 1955, Industrial Administration<br />
Carnegie Mellon University, Pittsburgh, Pa.<br />
Loyola University, Los Angeles.<br />
1965 Juris Doctor (JD)<br />
Loyola University, Los Angeles.</p>

<strong>EMPLOYMENT</strong>

<p>
Research Clerk, Court of Appeal<br />
California 4th Appellate District, 1966 - 1967.<br />
</p>
<strong>Associate</strong>
<p>
Tobin and Gassner, Upland, California General Civil and Appellate<br />
Practice, 1967 - 1978
</p>

<strong>Partner</strong>
<p>
Gassner & Gassner, Ontario, California.<br />
Now specializing in Family Law and Appellate Practice, 1978 to present.<br />
</p>
<strong>CERTIFICATES / ACCOMPLISHMENTS</strong><br />

<strong>Certified Specialist,</strong> <br />

<p>
Family Law, since May 1985,<br />
California State Bar Board of Legal Specialization.<br />
</p>

<strong>Member, Executive Committee,</strong> 
<p>
State Bar Family Law Section, 1987 to 1992. Reviewing and advising legislature on proposed Family Law legislation; presentation of CLE programs in Family Law.</p>

<strong>Liasion,</strong> 

<p>
Family Law Executive Committee to Law Revision Commission, 1990 to 1992. Preparation of Family Law Code.<br />

Lecturer, Family Law, for Continuing Education of the Bar.<br />

Family Law Advisory Commission, <br />

State Bar Legal Specialization Program, 1992 to 1997. Chair 1996-1997. Directing State Bar program for examination and certification of family law specialists.<br />

Occasional mediator, arbitrator, referee and Judge Pro Tem, Superior Court, San Bernardino County.<br />

Occasional mediator, arbitrator, referee and Judge Pro Tem,<br />

Superior Court, Los Angeles County.<br />

Co-founder and member, East/West Family Law Council,<br /> 

1987 to present. Local Bar Association of Family Law attorneys serving the Pomona and Rancho Cucamonga; courts.<br />
</p>
</asp:Content>

