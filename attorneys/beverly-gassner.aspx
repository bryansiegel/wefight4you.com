﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/SiteMainInternal.master" AutoEventWireup="true" CodeFile="beverly-gassner.aspx.cs" Inherits="attorneys_beverly_gassner" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainLogo" Runat="Server">
<a href="http://www.wefight4you.com/" title="California Lawyers">
<img src="http://www.wefight4you.com/images/teamofattorneys2.jpg" alt="California Lawyers" style="width: 800px; height: 244px"/>
</a>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="PageTitleH1" Runat="Server">
<h1 id="pageTitle">Beverly Jean Gassner Attorney At Law</h1>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="MainContent" Runat="Server">
<strong>("Certified Specialists in family law, State Bar Board of Legal specialization")</strong> 
<p>
1977 - Present: Attorney at Law. A partner in the firm of GASSNER & GASSNER, Inc. a firm consisting of Beverly Jean Gassner, Lawrence M. Gassner, and Stephen I.<br /> Gassner. The practice concentrates solely upon family law.
</p>
<strong>Certified Family Law Specialist, California Board of Legal Specialization since 1981</strong>

<p>1976 - 1977: Research Clerk for Judge Paul Egly, Los Angeles Superior Court, Law and Motion Court, East District.</p>

<strong>ACADEMIC BACKGROUND</strong>
<p>

1977: J.D. University of LaVerne College of Law, Magna Cum Laude. Elected Editor in Chief of the Journal of Juvenile Law, which published its first issue in 1977.<br />

1970: Advanced to PhD candidacy at Claremont Graduate School, with a specialty in American Legal History.<br />

1968: M.A. Claremont Graduate School in U.S. History, with emphasis on Constitutional History.<br />

1953: B.S. Carnegie Institute of Technology<br />
</p>
<strong>ACTIVITIES</strong>
<p>Member of California State Bar Committee of Bar Examiners<br />
Member Judicial Nominees Evaluation Commission, California State Bar, from January 1998 – 2001<br />
Member San Bernardino County Task Force regarding the courts and the community<br />
Member California State Bar Conference of Delegates Executive Committee 1994 – 1997<br />
Co-chaired Bar Leaders' Conference 1997<br />
President Western San Bernardino County Bar Association 1993-1994<br />
Member, California State Bar Committee on Minimum Continuing Legal Education 1990 – 1994<br />
Member, California State Bar Family Law Section, Executive Committee 1987 – 1990<br />
Consultant to CEB Family Law Advisory Committee 1986-present<br />
</p>

<strong>LECTURES AND PUBLICATIONS</strong>

<p>
CEB Speaker, Recent Development Family Law, 1985 – 1996<br />
Consultant for CEB Practice Guide under the California Family Law Act, 1992<br />
Consultant for California Family Law Trial Guide (Matthew Bender, 1992).<br />
</p>
</asp:Content>

