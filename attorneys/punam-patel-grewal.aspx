﻿<script runat="server">
private void Page_Load(object sender, System.EventArgs e)
{
Response.Status = "301 Moved Permanently";
Response.AddHeader("Location","http://www.wefight4you.com/");
}
</script>

<%--

<%@ Page Title="California Special Education Attorney Punam Patel Grewal | Law Offices of Marc Grossman" Language="C#" MasterPageFile="~/MasterPages/SiteMainInternal.master" AutoEventWireup="true" CodeFile="punam-patel-grewal.aspx.cs" Inherits="attorneys_punam_patel_grewal" %>

<%@ Register Assembly="RssToolkit" Namespace="RssToolkit.Web.WebControls" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
<meta name="description" content="Punam Patel Grewal is an attorney at law with the Law Offices of Marc Grossman. Marc specializes in criminal defense, family law and bankruptcy cases. Contact Us to speak with an attorney for your legal needs."/>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainLogo" Runat="Server">
<a href="http://www.wefight4you.com" title="California Lawyers">
 <img src="http://www.wefight4you.com/images/teamofattorneys2.jpg" alt="California Lawyers" style="width: 800px; height: 244px"/></a>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="PageTitleH1" Runat="Server">
<h1 id="pageTitle"> California Special Education Attorney</h1>
<h2 style="text-align:center">Punam Patel Grewal Attorney At Law</h2>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="MainContent" Runat="Server">
<p style="text-align:center">
    <img src="../images/punam-patel-grewal.jpg" /></p>
   
<p>Graduating from Brown University with a BA in both Political Science and Political Philosophy then from the California Western School of Law, Punam Patel Grewal has proven herself to be driven in all that she pursues with an unparalleled enthusiasm. While in law school, Punam clerked with the Riverside District Attorney’s Office and then later she was a prosecutor for the San Bernardino County District Attorney’s office. Here her professionalism, superb work ethic and preparation were a constant source of respect from her peers. After finding success in the legal field, Punam sought further personal challenges by entering the hospitality industry as a hotel owner and operator. Later, Punam took on greater responsibility as the Managing Attorney at the Law Offices of Marc E. Grossman where she is primarily responsible for the day- to-day management of staff, <a href="http://www.wefight4you.com/attorneys/Default.aspx">attorneys</a>, cases, accounting, etc. Her areas of practice at the firm include Special Education Law and <a href="http://www.wefight4you.com/criminal-defense/Default.aspx" title="Criminal Defense">Criminal Defense</a>.</p>
<p>Fluent in Hindi and Gujarati, Punam has numerous accomplishments at the Law Offices of Marc E. Grossman such as receiving the Patriotic Employer award from her staff, as well as notable cases that include plea bargaining a twenty-six month sentence on an attempted murder case (which carries twenty-five years to life)and successfully obtaining compensation for children and their families in matters of California Special Education.  Still committed whole-heartedly to practicing law, Punam continues to work diligently not only as a <a href="http://www.wefight4you.com/blog/post/2010/05/11/Special-Education-What-is-an-IEP.aspx">California Special Education lawyer</a>, but as an entrepreneur, a wife and most importantly as a mother. The tireless yet infinitely fulfilling work of raising two young boys is her most rewarding job as she seeks to shape the moral character that will serve them well throughout their lives.</p>
<p>Consistently professional, unparalleled drive, genuine leadership, commitment to preparation, maturity and personal growth, Punam Patel Grewal was born with the unique combination of traits that generate a sincerely gifted lawyer and an exceptional person.</p>
<h3 style="text-align:center">California Special Education News</h3>
<div class="callOut">
<asp:DataList ID="DataList1" runat="server" DataSourceID="RssDataSource1" 
        Width="404px">
 <ItemTemplate>
 <asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl='<%# Eval("link") %>' Text='<%# Eval("title") %>'></asp:HyperLink>
 </ItemTemplate>
    </asp:DataList>
    <cc1:RssDataSource ID="RssDataSource1" runat="server" MaxItems="4" 
        Url="http://www.wefight4you.com/blog/category/feed/Special-Education.aspx">
    </cc1:RssDataSource>
</div>
</asp:Content>
--%>
