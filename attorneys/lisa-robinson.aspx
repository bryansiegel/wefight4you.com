﻿<%@ Page Title="Lisa Robinson Attorney At Law | Law Offices of Marc Grossman" Language="C#" MasterPageFile="~/MasterPages/SiteMainInternal.master" AutoEventWireup="true" CodeFile="lisa-robinson.aspx.cs" Inherits="attorneys_lisa_robinson" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainLogo" Runat="Server">
<a href="http://www.wefight4you.com/" title="California Lawyers">
<img src="http://www.wefight4you.com/images/teamofattorneys2.jpg" alt="California Lawyers" style="width: 800px; height: 244px"/>
</a>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="PageTitleH1" Runat="Server">
<h1  id="pageTitle">Lisa Robinson Bankruptcy Attorney At Law</h1>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="MainContent" Runat="Server">
<p style="text-align:center">
    <img src="../images/lisa-robinson.jpg" /></p>
  <div class="callOut">
  <h3 style="text-align:center">Quick Facts</h3>
   <strong>Practice Areas:</strong><a href="http://www.wefight4you.com/bankruptcy/Default.aspx" title="bankruptcy attorney"> Bankruptcy</a>, <a href="http://www.wefight4you.com/immigration/Default.aspx" title="Immigration">Immigration</a><br />
      <strong>Loyola Law School</strong><br />
<b>American Bar Association</b><br />
<b>Can Practice in Federal Court</b></div>

<p>Lisa Robinson did her undergraduate work in Political Science at the University of California Irvine where she graduated Cum Laude in 2006. At UCI, Lisa was a member of the Political Science Honor Society, Pi Sigma Alpha. Lisa obtained her Juris Doctor from Loyola Law School in 2009. During law school, beyond doing internships in private practice focusing on bankruptcy and plaintiff’s civil work, Lisa interned at the Cancer Legal Resource Center providing aid and information to people suffering from cancer who had related legal issues. She was admitted to the California Bar in 2009 and came to work for the Law Offices of Marc E. Grossman in 2009.</p>

<p>Lisa Robinson has been heading the bankruptcy department since her arrival to the firm. She has a passion for helping people in their plight and understanding the needs and concerns of her clients. She has seen over 100 cases through to a successful discharge. Although primarily focusing in bankruptcy related matters, Lisa has also been working on plaintiff’s civil cases including car accident cases.</p>

<p>Lisa is a member of the California Bar and admitted to the United States District Court of California (Central). Lisa is a member of the Consumer Attorneys of Inland Empire.</p>

<p>In her free time, Lisa enjoys hiking and skiing. She lives in Rancho Cucamonga with her husband John and their chocolate lab Bailey.</p>
</asp:Content>

