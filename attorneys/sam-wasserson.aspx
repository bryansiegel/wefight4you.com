﻿<%@ Page Title="Sam Wasserson Attorney At Law" Language="C#" MasterPageFile="~/MasterPages/SiteMainInternal.master" AutoEventWireup="true" CodeFile="sam-wasserson.aspx.cs" Inherits="attorneys_sam_wasserson" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainLogo" Runat="Server">
<a href="http://www.wefight4you.com/" title="California Lawyers">
<img src="http://www.wefight4you.com/images/teamofattorneys2.jpg" alt="California Lawyers" style="width: 800px; height: 244px"/>
</a>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="PageTitleH1" Runat="Server">

<h1 id="pageTitle">Sam Wasserson Attorney At Law</h1>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="MainContent" Runat="Server">
<p>Sam Wasserson is one of California's first ever lawyers to attain California State Bar certification as a Family Law Specialst and one of only a handful of such specialists in the 909.  Sam  has been fighting for his clients for over 30 years and has numerous published cases where he has helped to change the law to protect his client's rights.  Sam is a former Judge Pro Tem, a founding member of the local East/West Family Law Council and on the State Bar's Standing Committee on Custody and Visitation.  Before becoming a lawyer in 1974, Sam was a commissioned officer in the National Guard for 8 years where he served as a Cavalry Platoon Leader. Sam is without a doubt an institution in Inland Empire Family Law.</p>
</asp:Content>

