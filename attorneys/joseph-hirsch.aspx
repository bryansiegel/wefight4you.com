﻿<script runat="server">
private void Page_Load(object sender, System.EventArgs e)
{
Response.Status = "301 Moved Permanently";
Response.AddHeader("Location","http://www.wefight4you.com/");
}
</script> 

<%--<%@ Page Title="Joseph Hirsch Attorney at Law - Law Offices of Marc Grossman" Language="C#" MasterPageFile="~/MasterPages/SiteMainInternal.master" AutoEventWireup="true" CodeFile="joseph-hirsch.aspx.cs" Inherits="attorneys_joseph_hirsch" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
<meta name="description" content="Joseph Hirsch is an attorney at law with the Law Offices of Marc Grossman. Joseph specializes in criminal defense, family law and bankruptcy cases. Contact Us to speak with an attorney for your legal needs."/>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainLogo" Runat="Server">
<a href="http://www.wefight4you.com" title="California Lawyers">
<img src="http://www.wefight4you.com/images/california-lawyers.jpg" alt="California Lawyers" />
</a>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="PageTitleH1" Runat="Server">
<h1 id="pageTitle">Joseph Hirsch Attorney at Law</h1>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="MainContent" Runat="Server">
<p style="text-align:center">
    <img src="http://www.wefight4you.com/images/joe-hirsch.jpg" /></p>
 <div class="callOut">
  <h3 style="text-align:center">Quick Facts</h3>
   <strong>Practice Areas:</strong> Family Law, Criminal, Civil, Bankruptcy<br />
<b>University of La Verne</b><br />
<b>American Bar Association</b><br />
<b>Can Practice in Federal Court</b></div>
<p>Joe Hirsch is an essential part of the Law Offices of Marc Grossman team. Joe joined the team in early 2009 and has already settled numerous cases for the Firm. Born in a small town in Illinois Joe was no stranger to hard work. Since attending La Verne Law School Joe has incorporated the same hard work from his days on a small farm in Illinois.</p>
<p>Joe Hirsch’s warming personality and knowledge of the Law makes him a crucial portion of the Law Firm. Joe handles all types of cases ranging from family law to civil cases. Joe’s even temperament may seem offsetting when you first meet him, but his a fierce opponent in the court room. 
</p>
<p>Joe Hirsch is the right attorney for you. To set up a Free Initial Consultation with Joe or any of our lawyers don’t hesitate to contact the Law Offices of Marc Grossman.</p>
</asp:Content>

--%>