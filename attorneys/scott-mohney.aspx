<%@ Page Title="Scott Mohney Civil Attorney | Law Offices of Marc Grossman" Language="C#" MasterPageFile="~/MasterPages/SiteMainInternal.master" AutoEventWireup="true" CodeFile="scott-mohney.aspx.cs" Inherits="attorneys_scott_mohney" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainLogo" Runat="Server">
<a href="http://www.wefight4you.com/" title="Bankruptcy, Divorce, Workers Compensation Attorneys">
<img src="http://www.wefight4you.com/images/teamofattorneys2.jpg" alt="California Bankruptcy, Divorce and Workers Compensation Lawyers" style="width: 800px; height: 244px"/></a>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="PageTitleH1" Runat="Server">
<h1 id="pageTitle">D. Scott Mohney Attorney At Law</h1>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="MainContent" Runat="Server">
<p style="text-align:center">
<img src="../images/scott-mohney.jpg" alt="Scott Mohney Attorney At Law" />
</p>
<p>Scott Mohney did his undergraduate work in American Studies at Cal State Los Angeles and Cal State Long Beach, and obtained his Juris Doctor from the University of La Verne Law School in 1985.  Admitted to the California Bar in 1986, Scott has specialized primarily in plaintiffs insurance bad faith litigation, and has obtained numerous and substantial judgments and settlements from insurance companies on behalf of insurance consumers.  With a primary focus in life, health and disability cases, Scott has also litigated many casualty and property actions and has significant trial experience (a combined total in excess of twenty-five jury and bench trials).  Although primarily focusing in insurance related matters, he has also successfully tried cases involving partnership disputes, personal injury, and intellectual property (misappropriation of right of publicity). </p>
<p>Scott also has extensive experience in handling appeals, briefing and/or arguing well over seventy-five appeals, including those before the California State Courts of Appeal, the Ninth Circuit Court of Appeal.  In 2002, Scott was privileged to argue an appeal before the California Supreme Court (Sweatman v. Cal-Vet).  Other appeals in which he participated resulting in published opinions include Gomez v. Pronto Trucking (Arizona), Cross v. Mutual Benefit, Hernandez v. GAB, and Sarraf v. Standard. </p>
<p>Scott is a member of the California Bar, and admitted to the United States District Courts in California (Central, Eastern, and Southern Districts), as well as the Ninth Circuit Court of Appeal.  He has assisted in the litigation of numerous insurance related class actions, and was a Professor of Law teaching legal research and writing courses for eleven years.  Prior to commencing law school, his early paralegal training at UCLA was invaluable in developing his research and writing abilities. </p>
<p>Scott is also an accomplished musician, having played keyboards in his own band and for recording artists in Southern California and in the Phoenix area.  He is also a published writer of fiction.</p>
<p>With his present association with the Law Offices of Marc E. Grossman, Scott continues his devotion to fighting insurance company abuses and in recovering damages for injured individuals.</p>
</asp:Content>