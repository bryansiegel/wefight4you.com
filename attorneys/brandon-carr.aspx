﻿<%@ Page Title="Brandon Carr Attorney At Law | Law Offices of Marc Grossman" Language="C#" MasterPageFile="~/MasterPages/SiteMainInternal.master" AutoEventWireup="true" CodeFile="brandon-carr.aspx.cs" Inherits="attorneys_brandon_carr" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainLogo" Runat="Server">
<a href="http://www.wefight4you.com/" title="California Lawyers">
<img src="http://www.wefight4you.com/images/teamofattorneys2.jpg" alt="California Lawyers" style="width: 800px; height: 244px"/>
</a>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="PageTitleH1" Runat="Server">
<h1  id="pageTitle">Brandon Carr Attorney At Law</h1>

</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="MainContent" Runat="Server">
<p style="text-align:center">
	<img src="../images/brandon-carr-large.jpg" alt="Brandon Carr Attorney At Law">
	</p>
<p>Brandon Carr joined the firm in November 2010. He has been a licensed attorney since December 2009.  He is dedicated to providing excellent representation for clients in civil litigation and criminal law matters. Prior to joining the firm, Brandon worked as a Law Clerk with the San Bernardino County District Attorney’s Office.</p>  
<p>Brandon received his law degree in May 2009 from Regent University School of Law in Virginia Beach, Virginia. He was a Senior Editor and Articles Editor on the Regent University Law Review. He also was elected Chair of the Regent University Trial Advocacy Board. While on the Trial Advocacy Board, Brandon competed in several nationally recognized trial competitions. In 2008, his team won Best Brief at the National Pretrial Competition. While in law school, he also organized and participated in political debates on campus. Brandon graduated in the top 15% of his law school class.</p>
<p>During law school Brandon was an Intern with the United States Attorney’s Office and served as an Extern for United States District Judge Mark S. Davis.</p>
<p>Brandon also attended Iowa State University in Ames, Iowa, where he received degrees in Political Science and Psychology. While there, he served as President of Beta Sigma Psi fraternity and was active in Iowa State Theatre.</p>
<p>In his free time, Brandon enjoys hiking and playing Ultimate Frisbee.  Brandon lives with his wife Heather and their rabbit Mugsy.</p>
</asp:Content>

