﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/SiteMainInternal.master" AutoEventWireup="true" CodeFile="paul-thomsen.aspx.cs" Inherits="attorneys_paul_thomsen" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainLogo" Runat="Server">
<a href="http://www.wefight4you.com" title="California Lawyers">
 <img src="http://www.wefight4you.com/images/teamofattorneys2.jpg" alt="California Lawyers" style="width: 800px; height: 244px"/></a>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="PageTitleH1" Runat="Server">
<h1 id="pageTitle"> California Workers Compensation Attorney</h1>
<h2 style="text-align:center">Paul Thomsen Attorney At Law</h2>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="MainContent" Runat="Server">
<p style="text-align:center">
    <img src="../images/paul-thomsen-large.jpg" /></p>
<div class="callOut"> Practice Areas: <a href="http://www.wefight4you.com/workers-compensation/Default.aspx">Worker's Compensation</a>, <a href="http://www.wefight4you.com/bankruptcy/Default.aspx">Bankruptcy</a>, <a href="http://www.wefight4you.com/divorce/Default.aspx">Divorce</a>, <a href="http://www.wefight4you.com/criminal-defense/Default.aspx">Criminal</a>, and <a href="http://www.wefight4you.com/civil-litigation/Default.aspx">Civil litigation </a></div>
<p>I graduated from the United States Merchant Marine Academy, Kings Point in June 1993.
I received a reserve commission in the United States Navy as an Ensign.  After 12 years of service I was Honorably Discharged from the Navy.</p>
<p>I worked for various parts of the AP Moller Group from Feb 1994 through June 2003.  Those companies included Maersk Pacific, Ltd.; AP Moller; APM Terminals North America, Maersk Pacific Ltd. While with those companies I became intimately familiar with the heavy industry environment including Maritime and Rail Container Terminals, Shipboard Operations and Customer Service.  Additionally, I was the project manager for software implementation at the Pier 400 cargo terminal in the Port of Los Angeles.</p>
<p>In 2003 I left the maritime industry to attend Whittier Law School.  In October 2007, I was admitted to the practice of law in the state of California. I was admitted to the practice of law before the U.S. Federal Court Central District of California Dec 2009.  Since being admitted I have worked in several practice areas including but not limited to Worker's Compensation, Bankruptcy, Family, Criminal, and Civil litigation.  I have tried many cases both to the bench and to a jury.</p>
</asp:Content>

