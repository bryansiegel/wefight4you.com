﻿<%@ Page Title="Kenton Koszdin Attorney At Law" Language="C#" MasterPageFile="~/MasterPages/SiteMainInternal.master" AutoEventWireup="true" CodeFile="kenton-koszdin.aspx.cs" Inherits="attorneys_kenton_koszdin" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
<META NAME="ROBOTS" CONTENT="NOINDEX, NOFOLLOW">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainLogo" Runat="Server">
<a href="http://www.wefight4you.com">
<img src="http://www.wefight4you.com/images/teamofattorneys2.jpg" style="width: 802px; height: 228px" />
</a>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="PageTitleH1" Runat="Server">
<h1 id="pageTitle">Kenton Koszdin Attorney At Law <br />(Of Counsel)</h1>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="MainContent" Runat="Server">
<p style="text-align:center">
    <img src="http://www.wefight4you.com/images/people/kenton-kozdin-web.jpg" /></p>
<div class="callOut">
  <h3 style="text-align:center">Quick Facts</h3>
   <strong>Practice Areas: </strong>Social Security Disability / SSI<br />
University of San Francisco, School of Law – JD 1996<br />
California State University, Northridge – BS Business Administration (Accounting) 1989<br />
Admitted to California Bar December 1996<br />
Admitted to Nevada Bar October 1997<br />
Licensed Certified Public Accountant (California) 1991<br /></div>
<p>Kenton Koszdin is a lawyer representing disabled people all over Southern California in Social Security Disability/SSI cases at all levels from the initial applicant through the hearing and appeals process.  He has handled hundreds of disability cases of all types and has presented disability cases in all Southern California branches of the Office of Disability Adjudication and Review.</p>
<p>Kenton Koszdin also has extensive experience in handling workers’ compensation cases and has the knowledge of how workers’ compensation cases and social security disability cases relate to each other.</p>
<p>Kenton Koszdin is s sustaining member of the National Organization of Social Security Claims Representatives (NOSSCR) and attends their conferences to keep up do date in the latest developments in Social Security Disability/SSI laws and regulations.</p>
</asp:Content>

