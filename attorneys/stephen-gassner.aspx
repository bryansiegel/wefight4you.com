﻿<%@ Page Title="Stephen Gassner Attorney At Law" Language="C#" MasterPageFile="~/MasterPages/SiteMainInternal.master" AutoEventWireup="true" CodeFile="stephen-gassner.aspx.cs" Inherits="attorneys_stephen_gassner" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainLogo" Runat="Server">
<a href="http://www.wefight4you.com/" title="California Lawyers">
<img src="http://www.wefight4you.com/images/teamofattorneys2.jpg" alt="California Lawyers" style="width: 800px; height: 244px"/>
</a>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="PageTitleH1" Runat="Server">
<h1 id="pageTitle">Stephen I. Gassner Attorney At Law</h1>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="MainContent" Runat="Server">
<strong>Holds the following degrees:</strong>
<p>
1984 BA Pitzer College Psychology<br />
1989 MBA National University Business Administration<br />
1994 JD University of La Verne College of Law Law<br />
1997 MA Claremont Graduate University Information Systems<br /> 
</p>
<strong>Stephen Gassner has been practicing family law since 1995.</strong><br /> 
<p>
From 1990-1994, while working as Computer Systems Engineering Consultant for Digital Equipment Corporation, attended law school at the University of La Verne, College of Law.</p>

<p>Graduating second in his class, Stephen left the field of Computer Systems Integration to pursue a career in Civil Litigation.</p> 

<p>Practice limited to resolving controversies involving Family Law, Child Custody, Child Support, Domestic Violence, Spousal Support, Property Division (including business valuation), Guardianships and Conservatorships, Juvenile Dependency.</p> 

<p>Experience in cases relating to sexual abuse of children, construction defects, landlord tenant disputes, mobilehome residency, intentional torts (including fraud and sexual assault), general negligence, great emotional distress, wrongful death, collections, commercial real estate, and breach of contract.</p>

<p>Participation at the State Bar of California<br />
1996-1999
</p>
 
<p>Board of Directors of the California Young Lawyers Association<br />
2001- Present Board of Directors of the State Bar Section on Law Office Management and Technology
</p>
</asp:Content>

