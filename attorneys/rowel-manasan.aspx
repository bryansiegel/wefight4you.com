﻿<%@ Page Title="Rowel Manasan Attorney At Law" Language="C#" MasterPageFile="~/MasterPages/SiteMainInternal.master" AutoEventWireup="true" CodeFile="rowel-manasan.aspx.cs" Inherits="attorneys_rowel_manasan" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainLogo" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="PageTitleH1" Runat="Server">
<h1 id="pageTitle">Rowel Manasan Attorney At Law</h1>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="MainContent" Runat="Server">

<p>Rowel Manasan has dedicated his life and business to helping young families in the community grow through education and cultural awareness.  He is a frequently requested guest on The Filipino channel, where he speaks on subjects ranging from Dual Citizenship to Civil Rights and Estate Planning.  Unlike other lawyers, rowel focuses his law practice on the relationship he develops with his clients and not just the documents he provides, which means you know you are going to have someone to turn to in times of challenge.</p>

<p>As a schoolteacher at Covina High School, Rowel witnessed firsthand how children within the foster care system suffer greatly - both personally and academically.  that's why his mission is to educate and provide parents with the tools necessary to ensure their children are always taken care of by the people you choose, in the way you want.  Rowel hosts educational events ranging from the 9 Steps to ensuring Your Kids are raised the Way You Want by the People You Want, to his popular guardianship Nomination Workshop, and to Asset Protection for young families.</p>
</asp:Content>

