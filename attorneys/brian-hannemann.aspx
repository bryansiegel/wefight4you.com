﻿<%@ Page Title="Brian Hannemann California Trial Attorney | Law Offices of Marc Grossman" Language="C#" MasterPageFile="~/MasterPages/SiteMainInternal.master" AutoEventWireup="true" CodeFile="brian-hannemann.aspx.cs" Inherits="attorneys_brian_hannemann" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainLogo" Runat="Server">
<a href="http://www.wefight4you.com" title="California Lawyers">
<img src="http://www.wefight4you.com/images/teamofattorneys2.jpg" alt="California Lawyers" style="width: 800px; height: 244px"/>
</a>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="PageTitleH1" Runat="Server">
<h1 id="pageTitle">Brian G. Hannemann</h1>
<h2 style="text-align:center;">Of Counsel</h2>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="MainContent" Runat="Server">
<p><strong>Undergraduate college:</strong></p> 
<p>Embry-Riddle Aeronautical University</p>

<p><strong>Undergraduate degree:</strong></p>
<p>Professional Aeronautics - Summa Cum</p>

<p><strong>Law school:</strong></p>
<p>Southwestern University School of Law</p>

<p><strong>Year graduated law school:</strong></p>
<p>1993 - Magna Cum Laude</p>

<p><strong>Bar information:</strong></p>
<p>Admitted to practice in California 1993 U.S. District Court,California, Central and Southern Districts, 1993</p>

<p><strong>Association information:</strong></p>
American Board of Trial Advocates<br />
Consumer Attorneys Association of Los Angeles<br />
Orange County Trial Lawyers Association<br />
Western San Bernardino County Bar Association<br />
<p>Seasoned trial attorney and former insurance defense attorney Brian Hannemann has been relentlessly fighting for justice in the 909 for nearly 20 years.  Brian is a member of the prestigious American Board of Trial Advocates and trials is what Brian does best.  Imagine a silent courtroom, somber mood, the jury foreperson saying "We have reached a verdict."  How important is it that YOU be represented by experienced trial counsel with a winning record?  Brian fuses his home-town values from growing up in Beaumont, his military experience as a helicopter pilot and his law degree from South Western Law school where he graduated Magna Cum Laude to bring aggressive, persistent and tenacious representation to his clients.</p>
</asp:Content>

