﻿<%@ Page Title="Marc Grossman Attorney at Law - Law Offices of Marc Grossman" Language="C#" MasterPageFile="~/MasterPages/SiteMainInternal.master" AutoEventWireup="true" CodeFile="marc-grossman.aspx.cs" Inherits="attorneys_marc_grossman" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
<meta name="description" content="Marc Grossman is an attorney at law with the Law Offices of Marc Grossman. Marc specializes in criminal defense, family law and bankruptcy cases. Contact Us to speak with an attorney for your legal needs."/>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainLogo" Runat="Server">
<a href="http://www.wefight4you.com" title="California Lawyers">
 <img src="http://www.wefight4you.com/images/teamofattorneys2.jpg" alt="California Lawyers" style="width: 800px; height: 244px"/></a>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="PageTitleH1" Runat="Server">
<h1  id="pageTitle">Marc Grossman Attorney at Law</h1>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="MainContent" Runat="Server">
   <p style="text-align:center">
    <img src="../images/marc-e-grossman.jpg" /></p>
  <div class="callOut">
  <h3 style="text-align:center">Quick Facts</h3>
   <strong>Practice Areas:</strong> <a href="http://www.wefight4you.com/divorce/Default.aspx" title="Divorce Attorney">Divorce</a>, <a href="http://www.wefight4you.com/criminal-defense/Default.aspx" title="Criminal Attorney">Criminal</a>, <a href="http://www.wefight4you.com/civil-litigation/Default.aspx">Civil</a>, <a href="http://www.wefight4you.com/bankruptcy/Default.aspx" title="Bankruptcy Attorney">Bankruptcy</a><br />
<b>University of La Verne</b><br />
<b>American Bar Association</b><br />
<b>Can Practice in Federal Court</b></div>

<p>Marc Elliot Grossman (born October 1, 1966 in Pomona, California) is a
practicing attorney, social activist, rare document dealer and former
musician.</p>
<h3>Marc Grossman in the News</h3>
<p><strong>State Supreme Court Upholds Decision regarding Civil Rights Violations Finding Continued Parole Rejections Tantamount to Turning Sentence Of Life in Prison with Possibility Of Parole Into Life Sentence Without Parole.</strong></p>
<br />
<p style="text-align: center"><object width="425" height="344"><param name="movie" value="http://www.youtube.com/v/tO76V6u--z8&hl=en_US&fs=1&rel=0"></param><param name="allowFullScreen" value="true"></param><param name="allowscriptaccess" value="always"></param><embed src="http://www.youtube.com/v/tO76V6u--z8&hl=en_US&fs=1&rel=0" type="application/x-shockwave-flash" allowscriptaccess="always" allowfullscreen="true" width="425" height="344"></embed></object>
<br />
<a href="/wins/Default.aspx">Click Here to See the full story about this case</a>
</p>
<h3>Marc Grossman's Pledge towards Divorce Clients</h3>
<p style="text-align:center"><object width="425" height="344"><param name="movie" value="http://www.youtube.com/v/xf7V_0L2EK0&hl=en_US&fs=1&"></param><param name="allowFullScreen" value="true"></param><param name="allowscriptaccess" value="always"></param><embed src="http://www.youtube.com/v/xf7V_0L2EK0&hl=en_US&fs=1&" type="application/x-shockwave-flash" allowscriptaccess="always" allowfullscreen="true" width="425" height="344"></embed></object></p>
<h3>Early Life and Education</h3>
<p>Marc Grossman was born to David Grossman and Adrienne Grossman (nee
Wasserman) and is the second of three sons.  Grossman's older brother, Scott
Grossman, Ph.D., is a noted theoretical astro-physicist and Grossman's
younger brother, Craig Grossman, is a practicing attorney as is Grossman's
father and wife.  Grossman attended Upland High School.</p>
<p>Grossman studied at the University of California at Los Angeles (1984 -
1987) matriculating in psychology and business and the University of
California at Santa Cruz (1988 - 1990) attaining a Bachelor of Arts degree
in Philosophy. During lawschool, Grossman worked as a substitute highschool
teacher and as a vest-pocket coin dealer.  Grossman attained a Juris Doctor
degree from the University of La Verne College of Law in 1998 and was
admitted to the California State Bar in November, 1998.</p>
<h3>Social Activism</h3>
<p>As a student activist at UCLA, Grossman was involved in helping to establish
UCLA as one of the first sanctuaries for Salvadorian refugees during the
civil conflicts of the 1980s and was also instrumental in the campaign
calling for the Regents to divest their funds from South Africa during the
Apartheid era.  Grossman worked for Campaign California, a grass roots fund
raising organization under the direction of Assemblyman and former "Chicago
Seven" member Tom Hayden, and fought successfully to pass Proposition 65,
the Safe Drinking Water and Toxic Enforcement Act of 1986</p>
<h3>Irvin Landrum Matter</h3>
<p>As an attorney, Grossman represented Plaintiffs in the Irvin Landrum matter
which gathered national attention when Claremont Police Officers were
accused of shooting an unarmed African-American 18 year old and of planting
a gun on him in 1999.  The officers had claimed they had shot Landrum in
self defense after he pulled a gun and had shot at them.  However,
ballistics reports confirmed the gun had never been fired and belonged to a
deceased police chief from a neighboring town.  In the wake of civil rights
demonstrations alleging racial profiling and murder, the City of Claremont
awarded the two officers involved in the shooting with Employee of the Year
Honors as did the Claremont Police Officers Association.  The case was
eventually settled for $450,000.00 and the awards were rescinded. In 2000,
California Chief Justice Ronald George presided over an award ceremony
wherein the California First Amendment Coalition awarded Claremont their
Black Hole Award in recognition of having been the public entity acting most
improperly regarding open government and first amendment freedoms in
connection with their Landrum related activities.  Further fallout from the
Landrum matter included several subsequent lawsuits regarding racial
profiling and police officer rights.</p>
<h3>Other High Profile Matters</h3>
<p>Grossman has been at the center of other high profile matters including
representing San Bernardino County gadflies Jeff Wright and Shirley Goodwin.
Wright was alleged to have been targeted for his political activities
targeting former San Bernardino County Supervisor Jerry Eaves.  Eaves would
ultimately face Federal Criminal corruption charges for which he would plead
guilty. </p>
<%--<p>Grossman also represented Claudia Oleesky whose custody case was at the
center of what was called by District Attorney Dennis Stout the largest
obstruction of justice case in the history of California at the time.
Oleesky's ex-husband, Steve Oleesky, and his attorney John Watkins faced
hundreds of felony charges in connection with the matter and were eventually
convicted of criminal charges in connection with various schemes including
money laundering and charity scams whereby millions of dollars were raised
and used to target Oleesky, her attorneys and the judges involved in their
custody case.  </p>--%>
<p>In 2001, Grossman formed the briefly lived South Western Poverty Law Center
as a coalition of local organizations to fight institutionalized
discrimination in the Inland Empire.  In 2003, Grossman was recognized by
the Los Angeles Times as one of the Inland Valley Times 66 most influential
persons.
</p>
<h3>Prisoner Rights Advocate</h3>
<p>Grossman has successfully obtained the release of dozens of lifers whom had
been determined to have been improperly denied parole following their
convictions and life sentences.  Grossman's work in this field has resulted
in numerous published opinions and has helped shape the law at the State and
Federal levels.  His clients have included Robert Rosenkrantz, Leslie Van
Houten and Richard Shaputis.</p>
<h3>Personal Life</h3>
<p>Grossman is an accomplished musician and poker player and currently plays in
a band with 80's rocker Gene "Rockin" Roland A.K.A. The Zig Zag Man and
former Velvet Chain drummer Craig Van Sant.  Grossman is also a respected
collector of rare historical manuscripts and the proprietor of Signature
House, an auction house devoted exclusively to the sale of historical
documents and manuscripts.  In 2008, Grossman formed the American Manuscript
Library, a non-profit organization providing scholarships and educational
programs throughout Southern California as well as providing a research
library which includes holographic exemplars from thousands of individuals.</p>
<p>Grossman is currently married to Evangeline Grossman, a partner at the
prestigious firm of Shernoff, Bidart and Echeveria whom are
recognized as the founders of the tort of Bad-Faith Insurance and also for
their Holocaust litigation whereby they sought to hold European insurance
companies accountable for thousands of life insurance policies never honored
following World War II.  Grossman and his wife
live in Upland, California with their 3 wonderful children.</p>
</asp:Content>

