﻿<%@ Page Title="California Bankruptcy Attorney, Divorce Attorneys, Workers Compensation and DUI Attorney" Language="C#" MasterPageFile="~/MasterPages/SiteMain.master" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="_Default" %>

<%@ Register Assembly="RssToolkit" Namespace="RssToolkit.Web.WebControls" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <meta name="description" content="Whether you need an Attorney in Upland or anywhere in San Bernardino County the Law Offices of Marc Grossman can help. We are the Inland Empires premier Law Firm specializing in Bankrupcy, Divorce, Workers Compensation and, DUI defense and criminal defense. Our Upland California family law attorneys can help you with everything from a simple divorce mediation to child custody and child visitation."/>
<meta name="keywords" content="lawyer, attorney, law firm, law office, legal advice, litigation, civil litigation, criminal defense, lawyer, attorney, DUI, DWI"/>
<link rel="canonical" href="http://www.wefight4you.com" />
<meta name="verify-v1" content="lmClrLLJKSbVte4srwmNKv7+TDHVuCMWlNGly57hYAs=" />
    <style type="text/css">
        .main_head
        {
            font-size: x-large;
            color: #FFFFFF;
        }
    </style>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="PageTitleH1" Runat="Server">
    <h2 style=" color:White; font-size:small">
    Experienced California Attorneys That Handle Bankruptcy, Divorce, DUI, Workers Compensation in the Inland Empire</h2>
    <br />
    <h3 style="text-align:center">Primary Practice Areas</h3>
    <p style="text-align:center"><a href="#bankruptcy_attorneys">Bankruptcy</a> | <a href="#divorce_attorneys">Divorce</a> | <a href="#workers_compensation">Workers Compensation</a> | <a href="#dui_attorneys">DUI</a></p>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" Runat="Server"> 
<h2 style="text-align:center">Latest News</h2>
<div class="callNews">
<asp:DataList ID="DataList1" runat="server" DataSourceID="RssDataSource1" 
        Width="404px">
 <ItemTemplate>
 <asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl='<%# Eval("link") %>' Text='<%# Eval("title") %>'></asp:HyperLink>
 </ItemTemplate>
    </asp:DataList>
    <cc1:RssDataSource ID="RssDataSource1" runat="server" MaxItems="4" 
        Url="http://feeds.feedburner.com/BlogBankruptcyDivorceWorkersCompensationInsuranceBadFaithDui">
    </cc1:RssDataSource>
</div>
<hr />   

 <p>When you are faced with a legal challenge, you need a Law Firm who will fight for you. Whether you're <a href="http://www.wefight4you.com/bankruptcy/Default.aspx" title="California Bankruptcy Attorney">filing for bankruptcy in California</a>, considering hiring a <a href="http://www.wefight4you.com/divorce/Default.aspx" title="divorce attorney">divorce attorney</a> or interested in <a href="http://www.wefight4you.com/workers-compensation/Default.aspx" title="workers compensation">workers compensation</a> we can help. We are a general Law Office that can represent you in a wide range of cases. Even if we don't specialize in the type of law you require, chances are we've worked with California attorneys that have. We have Law Offices in multiple locations to serve you from Upland to La Puente and have built relationships with other California attorneys in case we don't service your location. If you require legal representation you won't know your options if you don't call. Our initial consultations are FREE. That way you know without paying whether you have a case or not.</p>
    <!---locations-->
<h3 style="text-align:center">Locations We Serve</h3>
<hr />
<div style="width:497px;">
<!--left-->
<div style="width:240px; padding-right:8px; float:left;"><ul>
            <li><a href="http://www.wefight4you.com/lawyer/alta-loma.aspx" title="Alta Loma Attorney" style="text-decoration: none;"> Alta Loma</a></li>
			<li><a href="http://www.wefight4you.com/lawyer/claremont.aspx" title="lawyer claremont" style="text-decoration: none;">Claremont</a></li> 
			<li><a href="http://www.wefight4you.com/lawyer/chino.aspx" title="lawyer chino" style="text-decoration: none;">Chino</a></li> 
			<li><a href="http://www.wefight4you.com/lawyer/chino-hills.aspx" title="lawyer chino hills" style="text-decoration: none;">Chino Hills</a></li> 
			<li><a href="http://www.wefight4you.com/lawyer/corona.aspx" title="lawyer corona" style="text-decoration: none;">Corona</a></li> 
			<li><a href="http://www.wefight4you.com/lawyer/diamond-bar.aspx" title="lawyer diamond bar" style="text-decoration: none;">Diamond Bar</a></li>						
			<li><a href="http://www.wefight4you.com/lawyer/fontana.aspx" title="lawyer fontana"  style="text-decoration: none;">Fontana</a></li> 	
			<li><a href="http://www.wefight4you.com/lawyer/glendora.aspx" title="lawyer glendora"  style="text-decoration: none;">Glendora</a></li> 	
			<li><a href="http://www.wefight4you.com/lawyer/la-puente.aspx" title="La Puente Workers Comp Attorney" style="text-decoration: none;">La Puente Workers Compensation</a></li> 
			<li><a href="http://www.wefight4you.com/bankruptcy/la-puente.aspx" title="La Puente Bankruptcy" style="text-decoration: none;">La Puente Bankruptcy</a></li>
            </ul>
            </div>
            <!--right-->
<div style="width:240px; padding-left; float:right;">
<ul>
<li><a href="http://www.wefight4you.com/lawyer/la-verne.aspx" title="La Verne Attorney" style="text-decoration: none;"> La Verne</a></li>
			<li><a href="http://www.wefight4you.com/lawyer/mira-loma.aspx" title="Mira Loma Attorney" style="text-decoration: none;"> Mira Loma</a></li>
			<li><a href="http://www.wefight4you.com/lawyer/montclair.aspx" title="lawyer montclair" style="text-decoration: none;">Montclair</a></li> 
			<li><a href="http://www.wefight4you.com/lawyer/ontario.aspx" title="lawyer ontario" style="text-decoration: none;">Ontario</a></li> 
			<li><a href="http://www.wefight4you.com/lawyer/pomona.aspx" title="lawyer pomona" style="text-decoration: none;">Pomona</a></li>
			<li><a href="http://www.wefight4you.com/lawyer/rancho-cucamonga.aspx" title="lawyer rancho cucamonga" style="text-decoration: none;">Rancho Cucamonga</a></li>
			<li><a href="http://www.wefight4you.com/lawyer/riverside.aspx" title="lawyer riverside" style="text-decoration: none;">Riverside</a></li>
			<li><a href="http://www.wefight4you.com/lawyer/san-bernardino.aspx" title="San Bernardino Attorneys" style="text-decoration: none;">San Bernardino</a></li> 
			<li><a href="http://www.wefight4you.com/lawyer/san-dimas.aspx" title="San Dimas Attorney" style="text-decoration: none;"> San Dimas</a></li>
			<li><a href="http://www.wefight4you.com/lawyer/upland.aspx" title="lawyer upland" style="border-bottom-width: 0; text-decoration: none;">Upland</a></li>
</ul>
</div>
</div>
<p> However, the legal services we provide go much deeper than that. We look beyond the fight to see that all of your needs are met. We are committed to providing smart, honest legal representation that is truly right for you. The benefit of hiring the Law Offices of Marc Grossman is, instead of hiring only one attorney, you hire a 
    <a href="http://www.wefight4you.com/attorneys/Default.aspx" title="attorneys">team of attorneys</a> 
        with different specialties that are ready to fight for your rights!</p>
<p>Our Law Firm has recovered millions for our clients and continues to achieve 
    substantial settlements in all areas of Law. Our attorney&#39;s are not only 
    aggressive in the court room, but they are sympathetic to your needs. Once you 
    come into our office you&#39;ll feel right at home. We pride ourselves on high 
    standards of customer service while providing honest representation.</p>
<div class="callOut">
  <h2 style="text-align:center"><a name="bankruptcy_attorneys">Bankruptcy Attorney Legal Services</a></h2>
<a href="http://www.wefight4you.com/bankruptcy/Default.aspx" title="Filing for Bankruptcy">
<img src="http://www.wefight4you.com/images/bankruptcy-questionsmall.jpg" 
        align="left" alt="Questions about filing Bankruptcy?" style="padding-right: 5px;"/></a><p>Our <a href="http://www.wefight4you.com/bankruptcy/Default.aspx" title="California Bankruptcy Attorneys">bankruptcy attorneys</a> are a team of technically savvy individuals who have the experience 
    and legal knowledge to solve your financial problems. We primarily handle 
    <a href="http://www.wefight4you.com/bankruptcy/chapter-7.aspx" title="Chapter 7 Bankruptcy"> 
    Chapter 7 Bankruptcy</a> cases, but we&#39;re willing to take on 
    <a href="http://www.wefight4you.com/bankruptcy/chapter-13.aspx" title="Chapter 13 bankruptcy">Chapter 13 
    bankruptcy</a>. Since we offer free 
    initial consultations we'll either handle your case or we will recommend a 
	different Law Firm that specializes in your type of bankruptcy.</p>
    <h3>How much does the bankruptcy cost and how long does it take?</h3>
    <p>It depends upon the complexity of the case. Usually simple Chapter 7 bankruptcy's start off at a
        <b>thousand dollars for the attorney fees</b>. The length of a bankruptcy ranges, for 
        example A Chapter 7 case can take longer, if we have to apply for a modification or 
        reaffirmation agreement. </p>   
    <h3> Can I file for bankruptcy?</h3>
    <p><i>&quot;I got into a bad situation but I'm a good person. Medical, Family Issues, or the lender is not working with you.&quot;
        </i>If this sounds like your situation, speak to our bankruptcy attorneys for free.</p> 
</div>
        <h2 style="text-align:center"><a name="divorce_attorneys">Divorce Attorney Services</a></h2>
    <a href="http://www.wefight4you.com/divorce/Default.aspx" title="Divorce">
<img src="http://www.wefight4you.com/images/divorce-questionsmall.jpg" 
        align="left" alt="Questions about Divorce?" style="padding-right: 5px;"/></a><p>For the past 11 years our <a href="http://www.wefight4you.com/lawyer/upland.aspx" title="Upland Divorce Attorney">Upland divorce attorneys</a> have been helping people solve problems that are overwhelming to them. When you schedule a free consultation, we sit with you to understand the type of problems that you are facing with your divorce. Whether the right solution is hiring a attorney or not hiring a attorney, saving your money for another day or even just thinking about things for awhile the Law Offices 
        of Marc Grossman will help you 
	decide what the best decision is for you. We never make decisions for you! We use our experience and best 
	judgment to help you make the best decisions for yourself.</p>
<h3>Whether it's a simple or complicated Divorce we're on your side</h3>
<p>We provide a different a wide variety of different divorce services. We can assist the person who has limited funds in a simple divorce to the six figure multiple business dispute custody case nightmare. You don't know if you don't call, you'll be better off coming in 
    for a free initial consultation so that you can sit down with us so that we can go over your 
    situation.</p>
<div class="callOut">
<h2 style="text-align:center"><a name="workers_compensation">California Workers Compensation Service</a></h2>
<p>If you are experiencing pain from work you may be entitled for 
   <strong><a href="http://www.wefight4you.com/workers-compensation/Default.aspx" title="Workers Compensation">workers compensation</a></strong>. Most people are not aware that they qualify for benefits 
    nor what the law allows for workers comp claims. Does your back hurt from years of lifting? Or how about carpel tunnel in your wrists from working on a computer all day? </p>
    <p>Our Workers Compensation 
    reps have over 20 years of experience. </p>
    <p>We have helped thousands of injured workers suffering from a 
 work injury
        get the compensation they deserve. We offer free initial consultations to see if 
        you have a case. </p> 
    </div>
          <h2 style="text-align:center"><a name="dui_attorneys">DUI Attorney Legal Services</a></h2>
    <a href="http://www.wefight4you.com/criminal-defense/dui.aspx" title="DUI Attorney">
<img src="http://www.wefight4you.com/images/dui.jpg" 
        align="left" alt="DUI" style="padding-right: 5px;"/></a><p>Being charged with any crime such as <strong>DUI</strong> is a serious matter. Even seemingly small 
        DUI convictions can have drastic effects on your career and life. If you are charged with a crime you need a 
      <a href="http://www.wefight4you.com/criminal-defense/dui.aspx">DUI defense attorney</a> to protect your rights. At the Law Offices of Marc E. Grossman, we have a team of qualified 
        DUI lawyers who understand the intricacies of the criminal law system that will 
        stand up for your rights.</p>
   <h3>Generations of Lawyers Serving Upland, California</h3>
<p>Our Upland law firm is tightly rooted in the community. Our lead attorney, Marc E. Grossman, comes from a family of attorneys. Like his father, brother and wife, he holds the service he and his firm provide to a very high standard.</p>
<p>To find out more about what we can do to help you through any legal challenge you are faced with, call us toll free at 1-888-407-9068.</p>

    </asp:Content>
