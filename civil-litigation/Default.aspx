﻿<%@ Page Title="Civil and Business Litigation Attorneys, Lawyers | Law Offices of Marc Grossman" Language="C#" MasterPageFile="~/MasterPages/SiteMainInternal.master" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="civil_litigation_Default" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
<meta name="keywords" content="lawyer, attorney, law firm, law office, legal advice, business litigation, real estate litigation, commercial, civil litigation, tort, economic litigation, lawsuit, dispute, lawyer, attorney,">
<meta name="description" content="Do you need an experienced attorney to handle your civil or business litigation case? Call 1-866-493-6671 for a free consultation with an Upland, California lawyer at the Law Office of Marc E. Grossman.">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainLogo" Runat="Server">
<a href="http://www.wefight4you.com/" title="Civil Litigation Attorneys">
    <img src="../images/teamofattorneys2.jpg" alt="Civil and Business Litigation Attorneys" style="width: 800px; height: 244px" /></a>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="PageTitleH1" Runat="Server">
<h1 id="pageTitle">Civil Litigation Trial Attorneys</h1>
<h2 style="text-align:center">Because winning counts</h2>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="MainContent" Runat="Server">
<h2>Civil and Business Litigation</h2>
<p>At the Law Office of Marc E. Grossman, we can handle all civil litigation matters, including business disputes as well as those between individuals. We know that when you enlist an attorney, you are making an investment. We want you to know that we are well worth our reasonable fees because we believe in working hard to get you results. We strive to make certain you leave our office feeling your money was well spent.</p>
<h3>Free initial consultations are available</h3>
<p><a href="/Forms/contact.aspx" rel="nofollow">E-mail us</a> or call us toll free at 1-866-493-6671 to discuss your case with an experienced civil and business litigation lawyer.</p>
<p>Our representation starts at the initial consultation. We are prepared to spend time with you, learning about your situation. We will go over the various options you have at your disposal and get you started on the right path.</p>
<h3>Smart, honest business and civil litigation representation</h3>
<p>We are available to handle any type of litigation matter for any business or individual in Upland, California, or elsewhere in the Inland Empire.</p>
<p><a href="/Firm-Overview/">The Law Office of Marc E. Grossman</a> can handle a wide range of business and commercial litigation matters, including contract disputes, shareholder disputes, and much more.</p>
<p>We can address any type of dispute that arises between individuals, including:</p>

<ul>
   <li>Real Estate Litigation</li>
   <li>Disputes Over Property</li>
   <li>Title Disputes</li>
   <li>False Arrest</li>
   <li>Construction Disputes</li>
</ul>

<p>We can also handle all manners of <a href="../personal-injury/Default.aspx" title="personal injury">personal injury</a>, <a href="../wrongful-death/Default.aspx">wrongful death</a>, <a href="../insurance-bad-faith/Default.aspx" title="insurance bad faith">insurance disputes</a>, employment discrimination lawsuits and much more.</p>
<p>No matter what your civil or business litigation matter involves, we will stand by your side to see you through it. Whether you are the one bringing a lawsuit against another party or you have been threatened with a lawsuit, we are here for you.</p>
<p>For a free initial consultation with a dedicated business and civil litigation attorney, <a href="/Forms/contact.aspx" rel="nofollow">e-mail us</a> or call us toll free at 1-866-493-6671.</p>
</asp:Content>

