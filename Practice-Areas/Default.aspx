﻿<%@ Page Title="California Bankruptcy, Divorce, Wrongful Death, Criminal Defense, Family Law, Civil Litigation, Attorney, Lawyer" Language="C#" MasterPageFile="~/MasterPages/SiteMainInternal.master" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="Practice_Areas_Default" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
<meta name="keywords" content="lawyer, attorney, law firm, law office, legal advice, lawsuit, injury, criminal, employment, fire insurance, lawyer, attorney,">
<meta name="description" content="Do you need an experienced attorney to handle your lawsuit? Call 1-888-407-9068 for a free consultation with an Upland, California lawyer at the Law Office of Marc E. Grossman.">
<meta name="abstract" content="Do you need an experienced attorney to handle your lawsuit? Call 1-888-407-9068 for a free consultation with an Upland, California lawyer at the Law Office of Marc E. Grossman.">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainLogo" Runat="Server">
<a href="http://www.wefight4you.com/" title="California Bankruptcy, Divorce, Criminal Defense Attorney">
    <img src="../images/teamofattorneys2.jpg" title="California Bankruptcy, Divorce, Criminal Defense Attorney" style="width: 800px; height: 244px"/></a>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="PageTitleH1" Runat="Server">
<h1 id="pageTitle">Practice Areas</h1>
<h2 style="text-align:center">California Bankruptcy, Divorce, Criminal Defense</h2>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="MainContent" Runat="Server">
 <h3>Trusted Representation in Any Legal Matter</h3>
<p>At the <a href="http://www.wefight4you.com/Firm-Overview/Default.aspx" title="Law Offices of Marc Grossman">Law Offices of Marc Grossman</a>, we are dedicated to serving our clients effectively. Our goal is to do what is right for you whether you're <a href="http://www.wefight4you.com/bankruptcy/Default.aspx" title="filing for bankruptcy">interesting in filing for bankruptcy</a>. We are ready to fight when a fight is called for. We are ready to seek out alternative options when they are called for. Because of our commitment to personalized representation, we take the time to explain the options that you have available. We want you to turn to us any time you come up against a legal challenge.</p>
<p>At <a href="http://www.wefight4you.com/Firm-Overview/Default.aspx">the Law Office of Marc E. Grossman</a>,  we want you to be happy with the outcome of your case.</p>
<h3>Free initial consultations are available</h3>
<p><a href="http://www.wefight4you.com/Forms/Contact.aspx">E-mail us</a> or call us toll free at 1-888-407-9068 to schedule a meeting with an experienced attorney.</p>
<p>When you come to us for your consultation, you will have our full attention. We will be ready to spend a significant amount of time with you in order to understand your case. We want to explain your options and get you started down the right path to the resolution you want.</p>
<h3>We will stand by your side in any legal matter</h3>
<p>Our law firm is available to stand by your side in any type of lawsuit, including cases involving:</p>

<ul>
   <li><strong><a href="http://www.wefight4you.com/family-law/Default.aspx" title="Family Law Attorney">Family Law</a></strong>: Divorce, Child Custody, Child Support, Property Division</li>
   <li><strong><a href="http://www.wefight4you.com/personal-injury/Default.aspx" title="Personal Injury">Personal Injury</a></strong>: <a href="http://www.wefight4you.com/wrongful-death/Default.aspx" title="wrongful death">Wrongful Death</a>, Car Accidents, <a href="http://www.wefight4you.com/motorcycle-accidents/Default.aspx" title="motorcycle accidents>Motorcycle Accidents</a>, Truck Accidents, <a href="http://www.wefight4you.com/dog-bites/Default.aspx" title="dog bite attorney">Dog Bites</a>, Premises Liability and More</li>
   <li><strong><a href="http://www.wefight4you.com/workers-compensation/Default.aspx" title="workers compensation attorney">Workers&#8217; Compensation</a></strong>: Industrial Workplace Accidents, Construction Accidents and More</li>
   <li><strong><a href="http://www.wefight4you.com/criminal-defense/Default.aspx" title="criminal defense attorney">Criminal Defense</a></strong>: Felonies, Misdemeanors, DUI/DWI, Assault and More</li>
   <li><strong><a href="http://www.wefight4you.com/immigration/Default.aspx" title="immigration attorney">Immigration and Naturalization</a></strong>: Green Cards, Employee Visas and More</li>
   <li><strong><a href="http://www.wefight4you.com/insurance-bad-faith/Default.aspx" title="insurance bad faith attorney">Insurance Bad Faith</a></strong>: Car Insurance Bad Faith, <a href="http://www.wefight4you.com/fire-insurance-claims/">Fire Damage Claims</a>, Fire Insurance Bad Faith, <a href="http://www.wefight4you.com/fire-damage-homes/Default.aspx">Fire Damage to Homes</a> and More</li>
   <li><strong><a href="http://www.wefight4you.com/bankruptcy/Default.aspx">Bankruptcy</a></strong>: Chapter 7 Bankruptcy, Chapter 13 Bankruptcy</li>
   <li><strong><a href="http://www.wefight4you.com/discrimination-harrassment/Default.aspx">Employment Discrimination and Harassment</a></strong>: Sexual Harassment, Gender Discrimination, Racial Discrimination, Wrongful Termination</li>
   <li><strong><a href="http://www.wefight4you.com/civil-litigation/Default.aspx">Civil Litigation</a></strong>: Business Disputes, Real Estate Disputes and More</li>
   <li><strong><a href="http://www.wefight4you.com/corrections-prison-parole/Default.aspx">Corrections, Prison and Parole Law</a></strong>: Appeals, Habeas Corpus and More</li>
</ul>

<p>We are available to offer our trusted services to people requiring a <a href="http://www.wefight4you.com/lawyer/upland.aspx" title="Upland Lawyer'>lawyer in Upland, California</a> and throughout the Inland Empire.</p>
<p>For a free initial consultation about your lawsuit or other legal matter, <a href="http://www.wefight4you.com/Forms/Contact.aspx" rel="nofollow">e-mail us</a> or call us toll free at 1-888-407-9068.</p>
</asp:Content>

