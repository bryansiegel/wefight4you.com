<%@ Page Title="California Insurance Bad Faith Lawyer | Riverside County, San Bernardino, insurance Claim Denial" Language="C#" MasterPageFile="~/MasterPages/SiteMainInternal.master" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="insurance_bad_faith_Default" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
<meta name="description" content="California Insurance Bad Faith Lawyer within the Inland Empire that can assist you with insurance bad faith claims in California. FREE Initial Consultation.">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainLogo" Runat="Server">
<a href="http://www.wefight4you.com" title="California insurance bad faith attorney">
    <img src="../images/teamofattorneys2.jpg" alt="insurance bad faith lawyer" height="244px" width="800px"/></a>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="PageTitleH1" Runat="Server">
<h1 id="pageTitle">California Insurance Bad Faith Lawyer</h1>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="MainContent" Runat="Server">
<h2>Was your insurance claim denied in California? Speak to an California insurance bad faith lawyer with 
    over 20 years of experience</h2>
<h3 style="text-align:center">What is �Insurance Bad Faith?� </h3>
    <p>California Insurance bad faith is a legal cause of action that holds an insurance company 
        in California to a higher standard of care to a policyholder or insured.  It recognizes the inherently 
        unequal bargaining position between the parties to the contract.  It requires that an insurance company give as much consideration to the interests of the insured as it gives to its own interests.  It requires an insurer to deal fairly in its communications and negotiations with its insured.  Although based upon the insurance contract, violation of the dictates of good faith subjects an insurer to tort damages (the various damages listed above).</p>
    <ul>
    <li style="font-style:italic; padding-bottom: 20px; color:#78b7d0;"><strong>Has your health insurance company refused to pay a claim for medical treatment ordered by your doctor in California?</strong></li>
    <li style="font-style:italic; padding-bottom: 20px; color:#78b7d0;"><strong>Has your homeowner�s carrier denied or delayed payment of a claim for damage to your house caused by fire, or flood, or a storm, or for related additional living expenses?</strong></li>
    <li style="font-style:italic; padding-bottom: 20px; color:#78b7d0;"><strong>Has your California automobile insurance company denied or delayed payment of a claim following an accident involving your vehicle?</strong></li>
    <li style="font-style:italic; padding-bottom: 20px; color:#78b7d0;"><strong>Has a disability or life insurance company failed or refused to pay a claim for disability or lost income, or following the death of a named insured?</strong></li>
    <li style="font-style:italic; padding-bottom: 20px; color:#78b7d0;"><strong>Has an insurer cancelled or rescinded your insurance policy for an improper reason, or following a now-familiar practice by carriers called �post-claim underwriting� (the assessment or evaluation of the basic risk involved in writing the policy only AFTER a claim has occurred)?</strong></li>
    </ul>
   
    <h3><strong>If the answer to any of these questions is yes, you may have a �bad faith� claim and NEED REPRESENTATION.  The Law Offices of Marc E. Grossman can provide that needed representation.</strong></h3>
    <p><strong>�Insurance.�</strong>  Just the mention of the word makes many people cringe.  Many of us hate dealing with insurance companies.  Many of us believe that you can�t �fight city hall,� or that an insurance company will simply �paper you to death� if you file and pursue an insurance claim.  The fact is, you CAN fight the insurance companies.  You just need the right army and weapons.  At the Law Office of Marc E. Grossman, we have those.�Insurance.�  Just the mention of the word makes many people cringe.  Many of us hate dealing with insurance companies.  Many of us believe that you can�t �fight city hall,� or that an insurance company will simply �paper you to death� if you file and pursue an insurance claim.  The fact is, you CAN fight the insurance companies.  You just need the right army and weapons.  At the Law Office of Marc E. Grossman, we have those.</p>
    <p>Many times people have paid insurance premiums for years and years to protect themselves against a catastrophic problem - a storm damaged house, a nasty auto accident, disability or loss of income, or the death of a loved one - only to have their insurance company deny or delay payment of a legitimate claim.  All too frequently insurance companies don't pay all of the benefits that are owed under their contracts.  When the Law Offices of Marc Grossman gets involved in these cases we aim to get the client all the benefits they are owed from the insurance company.  Further, if the insurance company has acted wrongfully � e.g., in �bad faith� - there are additional damages that can be obtained:</p>
    <h3>Consequential damages</h3>
    <p>If you have suffered due to the insurer�s claim denial in California you might be entitled to consequential damages. For example if your health has been further damaged due to the denial of your health insurance claim, or wages have been lost due to the denial of your auto claim, or you have incurred out-of-pocket expenses due to additional living expenses when your home has been damaged by a storm, you may be entitled to reimbursement.</p>
    <h3>Emotional Distress damages</h3>
    <p>The law recognizes the special nature of an insurance contract.  In many instances, if the insurer has denied a 
        California insurance claim unreasonably, you may be entitled to recoup damages for mental and emotional distress.  Worry over bills, having to borrow money from family and friends, running through your credit, sleeplessness, tortured nights wondering where you will get the resources to enable you to go on with your life in a normal fashion; these are what emotional distress damages are meant to redress.</p>
    <h3>Punitive damages</h3>
    <p>Also called exemplary damages, these may be awarded by a judge or jury if the insurer has acted unreasonably, in bad faith or in a �despicable� manner.  While not often awarded
(and a showing must be made of oppression, fraud or malice in the conduct of the carrier), punitive damages are available in a bad faith cause of action.  Stories abound concerning these life-changing damages which an insured might recover. </p>
    <p>Insurance companies in California are powerful businesses. All too often, their power allows them to take advantage of their customers. They put their profit margins ahead of the needs of the people they are supposed to serve. At the Law Office of Marc E. Grossman, we will fight to see that insurance companies fulfill the promises they have made to their customers. Our 
        California insurance bad faith lawyers goal is for you to receive the treatment you deserve from your car insurance, home insurance or other insurance company.</p>
<p style="text-align:center">

</p>

<h3>We offer free initial consultations</h3>
<p><a href="/Forms/contact.aspx" rel="nofollow">E-mail us</a> or call us toll free at 1-888-407-9068 to schedule a meeting with a trusted insurance bad faith attorney.</p>
<p>During your initial consultation, we will take the time to learn about how your insurance company has failed to meet your needs. We will educate you about the law. We will let you know what options are available to you to pursue your insurance bad faith claim.</p>
<h3>Smart, honest insurance bad faith representation</h3>
<p>At <a href="/Firm-Overview/">Our Insurance Bad Faith law firm</a>, we are 
    available to serve people in within San Bernardino County and most of Southern 
    California.</p>
<p>We can handle all cases involving denial of legitimate insurance claims. Among the topics we can address are:</p>

<ul>
	<li>Homeowner's Insurance</li>
	<li>Life Insurance and Annuities</li>
	<li>Commercial/ Business Insurance</li>
	<li>Medical Insurance (HMO/PPO)</li>
	<li>Car Insurance</li>
	<li><a href="http://www.wefight4you.com/fire-insurance-claims/Default.aspx" title="fire insurance claims">Fire Insurance Claims</a></li>
	<li><a href="http://www.wefight4you.com/fire-damage-homes/Default.aspx" title="fire damage to homes">Fire Damage to Homes</a></li>
</ul>

<p>For more about our bad faith insurance practice, please visit our insurance law website.</p>
<p>In addition, you can learn more at our <a href="/insurance-bad-faith/Default.aspx" title="Insurance Bad Faith" >Insurance Bad Faith Information Center</a>.</p>
<p>You may be wondering what insurance bad faith means. Insurance bad faith is when you make all of your insurance payments, trusting that the company will cover your needs if something goes wrong. Then, when something does go wrong, the company unjustifiably denies the coverage you have paid for, either by offering an unreasonable interpretation of the policy or finding some other spurious reason to do so. We do not believe this is right.</p>
<p>For a free initial consultation with a dedicated insurance bad faith lawyer, <a href="/Forms/contact.aspx" rel="nofollow">e-mail us</a> or call us toll free at 1-888-407-9068.</p>
</asp:Content>

