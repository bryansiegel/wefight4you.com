<%@ Page Title="" Language="C#" MasterPageFile="~/Master/Attorney.master" AutoEventWireup="true" CodeFile="lucy-matevosyan.aspx.cs" Inherits="attorneys_Default" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="headerH1" Runat="Server">
    <h1>Lucy Matevosyan - Family Law Case Manager</h1>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainH1" Runat="Server">
    <h2>Lucy Matevosyan, Family Law Case Manager</h2>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="AttorneyVideo" Runat="Server">

    <img src="~/IMAGES/people/lucy.jpg" runat="server" />


</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="AttorneyQuickFacts" Runat="Server">
    <h3>Quick Facts</h3>
<p><strong>Practice Areas:</strong> Family Law, Divorce, Child Custody, Child Support.</p>
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="AttorneyMainContent" Runat="Server">
    <h2>Lucy Matevosyan Bio</h2>
<p>Lucy is our Family Law Case Manager. Lucy has been with the Law Offices of Marc Grossman for over a year and has constantly provided our clients with exceptional family law services. Lucy�s strength is in her passion for the law and helping her clients. Lucy is protective of her clients and does her best to meet their needs.  With Lucy on your side, you know for a fact that there is someone that will listen to you and fight for you.</p>
<p>Lucy is a very important part of the family law department at the Law Offices of Marc Grossman, where she constantly proves her ability as a family law case manager.		
To speak with Lucy regarding your family law related issues, contact her at the Law Offices of Marc Grossman today.</p>
<p>Did you know that we offer FREE INITIAL CONSULTATIONS? Call us today to set up your appointment.</p>
</asp:Content>

<asp:Content ID="Content7" runat="server" contentplaceholderid="attorneyImage">

    &nbsp;
                
</asp:Content>


<asp:Content ID="Content8" runat="server" 
    contentplaceholderid="SocialProfileTitle">

Contact Us

</asp:Content>



<asp:Content ID="Content9" runat="server" 
    contentplaceholderid="AttorneySocialProfile">

                <h1 style="text-align: center">
                    888-407-9068</h1>

</asp:Content>




