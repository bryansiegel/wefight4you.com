﻿<%@ Page Title="Law Office Internship Upland, San Bernardino County, Inland Empire - Law Offices of Marc Grossman" Language="C#" MasterPageFile="~/MasterPages/SiteMainInternal.master" AutoEventWireup="true" CodeFile="internships.aspx.cs" Inherits="Firm_Overview_internships" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
<meta name="keywords" content="lawyer, attorney, law firm, law office, legal advice, criminal, personal injury, employment, fire insurance, lawyer, attorney,">
<meta name="description" content="Law Students in Riverside, Upland, San Bernardino can apply for an Internship at the Law Offices of Marc Grossman. Here you will learn real world experience and find out what it's like to be a real world attorney at Law.">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainLogo" Runat="Server">
    <img src="../images/i-default.jpg" style="width: 802px; height: 228px" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="PageTitleH1" Runat="Server">
<<h1 id="pageTitle">Law Office Internships Upland CA</h1>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="MainContent" Runat="Server">
<p>Are you a Law Student in need of real world experience in a busy Law Office? There is no better place to learn the ropes that at the Law Offices of Marc Grossman. Conveniently located off the 10 fwy on N. Mountain Ave. Our offices are close to resturants and local entertainment. If you are practing law within a University and want to see if you're cut out to be an attorney at law there is no better place to learn that at our office.</p>
<h3>If you decide to intern at our office you'll recieve</h3>
<ul>
<li>Your own workspace</li>
<li>Computer</li>
<li>Real World Experience</li>
<li>Court Room field trips</li>
<li>You'll learn the ins and outs of the Law</li>
<li>you get to work with other passionate attorneys and office staff</li>
<li>Great office environment</li>
</ul>
<p>Internship is a non paid position where we train you. You may work flexible hours from Monday to Friday 8:30 a.m to 5:30 p.m. WE ARE NOT ACCEPTING APPLICATIONS FOR ATTORNEYS 
    OR OFFICE STAFF.</p>
</asp:Content>

