<%@ Page Title="Office Staff - The Law Offices of Marc Grossman - Wefight4you.com" Language="C#" MasterPageFile="~/MasterPages/SiteMainInternal.master" AutoEventWireup="true" CodeFile="office-staff.aspx.cs" Inherits="Firm_Overview_office_staff" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
<meta name="description" content="If you are in need of a Chino Hills attorney contact the divorce lawyer in Chino Hills that has the experience that you need. No matter what the case our family law attorneys can help you solve your legal matters. Contact the Law Offices of Marc Grossman today for a free initial consultation. "/>
<meta name="keywords" content="lawyer, attorney, law firm, law office, legal advice, litigation, civil litigation, criminal defense, lawyer, attorney,"/>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainLogo" Runat="Server">
<img src="../images/i-prac-civillitigation.jpg" style="width: 802px; height: 228px" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="PageTitleH1" Runat="Server">
<h1 id="pageTitle">Office Staff</h1>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="MainContent" Runat="Server">
<p>No good company is without their employees. At the Law Offices of Marc Grossman we have a wide-selection of talented individuals that take care of everyday operations ranging from answering the telephones to fixing our computers. We are a family-friendly Law Office that holds many company functions from birthday parties to company lunches. We believe in providing a healthy work environment where our clients feel welcome as soon as they walk through our door.</p>
<p>Our coorporate office is located in Upland California, which is in the Inland 
    Empire or San Bernardino County. We have many office locations to serve you from 
    La Puente to Riverside County. Our Free initial consultations for most cases 
    usually take place in our coorporate office in Upland.</p>
<p>The employees that work for the Law Offices of Marc Grossman would like to thank you for visiting our website and hope that you choose us for legal representation.</p>
<p><b>If you would like to meet our attorneys please 
    <a href="../attorneys/Default.aspx" title="California Attorneys">click here</a>.</b></p>
<h3>Office Manager</h3>
<p>Punam Patel Grewal</p>
<h3>Director of Marketing/IT/Web Development</h3>
<p>Bryan Siegel<br /></p>

<h3>Family Law</h3>
<p>Svetlana Kauper</p>
<h3>Receptionist</h3>
<p>Heather Nuss</p>
<p>Arleen Stasik</p>
<h3>HR</h3>
<p>Amber Williams</p>
</asp:Content>