﻿<%@ Page Title="Firm Overview of the Attorneys in Upland, and the Law Offices of Marc Grossman - WeFight4you.com" Language="C#" MasterPageFile="~/MasterPages/SiteMainInternal.master" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="Firm_Overview_Default" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
<meta name="keywords" content="lawyer, attorney, law firm, law office, legal advice, criminal, personal injury, employment, fire insurance, lawyer, attorney,">
<meta name="description" content="Law Offices of Marc Grossman has attorneys in Upland, Riverside and the entire Inland Empire to assist you. Learn about our Law Firm and how we are here to assist you with your claim .">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainLogo" Runat="Server">
<a href="http://www.wefight4you.com" title="California Bankruptcy, divorce and workers compensation attorney">
<img src="http://www.wefight4you.com/images/teamofattorneys2.jpg" alt="Law Offices of Marc Grossman" style="width: 800px; height: 244px" /></a>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="PageTitleH1" Runat="Server">
<h1 id="pageTitle">Firm Overview</h1>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="MainContent" Runat="Server">
<h2>A Law Firm You Can Trust</h2>
<p>At the <a href="http://www.wefight4you.com/Firm-Overview/Default.aspx" title="Law Offices of Marc E. Grossman">Law Offices of Marc E. Grossman</a>, you will find real people who care about you. Upon arrival, you will find that our office is different from the traditional law office. It is a virtual museum of local and national history. You will see a wide range of valued items, including a briefcase belonging to Franklin D. Roosevelt and many treasured manuscripts.</p>
<p>Most importantly, you will be introduced to a <a href="http://www.wefight4you.com/lawyer/upland.aspx" title="Upland California Lawyer">Upland lawyer</a> who is ready to listen to you and provide smart, honest guidance about a full range of legal matters, from <a href="http://www.wefight4you.com/bankruptcy/Default.aspx">Bankruptcy</a>, <a href="http://www.wefight4you.com/criminal-defense/Default.aspx" title="criminal defense attorney">criminal defense</a> to <a href="http://www.wefight4you.com/civil-litigation/Default.aspx" title="civil litigation attorney">civil litigation</a>.</p>
<p>At our law firm, we care about getting you the results you deserve. We take pride in the reputation we have earned by doing the right thing for our clients. We take pride in the fact that ethics and honesty mean a lot to us. No matter what your particular legal problem is, we are here to do what is right for you.</p>
<h3>A reputation for results</h3>
<p>Our reputation has been recognized on both a local and national level. Our lead attorney, <a href="http://www.wefight4you.com/attorneys/marc-grossman.aspx" title="Marc Grossman">Marc E. Grossman</a>,  has handled countless challenging legal matters. His creative and effective techniques earned him a place on the LA Times Inland Valley Voice's list of the 66 most influential people in the region. He has appeared on CBS news and many local news programs. That is the level of trusted reputation you will have on your side when you choose our firm.</p>
<p>Our results make a difference. Over the years, we have had many published decisions. This is one of the ways our cutting-edge use of the law and the resulting outcomes become known in the legal community and beyond.</p>
<p>We take pride in our community and our place in it. Our lead attorney, Marc E. Grossman, has lived in Upland, California, all of his life. He has practiced law here throughout his career. His father, brother and wife all practice law here, as well. When you call us, you will be talking to a firm with strong roots in the community.</p>
<p>We are available to guide you through a full range of legal matters, including criminal defense, personal injury, employment discrimination, fire insurance bad faith and more.</p>
<p>To learn more about us and to let us learn more about you, please <a href="/Forms/contact.aspx" rel="nofollow">e-mail us</a> or call us toll free at 1-888-407-9068 to schedule a free initial consultation.
</p>
<p>If you would like to know more about our <a href="office-staff.aspx">Office Staff
    </a>please <a href="office-staff.aspx">click here</a>. If you are a law student 
    and would like a <a href="internships.aspx">law office internship</a> please
    <a href="internships.aspx">click here</a>.</p>
</asp:Content>

