<%@ Page Title="Inland Empire Estate Planning Attorney, Attorneys, Lawyer, Lawyers" Language="C#" MasterPageFile="~/MasterPages/WorkComp.master" AutoEventWireup="true" CodeFile="estate-planning.aspx.cs" Inherits="estate_planning_Default" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainLogo" Runat="Server">
    <a href="http://www.wefight4you.com/" title="California workers compensation lawyers">
    <img src="../images/teamofattorneys2.jpg" alt="California Workers Compensation attorneys" height="244px" width="800px"/></a>
 
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="PageTitleH1" Runat="Server">
    <h1 id="pageTitle">Inland Empire Estate Planning Attorney</h1>

</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="MainContent" Runat="Server">

<p>Planning ahead. It’s the smart thing to do. And Estate Planning could be the smartest thing you do today.
It does not even matter if you have a big family or numerous assets - making a workable estate plan is an important issue to tackle, regardless of your age or health. A death, your death, can happen at any time - and so it is important to develop a plan that covers all your wishes before the unexpected occurs. An experienced Upland estate planning lawyer will help you develop that plan.</p>
<p>Good estate planning should cover all of the basics - your personal concerns, your financial concerns, and all of the other issues you believe in regarding the division of your property and other assets in the event of your untimely death. A qualified Upland estate planner will help you establish the groundwork of your plan, or modify your current and/or outdated plan. You need to be certain that your wishes will be carried out when that fateful day comes, and the time to get started is now.
Estate Planning requires complete and careful thought, so that you can rest assured that your wishes will be carried out to the letter, in the appropriate manner, and avoid disputes among beneficiaries or family members.</p>
<h3>FREE Initial Consultation</h3>
<p>The Law Offices of Marc Grossman and his team of experienced Estate Planning Attorneys can help. We know what is needed by law, and we know how to help you secure your wishes for the future of your family and beneficiaries. We eliminate the questionable or uncertain points in estate planning right from the start - and maximize your values in the long term by reducing expenses. And to back up this claim, we offer you a FREE Initial Consultation! Call today and schedule your first appointment. Estate Planning can seem overwhelming, so let us put your mind at ease with a FREE Initial Consultation today!</p>
<p>Proper Estate Planning with a qualified Upland attorney is key to protecting your plans and wishes. Without it, your assets may not be safely protected or distributed according to your wishes. Property, valuables, monies, life insurance and more may not be delivered to the people you want to receive them. Are you willing to take that gamble?</p>
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="VideoMain" Runat="Server">
<!--Video-->
<br />
<br />
</asp:Content>

