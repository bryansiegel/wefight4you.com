﻿<%@ Page Title="Discrimination Lawyer Riverside County, CA Sexual Harassment Attorney San Bernardino California" Language="C#" MasterPageFile="~/MasterPages/SiteMainInternal.master" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="discrimination_harassment_Default" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
<meta name="keywords" content="lawyer, attorney, law firm, law office, legal advice, discrimination, sexual harassment, racial discrimination, age discrimination, gender, sexual orientation, workplace, employment, litigation, lawsuit, workers' compensation, stress, constructive termination, wrongful termination, FEHA (Fair Employment and Housing Act), hostile work environment, quid pro quo, whistleblower, lawyer, attorney,">
<meta name="description" content="Do you need an experienced attorney to handle your discrimination case? Call 1-888-407-9068 for a free consultation with an Upland, California lawyer at the Law Office of Marc E. Grossman.">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainLogo" Runat="Server">
<a  href="http://www.wefight4you.com" title="Discrimination and Harassment attorney">
    <img src="../images/teamofattorneys2.jpg" alt="Discrimination and Harassment lawyer" style="width: 800px; height: 244px"/></a>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="PageTitleH1" Runat="Server">
<h1 id="pageTitle">Discrimination &amp; Harassment Attorneys</h1>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="MainContent" Runat="Server">
<h2>Discrimination, Harassment and Employment Discrimination Attorneys</h2>
<p>Have you been mistreated at work? Have you been the victim of some form of harassment or discrimination? You do not have to take it. At the Law Office of Marc E. Grossman, we are committed to holding employers accountable for the mistreatment of employees. We are committed to getting you compensation for the way you have been treated.</p>
<h3>We offer free initial consultations</h3>
<p><a href="/Forms/contact.aspx" rel="nofollow">E-mail us</a> or call us toll free at 1-888-407-9068 to schedule a meeting with a dedicated lawyer about employee discrimination or harassment.</p>
<p>Your initial consultation with us will be thorough and tailored to your needs. We will take the time to learn about how you have been wronged at work. We will let you know the options that you have and explain how we can help.</p>
<h3>Smart, honest employment law representation</h3>
<p><a href="/Firm-Overview/">Our law firm</a> is here to serve wronged employees in Upland, California, and throughout the Inland Empire.</p>
<p>We can handle a wide range of employment law matters, including:</p>
<ul>
   <li>FEHA Cases (Fair Employment and Housing Act Cases)</li>
   <li>Sexual Harassment</li>
   <li>Age Discrimination</li>
   <li>Gender Discrimination</li>
   <li>Racial Discrimination</li>
   <li>Constructive Termination</li>
   <li>Wrongful Termination</li>
   <li>Whistleblower Cases</li>
</ul>
<p>We are proud to be a part of this community and we are proud of the reputation we have earned here. Our lead attorney, Marc E. Grossman, comes from a family of lawyers, including his father, brother and wife. We know the legal community and know how to interact with the other lawyers, judges and everyone else who may be involved in your case. Above all, we know how to get results.</p>
<p>You can learn more at our <a href="/employment/">Employment Law Information Center</a>.</p>
<p>To find out more about how a dedicated lawyer can help you in your employment law or discrimination matter, <a href="/Forms/contact.aspx" rel="nofollow">e-mail us</a> or call us toll free at 1-888-407-9068 for a free initial consultation.</p>
</asp:Content>

