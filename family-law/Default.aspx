﻿<%@ Page Title="Family Law Lawyer - Divorce - Child Support - Law Offices of Marc Grossman " Language="C#" MasterPageFile="~/MasterPages/SiteVideoMain.master" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="family_law_Default" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
<meta name="description" content="Do you need an experienced attorney to handle your divorce case? The attorneys at the Law Offices can help you whether you need a divorce attorney or a child support.attorney">
	</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainLogo" Runat="Server">
     <table>
        <tr>
            <td valign="top" class="style2">
               <img src="../images/video-questions-divorce.jpg" /></td>
            <td valign="top">
                <object id="Object1" data="portal.swf" 
        style="width: 473px; height: 480px; float: right" 
        type="application/x-shockwave-flash">
		<param name="movie" value="portal.swf" />
</object></td>
        </tr>
        </table>
        
    </asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="PageTitleH1" Runat="Server">
    <h1 id="pageTitle">family Law Attorney that will fight for you</h1>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="MainContent" Runat="Server">
    <h2>Divorce - Child Support - Child Custody</h2>
<p>Your interests will be best served with an Upland<a href="Default.aspx"> family law attorney
    </a>who will work hard on your behalf. At the <a href="../Firm-Overview/Default.aspx">Law Office of Marc E. Grossman</a>, we are here to do that for you, in a way that applies our legal experience. We believe that meeting your needs starts by learning about your needs. The personal attention we provide means that we will always have the time to listen to you. We will strive to get you the results you deserve.</p>

<h3>We offer free initial consultations</h3>
<p><a href="/Forms/contact.aspx" rel="nofollow">E-mail us</a> or call us toll free at 1-888-407-9068 to schedule a meeting with an experienced family law and <a href="/divorce/Default.aspx">divorce attorney</a></p>
<p>Your initial consultation will be thorough and comprehensive. We will take the time to find out about your 
    <a href="../divorce/Default.aspx">divorce</a> or other family law case. We will offer guidance about the options you have available and the strengths and weaknesses of each.</p>
<h3>Smart, honest family law representation</h3>
<p>We are available to serve people in Upland, California and throughout the Inland Empire.</p>
<p><a href="/Firm-Overview/">Our team</a> is available to handle all family law matters, including:</p>

<ul>
   <li><a href="../divorce/Default.aspx">Divorce</a></li>
   <li>Child Support</li>

   <li>Child Custody and Visitation</li>
   <li>Alimony/Spousal Maintenance</li>
   <li>Property Distribution</li>
   <li>Modification and Enforcement of Decrees</li>
   <li>Adoption</li>
   <li>Paternity</li>

   <li>Property and Debt Disputes</li>
   <li>Domestic Violence and Restraining Orders</li>
   <li>Premarital Agreements/Prenuptial Agreements</li>
   <li>Mediation</li>
</ul>

<p>You want a <a href="../lawyer/upland.aspx">Upland lawyer</a> who will fight for you. We will do that and much more. We will get to know you. We will provide you with the personal attention need to help ensure that the path we take is the right one for you.</p>
<p>We have earned a well-earned reputation for success in representing our clients. Among the honors achieved by our lead attorney 
    in Upland, <a href="../attorneys/marc-grossman.aspx">Marc E. Grossman</a>, is inclusion on a list of the 66 most influential people in the region by the LA Times Inland Valley Voice. That is the level of trusted service that you will have on your side when you choose us to handle your divorce or family law case.</p>
<p>For a<a href="../Forms/Contact.aspx"> free initial consultation</a> about divorce or any other family law matter, <a href="/Forms/contact.aspx" rel="nofollow">e-mail us</a> or call us toll free at 1-888-407-9068.</p>
</asp:Content>