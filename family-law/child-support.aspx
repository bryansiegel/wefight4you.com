﻿<%@ Page Title="Child Support - Law Offices of Marc Grossman" Language="C#" MasterPageFile="~/Master/InteriorMain.master" AutoEventWireup="true" CodeFile="child-support.aspx.cs" Inherits="family_law_child_support" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
<meta name="description" content="If you are seeking Child Support due to a divorce consult with the attorneys at the Law Offices of Marc Grossman. We believe that children come first and we are lawyers that care about the children with your child support case." />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Mainh1" Runat="Server">
<h1>Child Support</h1>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainMasterh1" Runat="Server">
<h2>Facts about Child Support</h2>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="MainBodyContent" Runat="Server">
<p><b>Children Come First at Divorce</b><br />
At divorce, as in marriage, the best interests of the children are of paramount importance.  Children have the right to reasonable support.  This is something that the parents cannot waive or modify.  Here at the Law Offices of Marc E. Grossman we are all family men and women.  We are dedicated to ensuring that your children get the fair award of support they are entitled to by law.  
    <a href="../divorce/Default.aspx">Divorce</a> is hard enough on children without financial difficulties.  We will ensure that they get the fair award of support that they are entitled to.  
</p>
<p><b>Child Support Modification</b><br />
	If the awarded support is no longer fair because of changed circumstances our team of experienced family attorneys at The Law Offices of Marc E. Grossman, can help you to modify it.  We will advocate for balance between the needs of the children and hardship upon the parent paying the support.  Child support can usually be modified, regardless of agreements against it.
</p>
<p><b>Fair and Reasonable Child Support </b><br />
Courts determine a reasonable award of support based on the income of both parents.  So each parent is required to contribute in equal proportion to the support of their children.  The share of support will usually be based off of the parent’s income.  Sometimes it is based off of the parent’s earning potential.  In an emotionally charged divorce determining a fair award of support becomes difficult and these are the times when you and your children need the team of experienced family law 
    <a href="../attorneys/Default.aspx">attorneys at the Law Offices of Marc E. Grossman</a>.
</p>
<p><b>The Difference Between Earnings and Earning Capacity</b><br /><br />
<b>Earnings</b><br />
Earnings seem simple enough, but in the courts, things are never as simple as you would hope.  Earnings for child support are your net monthly disposable income.  Net monthly disposable income is the amount you are amount you are paid, after certain deductions.  The most common deductions are the amount of State and Federal Taxes actually paid, retirement benefit payments, work related expenses, and child support payments to other children.  There are also special hardship deductions that can be taken into account such as health problems, or the minimum living expenses of the children who live with the parent who would be paying child support.  
</p>
<p><b>Earning Capacity</b><br />
Net monthly disposable income this is the starting point for determining support awards, but sometimes this would not be fair because one of the parents is avoiding working.  When using net monthly disposable income would not be a fair way to calculate a support award, the court can base the payments on the parent’s earning potential. 
</p>
<p>For example it would not be fair if the children’s father is a doctor who could make $200,000 per year if he worked as a doctor instead he decides that he wants to be a tour guide for $20,000 per year.  In cases like this the court can award support based on that parent’s earning potential. </p>
<p>The same holds true for investment assets.  For instance if the fair rental value of a home is 2,000 per month, but the parent rents it for 500 per month, the court can count the 2000 as monthly income to compute the support payments.  Here at the law offices of Marc E. Grossman, our team of experienced family law attorneys will make sure your children get what they deserve.</p>

</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="Video" Runat="Server">
</asp:Content>

