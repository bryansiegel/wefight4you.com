﻿<%@ Page Title="Child Visitation Attorney, Lawyer" Language="VB" MasterPageFile="~/MasterPages/SiteVideoMain.master" AutoEventWireup="false" CodeFile="child-visitation.aspx.vb" Inherits="family_law_child_visitation" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
<meta name="description" content="Child Visitation Attorney that is on your side. Convenient locations in Upland, Chino Hills, Riverside, Ontario, Fontana and San Bernardino County. If you have a child visitation issue and wish to hire a lawyer contact us for a free intitial consultation." />
  <meta name="keywords" content="lawyer, attorney, law firm, law office, legal advice, divorce, family law, child custody, child support, spousal support, alimony, visitation, spousal maintenance, father's rights, property division, asset protection, paternity, modification, enforcement, lawyer, attorney," />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainLogo" Runat="Server">
<img src="../images/california-lawyers.jpg" alt="Child Custody" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="PageTitleH1" Runat="Server">
<h1 id="pageTitle">Child Visitation</h1>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="MainContent" Runat="Server">
<p>The Californian Court system has the right to determine child visitation rights. That's why it's important to hire an attorney. Did you know that even if you're not entitled for child custody, you can still recieve visitation rights? At the Law Offices of Marc Grossman we work with you so that you may have regular visitation rights.</p>
</asp:Content>

