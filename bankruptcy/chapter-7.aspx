﻿<%@ Page Title="California Chapter 7 Bankruptcy Attorney, Lawyer" Language="C#" MasterPageFile="~/MasterPages/Bankruptcy.master" AutoEventWireup="true" CodeFile="chapter-7.aspx.cs" Inherits="bankruptcy_chapter_7" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <meta name="keywords" content="lawyer, attorney, law firm, law office, legal advice, bankruptcy, Chapter 7, Chapter 13, save your home, real estate, house, personal bankruptcy, bankruptcy laws, lawyer, attorney,">
<meta name="description" content="Do you need an experienced Chapter 7 bankruptcy attorney to handle your chapter 7 bankruptcy case? Contact the Lawyers at the Law Offices of Marc Grossman for a free initial consulation.">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainLogo" Runat="Server">
    <p style="text-align:center">
<img src="chapter-7-bankruptcy.jpg" alt="Chapter 7 Bankruptcy" 
        style="width: 451px" />
        <a href="http://www.wefight4you.com/bankruptcy/bankruptcy-qualify.aspx" title="Free Bankruptcy Evaluation">
        <img id="Img1" src="~/images/onlinebankruptcyevalLarge.gif" 
        runat="server" style="height: 300px; width: 339px" /></a>
</p>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="PageTitleH1" Runat="Server">
    <h2 id="pageTitle">Chapter 7 Bankruptcy Attorneys</h2>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="MainContent" Runat="Server">
    <p>Chapter 7 is the liquidation chapter of the United States Bankruptcy Code.  They are the most common

<h3>Your Assets May be Protected</h3>

<p>California has opted “out” of the federal exemptions under § 522 of the US Bankruptcy Code. In California, there are two different sets of exemptions that attorneys may use to help clients protect their assets.</p>

<h3>What debts are not discharged with a Chapter 7 Bankruptcy</h3>

<p>Certain debts may not be discharged through chapter 7 bankruptcy.  The most common debt not discharged is student loans.  Student loans may be discharged if it may be shown that the debtor is suffering from “undue hardship.”  Other debts not dischargeable under chapter 7 bankruptcy are child or spousal support payments and some tax payments.</p>

<h3>How Much Will A Chapter 7 Bankruptcy Cost Me?</h3>

<p>Our bankruptcy attorney’s offer a free initial consultation.  During that consultation, our bankruptcy attorney will discuss your options and answer any questions you have relating to bankruptcy.  Our bankruptcy prices are competitive.  Let our experienced bankruptcy attorney help you file your bankruptcy case in Los Angeles, Riverside, or Santa Ana.</p>
</asp:Content>

<asp:Content ID="Content5" runat="server" contentplaceholderid="Toph1Page">
    Filing Chapter 7 Bankruptcy
</asp:Content>

<asp:Content ID="Content6" runat="server" contentplaceholderid="leftNav">
								<h2>Bankruptcy Information</h2>
<li><a href="/bankruptcy/Default.aspx" title="bankruptcy attorney" style="text-decoration: none;">Bankruptcy</a></li>
<li><a href="/bankruptcy/keeping-your-home.aspx" style="text-decoration: none;">Keeping Your Home</a></li>
<li><a href="/bankruptcy/will-you-loose-property.aspx" style="text-decoration: none;">Will You Loose Your Property</a></li>
<li><a href="/bankruptcy/bankruptcy-questions.aspx" style="text-decoration: none;">Top Bankruptcy Questions</a></li>
<li><a href="/bankruptcy/chapter-7.aspx" style="text-decoration: none;">Chapter 7 Bankruptcy</a></li>
<li><a href="/bankruptcy/chapter-11.aspx" style="text-decoration: none;">Chapter 11 Bankruptcy</a></li>
<li><a href="/bankruptcy/chapter-13.aspx" style="text-decoration: none;">Chapter 13 Bankruptcy</a></li>
</asp:Content>

<asp:Content ID="Content7" runat="server" contentplaceholderid="espanolNavigation">
					<li><a href="/espanol/quiebra.aspx" style="text-decoration: none;">Quiebra</a></li>
					<li><a href="/espanol/capitulo-7.aspx" style="text-decoration: none;">Capitulo 7</a></li>
					<li><a href="/espanol/capitulo-11.aspx" style="text-decoration: none;">Capitulo 11</a></li>
					<li><a href="/espanol/capitulo-13.aspx" style="text-decoration: none;">Capitulo 13</a></li>					
</asp:Content>

<asp:Content ID="Content8" runat="server" 
    contentplaceholderid="locationsNavigation">
      <li><a href="/lawyer/claremont.aspx" title="claremont Bankruptcy" style="text-decoration: none;">Claremont</a></li>
			<li><a href="/lawyer/chino.aspx" title="chino Bankruptcy" style="text-decoration: none;">Chino</a></li>
			<li><a href="/lawyer/chino-hills.aspx" title="chino hills Bankruptcy" style="text-decoration: none;">Chino Hills</a></li>
			<li><a href="/lawyer/corona.aspx" title="corona Bankruptcy" style="text-decoration: none;">Corona</a></li>
			<li><a href="/lawyer/fontana.aspx" title="fontana Bankruptcy"  style="text-decoration: none;">Fontana</a></li>		
			<li><a href="/lawyer/la-puente.aspx" title="La Puente Workers Bankruptcy" style="text-decoration: none;">La Puente</a></li>
			<li><a href="/lawyer/montclair.aspx" title="montclair Bankruptcy" style="text-decoration: none;">Montclair</a></li>
			<li><a href="/lawyer/ontario.aspx" title="ontario Bankruptcy" style="text-decoration: none;">Ontario</a></li>
			<li><a href="/lawyer/pomona.aspx" title="pomona Bankruptcy" style="text-decoration: none;">Pomona</a></li>
			<li><a href="/lawyer/rancho-cucamonga.aspx" title="rancho cucamonga Bankruptcy" style="text-decoration: none;">Rancho Cucamonga</a></li>
			<li><a href="/lawyer/riverside.aspx" title="riverside Bankruptcy" style="text-decoration: none;">Riverside</a></li>
			<li><a href="/lawyer/san-bernardino.aspx" title="San Bernardino Bankruptcy" style="text-decoration: none;">San Bernardino</a></li>
			<li><a href="/lawyer/upland.aspx" title="upland Bankruptcy" style="border-bottom-width: 0">Upland</a></li>
</asp:Content>

<asp:Content ID="Content9" runat="server" 
    contentplaceholderid="bottomfooterNav">
<p>The Law Offices of Marc Grossman are experience <a href="/">Chapter 7 Bankruptcy Attorneys</a> 
    Serving all of San Bernardino County. Contact us for a Free Initial 
    Consultation, where we may discuss your Chapter 7 Bankruptcy matters.</p>
</asp:Content>






