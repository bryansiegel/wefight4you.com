<%@ Page Title="Inland Empire Bankruptcy Attorney - File for Chapter 7, Chapter 11 and Chapter 13 Bankruptcy Lawyer" Language="C#" MasterPageFile="~/MasterPages/Bankruptcy.master" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="bankruptcy_Default" %>

<%@ Register Assembly="RssToolkit" Namespace="RssToolkit.Web.WebControls" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <meta name="description" content="Do you need an experienced bankruptcy attorney to handle your bankruptcy case? Call 1-888-407-9068 for a free consultation with an Upland, California lawyer at the Law Office of Marc E. Grossman." />
<meta name="keywords" content="lawyer, attorney, law firm, law office, legal advice, bankruptcy, Chapter 7, Chapter 13, save your home, real estate, house, personal bankruptcy, bankruptcy laws, lawyer, attorney," />
<link rel="canonical" href="http://www.wefight4you.com/bankruptcy/Default.aspx" />
    <style type="text/css">
    .style4
    {
        width: 408px;
    }
   
        .style5
        {
            height: 17px;
        }
   
        </style>
        
    </asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainLogo" Runat="Server">
     <img src="/images/799bankruptcy.jpg" />
    <h1 id="pageTitle">Are You Considering Bankruptcy in California?</h1>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="PageTitleH1" Runat="Server">
    <h3 id="pageTitle">Bankruptcy Lawyers that Can Help</h3>
<h2 style="text-align:center"><br>
  Bankruptcy Attorneys with 11 Years of serving San Bernardino County and 
    Riverside County</h2>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="MainContent" Runat="Server">
    <h3>So, You've considered filing for Bankruptcy</h3>
<p><strong>When it comes to <a href="http://www.wefight4you.com" title="filing for bankruptcy in California">filing for bankruptcy 
    in California </a>anyone can get into a financial bind.</strong> Sometimes 
    unforeseen events arise, events that cost money and you might not see a way out 
    of debt. These events could include <a href="http://www.wefight4you.com/divorce/Default.aspx" title="divorce">divorce</a>, medical bills, job loss or more. No 
    matter what has caused you to <b>find yourself in a difficult financial situation</b>, 
    the Law Office of Marc E. Grossman can help. We have helped many people just 
    like you file for bankruptcy.</p>
<p>If the above sounds like you then filing for bankruptcy is a good option for you. Right now you might be looking around the internet for 
    a bankruptcy attorney in order to see what your options are. While researching, you should know that you�re wasting valuable time. 
    <b>The process of bankruptcy takes a long time to process</b>. 
    <b><a href="http://www.wefight4you.com/Forms/Contact.aspx" rel="nofollow">Contact us for a FREE Initial Consultation</a></b>.</p>
    <p style="text-align:center">
		<a href="http://www.wefight4you.com/bankruptcy/bankruptcy-qualify.aspx" rel="nofollow">
        <img src="http://www.wefight4you.com/images/bankruptcy-qualify.jpg" border="0" 
            style="width: 388px; height: 215px" rel="nofollow"/></a></p>    
<h2>The Reality of Bankruptcy</h2>
<p>While bankruptcy might seem like the only option you should know what is bankruptcy before you consider filing a claim. The premise is to have a fresh start but, what you might not realize is that you may lose some assets along the way. Hiring an 
    attorney is a smart move because we do everything we can to help you keep your assets, such as your home or car and inform you of what your getting yourself into.</p>
    <h2>The Basics of Bankruptcy</h2>
<p>If you are considering bankruptcy this is a scary time for you. Whatever the case 
    you cannot pay your bills. You might be receiving calls from creditors that are 
    threatening, car repossession, foreclosures and even work garnishments. Before 
    your financial well-being dies you should understand what you might potentially 
    be in for. By no means is this the tell all guide to bankruptcy, but these 
    basics are meant as an introduction before you meet with one of our attorneys incase you are in the shopping around phase.</p>
    <h3>What Type of Bankruptcy Do I Qualify For?</h3>
    <p>There are six types of bankruptcy filings allowed under the Bankruptcy Code, however 
   Chapter 7, Chapter 11 and 
   Chapter 13 are the most important ones for consumers.  </p>    
    <div class="callOut">
    <p style="font-size:large; color:white; text-align:center">Most Common Types of Bankruptcy</p>
    
    <h3><a name="chapter-7-bankruptcy">Chapter 7 Bankruptcy</a></h3>
<p>While Chapter 7 is the most widely used section of the bankruptcy code, not all debts are liquidated under a Chapter 7 bankruptcy filing. If you are considering bankruptcy, chapter 7 is the first section that you should consider. <br />
    <a href="http://www.wefight4you.com/bankruptcy/chapter-7.aspx" title="Chapter 7 Bankruptcy">Chapter 7 
Bankruptcy</a></p>
<h3><a name="chapter-11-bankruptcy">Chapter 11 Bankruptcy</a></h3>
<p>Chapter 11 Bankruptcy Relief is used for business owners.  Under Chapter 11 Bankruptcy relief, as debtor, you would retain ownership of your assets, and maintain control of your business.<br />
    <a href="http://www.wefight4you.com/bankruptcy/chapter-11.aspx">Chapter 11 Bankruptcy</a></p>
<h3><a name="chapter-13-bankruptcy">Chapter 13 Bankruptcy</a></h3>
<p>In Chapter 13 Bankruptcy relief, as the debtor, you retain ownership and possession of all of your assets, but must devote some portion of your future income to repaying creditors. <br />
    <a href="http://www.wefight4you.com/bankruptcy/chapter-13.aspx" title="Chapter 13 Bankruptcy">Chapter 13 Bankruptcy</a></p>
</div>
<a href="http://www.wefight4you.com/Forms/contact.aspx" title="Click to stop harassing phone calls from creditors" rel="nofollow">
<img src="http://www.wefight4you.com/images/bankruptcy-keep-your-stuff.jpg" alt="Stop Creditor Harassment by fileing bankruptcy" border="0" 
        style="width: 388px; height: 215px" rel="nofollow"/></a>
<h3 style="text-align:center">Locations We Serve</h3>
<hr />
<div style="width:497px;">
<div style="width:240px; padding-right:8px; float:left;">
<li><a href="http://www.wefight4you.com/lawyer/alta-loma.aspx" title="Alta Loma Bankruptcy">Alta Loma Bankruptcy</a></li>
    <li><a href="http://www.wefight4you.com/lawyer/claremont.aspx" title="claremont Bankruptcy">Claremont Bankruptcy</a></li>
			<li><a href="http://www.wefight4you.com/lawyer/chino.aspx" title="chino Bankruptcy">Chino Bankruptcy</a></li>
			<li><a href="http://www.wefight4you.com/lawyer/chino-hills.aspx" title="chino hills Bankruptcy">Chino Hills Bankruptcy</a></li>
			<li><a href="http://www.wefight4you.com/lawyer/corona.aspx" title="corona Bankruptcy">Corona Bankruptcy</a></li>
			<li><a href="http://www.wefight4you.com/lawyer/diamond-bar.aspx" title="Diamond Bar Bankruptcy">Diamond Bar Bankruptcy</a></li>
			<li><a href="http://www.wefight4you.com/lawyer/fontana.aspx" title="fontana Bankruptcy">Fontana Bankruptcy</a></li>		
			<li><a href="http://www.wefight4you.com/lawyer/glendora.aspx" title="glendora Bankruptcy">Glendora Bankruptcy</a></li>		
			<li><a href="http://www.wefight4you.com/bankruptcy/la-puente.aspx" title="La Puente Bankruptcy">La Puente Bankruptcy</a></li>
</div>
<div style="width:240px; padding-left; float:right;">
<li><a href="http://www.wefight4you.com/lawyer/la-verne.aspx" title="La Verne Bankruptcy">La Verne Bankruptcy</a></li>
			<li><a href="http://www.wefight4you.com/lawyer/montclair.aspx" title="montclair Bankruptcy">Montclair Bankruptcy</a></li>
			<li><a href="http://www.wefight4you.com/lawyer/ontario.aspx" title="ontario Bankruptcy">Ontario Bankruptcy</a></li>
			<li><a href="http://www.wefight4you.com/lawyer/pomona.aspx" title="pomona Bankruptcy">Pomona Bankruptcy</a></li>
			<li><a href="http://www.wefight4you.com/lawyer/rancho-cucamonga.aspx" title="rancho cucamonga Bankruptcy">Rancho Cucamonga Bankruptcy</a></li>
			<li><a href="http://www.wefight4you.com/lawyer/riverside.aspx" title="riverside Bankruptcy">Riverside Bankruptcy</a></li>
			<li><a href="http://www.wefight4you.com/lawyer/san-bernardino.aspx" title="San Bernardino Bankruptcy">San Bernardino Bankruptcy</a></li>
			<li><a href="http://www.wefight4you.com/lawyer/upland.aspx" title="Upland Bankruptcy">Upland Bankruptcy</a></li>
</div>
</div>
<br class="clear" />
<br />
<h2>8 Steps of Filing for Bankruptcy</h2>
<p>Each bankruptcy case is different. Our FEES apply on a case by case basis, but feel assured we are very reasonable in price. What sets us apart from our competition is we don�t candy coat 
    your situation just to get your money. Bankruptcy is a serious matter and at the Law Offices of Marc Grossman you will receive ethical, honest representation on time every time.</p>
<h3>Filing for Bankruptcy with the Law Offices of Marc Grossman</h3>
<p>Below are the basics. To speed up the process make sure that you bring in all documentation from your creditors (bills, letters, etc) when you sit in for a free initial consultation. </p>
<h3>Step 1</h3>
<p>Call 800-123-4567 to setup a FREE Initial Consultation. Within that consultation we sit down and go over the amount of debts that you owe and how many creditors that you have. 
    <b>We try to convey what might be to come when you file for bankruptcy.</b></p>
<h3>Step 2</h3>
<p>If you decide to go through with the bankruptcy you will sign a flat fee retainer agreement. That fee will depend upon what is owed and the amount of creditors (your 
    attorney will go through the details within the free consultation).</p>
<h3>Step 4</h3>
<p>We obtain your credit report</p>
<h3>Step 5</h3>
<p>Once we have all/any statement and documents along with the credit report we prepare a petition for bankruptcy (your bankruptcy attorney will go over this with you).</p>
<h3>Step 6</h3>
<p>We file the petition for court.</p>
<h3>Step 7</h3>
<p>A �Meeting of the Creditors is then set up about 45 days from which the petition was received in which they are provided with representation from an attorney. In the meeting, a trustee is designated to the case to handle all property to try and give the creditors a little of what is owed (not to mention, usually if the income is so low, the creditors may walk away with nothing).</p>
<h3>Step 8</h3>
<p>After the meeting and disbursement of funds, if any, to creditors is done, the petitioner is then discharged of all their debt, thus placing them at zero to start all over again.</p>

<br class="clear"/>
<br />
<h2>Bankruptcy Law</h2>
<p>The United States Constitution gives Congress power to enact uniform laws on the subject of bankruptcies throughout the United States.�  This means that bankruptcy in the United States is a federal matter.  To file for bankruptcy, you must file in the United States Bankruptcy Courts.  There are some ancillary matters which are decided under state law, and will vary depending on which state you live in.</p>
<p>The Bankruptcy Abuse Prevention and Consumer Protection Act of 2005, drastically overhauled the Bankruptcy Code.  The 
BAPCPA was advocated by consumer lenders who stand to be better protected under the new law.  This better protection for lenders seems to have come at the expenses of consumers who need bankruptcy relief.  </p>
<p>One of the changes under BAPCPA, there is now a means test, that makes it significantly more difficult for debtors with mostly consumer debts to qualify for Chapter 7 bankruptcy relief.  
    Chapter 7 is the most favorable bankruptcy for consumers, so limiting its availability makes filing for bankruptcy significantly more involved and difficult.  Under BAPCPA's means test, you must have less than an average income for a family of the same size as yours.  The income is computed over 180 days preceding the filing, and there are certain allowances or living expenses and payments to secured  creditors.  The result is a very complex calculation, and does not necessarily reflect your disposable income, but you must qualify under the means test to be allowed to file for Chapter 7, the most desirable form of consumer bankruptcy.  Instead, if you fail the means test you must file under Chapter 13.</p>
<p>BAPCPA also requires that prior to filing a petition for bankruptcy relief, the debtor must undertake approved credit counseling.  This counseling is sometimes of little benefit, because for many people, a petition for bankruptcy is their only legitimate choice.  
Bankruptcy is complicated law, and has recently become more so.  This is why you need an experienced lawyer who is versed in the law of bankruptcy to help you through your case.  Come to the Law Offices of Marc Grossman, and talk about your options under the law of bankruptcy.  Our team of experienced attorneys is here to help you.  After your free initial consultation you will better understand your rights and be better prepared for the choices you face.</p>
<p><a href="http://www.wefight4you.com/Forms/contact.aspx" rel="nofollow">E-mail us</a> or call us toll free at 1-888-407-9068 to discuss your case with a knowledgeable bankruptcy attorney.</p>
<p>We offer an initial consultation to provide you with a solid starting point for your case 
    whether you&#39;re filing a chapter 7 or chapter 13 bankruptcy claim. We want to learn about your situation. Let us provide you with clear information about how we can guide you through it.</p>
<b>Smart, honest bankruptcy representation</b>
<p>We offer an initial consultation to provide you with a solid starting point for your case whether you're filing a chapter 7 or chapter 11 bankruptcy claim</a>. We want to learn about your situation. Let us provide you with clear information about how we can guide you through it.</p>
<p>The<b> Law Office of Marc Grossman is available to handle bankruptcy cases</b> for people in Upland, California, and throughout the Inland Empire.
Our knowledge of bankruptcy laws means that you can trust us to help you find a path to resolve your financial challenge. We will find a method that is right for you, whether it means filing Chapter 7 bankruptcy, Chapter 13 bankruptcy or finding some other solution.
</p>
<p><i>Bankruptcy can help you save your home from foreclosure. It can help you get a fresh start on your financial future. It can help you get all of this stress off your shoulders so you can get on with your life.</i></p>
<p>To find out more about how an experienced bankruptcy lawyer can help you through a difficult debt situation, call us toll free at 1-888-407-9068 for a free initial consultation.
We are a debt relief agency. We help people file for bankruptcy relief under the bankruptcy code.
</p>

<br />
    <p style="text-align:center"><strong>Why People File For Bankruptcy</strong></p>
    <p>
        <b>Studies have shown that the most common reasons of filing for bankruptcy are;</b></p>
    <ul>
        <li>Job Loss</li>
        <li>Medical Expenses that are not reimbursed by insurance companies.</li>
        <li>Divorce or Legal Separation.</li>
        <li>Small Business failures</li>
        <li>The Foreclosure crisis.</li>
    </ul>

</asp:Content>
<asp:Content ID="Content5" runat="server" contentplaceholderid="Toph1Page">
    Inland Empire Bankruptcy Attorneys that can help you file for Chapter 7, Chapter 11 and Chapter 13 
    Bankruptcy 
</asp:Content>
<asp:Content ID="Content6" runat="server" contentplaceholderid="leftNav">
    <h3>Bankruptcy Information</h3>
<li><a href="http://www.wefight4you.com/bankruptcy/Default.aspx" title="bankruptcy" style="text-decoration: none;">Bankruptcy</a></li>
<li><a href="http://www.wefight4you.com/bankruptcy/keeping-your-home.aspx" style="text-decoration: none;">Keeping Your Home</a></li>
<li><a href="http://www.wefight4you.com/bankruptcy/will-you-loose-property.aspx" style="text-decoration: none;">Will You Loose Your Property</a></li>
    <li><a href="http://www.wefight4you.com/bankruptcy/bankruptcy-questions.aspx" style="text-decoration: none;">Top 
	Bankruptcy Questions</a></li>
<li><a href="http://www.wefight4you.com/bankruptcy/chapter-7.aspx" title="Chapter 7 Bankruptcy" style="text-decoration: none;">Chapter 7 Bankruptcy</a></li>
<li><a href="http://www.wefight4you.com/bankruptcy/chapter-11.aspx" title="Chapter 11 bankruptcy" style="text-decoration: none;">Chapter 11 Bankruptcy</a></li>
<li><a href="http://www.wefight4you.com/bankruptcy/chapter-13.aspx" title="Chapter 13 bankruptcy" style="text-decoration: none;">Chapter 13 Bankruptcy</a></li>
</asp:Content>
<asp:Content ID="Content7" runat="server" 
    contentplaceholderid="espanolNavigation">
                    <li><a href="http://www.wefight4you.com/espanol/quiebra.aspx" title="Quiebra" style="text-decoration: none;">Quiebra</a></li>
					<li><a href="http://www.wefight4you.com/espanol/capitulo-7.aspx" title="Capitulo 7" style="text-decoration: none;">Capitulo 7</a></li>
					<li><a href="http://www.wefight4you.com/espanol/capitulo-11.aspx" title="Capitulo 11" style="text-decoration: none;">Capitulo 11</a></li>
					<li><a href="http://www.wefight4you.com/espanol/capitulo-13.aspx" title="Capitulo 13" style="text-decoration: none;">Capitulo 13</a></li>
</asp:Content>
<asp:Content ID="Content8" runat="server" 
    contentplaceholderid="locationsNavigation">
</asp:Content>
<asp:Content ID="Content9" runat="server" 
    contentplaceholderid="bottomfooterNav">
    <p>If you are filing for Bankruptcy in California contact the Law Offices of Marc Grossman for a 
    Free Initial Consultation. Since 1998 we have helped people in San Bernardino 
    County California with their bankruptcy matters.</p>
</asp:Content>
