﻿<%@ Page Title="Common Bankruptcy Questions - Is Bankruptcy Right For You" Language="C#" MasterPageFile="~/MasterPages/Bankruptcy.master" AutoEventWireup="true" CodeFile="bankruptcy-questions.aspx.cs" Inherits="bankruptcy_bankruptcy_questions" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
<style type="text/css">
        .style2
        {
            width: 443px;
        }
        .style3
        {
            width: 41px;
            color: White;
            font-weight: bold;
            font-size: medium;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Toph1Page" Runat="Server">
    Filing for Bankruptcy Questions and Answers
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainLogo" Runat="Server">
    <table 
        class="style1">
        <tr>
            <td valign="top" class="style2">
                <img src="../images/bankruptcy-questions.jpg" /></td>
            <td valign="top">
                <table bgcolor="#081f2a" style="height: 294px; width: 347px" cellpadding="5">
                    <tr>
                        <td colspan="2" style="text-align:center; color:White; font-size:large;">
                            Free Bankruptcy Evaluation</td>
                    </tr>
                    <tr>
                        <td class="style3">
                            Name:</td>
                        <td>
                            <asp:TextBox id="txtName" EnableViewState="false" runat="server"></asp:TextBox>
                            &nbsp;</td>
                    </tr>
                    <tr>
                        <td class="style3">
                            Email:</td>
                        <td>
                            <asp:TextBox id="txtEmail" EnableViewState="false" runat="server"></asp:TextBox>
                            &nbsp;</td>
                    </tr>
                    <tr>
                        <td class="style3">
                            Telephone:</td>
                        <td>
                            <asp:TextBox id="txtTelephone" EnableViewState="false" runat="server"></asp:TextBox>
                            &nbsp;</td>
                    </tr>
                    <tr>
                        <td class="style3">
                            Questions:</td>
                        <td>
                            <asp:TextBox id="txtQuestions" EnableViewState="false" runat="server" TextMode="MultiLine"></asp:TextBox>
                            &nbsp;</td>
                    </tr>
                    <tr>
                        <td class="style3">
                            &nbsp;</td>
                        <td>
                            <asp:Button id="btnSubmit" EnableViewState="false" runat="server" Text="Button" 
                                onclick="btnSubmit_Click" />
                            &nbsp;</td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="PageTitleH1" Runat="Server">
    <h2 style="text-align:center">Bankruptcy Questions</h2>
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="MainContent" Runat="Server">
    <p><b>Q:</b> <a href="/bankruptcy/Default.aspx">What is bankruptcy</a>?<br />
    <b>A:</b> Bankruptcy is a legal procedure where a debtor can be relieved of repayment of certain obligations.</p>
<p><b>Q:</b> How long will <a href="/bankruptcy/Default.aspx">bankruptcy</a> remain on my credit report?<br />
    <b>A:</b> Bankruptcy can remain on your credit from 7 to 10 years.</p>
<p><b>Q:</b> Will <a href="/bankruptcy/Default.aspx">bankruptcy</a> stop all debt collector calls and mail?<br />
    <b>A:</b> Yes an "automatic stay" will put an automatic injunction which prevents the debt collector from lawsuits, foreclosure, and wage garnishments once the bankruptcy petition is filed.</p>
<p><b>Q:</b> Can I loose my home if I file for <a href="/bankruptcy/Default.aspx">bankruptcy</a>?<br />
    <b>A:</b> Depending upon the certain type of bankruptcy that you file will determine if you loose your home or not. Speak to your attorney to find out which bankruptcy is right for you.</p>
<p><b>Q: </b>If I file for <a href="will-you-loose-property.aspx">bankruptcy can I loose my personal property</a>?<br />
    <b>A:</b>Exemptions range from state to state. Your attorney will discuss those exemptions with you.</p>
<p><b>Q:</b> What happens if I decide to file but my spouse does not?<br />
    <b>A:</b> This is a matter between you and your attorney. Your spouse may be liable for certain debt.</p>
<p><b>Q:</b> If I co-signed on a debt am I held responsible?<br />
    <b>A:</b> Yes you can be held responsible for a debt that you co-signed on.</p>
<p><b>Q:</b> Once I file <a href="/bankruptcy/Default.aspx">bankruptcy</a> will all my debts be discharged?<br />
    <b>A:</b> No. Student loans, back taxes are an example of debt you cannot discharge.</p>
<p><b>Q:</b> Can I choose which debts that I put onto the 
    <a href="/bankruptcy/Default.aspx">bankruptcy</a>?
A: No. You must provide all of your debts.</p>
<p><b>Q</b>: How long after I file bankruptcy will I receive a discharge?<br />
    <b>A:</b> <a href="/bankruptcy/chapter-7.aspx">Chapter 7</a> - 60 days after 341 meeting (or the first meeting of the creditors).Your 
    <a href="/bankruptcy/chapter-13.aspx">Chapter 13</a> discharge happens after you have completed the payments under your<a 
        href="/bankruptcy/chapter-13.aspx"> Chapter 13 plan</a>.</p>
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="leftNav" Runat="Server">
   <h2>Bankruptcy Information</h2>
<li><a href="/bankruptcy/Default.aspx" title="bankruptcy attorney" style="text-decoration: none;">Bankruptcy</a></li>
<li><a href="/bankruptcy/keeping-your-home.aspx" style="text-decoration: none;">Keeping Your Home</a></li>
<li><a href="/bankruptcy/will-you-loose-property.aspx" style="text-decoration: none;">Will You Loose Your Property</a></li>
    <li><a href="bankruptcy-questions.aspx" style="text-decoration: none;">Top Bankruptcy Questions</a></li>
<li><a href="/bankruptcy/chapter-7.aspx" style="text-decoration: none;">Chapter 7 Bankruptcy</a></li>
<li><a href="/bankruptcy/chapter-11.aspx" style="text-decoration: none;">Chapter 11 Bankruptcy</a></li>
<li><a href="/bankruptcy/chapter-13.aspx" style="text-decoration: none;">Chapter 13 Bankruptcy</a></li>
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="espanolNavigation" Runat="Server">
    <li><a href="/espanol/quiebra.aspx" style="text-decoration: none;">Quiebra</a></li>
					<li><a href="/espanol/capitulo-7.aspx" style="text-decoration: none;">Capitulo 7</a></li>
					<li><a href="/espanol/capitulo-11.aspx" style="text-decoration: none;">Capitulo 11</a></li>
					<li><a href="/espanol/capitulo-13.aspx" style="text-decoration: none;">Capitulo 13</a></li>
</asp:Content>
<asp:Content ID="Content8" ContentPlaceHolderID="locationsNavigation" Runat="Server">
   <li><a href="/lawyer/claremont.aspx" title="claremont Bankruptcy" style="text-decoration: none;">Claremont Bankruptcy</a></li>
			<li><a href="/lawyer/chino.aspx" title="chino Bankruptcy" style="text-decoration: none;">Chino Bankruptcy</a></li>
			<li><a href="/lawyer/chino-hills.aspx" title="chino hills Bankruptcy" style="text-decoration: none;">Chino Hills Bankruptcy</a></li>
			<li><a href="/lawyer/corona.aspx" title="corona Bankruptcy" style="text-decoration: none;">Corona Bankruptcy</a></li>
			<li><a href="/lawyer/fontana.aspx" title="fontana Bankruptcy" style="text-decoration: none;">Fontana Bankruptcy</a></li>		
			<li><a href="/lawyer/la-puente.aspx" title="La Puente Workers Bankruptcy" style="text-decoration: none;">La Puente Bankruptcy</a></li>
			<li><a href="/lawyer/montclair.aspx" title="montclair Bankruptcy" style="text-decoration: none;">Montclair Bankruptcy</a></li>
			<li><a href="/lawyer/ontario.aspx" title="ontario Bankruptcy" style="text-decoration: none;">Ontario Bankruptcy</a></li>
			<li><a href="/lawyer/pomona.aspx" title="pomona Bankruptcy" style="text-decoration: none;">Pomona Bankruptcy</a></li>
			<li><a href="/lawyer/rancho-cucamonga.aspx" title="rancho cucamonga Bankruptcy" style="text-decoration: none;">Rancho Cucamonga Bankruptcy</a></li>
			<li><a href="/lawyer/riverside.aspx" title="riverside Bankruptcy" style="text-decoration: none;">Riverside Bankruptcy</a></li>
			<li><a href="/lawyer/san-bernardino.aspx" title="San Bernardino Bankruptcy" style="text-decoration: none;">San Bernardino Bankruptcy</a></li>
			<li><a href="/lawyer/upland.aspx" title="upland Bankruptcy" style="border-bottom-width: 0">Upland Bankruptcy</a></li>
</asp:Content>
<asp:Content ID="Content9" ContentPlaceHolderID="bottomfooterNav" Runat="Server">
    <p>The Law Offices of Marc Grossman provide Bankruptcy Services in San Bernardino County California. If you have absolutely any bankruptcy questions feel free to come in for a Free Initial Consultation.</p>
</asp:Content>

