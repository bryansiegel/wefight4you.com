﻿<%@ Page Title="Chapter 7 Bankruptcy - Will You Loose Your Property" Language="C#" MasterPageFile="~/MasterPages/Bankruptcy.master" AutoEventWireup="true" CodeFile="will-you-loose-property.aspx.cs" Inherits="bankruptcy_will_you_loose_property" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
<style type="text/css">
        .style2
        {
            width: 443px;
        }
        .style3
        {
            width: 41px;
            color: White;
            font-weight: bold;
            font-size: medium;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Toph1Page" Runat="Server">

Bankruptcy - Will You Loose Your Stuff
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainLogo" Runat="Server">
<table 
        class="style1">
        <tr>
            <td valign="top" class="style2">
                <img src="../images/bankruptcy-your-stuff.jpg" style="height: 291px" /></td>
            <td valign="top">
                <table bgcolor="#081f2a" style="height: 294px; width: 347px" cellpadding="5">
                    <tr>
                        <td colspan="2" style="text-align:center; color:White; font-size:large;">
                            Free Evaluation</td>
                    </tr>
                    <tr>
                        <td class="style3">
                            Name:</td>
                        <td>
                            <asp:TextBox id="txtName" EnableViewState="false" runat="server"></asp:TextBox>
                            &nbsp;</td>
                    </tr>
                    <tr>
                        <td class="style3">
                            Email:</td>
                        <td>
                            <asp:TextBox id="txtEmail" EnableViewState="false" runat="server"></asp:TextBox>
                            &nbsp;</td>
                    </tr>
                    <tr>
                        <td class="style3">
                            Telephone:</td>
                        <td>
                            <asp:TextBox id="txtTelephone" EnableViewState="false" runat="server"></asp:TextBox>
                            &nbsp;</td>
                    </tr>
                    <tr>
                        <td class="style3">
                            Questions:</td>
                        <td>
                            <asp:TextBox id="txtQuestions" EnableViewState="false" runat="server" TextMode="MultiLine"></asp:TextBox>
                            &nbsp;</td>
                    </tr>
                    <tr>
                        <td class="style3">
                            &nbsp;</td>
                        <td>
                            <asp:Button id="btnSubmit" EnableViewState="false" runat="server" Text="Button" 
                                onclick="btnSubmit_Click" />
                            &nbsp;</td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="PageTitleH1" Runat="Server">
<h2>Will You Loose Your Stuff in a Bankruptcy?</h2>
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="MainContent" Runat="Server">
<p><a href="chapter-7.aspx">Chapter 7 bankruptcy</a> essentially offers this deal: If you are willing to give up your nonexempt property (ore exempt property of equivalent value) to be sold for the benefit of your creditors, the court will release your dischargeable debts. If you can keep ;most of the things you care about, Chapter 7 bankruptcy can be a very effective remedy for your debt problems. But if <a href="/bankruptcy/chapter-7.aspx"> Chapter 7 bankruptcy</a> would force you to part with treasured property, you may need to look for another solution.</p>
<p>The laws that control what property you can keep in a <a href="/bankruptcy/chapter-7.aspx">Chapter 7 bankruptcy</a> are called exceptions. Each States Legislature produces a set of exemptions for use by people who are sued in that state. These same exemptions are available to people who file for <a href="/bankruptcy/Default.aspx"> bankruptcy</a> in that state and meet the residency requirements. </p>
<p>Property is not exempt and it can be taken from you and sold by the trustee to pay off your unsecured creditors.  You can avoid this result by finding some cash to pay the trustee what the property is worth or convincing the trustee to accept some exempt property of roughly equal value as a substitute. Also, if nonexempt property won't produce money at a public sale, the trustee may decide to let you keep it. For instance, few trustees bother to take well-used furniture or second-hand electric gadgets or appliances. These items generally aren't worth what it would cost to sell them.</p>
<p>As you've no doubt figured out, the key to getting the most out of the <a href="/bankruptcy/Default.aspx">bankruptcy process</a> is to use exemptions to keep as much of your property as possible, while erasing as many debts as you can. To make full and proper use of your exemptions, you'll want to:</p>
<ul>
<li>learn which exemptions are available to you.</li>
<li>become familiar with the exemptions you can use, and</li>
<li>use the available exemptions in the way that lets you keep more of your treasured property.</li>
</ul>
<h2>If <a href="/bankruptcy/chapter-7.aspx">Chapter 7</a> Won't let you keep your property consider a <a href="/bankruptcy/chapter-13.aspx">Chapter 13</a> </h2>
<p>If it looks like <a href="/bankruptcy/chapter-7.aspx">Chapter 7</a> won't let you keep your treasured property you should consider a <a href="/bankruptcy/chapter-13.aspx">Chapter 13 bankruptcy</a>. <a href="/bankruptcy/chapter-13.aspx">Chapter 13</a> lets you keep your property regardless of your exempt status, as long as you will have sufficient income over the next three to five years to pay off all of a portion of your unsecured debts and to pay off all or a portion of your unsecured debts and to pay any priority debts you have (such as back child support, alimony, and taxes) in full. However, even in <a href="/bankruptcy/chapter-13.aspx">Chapter 13</a>, you will be required to propose a plan that pays your unsecured creditors a total amount that is at least equal to the value of your nonexempt property.
</p>
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="leftNav" Runat="Server">
						<h2>Bankruptcy Information</h2>
<li><a href="/bankruptcy/Default.aspx" title="bankruptcy attorney" style="text-decoration: none;">Bankruptcy</a></li>
<li><a href="/bankruptcy/keeping-your-home.aspx" style="text-decoration: none;">Keeping Your Home</a></li>
<li><a href="/bankruptcy/will-you-loose-property.aspx" style="text-decoration: none;">Will You Loose Your Property</a></li>
    <li><a href="bankruptcy-questions.aspx" style="text-decoration: none;">Top Bankruptcy Questions</a></li>
<li><a href="/bankruptcy/chapter-7.aspx" style="text-decoration: none;">Chapter 7 Bankruptcy</a></li>
<li><a href="/bankruptcy/chapter-11.aspx" style="text-decoration: none;">Chapter 11 Bankruptcy</a></li>
<li><a href="/bankruptcy/chapter-13.aspx" style="text-decoration: none;">Chapter 13 Bankruptcy</a></li>
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="espanolNavigation" Runat="Server">
<li><a href="/espanol/quiebra.aspx" style="text-decoration: none;">Quiebra</a></li>
					<li><a href="/espanol/capitulo-7.aspx" style="text-decoration: none;">Capitulo 7</a></li>
					<li><a href="/espanol/capitulo-11.aspx" style="text-decoration: none;">Capitulo 11</a></li>
					<li><a href="/espanol/capitulo-13.aspx" style="text-decoration: none;">Capitulo 13</a></li>
</asp:Content>
<asp:Content ID="Content8" ContentPlaceHolderID="locationsNavigation" Runat="Server"> 
            <li><a href="/lawyer/claremont.aspx" title="claremont Bankruptcy" style="text-decoration: none;">Claremont Bankruptcy</a></li>
			<li><a href="/lawyer/chino.aspx" title="chino Bankruptcy" style="text-decoration: none;">Chino Bankruptcy</a></li>
			<li><a href="/lawyer/chino-hills.aspx" title="chino hills Bankruptcy" style="text-decoration: none;">Chino Hills Bankruptcy</a></li>
			<li><a href="/lawyer/corona.aspx" title="corona Bankruptcy" style="text-decoration: none;">Corona Bankruptcy</a></li>
			<li><a href="/lawyer/fontana.aspx" title="fontana Bankruptcy"  style="text-decoration: none;">Fontana Bankruptcy</a></li>		
			<li><a href="/lawyer/la-puente.aspx" title="La Puente Workers Bankruptcy" style="text-decoration: none;">La Puente Bankruptcy</a></li>
			<li><a href="/lawyer/montclair.aspx" title="montclair Bankruptcy" style="text-decoration: none;">Montclair Bankruptcy</a></li>
			<li><a href="/lawyer/ontario.aspx" title="ontario Bankruptcy" style="text-decoration: none;">Ontario Bankruptcy</a></li>
			<li><a href="/lawyer/pomona.aspx" title="pomona Bankruptcy" style="text-decoration: none;">Pomona Bankruptcy</a></li>
			<li><a href="/lawyer/rancho-cucamonga.aspx" title="rancho cucamonga Bankruptcy" style="text-decoration: none;">Rancho Cucamonga Bankruptcy</a></li>
			<li><a href="/lawyer/riverside.aspx" title="riverside Bankruptcy" style="text-decoration: none;">Riverside Bankruptcy</a></li>
			<li><a href="/lawyer/san-bernardino.aspx" title="San Bernardino Bankruptcy" style="text-decoration: none;">San Bernardino Bankruptcy</a></li>
			<li><a href="/lawyer/upland.aspx" title="upland Bankruptcy" style="border-bottom-width: 0">Upland Bankruptcy</a></li>
</asp:Content>
<asp:Content ID="Content9" ContentPlaceHolderID="bottomfooterNav" Runat="Server">
<p>The Law Offices of Marc Grossman are Bankruptcy Attorneys that can assist you with a Chapter 7 or Chapter 13 bankruptcy.</p>
</asp:Content>

