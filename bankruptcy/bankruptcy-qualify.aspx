﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="bankruptcy-qualify.aspx.cs" Inherits="bankruptcy_bankruptcy_qualify" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Do You Qualify For Bankruptcy</title>

    <link href="../css/site_working.css" rel="stylesheet" type="text/css" />

    <style type="text/css">
        .style1
        {
            width: 100%;
        }
        .style2
        {
            font-size: medium;
        }
        .style3
        {
            height: 24px;
        }
        .style4
        {
            font-size: small;
            font-weight: bold;
        }
    </style>

</head>
<body class="design home">
    <form id="form1" runat="server">
    <h1 style="font-size:small; color:White; text-align:center">
See if you qualify for bankruptcy
    </h1>
     <div id="containerPage">
    <img src="http://www.wefight4you.com/images/printBanner.gif" id="printBanner" class="printElement" alt="" runat="server" />
    <div class="handheldElement"></div>
<div class="lock">
	<div id="bgBar"></div>
</div>

	<div id="banner">
		<div id="containerFlash">
			<img src="http://www.wefight4you.com/images/lettermark.gif" id="lettermark" width="43" height="87" 
                alt="" runat="server" />
			<img src="http://www.wefight4you.com/images/logo.gif" id="logo" width="333" height="72" 
                alt="Law Office of Marc E. Grossman" 
                style="left: 119px; top: 46px" runat="server" />
		</div>
	<br />	
		<br />	
		<div id="navigationMain">
						<ul>
				<li><a href="http://www.wefight4you.com/">
				<img src="http://www.wefight4you.com/images/n-main-home.gif" id="homepage" width="145" height="22" alt="Home Page" /></a></li>
				<li><a href="http://www.wefight4you.com/Firm-Overview/Default.aspx">
				<img src="http://www.wefight4you.com/images/n-main-firm.gif" id="firmoverview" width="145" height="22" alt="Firm Overview"/></a></li>
				<li><a href="http://www.wefight4you.com/attorneys/Default.aspx">
				<img src="http://www.wefight4you.com/images/n-main-atto.gif" id="attorneyprofiles" width="145" height="22" alt="Attorney Profiles"/></a></li>
				<li><a href="http://www.wefight4you.com/Practice-Areas/Default.aspx">
				<img src="http://www.wefight4you.com/images/n-main-prac.gif" id="practiceareas" width="145" height="22" alt="Practice Areas"/></a></li>
				<li><a href="http://www.wefight4you.com/Forms/Contact.aspx" rel="nofollow">
				<li><a href="http://www.wefight4you.com/blog/">
				<img src="http://www.wefight4you.com/images/blog-nav.jpg" id="Img1" width="145" height="22" alt="Blog" /></a></li><a href="../Forms/contact.aspx" rel="nofollow">
				<img src="http://www.wefight4you.com/images/n-main-cont.gif" id="contactus" width="145" height="22" alt="Contact Us" /></a></li>
			</ul>
		</div>
	</div>
	<p style="color:White">
	<asp:SiteMapPath ID="SiteMapPath1" runat="server">  
        </asp:SiteMapPath>  
        
        <asp:SiteMapDataSource ID="SiteMapDataSource1" runat="server" /> 
	</p>
    <img src="http://www.wefight4you.com/images/bkqualify.jpg" alt="qualify for Bankruptcy?" />
&nbsp;<div style="height:400px"; "width:800px">
<p style="text-align:center; color:White;">Please Fill in the form below to see if you qualify for 
    <a href="http://www.wefight4you.com/bankruptcy/Default.aspx" title="bankruptcy">bankruptcy</a>.</p>
           <br />
           <center>
               <asp:Wizard ID="Wizard1" runat="server" ActiveStepIndex="0" BackColor="#E6E2D8" 
                   BorderColor="#999999" BorderStyle="Solid" BorderWidth="1px" 
                   Font-Names="Verdana" Font-Size="0.8em" Height="354px" Width="600px" 
                   FinishDestinationPageUrl="http://www.wefight4you.com" 
                   onfinishbuttonclick="Wizard1_FinishButtonClick">
                   <HeaderStyle BackColor="#666666" BorderColor="#E6E2D8" BorderStyle="Solid" 
                       BorderWidth="2px" Font-Bold="True" Font-Size="0.9em" ForeColor="White" 
                       HorizontalAlign="Center" />
                   <NavigationButtonStyle BackColor="White" BorderColor="#C5BBAF" 
                       BorderStyle="Solid" BorderWidth="1px" Font-Names="Verdana" Font-Size="0.8em" 
                       ForeColor="#1C5E55" />
                   <SideBarButtonStyle ForeColor="White" />
                   <SideBarStyle BackColor="#1C5E55" Font-Size="0.9em" VerticalAlign="Top" 
                       Width="150px" />
                   <StepStyle BackColor="#F7F6F3" BorderColor="#E6E2D8" BorderStyle="Solid" 
                       BorderWidth="2px" VerticalAlign="Top" />
                   <WizardSteps>
                       <asp:WizardStep runat="server" Title="Contact Information">
                           <table cellspacing="10px" class="style1">
                               <tr>
                                   <td class="style2">
                                       <b>1. Personal Information</b></td>
                               </tr>
                               <tr>
                                   <td>
                                       <b>First Name</b></td>
                               </tr>
                               <tr>
                                   <td>
                                       <asp:TextBox ID="txtFirstName" runat="server"></asp:TextBox>
                                       <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" 
                                           ControlToValidate="txtFirstName" ErrorMessage="First Name Required">*</asp:RequiredFieldValidator>
                                       <br />
                                   </td>
                               </tr>
                               <tr>
                                   <td>
                                       <b>Last Name</b></td>
                               </tr>
                               <tr>
                                   <td>
                                       <asp:TextBox ID="txtLastName" runat="server"></asp:TextBox>
                                       <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" 
                                           ControlToValidate="txtLastName" ErrorMessage="Last Name Required">*</asp:RequiredFieldValidator>
                                       <br />
                                   </td>
                               </tr>
                               <tr>
                                   <td>
                                       <b>Telephone</b></td>
                               </tr>
                               <tr>
                                   <td>
                                       <asp:TextBox ID="txtTelephone" runat="server"></asp:TextBox>
                                       <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" 
                                           ControlToValidate="txtTelephone" ErrorMessage="Telephone Required">*</asp:RequiredFieldValidator>
                                       <br />
                                   </td>
                               </tr>
                               <tr>
                                   <td>
                                       <b>Email</b></td>
                               </tr>
                               <tr>
                                   <td>
                                       <asp:TextBox ID="txtEmail" runat="server"></asp:TextBox>
                                       <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" 
                                           ControlToValidate="txtEmail" ErrorMessage="Email Required">*</asp:RequiredFieldValidator>
                                       <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" 
                                           ControlToValidate="txtEmail" ErrorMessage="Email in incorrect format" 
                                           ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*">*</asp:RegularExpressionValidator>
                                       <br />
                                   </td>
                               </tr>
                               <tr>
                                   <td>
                                       <asp:ValidationSummary ID="ValidationSummary1" runat="server" />
                                   </td>
                               </tr>
                           </table>
                       </asp:WizardStep>
                       <asp:WizardStep runat="server" Title="Financial Information">
                           <table cellspacing="10px" class="style1">
                               <tr>
                                   <td>
                                       2. Financial Information</td>
                               </tr>
                               <tr>
                                   <td>
                                       What is your Gross Monthly Income?</td>
                               </tr>
                               <tr>
                                   <td>
                                       <asp:TextBox ID="txtGrossMonthlyIncome" runat="server"></asp:TextBox>
                                       <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" 
                                           ControlToValidate="txtGrossMonthlyIncome" 
                                           ErrorMessage=" Gross Monthly Income Required">*</asp:RequiredFieldValidator>
                                       <asp:RangeValidator ID="RangeValidator1" runat="server" 
                                           ControlToValidate="txtGrossMonthlyIncome" 
                                           ErrorMessage="Please Enter Your Gross Monthly Income" MaximumValue="1000000000" 
                                           MinimumValue="0" Type="Integer">*</asp:RangeValidator>
                                   </td>
                               </tr>
                               <tr>
                                   <td>
                                       What is your Spouse&#39;s Gross Monthly Income?</td>
                               </tr>
                               <tr>
                                   <td>
                                       <asp:TextBox ID="txtSpouseGrossMonthlyIncome" runat="server"></asp:TextBox>
                                   </td>
                               </tr>
                               <tr>
                                   <td>
                                       Have You Ever Filed For Bankruptcy Before?</td>
                               </tr>
                               <tr>
                                   <td>
                                      <asp:RadioButtonList ID="rdFiledBankruptcyBefore" runat="server" 
                                           AutoPostBack="True">
                                      <asp:ListItem Text="yes" Value="yes"></asp:ListItem>
                                      <asp:ListItem Text="no" Value="no"></asp:ListItem>
                                      </asp:RadioButtonList>
                                      </td>
                               </tr>
                               <tr>
                                   <td>
                                       If so, when? mm/dd/yyyy</td>
                               </tr>
                               <tr>
                                   <td>
                                       <asp:TextBox ID="txtWhenFiledBankruptcy" runat="server"></asp:TextBox>
                                   </td>
                               </tr>
                               <tr>
                                   <td class="style3">
                                       How Many Minor Children do you have living with you?</td>
                               </tr>
                               <tr>
                                   <td>
                                       <asp:TextBox ID="txtHowManyChildren" runat="server"></asp:TextBox>
                                   </td>
                               </tr>
                               <tr>
                                   <td>
                                       <asp:ValidationSummary ID="ValidationSummary2" runat="server" />
                                   </td>
                               </tr>
                           </table>
                       </asp:WizardStep>
                       <asp:WizardStep runat="server" StepType="Finish" Title="Finish">
                           <span class="style4">Thank You for Filing out the Form A Bankruptcy Attorney 
                           will contact you shortly to let you know if you qualify for bankruptcy. You will 
                           now be redirected to the homepage when you click on Finish.</span>
                       </asp:WizardStep>
                   </WizardSteps>
               </asp:Wizard>
           </center>
       <p style="color:White">Disclaimer:</p>
           <p style=" color:White">The information you obtain at this site is not, nor is it intended to be, legal advice. You should consult an attorney for advice regarding your individual situation. We invite you to contact us and welcome your calls, letters and electronic mail. Contacting us does not create an attorney-client relationship. Please do not send any confidential information to us until such time as an attorney-client relationship has been established.</p>
       <br />
       <br />
       <br />
	</div>
     <br />
        <br />
        <br />
        <br />
        <br />
</div>
<div id="footerWrap">
	<div id="lowerWrap">
		<div id="navigationLower"><a href="http://www.wefight4you.com/">California Bankruptcy</a> <a href="http://www.wefight4you.com/divorce/Default.aspx" title="Divorce">Divorce</a> <a href="http://www.wefight4you.com/workers-compensation/Default.aspx">Workers Compensation</a> <a href="http://www.wefight4you.com/sitemap.aspx">Sitemap</a></div>
	</div>
	<div id="footer" class="clearfix">
		<div id="contactInformation">
<p><strong>Law Office of Marc E. Grossman</strong><br />
818 N. Mountain Avenue, Suite 111<br />
Upland, CA 91786<br />
Toll Free: 888-407-9068</p>
		</div>
		<div id="geographicalFooter">
		<img src="~/images/logoSmall.gif" id="smalllogo" width="289" height="46" alt="MG - Law Office of Marc E. Grossman" runat="server" /><br />
		</div>
	</div>
	<div id="finePrintWrap">
		<div id="finePrint">
			<p id="copyright">© 2010 by Law Office of Marc E. Grossman. All rights reserved. <a href="http://www.wefight4you.com/disclaimer.aspx" rel="nofollow">Disclaimer</a></p>
			<p id="branding">&nbsp;</p>
		</div>
		<br class="clear">
	</div>
</div>
    <%--<script language="JavaScript1.2" type="text/javascript"> preroll(); </script>--%>
<script type="text/javascript">
    var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
    document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
</script>
<script type="text/javascript">
    try {
        var pageTracker = _gat._getTracker("UA-1368144-9");
        pageTracker._trackPageview();
    } catch (err) { }</script>
<script type="text/javascript" src="silverlight.js"></script>
<script type="text/javascript">
    var hasSilverlight = Boolean(window.Silverlight);
    var hasSilverlight = hasSilverlight && Silverlight.isInstalled("1.0");
    var hasSilverlight = hasSilverlight && Silverlight.isInstalled("2.0");
    if (hasSilverlight1) {pageTracker._trackEvent("silverlight", "v1");
    if (hasSilverlight1) {pageTracker._trackEvent("silverlight", "v2");
    if (!hasSilverlight1 && !hasSilverlight2) {pageTracker._trackEvent("silverlight", "none");}
</script>
    </form>
</body>
</body>
</html>
