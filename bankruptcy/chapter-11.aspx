﻿<%@ Page Title="California Chapter 11 Bankruptcy Attorney, Lawyer" Language="C#" MasterPageFile="~/MasterPages/Bankruptcy.master" AutoEventWireup="true" CodeFile="chapter-11.aspx.cs" Inherits="bankruptcy_chapter_11" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <meta name="keywords" content="lawyer, attorney, law firm, law office, legal advice, bankruptcy, Chapter 7, Chapter 11, save your home, real estate, house, personal bankruptcy, bankruptcy laws, lawyer, attorney,">
<meta name="description" content="Do you need an experienced Chapter 11 bankruptcy attorney to handle your chapter 11 bankruptcy case? Contact the Lawyers at the Law Offices of Marc Grossman for a free initial consulation.">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainLogo" Runat="Server">
    <p style="text-align:center">
<img src="chapter-11-bankruptcy.jpg" alt="Chapter 11 Bankruptcy" 
        style="width: 451px" />
        <a href="http://www.wefight4you.com/bankruptcy/bankruptcy-qualify.aspx" title="Free Bankruptcy Evaluation">
        <img id="Img1" src="~/images/onlinebankruptcyevalLarge.gif" 
        runat="server" style="height: 300px; width: 339px" /></a>
</p>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="PageTitleH1" Runat="Server">
    <h2 id="pageTitle">Chapter 11 Bankruptcy Attorneys</h2>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="MainContent" Runat="Server">
<p>A Chapter 11 most likely is a reorganization of debt that tends to involve a business or partnership. A chapter 11 tends to keep the business alive while paying back the debt owed over time. Chapter 11 is not tied down to business owners but corporations and individuals can file for Chapter 11 bankruptcy. Chapter 11 is most often referred to as “reorganization bankruptcy”.</p>
<h3>What Steps Are involved in Chapter 11 Bankruptcy</h3>
<p>The Chapter 11 bankruptcy filing begins with filing a petition with the bankruptcy court within the debtor’s service or residence location.  The court required case filing fee is $1,000 and a $39 dollar miscellaneous admin fee. This fee must be paid before the filing unless otherwise specified by the court.</p>
<h3>About Chapter 11 Bankruptcy</h3>
    <p>Chapter 11 Bankruptcy Relief is used for business owners.  Under Chapter 11 Bankruptcy relief, as debtor, you would retain ownership of your assets, and maintain control of your business.  Under Chapter 11, you would be called a “debtor in possession.”  As debtor in possession, you run the day to day operations of the business while creditors work with you and the 
        <a href="Default.aspx">Bankruptcy</a> Court to negotiate a plan towards repayment.  The Chapter 11 plan must be fair to the creditors, and there is an order of priority among the creditors.  Creditors are allowed to vote on the proposed plan.  If the creditors vote to confirm the Chapter 11 plan, as debtor, you will continue to operate your business and pay your bills under the terms of the confirmed plan.  If the creditors do not vote to confirm the plan, the court will take a more active role in shaping the plan, and may impose additional requirements to confirm the plan.</p>

</asp:Content>

<asp:Content ID="Content5" runat="server" contentplaceholderid="leftNav">

								<h2>Bankruptcy Information</h2>
<li><a href="/bankruptcy/Default.aspx" title="bankruptcy attorney">Bankruptcy</a></li>
<li><a href="/bankruptcy/keeping-your-home.aspx">Keeping Your Home</a></li>
<li><a href="/bankruptcy/will-you-loose-property.aspx">Will You Loose Your Property</a></li>
    <li><a href="bankruptcy-questions.aspx">Top Bankruptcy Questions</a></li>
<li><a href="/bankruptcy/chapter-7.aspx">Chapter 7 Bankruptcy</a></li>
<li><a href="/bankruptcy/chapter-11.aspx">Chapter 11 Bankruptcy</a></li>
<li><a href="/bankruptcy/chapter-13.aspx">Chapter 13 Bankruptcy</a></li>

</asp:Content>


<asp:Content ID="Content6" runat="server" 
    contentplaceholderid="espanolNavigation">
                    <li><a href="/espanol/quiebra.aspx">Quiebra</a></li>
					<li><a href="/espanol/capitulo-7.aspx">Capitulo 7</a></li>
					<li><a href="/espanol/capitulo-11.aspx">Capitulo 11</a></li>
					<li><a href="/espanol/capitulo-13.aspx">Capitulo 13</a></li>			
</asp:Content>



<asp:Content ID="Content7" runat="server" 
    contentplaceholderid="locationsNavigation">
 <li><a href="/lawyer/claremont.aspx" title="claremont Bankruptcy">Claremont Bankruptcy</a></li>
			<li><a href="/lawyer/chino.aspx" title="chino Bankruptcy">Chino Bankruptcy</a></li>
			<li><a href="/lawyer/chino-hills.aspx" title="chino hills Bankruptcy">Chino Hills Bankruptcy</a></li>
			<li><a href="/lawyer/corona.aspx" title="corona Bankruptcy">Corona Bankruptcy</a></li>
			<li><a href="/lawyer/fontana.aspx" title="fontana Bankruptcy" >Fontana Bankruptcy</a></li>		
			<li><a href="/lawyer/la-puente.aspx" title="La Puente Workers Bankruptcy">La Puente Bankruptcy</a></li>
			<li><a href="/lawyer/montclair.aspx" title="montclair Bankruptcy">Montclair Bankruptcy</a></li>
			<li><a href="/lawyer/ontario.aspx" title="ontario Bankruptcy">Ontario Bankruptcy</a></li>
			<li><a href="/lawyer/pomona.aspx" title="pomona Bankruptcy">Pomona Bankruptcy</a></li>
			<li><a href="/lawyer/rancho-cucamonga.aspx" title="rancho cucamonga Bankruptcy">Rancho Cucamonga Bankruptcy</a></li>
			<li><a href="/lawyer/riverside.aspx" title="riverside Bankruptcy">Riverside Bankruptcy</a></li>
			<li><a href="/lawyer/san-bernardino.aspx" title="San Bernardino Bankruptcy">San Bernardino Bankruptcy</a></li>
			<li><a href="/lawyer/upland.aspx" title="upland Bankruptcy" style="border-bottom-width: 0">Upland Bankruptcy</a></li>
</asp:Content>




<asp:Content ID="Content8" runat="server" contentplaceholderid="Toph1Page">
    Filing Chapter 11 Bankruptcy
</asp:Content>





<asp:Content ID="Content9" runat="server" 
    contentplaceholderid="bottomfooterNav">
<p>If you need a Chapter 11 Bankruptcy, the Law Offices of Marc Grossman can help. 
    We have served the Inland Empire since 1998 and would like the opportunity to 
    assist you with your Chapter 11 Bankruptcy matter. Stop by our Upland California 
    office today for a Free Initial Consultation.</p>
		

</asp:Content>






