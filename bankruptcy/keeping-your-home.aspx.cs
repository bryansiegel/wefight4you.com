﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Net.Mail;

public partial class bankruptcy_keeping_your_home : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
       
    }
    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        MailMessage mailMsg = new MailMessage("info@wefight4you.com", "bryan@wefight4you.com");
        mailMsg.Subject = "Wefight4you.com Keeping Your Home Inquiry";
        mailMsg.Body = ("\n" + DateTime.Now +
            "\nEmail: " + txtEmail.Text +
            "\nTelephone: " + txtTelephone.Text +
            "\nName: " + txtName.Text +
            "\nCase Description: " + txtQuestions.Text);
        //mailMsg.IsBodyHtml = false;

        SmtpClient smtp = new SmtpClient();
        smtp.Host = "relay-hosting.secureserver.net";
        smtp.Credentials = new System.Net.NetworkCredential("bryan@wefight4you.com", "advanced");
        smtp.Send(mailMsg);

        //Reset Values from Textbox
        txtName.Text = "";
        txtEmail.Text = "";
        txtQuestions.Text = "";
        txtTelephone.Text = "";
    }
}
