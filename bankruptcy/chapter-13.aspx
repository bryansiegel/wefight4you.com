﻿<%@ Page Title="California Chapter 13 Bankruptcy Attorney, Lawyer" Language="C#" MasterPageFile="~/MasterPages/Bankruptcy.master" AutoEventWireup="true" CodeFile="chapter-13.aspx.cs" Inherits="bankruptcy_chapter_13" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <meta name="keywords" content="lawyer, attorney, law firm, law office, legal advice, bankruptcy, Chapter 7, Chapter 11, save your home, real estate, house, personal bankruptcy, bankruptcy laws, lawyer, attorney,">
<meta name="description" content="Do you need an experienced Chapter 11 bankruptcy attorney to handle your chapter 11 bankruptcy case? Contact the Lawyers at the Law Offices of Marc Grossman for a free initial consulation.">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainLogo" Runat="Server">
<p style="text-align:center">
<img src="chapter-13-bankruptcy.jpg" alt="Chapter 13 Bankruptcy" 
        style="width: 451px" />
        <a href="http://www.wefight4you.com/bankruptcy/bankruptcy-qualify.aspx" title="Free Bankruptcy Evaluation" rel="nofollow">
        <img src="~/images/onlinebankruptcyevalLarge.gif" 
        runat="server" style="height: 300px; width: 339px" /></a>
</p>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="PageTitleH1" Runat="Server">
    <h1 id="pageTitle">Chapter 13 Bankruptcy Attorneys</h1>
    <h2 style="text-align:center">Chapter 13 Bankruptcy Information</h2>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="MainContent" Runat="Server">
<h3 style="text-align:center">What is Chapter 13 Bankruptcy?</h3>
    <p>In Chapter 13 Bankruptcy relief, as the debtor, you retain ownership and possession of all of your assets, but must devote some portion of your future income to repaying creditors.  This generally lasts for a period lasting from three to five years.  There are many factors influencing the period of repayment.  These include the value of your property, the amount of your income and monthly expenses. </p>
    <h3 style="text-align:center">Why Should I consider a Chapter 13?</h3>
    <p>The advantage of Chapter 13 bankruptcy over <a href="http://www.wefight4you.com/bankuptcy/chapter-7.aspx">Chapter 7</a> is that the debtor can keep all of his/her property, but the court establishes an interest free repayment plan.  The repayment plan tends to be in effect within 30 - 45 days after the case has started.</p>
    <h3 style="text-align:center">Do I qualify for Chapter 13 Bankruptcy?</h3>
    <p>Ultimately whether or not you qualify for Chapter 13 will be advised by your attorney. In order to be considered for filing Chapter 13 bankruptcy you must have a steady job.</p>
    <h3 style="text-align:center">Steps for filing a Chapter 13 Bankruptcy</h3>
    <ul>
    <li>Our attorneys walk you through the process, but in order for you to know what you're getting into here's the general process of filing a Chapter 13 bankruptcy.</li>
<li>First off you must contact our Law Firm to schedule a FREE initial consultation.</li>
<li>During that consultation our attorney will sit down with your paperwork to determine whether you qualify for a Chapter 13.
</li>
    </ul>
<table class="style1">
        <tr>
            <td>
               <a id="A1" href="http://www.wefight4you.com/lawyer/chino-hills.aspx" title="Chino Hills Chapter 13 Bankruptcy Attorney" runat="server">Chino Hills 
                Chapter 13 Bankruptcy Attorney</a> </td>
            <td>
                <a id="A2" href="http://www.wefight4you.com/lawyer/ontario.aspx" title="Ontario Chapter 13 Bankruptcy Attorney" runat="server">Ontario 
                Chapter 13 Bankruptcy Attorney</a> </td>
        </tr>
        <tr>
            <td>
                <a id="A3" href="http://www.wefight4you.com/lawyer/chino.aspx" title="Chino Chapter 13 Bankruptcy Attorney" runat="server">Chino 
                Chapter 13&nbsp; Bankruptcy Attorney</a> </td>
            <td>
                <a id="A4" href="http://www.wefight4you.com/lawyer/pomona.aspx" title="Pomona Chapter 13 Bankruptcy Attorney" runat="server">Pomona 
                Chapter 13 Bankruptcy Attorney</a> </td>
        </tr>
        <tr>
            <td>
               <a id="A5" href="http://www.wefight4you.com/lawyer/claremont.aspx" title="Claremont Chapter 13 Bankruptcy Attorney" runat="server"> Claremont 
                Chapter 13 Bankruptcy Attorney</a> </td>
            <td>
                <a id="A6" href="http://www.wefight4you.com/lawyer/rancho-cucamonga.aspx" title="Rancho Cucamonga Chapter 13 Bankruptcy Attorney" runat="server">Rancho Cucamonga 
                Chapter 13 Bankruptcy Attorney</a> </td>
        </tr>
        <tr>
            <td>
                <a id="A7" href="http://www.wefight4you.com/lawyer/corona.aspx" title="Corona Chapter 13 Bankruptcy Lawyer" runat="server">Corona 
                Chapter 13 Bankruptcy Attorney</a> </td>
            <td>
                <a id="A8" href="http://www.wefight4you.com/lawyer/riverside.aspx" title="Riverside Chapter 13 Bankruptcy Lawyer" runat="server">Riverside 
                Chapter 13 Bankruptcy Attorney</a> </td>
        </tr>
        <tr>
            <td>
                <a id="A9" href="http://www.wefight4you.com/lawyer/fontana.aspx" title="Fontana Chapter 13 Bankruptcy Lawyer" runat="server">Fontana 
                Chapter 13 Bankruptcy Attorney</a> </td>
            <td>
                <a id="A10" href="http://www.wefight4you.com/lawyer/san-bernardino.aspx" title="San Bernardino Chapter 13 Bankruptcy Lawyer" runat="server">San Bernardino 
                Chapter 13 Bankruptcy Attorney</a> </td>
        </tr>
        <tr>
            <td>
                <a id="A11" href="http://www.wefight4you.com/lawyer/montclair.aspx" title="Montclair Chapter 13 Bankruptcy Lawyer" runat="server">Montclair 
                Chapter 13 Bankruptcy Attorney</a> </td>
            <td>
                <a id="A12" href="http://www.wefight4you.com/lawyer/upland.aspx" title="Upland Chapter 13 Bankruptcy Lawyer" runat="server">Upland 
                Chapter 13 Bankruptcy Attorney</a> </td>
        </tr>
        </table>
</asp:Content>
<asp:Content ID="Content5" runat="server" contentplaceholderid="Toph1Page">
    Filing Chapter 13 Bankruptcy
</asp:Content>
<asp:Content ID="Content6" runat="server" contentplaceholderid="leftNav">
 <h2>Bankruptcy Information</h2>
<li><a href="/bankruptcy/Default.aspx" title="bankruptcy attorney">Bankruptcy</a></li>
<li><a href="/bankruptcy/keeping-your-home.aspx">Keeping Your Home</a></li>
<li><a href="/bankruptcy/will-you-loose-property.aspx">Will You Loose Your Property</a></li>
    <li><a href="bankruptcy-questions.aspx">Top Bankruptcy Questions</a></li>
<li><a href="/bankruptcy/chapter-7.aspx">Chapter 7 Bankruptcy</a></li>
<li><a href="/bankruptcy/chapter-11.aspx">Chapter 11 Bankruptcy</a></li>
<li><a href="/bankruptcy/chapter-13.aspx">Chapter 13 Bankruptcy</a></li>
</asp:Content>
<asp:Content ID="Content7" runat="server" 
    contentplaceholderid="espanolNavigation">
<li><a href="/espanol/quiebra.aspx">Quiebra</a></li>
					<li><a href="/espanol/capitulo-7.aspx">Capitulo 7</a></li>
					<li><a href="/espanol/capitulo-11.aspx">Capitulo 11</a></li>
					<li><a href="/espanol/capitulo-13.aspx">Capitulo 13</a></li>
</asp:Content>
<asp:Content ID="Content8" runat="server" 
    contentplaceholderid="locationsNavigation">
 <li><a href="/lawyer/claremont.aspx" title="claremont Bankruptcy">Claremont Bankruptcy</a></li>
			<li><a href="/lawyer/chino.aspx" title="chino Bankruptcy">Chino Bankruptcy</a></li>
			<li><a href="/lawyer/chino-hills.aspx" title="chino hills Bankruptcy">Chino Hills Bankruptcy</a></li>
			<li><a href="/lawyer/corona.aspx" title="corona Bankruptcy">Corona Bankruptcy</a></li>
			<li><a href="/lawyer/fontana.aspx" title="fontana Bankruptcy" >Fontana Bankruptcy</a></li>		
			<li><a href="/lawyer/la-puente.aspx" title="La Puente Workers Bankruptcy">La Puente Bankruptcy</a></li>
			<li><a href="/lawyer/montclair.aspx" title="montclair Bankruptcy">Montclair Bankruptcy</a></li>
			<li><a href="/lawyer/ontario.aspx" title="ontario Bankruptcy">Ontario Bankruptcy</a></li>
			<li><a href="/lawyer/pomona.aspx" title="pomona Bankruptcy">Pomona Bankruptcy</a></li>
			<li><a href="/lawyer/rancho-cucamonga.aspx" title="rancho cucamonga Bankruptcy">Rancho Cucamonga Bankruptcy</a></li>
			<li><a href="/lawyer/riverside.aspx" title="riverside Bankruptcy">Riverside Bankruptcy</a></li>
			<li><a href="/lawyer/san-bernardino.aspx" title="San Bernardino Bankruptcy">San Bernardino Bankruptcy</a></li>
			<li><a href="/lawyer/upland.aspx" title="upland Bankruptcy" style="border-bottom-width: 0">Upland Bankruptcy</a></li>
</asp:Content>
<asp:Content ID="Content9" runat="server" 
    contentplaceholderid="bottomfooterNav">
<p>The Law Office of Marc Grossman are qualified Chapter 13 Bankruptcy Attorneys that provide exceptional services within San Bernardino County California. If you have any questions or concerns regarding filing for chapter 13 bankruptcy contact us for a Free Initial Consultation, and remember at the Law Offices of Marc Grossman "We Fight For You!"</p>	
</asp:Content>





