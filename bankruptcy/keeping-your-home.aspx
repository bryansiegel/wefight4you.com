﻿<%@ Page Title="Keeping Your Home During a Bankruptcy - How to Stop Foreclosure with Bankruptcy" Language="C#" MasterPageFile="~/MasterPages/Bankruptcy.master" AutoEventWireup="true" CodeFile="keeping-your-home.aspx.cs" Inherits="bankruptcy_keeping_your_home" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
 <style type="text/css">
        .style2
        {
            width: 443px;
        }
        .style3
        {
            width: 41px;
            color: White;
            font-weight: bold;
            font-size: medium;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Toph1Page" Runat="Server">
Bankruptcy | Can I Keep My Home?
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainLogo" Runat="Server">
<table 
        class="style1">
        <tr>
            <td valign="top" class="style2">
                <img src="../images/stop-foreclosure.jpg" /></td>
            <td valign="top">
                <table bgcolor="#081f2a" style="height: 294px; width: 347px" cellpadding="5">
                    <tr>
                        <td colspan="2" style="text-align:center; color:White; font-size:large;">
                            Free Evaluation</td>
                    </tr>
                    <tr>
                        <td class="style3">
                            Name:</td>
                        <td>
                            <asp:TextBox id="txtName" EnableViewState="false" runat="server"></asp:TextBox>
                            &nbsp;</td>
                    </tr>
                    <tr>
                        <td class="style3">
                            Email:</td>
                        <td>
                            <asp:TextBox id="txtEmail" EnableViewState="false" runat="server"></asp:TextBox>
                            &nbsp;</td>
                    </tr>
                    <tr>
                        <td class="style3">
                            Telephone:</td>
                        <td>
                            <asp:TextBox id="txtTelephone" EnableViewState="false" runat="server"></asp:TextBox>
                            &nbsp;</td>
                    </tr>
                    <tr>
                        <td class="style3">
                            Questions:</td>
                        <td>
                            <asp:TextBox id="txtQuestions" EnableViewState="false" runat="server" TextMode="MultiLine"></asp:TextBox>
                            &nbsp;</td>
                    </tr>
                    <tr>
                        <td class="style3">
                            &nbsp;</td>
                        <td>
                            <asp:Button id="btnSubmit" EnableViewState="false" runat="server" Text="Button" />
                            &nbsp;</td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="PageTitleH1" Runat="Server">
<h2 style="font-size:x-large; color:White; text-align:center;">Keeping Your Home When You File for Bankruptcy</h2>
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="MainContent" Runat="Server">
<p>You must keep making your mortgage payments. As you probably know you don't actually own your home the bank or lender does. Until the mortgage is paid off the lender has the right to foreclose if you miss your mortgage payments. 
    <a href="/bankruptcy/chapter-7.aspx">Chapter 7 bankruptcy</a> doesn't change this, although it might pause the foreclosure for awhile.</p>
    <p>If you currently &quot;own&quot; a home it&#39;s important that you hire an experienced 
        <a href="/bankruptcy/Default.aspx">bankruptcy attorney</a>. Attorneys can help you with filing for 
        <a href="/bankruptcy/Default.aspx">bankruptcy</a> and know 
        the law. Why should you take the risk of losing everything you own to creditors 
        when you can contact an attorney to take care of it for you.</p>
<h2>Liens on your home</h2>
<p><a href="/bankruptcy/chapter-7.aspx">Chapter 7 bankruptcy</a> won't eliminate liens on your home that were created with your consent or certain nonconsensual liens (such as tax liens or mechanics liens). If you've pledged your home as security for loans other than your mortgage those creditors have claims against your home.</p>
<h2>Keeping Your Home</h2>
<p>Unafortunaly, even if you pay your mortgage payments you can still loose your home unless a homestead exception protects your equity. If you were to sell your home today, without filing for 
    <a href="/bankruptcy/Default.aspx">bankruptcy</a>, the money raised by the sale would first go to the mortgage lender then to lienholders to pay off the liens, and finally to pay off the costs of sale and any taxes due. If there is anything left over that's yours to keep.</p>
    <p>There are other circumstances that might cause you to loose a home. Such as;</p>
    <ul>
    <li>If you're behind on your mortgage payments.</li>
    <li>Bankruptcy Caused by a divorce.</li>
    <li>The worth of your home is less than what you owe.</li>  
    </ul>
    <p>All or any of these conditions might apply to you, which is why it's important for you to hire someone to handle your bankruptcy. Our 
        <a href="/bankruptcy/Default.aspx">bankruptcy attorneys</a> have helped thousands of people who were in your exact situation. At the Law Offices of 
        <a href="/attorneys/marc-grossman.aspx">Marc Grossman</a> we have the knowledge and know how to get you through this difficult time in your life and will fight for you every step of the way. Why wait? 
        <a href="../Forms/Contact.aspx">Contact Us Today for a Free Initial Consultation</a>.</p>

</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="leftNav" Runat="Server">
<h2>Bankruptcy Information</h2>
<li><a href="/bankruptcy/Default.aspx" title="bankruptcy attorney" style="text-decoration: none;">Bankruptcy</a></li>
<li><a href="/bankruptcy/keeping-your-home.aspx" style="text-decoration: none;">Keeping Your Home</a></li>
<li><a href="/bankruptcy/will-you-loose-property.aspx" style="text-decoration: none;">Will You Loose Your Property</a></li>
    <li><a href="/bankruptcy/bankruptcy-questions.aspx" style="text-decoration: none;">Top Bankruptcy Questions</a></li>
<li><a href="/bankruptcy/chapter-7.aspx" style="text-decoration: none;">Chapter 7 Bankruptcy</a></li>
<li><a href="/bankruptcy/chapter-11.aspx" style="text-decoration: none;">Chapter 11 Bankruptcy</a></li>
<li><a href="/bankruptcy/chapter-13.aspx" style="text-decoration: none;">Chapter 13 Bankruptcy</a></li>
<br />
<h3>Locations that We Serve</h3>
<ul><li><a href="/lawyer/claremont.aspx" title="Claremont Bankruptcy" style="text-decoration: none;">Claremont</a></li>
			<li><a href="/lawyer/chino.aspx" title="Chino Bankruptcy" style="text-decoration: none;">Chino</a></li>
			<li><a href="/lawyer/chino-hills.aspx" title="Chino Hills Bankruptcy" style="text-decoration: none;">Chino Hills</a></li>
			<li><a href="/lawyer/corona.aspx" title="Corona Bankruptcy" style="text-decoration: none;">Corona</a></li>
			<li><a href="/lawyer/fontana.aspx" title="Fontana bankruptcy" style="text-decoration: none;">Fontana</a></li>		
			<li><a href="/lawyer/la-puente.aspx" title="La Puente Workers Comp Attorney" style="text-decoration: none;">La Puente</a></li>
			<li><a href="/lawyer/montclair.aspx" title="lawyer montclair" style="text-decoration: none;">Montclair</a></li>
			<li><a href="/lawyer/ontario.aspx" title="lawyer ontario" style="text-decoration: none;">Ontario</a></li>
			<li><a href="/lawyer/pomona.aspx" title="lawyer pomona" style="text-decoration: none;">Pomona</a></li>
			<li><a href="/lawyer/rancho-cucamonga.aspx" title="lawyer rancho cucamonga" style="text-decoration: none;">Rancho Cucamonga</a></li>
			<li><a href="/lawyer/riverside.aspx" title="lawyer riverside" style="text-decoration: none;">Riverside</a></li>
			<li><a href="/lawyer/san-bernardino.aspx" title="San Bernardino Attorneys" style="text-decoration: none;">San Bernardino</a></li>
			<li><a href="/lawyer/upland.aspx" title="lawyer upland" style="border-bottom-width: 0">Upland</a></li></ul>
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="espanolNavigation" Runat="Server">
<li><a href="/espanol/quiebra.aspx" style="text-decoration: none;">Quiebra</a></li>
					<li><a href="/espanol/capitulo-7.aspx" style="text-decoration: none;">Capitulo 7</a></li>
					<li><a href="/espanol/capitulo-11.aspx" style="text-decoration: none;">Capitulo 11</a></li>
					<li><a href="/espanol/capitulo-13.aspx" style="text-decoration: none;">Capitulo 13</a></li>
</asp:Content>
<asp:Content ID="Content8" ContentPlaceHolderID="locationsNavigation" Runat="Server">
</asp:Content>
<asp:Content ID="Content9" ContentPlaceHolderID="bottomfooterNav" Runat="Server">
</asp:Content>

