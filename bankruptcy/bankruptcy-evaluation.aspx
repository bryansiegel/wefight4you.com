﻿<%@ Page Title="FREE Bankruptcy Evaluation | Do You Qualify For Bankruptcy in California?" Language="C#" MasterPageFile="~/MasterPages/Bankruptcy.master" AutoEventWireup="true" CodeFile="bankruptcy-evaluation.aspx.cs" Inherits="bankruptcy_bankruptcy_evaluation" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
<%-- <style type="text/css">
        .style2
        {
            width: 443px;
        }
        .style3
        {
            width: 41px;
            color: White;
            font-weight: bold;
            font-size: medium;
        }
    </style>--%>
   <META NAME="ROBOTS" CONTENT="NOINDEX, NOFOLLOW">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Toph1Page" Runat="Server">
    <%--Bankruptcy Evaluation --%>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainLogo" Runat="Server">






<%--<table 
        class="style1">
        <tr>
            <td valign="top" class="style2">
                <img src="../images/debt.jpg" /></td>
            <td valign="top">
                <table bgcolor="#081f2a" style="height: 294px; width: 347px" cellpadding="5">
                    <tr>
                        <td colspan="2" style="text-align:center; color:White; font-size:large;">
                            Free Evaluation</td>
                    </tr>
                    <tr>
                        <td class="style3">
                            Name:</td>
                        <td>
                            <asp:TextBox id="txtName" EnableViewState="false" runat="server"></asp:TextBox>
                            &nbsp;</td>
                    </tr>
                    <tr>
                        <td class="style3">
                            Email:</td>
                        <td>
                            <asp:TextBox id="txtEmail" EnableViewState="false" runat="server"></asp:TextBox>
                            &nbsp;</td>
                    </tr>
                    <tr>
                        <td class="style3">
                            Telephone:</td>
                        <td>
                            <asp:TextBox id="txtTelephone" EnableViewState="false" runat="server"></asp:TextBox>
                            &nbsp;</td>
                    </tr>
                    <tr>
                        <td class="style3">
                            Questions:</td>
                        <td>
                            <asp:TextBox id="txtQuestions" EnableViewState="false" runat="server" TextMode="MultiLine"></asp:TextBox>
                            &nbsp;</td>
                    </tr>
                    <tr>
                        <td class="style3">
                            &nbsp;</td>
                        <td>
                            <asp:Button id="btnSubmit" EnableViewState="false" runat="server" Text="Submit" 
                                onclick="btnSubmit_Click" /><br />
                                <asp:Label ID="lbl_error" Text="" runat="server" />
                            &nbsp;</td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="PageTitleH1" Runat="Server">
<p style="color:White; font-size:x-large">You&#39;re Not Alone. If you are suffering 
    from Debt, whether your debt comes from credit cards or job loss you can&#39;t 
    afford to wait another day. Contact Us Now. Your first consultation is FREE.</p>
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="MainContent" Runat="Server">
<p style="text-align:center; color: white; font-size:xx-large;">Stop Foreclosure</p>
<p style="text-align:center"><img src="http://www.wefight4you.com/images/stop-foreclosure.jpg" /></p>
<p style="text-align:center; color: white; font-size:xx-large;">Eliminate Credit Card Debt
<p style="text-align:center"><img src="http://www.wefight4you.com/images/credit-cards.jpg" 
        style="width: 443px" /></p>
<p style="text-align:center; color: white; font-size:xx-large;">Live Debt Free</p>
<p style="text-align:center"><img src="http://www.wefight4you.com/images/debt-free.jpg" /></p>
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="espanolNavigation" Runat="Server">
<li><a href="http://www.wefight4you.com/espanol/quiebra.aspx">Quiebra</a></li>
					<li><a href="http://www.wefight4you.com/espanol/capitulo-7.aspx">Capitulo 7</a></li>
					<li><a href="http://www.wefight4you.com/espanol/capitulo-11.aspx">Capitulo 11</a></li>
					<li><a href="http://www.wefight4you.com/espanol/capitulo-13.aspx">Capitulo 13</a></li>
</asp:Content>
<asp:Content ID="Content8" ContentPlaceHolderID="locationsNavigation" Runat="Server">
<li><a href="/lawyer/claremont.aspx" title="lawyer claremont">Claremont Bankruptcy</a></li>
			<li><a href="http://www.wefight4you.com/lawyer/chino.aspx" title="lawyer chino">Chino Bankruptcy</a></li>
			<li><a href="http://www.wefight4you.com/lawyer/chino-hills.aspx" title="lawyer chino hills">Chino Hills Bankruptcy</a></li>
			<li><a href="http://www.wefight4you.com/lawyer/corona.aspx" title="lawyer corona">Corona Bankruptcy</a></li>
			<li><a href="http://www.wefight4you.com/lawyer/fontana.aspx" title="lawyer fontana" >Fontana Bankruptcy</a></li>		
			<li><a href="http://www.wefight4you.com/lawyer/la-puente.aspx" title="La Puente Workers Comp Attorney">La Puente Bankruptcy</a></li>
			<li><a href="http://www.wefight4you.com/lawyer/montclair.aspx" title="lawyer montclair">Montclair Bankruptcy</a></li>
			<li><a href="http://www.wefight4you.com/lawyer/ontario.aspx" title="lawyer ontario">Ontario Bankruptcy</a></li>
			<li><a href="http://www.wefight4you.com/lawyer/pomona.aspx" title="lawyer pomona">Pomona Bankruptcy</a></li>
			<li><a href="http://www.wefight4you.com/lawyer/rancho-cucamonga.aspx" title="lawyer rancho cucamonga">Rancho Cucamonga Bankruptcy</a></li>
			<li><a href="http://www.wefight4you.com/lawyer/riverside.aspx" title="lawyer riverside">Riverside Bankruptcy</a></li>
			<li><a href="http://www.wefight4you.com/lawyer/san-bernardino.aspx" title="San Bernardino Attorneys">San Bernardino Bankruptcy</a></li>
			<li><a href="http://www.wefight4you.com/lawyer/upland.aspx" title="lawyer upland" style="border-bottom-width: 0">Upland Bankruptcy</a></li>
</asp:Content>
<asp:Content ID="Content9" ContentPlaceHolderID="bottomfooterNav" Runat="Server">
</asp:Content>

<asp:Content ID="Content10" runat="server" contentplaceholderid="leftNav">
<h2>Bankruptcy Information</h2>
<li><a href="http://www.wefight4you.com/bankruptcy/Default.aspx" title="bankruptcy attorney">Bankruptcy</a></li>
<li><a href="http://www.wefight4you.com/bankruptcy/keeping-your-home.aspx">Keeping Your Home</a></li>
<li><a href="http://www.wefight4you.com/bankruptcy/will-you-loose-property.aspx">Will You Loose Your Property</a></li>
    <li><a href="/http://www.wefight4you.com/bankruptcy/bankruptcy-questions.aspx">Top Bankruptcy Questions</a></li>
<li><a href="http://www.wefight4you.com/bankruptcy/chapter-7.aspx">Chapter 7 Bankruptcy</a></li>
<li><a href="http://www.wefight4you.com/bankruptcy/chapter-11.aspx">Chapter 11 Bankruptcy</a></li>
<li><a href="http://www.wefight4you.com/bankruptcy/chapter-13.aspx">Chapter 13 Bankruptcy</a></li>
--%>
</asp:Content>


