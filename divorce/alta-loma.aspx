﻿<%@ Page Title="Alta Loma CA Attorney & Lawyer, Child Support, Child Custody" Language="C#" MasterPageFile="~/MasterPages/Divorce.master" AutoEventWireup="true" CodeFile="alta-loma.aspx.cs" Inherits="divorce_alta_loma" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <meta name="description" content="Alta Loma CA attorneys at the Law Offices of Marc Grossman provide free initial consultations for your divorce. Contact us today to schedule your appointment." />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="PageTitleH1" Runat="Server">
    <h1 id="pageTitle">Filing for Divorce in Alta Loma</h1>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" Runat="Server">
    <p>Your considering filing for divorce. Now what? Who do you speak to? What will happen to my children in the divorce? Do I need a 
    <a href="http://www.wefight4you.com/divorce/Default.aspx" title="California Divorce Attorney">divorce attorney</a>? You're not alone in asking these questions. At the Law Offices of Marc Grossman we help people just like you through their divorce every day.</p> 
<h3>Why should you contact us</h3>
<p>Our Law Firm has been helping people like you file for divorce since 1998. We have years of experience with handling uncontested divorces and contested divorces. We have handled minor divorce cases to multi-million dollar, multiple property disputes. When it comes to the divorce process we've seen it all.</p>
<p>On top of experience we believe in keeping the divorce cost as low as possible. We found over the years that by living by that philosophy satisfies our clients, and satisfied clients is the livelihood of a Law Firm.</p>
<h3>Family Law Services</h3>
<ul>
<li>Child Custody</li>
<li>Child Support</li>
<li>Alimony</li>
<li>Spousal Support</li>
<li>divorce mediation</li>
</ul>
</asp:Content>

