﻿<%@ Page Title="Chino Hills Divorce Lawyer" Language="C#" MasterPageFile="~/MasterPages/SiteMainInternal.master" AutoEventWireup="true" CodeFile="chino-hills.aspx.cs" Inherits="divorce_chino_hills" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
<meta name="description" content="Chino Hills Divorce Attorneys that fight for you. The Law Offices of Marc Grossman is a full service law firm that can help you with divorce, child custody, child visitation, Alimony, Property Division, Divorce Mediation. "/>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainLogo" Runat="Server">
<a href="http://www.wefight4you.com" title="Chino Hills Divorce Lawyers">
<img src="http://www.wefight4you.com/images/chino-hills-ca-attorneys.jpg" alt="Chino hills Divorce Attorneys" />
</a>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="PageTitleH1" Runat="Server">
<h1 id="pageTitle">Chino Hills Divorce Lawyers</h1>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="MainContent" Runat="Server">
<p>The Law Offices of Marc Grossman is a full service Chino Hills Divorce Law Firm. Our law firm has represented 
    <a href="http://www.wefight4you.com/lawyer/chino-hills.aspx" title="Chino Hills">Chino Hills </a>clients in small divorces to large multi-property, six figure disputes. We have 6 attorneys on staff and a Family Law Case Manager available to you when you need us. We understand what you're going through and make sure that we make you feel comfortable when you visit our law firm for the first time.  
</p>
<h3>Should I hire an Attorney for my Chino Hills Divorce?</h3>
<p>The decision to hire a Chino Hills Divorce Attorney is a matter of hiring the right person for the job. Would you hire a mechanic to do brain surgery? I hope not. Our years of experience and honesty will speak for itself. We offer FREE Initial Divorce Consultations so that you get to know our law firm to see if we are the right choice for you.</p>
<p><strong>Why Should You Hire Us?</strong></p>
<p>Our Law Firm thinks about your well being first and pocket book second. While we don't make decisions for you, our recommendations are always geared to settle your divorce in a timely, affordable manner. We are a law firm built upon principles to uphold the highest degree of professionalism and guidance for our clients. We understand that you have many Chino Hills Divorce Attorneys to choose from, and thank you for visiting our website.</p>
<h3>Divorce Services</h3>
<ul>
</li>
<li>child custody</li>
<li>Collaborative Divorce</li>
<li>Child visitation</li>
<li>child support</li>
<li>alimony</li>
<li>property division</li>
<li>divorce mediation</li>
</ul>
</asp:Content>

