﻿<%@ Page Title="Corona CA Divorce Attorney & Lawyer, Child Support and Child Custody" Language="C#" MasterPageFile="~/MasterPages/Divorce.master" AutoEventWireup="true" CodeFile="corona.aspx.cs" Inherits="divorce_corona" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
<meta name="description" content="Corona California Divorce Attorneys that provide Free intial divorce consultations. Contact us today to schedule an appointment if you live in Corona." />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="PageTitleH1" Runat="Server">
<h1 id="pageTitle">The Corona Divorce Attorney that you need</h1>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" Runat="Server">
<p>The marriage is over. You must hire a Corona Divorce attorney. Some may be embarrassed by the fact of divorce and what about the children, your way of life; what will happen now? Our Law Firm can go into how we have been open since 1998 and we've handled millions of cases and that we're the best divorce attorney in <a href="http://www.wefight4you.com/lawyer/corona.aspx" title="corona">Corona</a> or even the world but, we're not. We are going to let you know that we are here for YOU.  "YOU" is the reason our attorneys went to school and study the Law for years. "YOU" is the reason why we got into Law in the first place. "YOU" is why the Law Offices of Marc Grossman exists.</p>
<h3>FREE Initial Divorce Consultations</h3>
<p>The first divorce consultation is FREE. Wow, you'll let me speak to a <a href="http://www.wefight4you.com/divorce/Default.aspx" title="California divorce attorney">divorce attorney</a> for FREE? Yes, that's because we understand that you have choices. We're not the only Corona divorce lawyer in town. We're professionals at what we do. Which means if we can save you money and hassle we will. It also means that we're confident once you meet us you'll hire us. Consider the consultation as a civil service. We tell you what you can expect, we get to know your situation and at last we tell you the divorce cost.</p> 
<h3>Family Law Services</h3>
<ul>
<li>Child Custody</li>
<li>Child Support</li>
<li>Alimony</li>
<li>Spousal Support</li>
<li>divorce mediation</li>
</ul>
</asp:Content>

