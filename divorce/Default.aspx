﻿<%@ Page Title="California Divorce Attorneys & Lawyers from the Law Offices of Marc Grossman" Language="C#" MasterPageFile="~/MasterPages/Divorce.master" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="divorce_divorce" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
<meta name="keywords" content="lawyer, attorney, law firm, law office, legal advice, divorce, family law, child custody, child support, spousal support, alimony, visitation, spousal maintenance, father's rights, property division, asset protection, paternity, modification, enforcement, lawyer, attorney," />
<meta name="description" content="Experienced Divorce attorneys that can take care of your case? The attorneys at the Law Offices can help you whether you need a divorce attorney or a child support attorney" />
<link rel="canonical" href="http://www.wefight4you.com/divorce/Default.aspx" />
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="PageTitleH1" Runat="Server">
<h1 id="pageTitle">California divorce attorneys can help you through your divorce</h1>
    <p style="text-align:center; color:white; font-weight:bold;"><a href="#divorce-process">Divorce Process</a> | <a href="#steps-of-a-divorce">Steps of a Divorce</a> | <a href="#division-of-divorce-debt">Division of Divorce Debt</a> | <br /><a href="#student-loan-debt">Student Loan Debt and Settlement</a> | <a href="#child-custody">Child Custody</a> | <a href="#child-support">Child Support</a></p>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="MainContent" Runat="Server">
<h2>Struggling with a California divorce and need an attorney?</h2>
<p>One of the hardest things to go through is a divorce. It's important to have a 
    California divorce attorney on your side that knows the law and will fight for you. Since 1998 The Law Offices of 
   Marc E. Grossman has helped clients just like you settle their 
     case and get them what they are entitled to.</p>
<p>When you are faced with divorce,<b> you may not know what to do</b>.  You may not know how to proceed, that’s why you need the entire team of compassionate 
    attorneys at the Law Offices of Marc E. Grossman can help guide you through the 
    process.</p>
    <p style="text-align:center"><a href="http://www.wefight4you.com/divorce/divorce-questions.aspx">
    <img src="http://www.wefight4you.com/images/divorce-questions.jpg" alt="Divorce Questions"/>
    </a>
    </p>
    <h3 style="text-align:center">Locations We Serve</h3>
<hr />
<div style="width:497px;">
<!--left-->
<div style="width:240px; padding-right:8px; float:left;"><ul>
            <li><a href="http://www.wefight4you.com/lawyer/alta-loma.aspx" title="Alta Loma Divorce Attorney" style="text-decoration: none;"> Alta Loma</a></li>
			<li><a href="http://www.wefight4you.com/lawyer/claremont.aspx" title="Divorce lawyer claremont" style="text-decoration: none;">Claremont</a></li> 
			<li><a href="http://www.wefight4you.com/lawyer/chino.aspx" title="Divorce lawyer chino" style="text-decoration: none;">Chino</a></li> 
			<li><a href="http://www.wefight4you.com/lawyer/chino-hills.aspx" title="Divorce lawyer chino hills" style="text-decoration: none;">Chino Hills</a></li> 
			<li><a href="http://www.wefight4you.com/lawyer/corona.aspx" title="Divorce lawyer corona" style="text-decoration: none;">Corona</a></li> 
			<li><a href="http://www.wefight4you.com/lawyer/diamond-bar.aspx" title="Divorce lawyer diamond bar" style="text-decoration: none;">Diamond Bar</a></li>						
			<li><a href="http://www.wefight4you.com/lawyer/fontana.aspx" title="Divorce lawyer fontana"  style="text-decoration: none;">Fontana</a></li> 	
			<li><a href="http://www.wefight4you.com/lawyer/glendora.aspx" title="Divorce lawyer glendora"  style="text-decoration: none;">Glendora</a></li> 	
			<li><a href="http://www.wefight4you.com/lawyer/la-puente.aspx" title="La Puente Workers Comp Attorney" style="text-decoration: none;">La Puente Workers Compensation</a></li> 
			<li><a href="http://www.wefight4you.com/bankruptcy/la-puente.aspx" title="La Puente Bankruptcy" style="text-decoration: none;">La Puente Bankruptcy</a></li>

            </ul>
            </div>
            <!--right-->
<div style="width:240px; padding-left; float:right;">
<ul>
<li><a href="http://www.wefight4you.com/lawyer/la-verne.aspx" title="La Verne Divorce Attorney" style="text-decoration: none;"> La Verne</a></li>
			<li><a href="http://www.wefight4you.com/lawyer/mira-loma.aspx" title="Mira Loma Divorce Attorney" style="text-decoration: none;"> Mira Loma</a></li>
			<li><a href="http://www.wefight4you.com/lawyer/montclair.aspx" title="Divorce lawyer montclair" style="text-decoration: none;">Montclair</a></li> 
			<li><a href="http://www.wefight4you.com/lawyer/ontario.aspx" title="Divorce lawyer ontario" style="text-decoration: none;">Ontario</a></li> 
			<li><a href="http://www.wefight4you.com/lawyer/pomona.aspx" title="Divorce lawyer pomona" style="text-decoration: none;">Pomona</a></li>

			<li><a href="http://www.wefight4you.com/lawyer/rancho-cucamonga.aspx" title="Divorce lawyer rancho cucamonga" style="text-decoration: none;">Rancho Cucamonga</a></li>
			<li><a href="http://www.wefight4you.com/lawyer/riverside.aspx" title="Divorce lawyer riverside" style="text-decoration: none;">Riverside</a></li>
			<li><a href="http://www.wefight4you.com/lawyer/san-bernardino.aspx" title="San Bernardino Divorce Attorneys" style="text-decoration: none;">San Bernardino</a></li> 
			<li><a href="http://www.wefight4you.com/lawyer/san-dimas.aspx" title="San Dimas Divorce Attorney" style="text-decoration: none;"> San Dimas</a></li>
			<li><a href="http://www.wefight4you.com/lawyer/upland.aspx" title="Divorce lawyer upland" style="border-bottom-width: 0; text-decoration: none;">Upland</a></li>
</ul>
</div>

</div>

<h3><a name="divorce-process">The Divorce Process</a></h3>
<p>1. <b>Division of Community and/or marital property</b><br />
California is a community property state.  All property acquired by a married person during that marriage is presumed to be community property.  Community property is divided equally upon termination of the marriage.  
As a spouse you may be entitled to: 
</p>
<ul>
<li>A portion of your spouse’s retirement account or pension</li>
<li>A portion of your family home regardless of whom is named on the deed</li>
<li>A portion of the personal property owned by the community</li>
<li>A portion of the value of your spouse’s business</li>
<li>A portion of the value of your spouse’s professional practice goodwill.</li>
</ul>
<h3><a name="division-of-divorce-debt">2. <b>Division of debt</b></a><br /></h3>
<p>
In California during marriage, the marital community is responsible for all debts of either spouse, whether incurred before or during marriage.  However upon divorce, the debts of the spouses, which were incurred before marriage, will be confirmed to the spouse who incurred the debt.  It is very important to make sure that your debts are fairly settled and assigned, so that when you start over you do not pay what you should not have to.
</p>
<h3><a name="student-loan-debt"><b>Student Loan Debt and Settlement:</b></a></h3>
<p>
Student loan debts are assigned to the student spouse at divorce.  Did you know that payments on your spouse’s student loans with community funds means that you (through the community) are entitled to reimbursement for these payments?  This right to reimbursement is something you should investigate before making important decisions
</p>
<h3><a name="child-custody">3. <b>Custody of Children</b></a></h3>
<p>
Deciding what is best for the children is one of the most important questions to be answered during a divorce.  The court attempts to ensure that the whatever decree of custody is issued, that it is sensitive to all of the needs of the children.  Divorce is hard enough on children, and we share your concern that it not be made any harder that it already is.  We can help you by advocating to ensure that the court’s decision is sensitive to your child’s need for stability during this troubled time.  Remember that if you have children and are faced with divorce, that the children have interests which must be protected as well.
</p>
<h4>Deciding Factors of Child Visitation</h4>
<ul>
<li><a href="http://www.wefight4you.com/family-law/child-visitation.aspx">Child Visitation</a></li>
<li>Joint Visitation</li>
<li>Sole Visitation</li>
</ul>
<h3><a name="child-support">4. <b>Payment of child support</b></a></h3>
<p>Parents have a duty to support their children.  At the Law Offices of Marc Grossman, we have a team of trained advocates, who can make sure your children get the fair award of support that they deserve.  
Child support should be fair, if the award of support is no longer fair due to a recent loss of income, call us and we can help you to modify it to ensure that it is fair for all involved.
</p>
<h2><a name="steps-of-a-divorce">The Steps of a Divorce</a></h2>
<p><b>The petition or summons</b></p>
<ul>
<li>the divorcing party</li>
<li>children if any</li>
<li>separate or community property</li>
<li>child custody, and or child support</li>
</ul>
<p><b>The papers have to be served</b></p>
<ul>
<li>service of process</li>
<li>the other spouse needs to sign</li>
<li>after 6 months it goes into default</li>
</ul>
<p><b>The other spouse cannot</b></p>
<ul>
<li>take the children out of state</li>
<li>sell any property</li>
<li>borrow against property</li>
<li>borrow or sell insurance held for the other spouse.</li>
</ul>
<p><b>The other spouse (respondent) faq</b></p>
<ul>
<li>the respondent can respond to the petition stating he/she agrees
which will lead to no court
</li>
    <br />
<ul><p><b>Both parties must disclose their assets (Conduct Discovery)</b></p>
<li>liabilities</li>
<li>income</li>
<li>expenses</li>
</ul>
</ul>
<p><b>We offer Free initial consultations for your divorce case</b></p>
<p>E-mail us or call us toll free at 1-888-407-9068 to schedule a meeting with an experienced family law and divorce attorney. 
    We provide the following services;</p>
<ul>
<li>Divorce</li>
<li>Child Support</li>
<li>Child Custody and Visitation</li>
<li>Alimony/Spousal Maintenance</li>
<li>Property Distribution</li>
<li>Modification and Enforcement of Decrees</li>
<li>Adoption</li>
<li>Paternity</li>
<li>Property and Debt Disputes</li>
<li>Domestic Violence and Restraining Orders</li>
<li>Premarital Agreements/Prenuptial Agreements</li>
<li>Mediation</li>
</ul>
<h3>We offer free initial divorce consultations</h3>
<p><a href="http://www.wefight4you.com/Forms/contact.aspx" rel="nofollow">E-mail us</a> or call us toll free at 1-888-407-9068 to schedule a meeting with an experienced family law and divorce attorney.</p>
</asp:Content>
