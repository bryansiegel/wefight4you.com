﻿<%@ Page Title="Claremont CA Divorce Attorney & Lawyer, Child Support and Child Custody" Language="C#" MasterPageFile="~/MasterPages/Divorce.master" AutoEventWireup="true" CodeFile="claremont.aspx.cs" Inherits="divorce_claremont" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
<meta name="description" content="Claremont California Divorce Attorneys that provide Free initial Divorce Consultations. Contact us today to schedule your appointment" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="PageTitleH1" Runat="Server">
<h1 id="pageTitle">Claremont Divorce Attorneys</h1>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" Runat="Server">
<p>You're filing for divorce in Claremont. You must have many questions and who better to get the answers from than a Claremont divorce lawyer? How about getting your questions answered for FREE? At the Law Offices of Marc Grossman we provide FREE initial consultations for 
    divorce</a>. All that you need to do is call to schedule an appointment with our 
    <a href="http://www.wefight4you.com/lawyer/upland.aspx" title="Upland Law Firm">Upland office</a>.</p>
<p>When you come by our office for your consultation we go through the situation of your <a href="http://www.wefight4you.com/divorce/Default.aspx" title="divorce">divorce</a>, what you can expect from the process and the divorce cost.  The Law Offices of Marc Grossman has helped hundreds of Claremont divorce clients with their case and would love the opportunity to help you with yours. </p>
<h3>Family Law Services</h3>
<ul>
<li>Child Custody</li>
<li>Child Support</li>
<li>Alimony</li>
<li>Spousal Support</li>
<li>divorce mediation</li>
</ul>

</asp:Content>

