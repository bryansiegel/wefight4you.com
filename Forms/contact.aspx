<%@ Page Title="Contact the Law Offices of Marc Grossman Attorney" Language="C#" MasterPageFile="~/MasterPages/SiteMainInternal.master" AutoEventWireup="true" CodeFile="Contact.aspx.cs" Inherits="Forms_Contact" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
<META NAME="ROBOTS" CONTENT="NOINDEX, NOFOLLOW">

    <style type="text/css">
        .style2
        {
            font-size: x-large;
        }
    </style>
    </asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainLogo" Runat="Server">
    <img src="../images/teamofattorneys2.jpg"  style="width: 800px; height: 244px"/>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="PageTitleH1" Runat="Server">
    <h1 id="pageTitle">Contact Us</h1>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="MainContent" Runat="Server">
   <table style="width: 304px; height: 265px;">
                        <tr>
                            <td colspan="2">
                                <b style="font-size:large; text-align:center">Free Initial Consultation</b></td>
                        </tr>
                        <tr>
                            <td valign="top">
                                Name:               
                            </td>
                            <td>
                                 <asp:TextBox ID="txtName" runat="server" CausesValidation="True" EnableViewState="False"></asp:TextBox>
                                 <asp:RequiredFieldValidator ID="RequiredFieldValidator1" EnableViewState = "false" runat="server" 
            ControlToValidate="txtName" ErrorMessage="Name is Required" Display="Dynamic" BackColor="#eddea3"></asp:RequiredFieldValidator>
        </td>
                        </tr>
                        <tr>
                            <td valign="top">
                                Telephone:</td>
                            <td valign="top" style="width: 128px">
                                <asp:TextBox ID="txtTelephone" runat="server" EnableViewState="False"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator3" EnableViewState="false" runat="server" 
                                     ControlToValidate="txtTelephone" ErrorMessage="Telephone is Required" Display="Dynamic" BackColor="#eddea3"></asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr>
                            <td valign="top">
                                Email:</td>
                            <td style="width: 128px">
                                <asp:TextBox ID="txtEmail" EnableViewState="false" runat="server"></asp:TextBox>
        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" EnableViewState="false" runat="server" 
            ControlToValidate="txtEmail" ErrorMessage="Email is Required" Display="Dynamic"  BackColor="#eddea3"></asp:RequiredFieldValidator></td>
                        </tr>
                        <tr>
                            <td>
                                Case Type:</td>
                            <td style="width: 128px">
                                <asp:DropDownList ID="ddCaseType" runat="server" Width="144px" EnableViewState="False">
            <asp:ListItem Selected="True">Select Case Type....</asp:ListItem>
                                    <asp:ListItem>not sure</asp:ListItem>
            <asp:ListItem>divorce</asp:ListItem>
            <asp:ListItem>civil</asp:ListItem>
            <asp:ListItem>bankruptcy</asp:ListItem>
                                    <asp:ListItem>family law</asp:ListItem>
                                    <asp:ListItem>corrections, prison, parole</asp:ListItem>
                                    <asp:ListItem>criminal defense</asp:ListItem>
                                    <asp:ListItem>discrimination, harassment</asp:ListItem>
                                    <asp:ListItem>dog bites</asp:ListItem>
                                    <asp:ListItem>employment</asp:ListItem>
                                    <asp:ListItem>fire damage homes</asp:ListItem>
                                    <asp:ListItem>fire insurance claims</asp:ListItem>
                                    <asp:ListItem>immigration</asp:ListItem>
                                    <asp:ListItem>insurance bad faith</asp:ListItem>
                                    <asp:ListItem>motorcycle accidents</asp:ListItem>
                                    <asp:ListItem>auto accidents</asp:ListItem>
                                    <asp:ListItem>personal injury</asp:ListItem>
                                    <asp:ListItem>workers compensation</asp:ListItem>
                                    <asp:ListItem>work injury</asp:ListItem>
                                    <asp:ListItem>wrongful death</asp:ListItem>
        </asp:DropDownList></td>
                        </tr>
                        <tr>
                            <td>
                                Tell Us about your case</td>
                            <td style="width: 128px">
                                <asp:TextBox ID="txtDescribeYourCase" Rows="4" TextMode="MultiLine" 
                                    runat="server" Width="142px" EnableViewState="False"></asp:TextBox>
                                    <br />
                                <asp:Button ID="btnSubmit" Text="Submit" EnableViewState="false" runat="server" 
                                    onclick="btnSubmit_Click" /></td>
                        </tr>
                        </table>
   

</asp:Content>

