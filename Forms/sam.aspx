﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="sam.aspx.cs" Inherits="Forms_sam" %>

<%@ Register assembly="Telerik.Web.UI" namespace="Telerik.Web.UI" tagprefix="telerik" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>

    <link href="http://www.wassersonlaw.com/style.css" rel="stylesheet" type="text/css" />
    <link href="http://www.wassersonlaw.com/mainstyle2.css" rel="stylesheet" type="text/css" />
    <link href="http://www.wassersonlaw.com/fancy.css" rel="stylesheet" type="text/css" />
    <link href="http://www.wassersonlaw.com/jqueryslidemenu.css" rel="stylesheet" type="text/css" />
    <link href="http://www.wassersonlaw.com/rtl.css" rel="stylesheet" type="text/css" />
    <link href="http://www.wassersonlaw.com/s3slider.css" rel="stylesheet" type="text/css" />
    <link href="http://www.wassersonlaw.com/colorbox.css" rel="stylesheet" type="text/css" />

</head>
<body>
    <form id="form1" runat="server">
     <div id="wrapper"> 
	<div id="container"> 
		<div id="top"> 
									<div id="logoimage"> 
				<a href="http://www.wassersonlaw.com"><img src="http://www.wassersonlaw.com/images/logo.jpg" alt="" height="100" /></a> 
			</div> 
						
								</div><!-- end #top --> 
		<div id="topnavigation"> 
		
				
							
			<div id="topnav"> 
										<div id="myslidemenu" class="jqueryslidemenu"> 
					<ul> 
						<li class="home current_page_item"> 
							<a href="http://www.wassersonlaw.com" title="Upland Divorce Attorney">Home</a> 
						</li> 
						<li class="page_item page-item-6"><a href="http://www.wassersonlaw.com/Overview.shtml" title="Firm Overview">Firm Overview</a>  
</li> 
<li class="page_item page-item-9"><a href="http://www.wassersonlaw.com/PracticeAreas.shtml" title="Contact">Practice Areas</a></li> 
<li class="page_item page-item-7"><a href="http://www.wassersonlaw.com/Collaborative-Divorce.shtml" title="Portfolio">Divorce</a></li> 
<li class="page_item page-item-8"><a href="http://www.wassersonlaw.com/Probate-Estate-Planning.shtml" title="Blog">Probate Estate Planning</a></li> 
<li class="page_item page-item-9"><a href="http://www.wassersonlaw.com/DynamicAttorneys.shtml" title="Attorney Profile">About</a></li> 


					</ul> 
					</div> 
			</div><!-- end #topnav --> 
						<div id="topsearch"> 
				 
<p style="color:#ba5409; font-size:medium; font-weight:bold;">(800)-781-8652</p> 
		</div><!-- end #topsearch --> 
					</div><!-- end #topnavigation --> 
		<div id="header-inner"> 
				<h1 class="pagetitle">Contact Us</h1> 
			</div><!-- end #header-inner --> 
<div id="content"> 
	<div id="content-left"> 
		<div id="maintext"> 
				</div>

					<table align="center" style="width: 66%">
						<tr>
							<td style="width: 165px"><strong>Name:</strong></td>
							<td>
                              
                                <asp:TextBox ID="txtname" runat="server"></asp:TextBox>
                              
                              
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" 
                                    ControlToValidate="txtname" Display="Dynamic" ErrorMessage="*Required"></asp:RequiredFieldValidator>
                              
                              
                            </td>
						</tr>
						<tr>
							<td style="width: 165px"><strong>Telephone:</strong></td>
							<td>
                                
                                <asp:TextBox ID="txttelephone" runat="server"></asp:TextBox>
                                
                                
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" 
                                    ControlToValidate="txttelephone" Display="Dynamic" ErrorMessage="*Required"></asp:RequiredFieldValidator>
                                
                                
                            </td>
						</tr>
						<tr>
							<td style="width: 165px"><strong>Email:s</strong></td>
							<td>
                                
                                
                                <asp:TextBox ID="txtemail" runat="server"></asp:TextBox>
                         
                                
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" 
                                    ControlToValidate="txtemail" ErrorMessage="*Required"></asp:RequiredFieldValidator>
                         
                                
                            </td>
						</tr>
						<tr>
							<td style="width: 165px"><strong>Comments/Questions:</strong></td>
							<td>
                               
                                <asp:TextBox ID="txtcomments" runat="server" TextMode="MultiLine"></asp:TextBox>
                               
                            </td>
						</tr>
						<tr>
							<td style="width: 165px">&nbsp;</td>
							<td>
							</td>
						</tr>
						<tr>
							<td style="width: 165px">&nbsp;</td>
							<td>
         
                                <asp:Button ID="btnsubmit" runat="server" Text="Submit" 
                                    onclick="btnsubmit_Click" />
                            </td>
						</tr>
					</table>
				<br />
				
<!-- You can start editing here. --> 
 
 
			<!-- If comments are closed. --> 
		<!-- <p class="nocomments">Comments are closed.</p> --> 
 
	
 
		</div><!-- end #maintext --> 
	</div><!-- end #content-left --> 
	<!-- end #content-right --> 
	<div class="clr"></div><!-- end clear float --> 
</div><!-- end #content --> 
 
		<!-- end #footer --> 
	</div><!-- end #container -->	
</div><!-- end #wrapper -->	

    </form>
</body>
</html>
