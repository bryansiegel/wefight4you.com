﻿<%@ Page Title="Capítulo 7 abogado - Capítulo 7 quiebra, Upland, Montclair, Claremont, Chino Hills" Language="C#" MasterPageFile="~/MasterPages/SiteMainInternal.master" AutoEventWireup="true" CodeFile="capitulo-7.aspx.cs" Inherits="espanol_capitulo_7" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
<meta http-equiv="Content-Language" content="ES" /> 
<meta name="description" content="¿Necesita un abogado de quiebra de capítulo 7 experimentado para manejar el caso de quiebra 7 capítulo? En contacto con los abogados en las oficinas de ley de Marc Grossman para un libre consulation inicial." />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainLogo" Runat="Server">
<img src="../images/i-prac-bankruptcy.jpg" style="width: 802px; height: 228px"/>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="PageTitleH1" Runat="Server">
<h1 id="pageTitle">Capítulo 7 abogado - Capítulo 7 quiebra</h1>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="MainContent" Runat="Server">
<p>El capítulo 7 es la sección más utilizada de la quiebra de código.  Más de la mitad de las quiebras registradas para en los Estados Unidos se presentan en virtud del capítulo 7.  En el capítulo 7 como el deudor usted debe renunciar a su propiedad no exentos a un fideicomisario de quiebras. Un fideicomisario de quiebras suele ser un abogado nombrado por el Tribunal.  Su deber es distribuir los fondos lo exija la legislación.  El fideicomisario será liquidar la propiedad; después de la propiedad es liquidada se dividirlo entre los acreedores no seguras cuales son adeudados dinero.  Las recaudaciones que el fideicomisario recibe de la venta de la propiedad no será suficiente para pagar todas las deudas, por lo que va a negociar el fideicomisario para reducir la cantidad debida.</p>
<p>Es muy importante que como un deudor está honesto acerca de sus activos cuando de presentación de quiebra.  Esto es porque si se intenta ocultar propiedad el Tribunal puede negarse a cumplir con la deuda.  Usted puede presentar sólo para el capítulo 7 alivio una vez cada ocho años. </p>
<p>Existen algunas deudas que no son dados de alta durante la bancarrota.  Los más comunes son préstamos estudiantiles, pagos de apoyo infantil o spousal y algunos pagos de impuestos.  Algunos propiedad queda exento de liquidación.  Generalmente se trata de ropa y bienes domésticos o un automóvil que es más antiguo, pero no muy valiosa.  Estos artículos exentos varían de Estado a Estado. </p>
<p>Segura acreedor derecho a la propiedad colateral generalmente continúa a pesar de que la deuda es descargada.  Esto significa, por ejemplo, que si tienes un automóvil que está comprando en los pagos, independientemente de que la deuda ha sido descargada por el Tribunal, el acreedor puede todavía recuperar el automóvil en que él tiene un interés propiedad colaterales.  Si usted desea mantener el auto se debe reafirmar la deuda y seguir haciendo los pagos, si no desea mantener el auto, usted debe renunciar.</p>
</asp:Content>

