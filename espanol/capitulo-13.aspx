﻿<%@ Page Title="Capítulo 13 - quiebra de capítulo 13, Upland, Montclair, Claremont, Chino Hills" Language="C#" MasterPageFile="~/MasterPages/SiteMainInternal.master" AutoEventWireup="true" CodeFile="capitulo-13.aspx.cs" Inherits="espanol_capitulo_13" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <meta http-equiv="Content-Language" content="ES" /> 
<meta name="description" content="¿Necesita un abogado de quiebra de capítulo 13 experimentado para manejar el caso de quiebra 13 capítulo? En contacto con los abogados en las oficinas de ley de Marc Grossman para un libre consulation inicial." />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainLogo" Runat="Server">
    <img src="../images/i-prac-bankruptcy.jpg" style="width: 802px; height: 228px"/>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="PageTitleH1" Runat="Server">
    <h1 id="pageTitle">quiebra de capítulo 13</h1>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="MainContent" Runat="Server">
    <p>En alivio de la quiebra de capítulo 13, como el deudor, usted conservar la propiedad y posesión de todos sus activos, pero debe dedicar una parte de sus ingresos futuros para pagar los acreedores.  Esto generalmente dura un período de duración de tres a cinco años.  Hay muchos factores que influyen en el período de reembolso.  Estos incluyen el valor de su propiedad, la cantidad de sus ingresos y gastos mensuales. </p>
</asp:Content>

