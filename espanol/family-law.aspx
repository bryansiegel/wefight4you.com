﻿<%@ Page Title="Abogado con Experiencia en Derecho Familiar en General" Language="C#" MasterPageFile="~/MasterPages/SiteMainInternal.master" AutoEventWireup="true" CodeFile="family-law.aspx.cs" Inherits="espanol_family_law" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
<meta name="keywords" content="Abogado, con, Experiencia, en, Derecho Familiar, General,  oficina legal, Marc E. Grossman, ubicada, Upland, California., concentra, sus, &#225;reas, practica, con, el, prop&#243;sito, ayudar, a, las, personas, en, busca, abogados, quienes, otorguen, resultados, efectivos, luchen, arduamente, para, hacer, valer, sus, as&#237;, usted , seres, queridos, puedan, tener, la, tranquilidad, vida, que, siempre, han, deseado, Divorcio, custodia de menores, Manutenci&#243;n de menores, Manutenci&#243;n de c&#243;nyuge, Divisi&#243;n de propiedades, adopciones, visitaci&#243;n, paternidad, mediaci&#243;n, Ordenes de restricci&#243;n, Contratos prenupciales, Y, m&#225;s">
<meta http-equiv="Content-Language" content="ES" /> 
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainLogo" Runat="Server">
    <img alt="Abogado con Experiencia en Derecho Familiar en General" src="../images/i-espa-espanolfamilylaw.jpg"  style="width: 802px; height: 228px"/>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="PageTitleH1" Runat="Server">
<h1 id="pageTitle">Derecho Familiar en General Centro de información</h1>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="MainContent" Runat="Server">
<p>Mas del 67% de los recién casados creen que el conflicto más serio de su primer año de matrimonio tienen que ver con dinero.</p>
        <h3>Preguntas Frecuentes sobre Ley de Familia</h3>
        <p>
          <strong>Pregunta: ¿Cuál es la definición legal de Matrimonio?</strong>
        </p>
        <p>
          <strong>Respuesta:</strong> Muchos estados definen el matrimonio como un contrato civil entre un hombre y una mujer en el cual ellos son esposo y esposa. La manera corriente de casarse es obtener una licencia de matrimonio de un oficial autorizado por el estado y enseguida participar en una ceremonia civil formal o una ceremonia religiosa.</p>
        <p>
          <strong>Pregunta: ¿Cuáles son los efectos legales del matrimonio?</strong>
        </p>
        <p>
          <strong>Respuesta:</strong> Hay varias leyes federales y estatales que benefician a una pareja de casados.</p>
      <h2>Abogado con Experiencia en Derecho Familiar en General</h2>
      <p>La oficina legal de Marc E. Grossman ubicada en Upland, California., tiene el propósito de ayudar a las personas en busca de abogados con experiencia en el derecho familiar. Somos abogados que ofrecemos resultados efectivos y luchamos arduamente para hacer valer sus derechos. Queremos que usted y sus seres queridos puedan tener la tranquilidad de vida que siempre han deseado.</p>
      <p>Si no se puede llegar a un acuerdo, estaremos siempre preparados para un litigio en las cortes para que usted pueda obtener resultados favorables. Nuestra extensa práctica se concentra en casos tales como: divorcio, custodia de menores, manutención de menores, manutención de cónyuge, división de propiedades, adopciones, visitación, paternidad, mediación, ordenes de restricción, contratos prenupciales, y más.</p>
      <p>Para una consulta inicial gratuita y evaluación de su caso, <a href="/Forms/contact.aspx" rel="nofollow">contacte a nuestras oficinas legales</a> para más información. Ofrecemos servicios relacionados con el derecho familiar en general, por favor llame al 888-407-9068 o al 909-297-5479. Nuestros abogados están siempre listos para otorgarle la asesoría que usted merece.</p>
      <h3>Ley de Familia: Generalidades</h3>
      <p>La ley familiar es el término que se aplica a las leyes y reglamentos que tienen que ver con las relaciones de la familia. Las reglas legales de familia no solo definen la relación entre los miembros de la familia sino también entre la familia y la sociedad en general. Más que ninguna otra área de las ley la ley de familia refleja los valores que la sociedad comparte en lo que se refiere a como deben tratarse las personas que están relacionadas.</p>
      <p>Típicamente, un abogado de familia ayuda a la gente a mantener o romper las relaciones familiares. Las áreas específicas de representación generalmente incluyen el matrimonio y el planeamiento de las relaciones, divorcio, paternidad, custodia de niños y manutención de niños. Algunos abogados de familia también proveen asistencia en el área de adopción. 

Cuando usted enfrenta una decisión importante en su vida que tiene que ver con una relación familiar, el consejo y asistencia de un abogado experimentado en ley de la familia, como los del bufete Law Office of Marc E. Grossman en la Ciudad de Upland, es esencial.
	</p>
      <h3>Matrimonio </h3>
      <p>El matrimonio es tanto una unión legal y de negocios como es una relación romántica. Aun cuando las limitaciones y requisitos varían de estado a estado, las leyes básicas del matrimonio son similares. Todos los estados prohíben el matrimonio entre más de dos personas y entre familiares cercanos. Algunas de las limitaciones más comunes son:</p>
      <ul>
        <li>Se prohíbe el matrimonio entre hermanos y hermanas, entre hijos y padres y entre tíos y sobrinos o sobrinas.<br><br></li>
        <li>La edad mínima para casarse es generalmente 18 años. Muchos estados permiten el matrimonio de personas menores de 18 siempre y cuando los padres den su consentimiento.<br><br></li>
        <li>Uno o los dos novios deben residir en el estado por un periodo de tiempo específico.</li>
      </ul>
      <p>Muchos estados también requieren una ceremonia formal con alguna clase de testigos y un oficial público o religioso.</p>
      <p>Las leyes federales y estatales dan a las parejas casadas algunos beneficios que incluyen:</p>
      <ul>
        <li>privilegios en los impuestos</li>
        <li>derechos a beneficios tales como seguro social, desempleo, pensión de veteranos; y beneficios de asistencia pública</li>
        <li>derechos de herencia bajo las leyes estatales de sucesión sin testamento</li>
        <li>derechos por perdida de consorcio</li>
        <li>derechos a hacer decisiones médicas si es que un cónyuge esta deshabilitado; y</li>
        <li>el derecho legal de protección de las comunicaciones maritales</li>
      </ul>
      <p>
Porque el matrimonio es un acuerdo legal, puede ser sabio consultar con un abogado en el bufete Law Office of Marc E. Grossman en la ciudad de Upland acerca de las ventajas de entrar en un acuerdo premarital.
	</p>
      <h3>Divorcio</h3>
      <p>El divorcio es un método para terminar un contrato de matrimonio entre dos individuos. Desde el punto de vista legal, el divorcio devuelve el derecho de casarse con otra persona. El proceso también divide legalmente los bienes de la pareja y determina el cuidado y custodia de los niños. Cada estado enfrenta esto de manera diferente. Sin embargo, la mayoría de los estados siguen los mismos principios básicos y usan normas relativamente uniformes.</p>
      <p>En algunos estados se necesita probar que hay culpa, comúnmente llamado base para obtener el divorcio. La mayoría de los estados permiten por lo menos una forma de divorcio sin-culpa, en el cual no se requiere probar la culpa. Si hay divorcio sin-culpa en su estado, cualquiera de las dos partes pueden obtener el divorcio aun cuando la otra parte no lo quiera. En algunos estados hay divorcio con-culpa y sin-culpa. Un abogado con experiencia puede ayudarle a pedir cualquiera de los dos tipos de divorcio.</p>
      <p>Lo primero que se debe decidir durante el divorcio es la pensión y manutención del cónyuge, división de la propiedad y, si es que hay niños, lo que se refiere a la custodia, visitas y manutención. Cuando los esposos están de acuerdo pueden obtener un divorcio rápidamente. Más típicamente, los esposos tienen disputas referente a los arreglos financieros para después del divorcio y referente al cuidado y custodia de los niños.</p>
      <p>Cada estado tiene diferentes leyes referentes a la división de la propiedad matrimonial. 13 estados siguen el sistema de propiedad comunitaria en el cual los bienes se reparten igualmente. El resto de los estados usan el sistema de repartición equitativa que divide la propiedad después de considerar un número de elementos de una manera que las cortes consideran justo según las circunstancias. Cualquier decreto de pensión y apoyo financiero continuo de un cónyuge al otro se hace en conjunto con la distribución de los bienes de los cónyuges. La división de la propiedad y pensión son casi siempre reclamados de modo que el consejo de un abogado de familia con experiencia le ayudará a tener el mejor resultado.</p>
      <h3>Custodia y Visitas de los Niños</h3>
      <p>El cuidado y la crianza de los niños después de un divorcio es casi siempre una fuente de continuo conflicto entre los padres que se divorcian. La custodia debe cubrir la custodia física o los derechos y responsabilidades del cuidado diario, y las actividades y responsabilidades y derechos asociados con la crianza. A veces la corte determina esto. En el pasado, las cortes daban de manera rutinaria la custodia a la madre y daban derechos de visita al padre, a veces también daban custodia física de los niños a los dos cónyuges con los niños residiendo en la casa de la madre. Hoy, las cortes han empezado a darse cuenta que a veces es en el mejor interés del niño residir con el padre y reversar los roles de los padres. Las cortes favorecen la crianza de los niños en forma conjunta en la cual los dos padres toman responsabilidad por la crianza de los hijos y permiten que estos residan donde es mas practico y donde pueden crecer más exitosamente.</p>
      <p>Los padres que se divorcian generalmente resuelven los problemas de custodia y visita tan pronto como se separan. Las cortes generalmente aceptan los acuerdos a que llegan los padres. Cuando la custodia es disputada, la mayoría de las cortes requieren que los padres participen en sesión de resolución de disputas. La mediación es un proceso alternativo en el cual los padres trabajan con un mediador privado para tratar de resolver alguno o todos los desacuerdos. Si la mediación no tiene éxito, la corte determinará la custodia. Aun cuando las leyes varían de estado a estado, la mayoría de las cortes alcanzan decisiones de custodia y visita después que examinan cual decisión es la que mejor sirve los intereses del niño. Las cortes casi siempre usan evaluaciones hechas por un experto privado que les ayuda a llegar a una determinación.</p>
      <p>Excepto cuando las dos partes se ponen de acuerdo, las cortes casi siempre imponen las normas de custodia y visita. Un horario de visita típico es el cual permite al padre que no tiene la custodia ver a los niños una noche a la semana, un fin de semana en un periodo de quince días y alguna porción de las vacaciones escolares y de verano. Si se cambia la orden de custodia y visita, el padre que quiere el cambio debe mostrar que las circunstancias han cambiado en forma radical.
Algunos estados solo consideran una petición de modificación dentro de dos años de la determinación inicial si es que se muestra que el niño esta en peligro con el horario actual. Para prevenir que los padres vayan de un estado a otro buscando una mejor resolución, algunos estados solo consideran cambios si el niño ha residido en ese estado por seis meses o mas.</p>
      <h3>Mantención del niño</h3>
      <p>Los padres biológicos tienen que apoyar financieramente a sus niños. Esta obligación generalmente llega hasta que el niño llega a la mayoría de edad (según la ley del estado puede ser 18 o 21 años) o hasta que se puede mantener solo. La responsabilidad de proveer apoyo en la forma de pagos regulares generalmente sucede cuando uno de los padres tiene la custodia primaria del niño. Una orden de apoyo financiero puede ser presentada durante o después del divorcio. Puede que cada padre sea responsable de pagar según como sea el arreglo de custodia. Una madre soltera también puede solicitar apoyo financiero para el niño y una orden de apoyo puede ser dada una vez que la paternidad ha sido establecida.</p>
      <p>En cada estado, la cantidad asignada se establece después que las necesidades del niño y las entradas de los padres han sido consideradas usando guías estatales específicas. El padre que paga debe pagar regularmente la cantidad ordenada. Si no se hacen los pagos al día, el padre se expone a castigos significativos. Cada estado tiene una oficina para el cumplimiento de los pagos. Además de las cortes, estas agencias tienen el poder de suspender licencias profesionales o de negocios, quitar la licencia de conducir o de recreación, requerir pagos adelantados o poner los que no pagan en la cárcel.</p>
      <p>Una vez que los pagos son ordenados, los dos padres tienen el derecho de pedir cambios. Algunos estados revisan las órdenes existentes en forma regular sin la necesidad de solicitudes especiales. Los padres que deben pagar se enfrentan con dificultades cuando piden que se reduzca el pago. Aun si las entradas del pagante son insuficientes para cumplir con las obligaciones de pago, la corte puede asignar una capacidad de entradas mayor y basar el pago en la capacidad de ganar más dinero. 
Porque los requisitos específicos varían de un estado a otro, los padres pueden beneficiarse del consejo y la participación de un abogado experimentado en asuntos de la ley de familia del bufete Law Office of Marc E. Grossman en la ciudad de Upland cuando se presentan asuntos de manutención de niños.
	</p>
      <h3>Adopción</h3>
      <p>La adopción es el proceso legal que permite a una familia hacer que un niño que no es biológicamente parte de la familia se convierta en su propio hijo y en parte de la familia. Cada adopción, sea nacional o extranjera, requiere la acción y aprobación de la corte para que sea finalizada.</p>
      <p>Cada estado tiene sus propias leyes. Hay procedimientos que se deben seguir para determinar si los padres adoptivos son calificados. Después de la adopción los niños adoptados generalmente reciben todos los beneficios que tienen los niños naturales y los padres le deben todos los deberes legales de cuidado y apoyo igual que a los niños nacidos del matrimonio.</p>
      <p>La adopción libera para siempre a los padres naturales de las responsabilidades financieras que les deben a sus hijos. En el pasado, la adopción quería decir que los padres de nacimiento perdían el privilegio de ver a sus niños o de estar envueltos en sus vidas. Sin embargo, especialmente en adopciones en el país, las reglas han cambiado y a veces los padres biológicos retienen ciertos derechos como lo es el poder mantener contacto con sus hijos después que la adopción es finalizada.</p>
      <p>
Los abogados de la ley de familia del bufete Law Office of Marc E. Grossman en la ciudad de Upland ofrecen servicios relacionados con la adopción y pueden ayudarle en todas las fases del proceso. 
	</p>
      <h3>Conclusión</h3>
      <p>Las relaciones familiares crean muchas consecuencias legales.
Si usted esta contemplando matrimonio o divorcio, o considera adoptar, un abogado experimentado en ley de familia del bufete Law Office of Marc E. Grossman en la ciudad de Upland puede explicarle las leyes que se aplican a su situación especifica y puede ayudarle en entender los efectos de la ley para que usted tome las mejores decisiones para usted y su familia.
	</p>
</asp:Content>

