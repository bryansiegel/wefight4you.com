﻿<%@ Page Title="Compensacion A Los Trabajadores - Accidente De Trabajo - La Puente, Upland, Van Nuys" Language="C#" MasterPageFile="~/MasterPages/SiteMainInternal.master" AutoEventWireup="true" CodeFile="compensacion-a-los-trabajadores.aspx.cs" Inherits="espanol_compensacion_a_los_trabajadores" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">

<meta http-equiv="Content-Language" content="ES" /> 
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainLogo" Runat="Server">
 <img src="../images/i-prac-workinjury.jpg" style="width: 802px; height: 228px"/>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="PageTitleH1" Runat="Server">
<h1 id="pageTitle">Compensacion A Los Trabajadores</h1>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="MainContent" Runat="Server">
<p> Compensación a los trabajadores es un tipo de póliza de seguro  la cuál la ley de California requiere que los empleadores en este Estado adquieran como protección para sus empleados en caso de que sufra alguna lesion ó accidente en el trabajo. Si un empleador no adquiere tal seguro, aún así un empleado lesionado sera protejido si tiene un caso valido por la agencia del Estado llamada “The Uninsured Employers’ Fund”.</p>
<h3>Algunos de los servicios que provee el seguro de Compensacion a los Trabajadores son:</h3>
<p> <strong>Beneficios de Incapacidad Temporal.</strong> Si un doctor que está en la “Red de Medicos Proveedores” la compañia de seguros de Compensación a los Trabajadores del empleador redactará un reporte médico certificando que en su opinión usted esta temporalment incapacitado para trabajar, usted tendra derecho a recibir 2/3 de su salario bruto hasta que sea medicamente declarado capaz de regresar al trabajo. Si el doctor lo declara capaz de regresar a trabajar solo por medio tiempo mientras usted esta en tratamiento, la cantidad de pago que usted reciba de su empleador sera suplementada por el seguro de Compensación a los Trabajadores hast ala cantidad que iguale a ⅔ del ingreso neto de sus ganancias al momento de su lesión.  Estos beneficios cesarán después de 2 años del dia de su lesión.</p>
<p> <strong>Beneficios por Discapacidad Permanente</strong>. Cuando su médico tratante decide que su condición de salud se ha estabilizado, y que su condición no empeorará ni mejoprará, usted tendra derecho de recibir compensación monetaria por la incapacidad medica restante. El médico redactará un reporte indicando el porcentaje estimado al cuál se ha reducido su abilidad de  trabajo del 100% que era antes de su lesión.</p>
<p><strong>Tratamiento Médico</strong>.  Aunque la compañia de seguros de Compensación a los Trabajadores discuta sobre atención médica rasonable, esto será un desacuerdo entre el proveedor médico y la compañia de seguro.  Estos cargos pueden ser negociados ó litigados, pero no pasarán a ser responsabilidad del trabajador lesiondo.</p>
<p><strong>Reabilitación</strong>. Bajo la ”nueva” ley, la compañía sólo requiere de proveer al trabajador lesionado con un “voucher” ó una carta prometiendo pagar a un colegio acreditado ó a una escuela de intercambio la cantidad de dinero por entrenamiento de trabajo de acuerdo a  la siguiente escala:

si la discapacidad permanente es determinada entre 0% y 15%, $4,000.00; entre 16% y 25%, $6,000.00; entre 26% y 50%, $8,000.00; entre 51% y 91%, $10,000.00.  El 100% de discapacidad permanente indica que el trabajador esta totalmente incapacitado y por lo tanto, no podra ser entrenado para ser productivo en ningun tipo de trabajo.</p>
<p>Una compañía de seguros de Compensación a los Trabajadores tiene derecho de estar en desacuerdo con todos estos temas y obtener reportes medicos que refuerzén su posición.

Los desacuerdos resultantes pueden ser resueltos a travez de negociación ó en su alternativa, cualquera de las 2 partes puede pedir que el desacuerdo sea resuelto por un juez de Compensación al trabajador pidiendo una audiencia en el Workers’ Compensation Appeals Board.</p>
<p style="font-size:large"><strong>CONSULTE UN ABOGADO! LAS COMPAÑIAS DE SEGUROS NO REGALAN SU DINERO</strong></p>
</asp:Content>

