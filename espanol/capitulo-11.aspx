﻿<%@ Page Title="Capítulo 11 - quiebra de capítulo 11, Upland, Montclair, Claremont, Chino Hills " Language="C#" MasterPageFile="~/MasterPages/SiteMainInternal.master" AutoEventWireup="true" CodeFile="capitulo-11.aspx.cs" Inherits="espanol_capitulo_11" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
<meta http-equiv="Content-Language" content="ES" /> 
<meta name="description" content="¿Necesita un abogado de quiebra de capítulo 11 experimentado para manejar el caso de quiebra 11 capítulo? En contacto con los abogados en las oficinas de ley de Marc Grossman para un libre consulation inicial." />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainLogo" Runat="Server">
<img src="../images/i-prac-bankruptcy.jpg" style="width: 802px; height: 228px"/>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="PageTitleH1" Runat="Server">
<h1 id="pageTitle">quiebra de capítulo 11</h1>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="MainContent" Runat="Server">
<p>Capítulo 11 quiebra socorro se utiliza para los propietarios de negocios.  En alivio de la quiebra de capítulo 11, como deudor, usted podría conservar sus activos de propiedad y mantener el control de su negocio.  En virtud del capítulo 11, se llamaría a un “ deudor en posesión ”.  Como deudor en posesión, ejecuta las operaciones de día a día de la empresa mientras acreedores trabajan con usted y a la Corte de bancarrota para negociar un plan hacia el reembolso.  El plan de capítulo 11 debe ser justo a los acreedores, y hay una orden de prioridad entre los acreedores.  Los acreedores están autorizados a votar sobre el plan propuesto.  Si los acreedores votan para confirmar el plan de capítulo 11, como deudor, podrá seguir operar su negocio y pagar sus cuentas en virtud del plan confirmado.  Si los acreedores no votan para confirmar el plan, el Tribunal tendrá un papel más activo en la formulación del plan y puede imponer requisitos adicionales para confirmar el plan.</p>
</asp:Content>

