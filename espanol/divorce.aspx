﻿<%@ Page Title="Abogado con Experiencia en Todo lo Relacionado con Casos de Divorcio" Language="C#" MasterPageFile="~/MasterPages/SiteMainInternal.master" AutoEventWireup="true" CodeFile="divorce.aspx.cs" Inherits="espanol_divorce" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
<meta name="keywords" content="Abogado, con, Experiencia, en, Todo, lo, Relacionado, con, Casos, de, Divorcio, Desde, nuestras, oficina legal, ubicada, en, Upland, California, contamos, la, experiencia, que, usted, est&#225;, buscando, para, resolver, sus, necesidades, legales, relacionadas, con, casos">
<meta name="description" content="Desde nuestras oficinas legales ubicadas en Upland, California., contamos la experiencia que usted est&#225; buscando para resolver sus necesidades legales relacionadas con casos de divorcio.">
<meta http-equiv="Content-Language" content="ES"> 
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainLogo" Runat="Server">
    <img src="../images/i-espa-espanoldivorce.jpg" style="width: 802px; height: 228px"/>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="PageTitleH1" Runat="Server">
<h1 id="pageTitle">Divorcio Centro de información</h1>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="MainContent" Runat="Server">
<div id="practicePage-main-callToAction">
        <p>Si usted tiene alguna pregunta sobre el divorcio, comuníquese con nosotros. </p>
      </div>
      <div id="practicePage-main-faqAbstract">
        <h3>Preguntas Frecuentes Respecto al Divorcio</h3>
        <p>
          <strong>P: ¿Debo presentar la petición de divorcio dentro del estado donde nos casamos?</strong>
        </p>
        <p>
          <strong>R:</strong> No, no siempre es necesario presentar la petición de divorcio dentro del estado donde usted contrajo matrimonio. La mayoría de los divorcios son presentados dentro del estado donde los demandantes viven, donde quizás este sea el estado donde se casaron o no. Cada estado cuenta con reglas diferentes de acuerdo a la jurisdicción o a las autoridades de la corte que admiten el divorcio. La mayoría requieren que el cónyuge viva en el estado por un período de tiempo mínimo antes del divorcio.</p>
        <p>
          <strong>P: ¿Qué es el divorcio basado en culpa?</strong>
        </p>
        <p>
          <strong>R:</strong> Muchas cortes tradicionalmente requieren prueba de que uno de los cónyuges haya hecho algo erróneo dentro del matrimonio, como el cometer adulterio, crueldad, abandono o cualquier otro acto desagradable, antes de que el divorcio sea concedido. Mientras que algunos estados no requieren de culpa alguna antes de que el divorcio pueda ser concedido, la culpa de uno de los cónyuges puede tener impacto en los términos de su divorcio, como en la manera en que la custodia de los menores es asignada o en la división de la propiedad- todo dependiendo de las leyes de su estado.</p>
 
  
      <h2>Abogado con Experiencia en Todo lo Relacionado con Casos de Divorcio</h2>
      <p>Desde nuestras oficinas legales ubicadas en Upland, California., contamos con la experiencia que usted está buscando para resolver sus necesidades legales relacionadas con el divorcio. ¿Tiene alguna pregunta sobre el derecho familiar? ¿Esta pensando en divorciarse y le gustaría asesorarse? <a href="/Forms/contact.aspx" rel="nofollow">Contacte</a> a la oficina legal de Marc E. Grossman para una consulta inicial gratuita y evaluación de su caso.</p>
      <p>En nuestra oficina legal sabemos que el proceso de un divorcio puede ser largo y complicado. Nosotros le ofrecemos nuestra experiencia y conocimientos para asesorarlo y guiarlo a través de tan devastador proceso. Si usted o un ser querido ha pensado en iniciar un proceso de divorcio o solamente le interesa asesorarse para saber más acerca de cómo se llevaría éste, llámenos al 888-407-9068 o al 909-297-5479. Nuestros abogados están siempre listos para otorgarle la asesoría que usted merece.</p>


      <h3>Visión General del Divorcio - Lo Básico</h3>
      <p>El divorcio es la disolución legal del contrato de matrimonio. El divorcio involucra muchas cuestiones legales y emocionales complicadas y el proceso legal puede ser difícil para las partes involucradas, especialmente si existen hijos de por medio.  

	El involucrar el conocimiento del abogado con experiencia en derecho familiar del despacho legal Law Office of Marc E. Grossman ubicado en Upland, California, en su proceso lo más pronto posible, puede hacer que su divorcio sea menos estresante y con resultados más favorables.
	</p>
      <h3>Cuál es la Diferencia entre Divorcio y Anulación</h3>
      <p>Muchas personas están confundidas por los conceptos de divorcio y anulación. Dentro de la anulación, el juez hace una declaración legal de que el matrimonio es inválido. A diferencia del divorcio, una anulación establece que el matrimonio nunca existió dentro de la ley. La anulación es solo otorgada en ciertas circunstancias. Usualmente, esto incluye situaciones relacionadas con fraude, bigamia o incompetencia mental. Por ejemplo, el matrimonio puede ser anulado si una de las partes ya estaba casada o si el consentimiento del matrimonio fue basado en coacción.</p>
      <h3>Requisito de Separación</h3>
      <p>Algunos estados tienen el requisito de separación antes de que el divorcio sea otorgado, especialmente en los estados donde existe el divorcio sin culpa. La habilidad para presentar una acción por separación, los términos para un acuerdo de separación, los efectos de la separación legal en el divorcio y las leyes que guían los procedimientos de separación legal varían de estado a estado.</p>
      <h3>Causal de Divorcio y Divorcio sin Culpa</h3>
      <p>Históricamente, para poder obtener un divorcio, el cónyuge que deseaba el divorcio tenia que demostrar la culpa de la otro parte. El divorcio solo era otorgado si uno de los cónyuges cometió cierto tipo de actos tales como adulterio, crueldad, abandono o algún delito grave. La culpa era un factor importante en la determinación para que el juez otorgara o no el divorcio y era critico también para las decisiones relacionadas con cualquier asunto involucrado en este, tales como la división de la propiedad y la manutención.</p>
      <p>El divorcio sin culpa fue establecido a través de Estados unidos entre 1960 y 1970, como respuesta al deseo de disminuir las acusaciones falsas hechas por los cónyuges bajo las leyes del divorcio con culpa. En un divorcio sin culpa, a las partes no les es requerido probar culpa, sino solo el aludir al "quebrantamiento irreparable del matrimonio." Ahora todos los estados han adoptado el divorcio sin culpa, ya sea que sea la única causal del divorcio o como una causal adicional del divorcio.</p>
      <h3>Resolución en Cuestiones de Divorcio Contencioso</h3>
      <p>Antes de que un divorcio sea otorgado, existen usualmente una serie de asuntos básicos que se deben resolver. La custodia de los menores y la visitación debe ser establecida. La propiedad debe ser dividida. La manutención de los menores y la pensión alimenticia del otro cónyuge debe ser considerada.</p>
      <p>Si existen desacuerdos entre las partes en cualquiera de los asuntos básicos, entonces existe un divorcio contencioso. Cundo un divorcio es contencioso, la pareja debe de pasar por todas las fases del litigio, incluyendo un juicio ante un juez en una corte familiar. Sin embargo, actualmente solo un número muy pequeño de de divorcios va a juicio. Muchas parejas resuelven sus disputas con la ayuda de sus abogados. Muchos voluntariamente buscan resolver sus disputas con métodos alternativos tales como con mediación o divorcio colaborativo. A muchos otros la corte les ordena ciertas acciones como parte de los requerimientos establecidos para el de divorcio.</p>
      <h3>El Proceso de un Litigio de Divorcio</h3>
      <p>El proceso legal para un divorcio varia según el estado. Sin embargo, la mayoría de los procesos de la terminación matrimonial, usualmente incluye una serie de pasos estándares. El proceso de un litigio de divorcio comienza con la petición de divorcio, usualmente va acompañado con la citación judicial del otro cónyuge. El litigio entonces continua con la proposición de pruebas, en donde ambas partes pueden buscar información relevante de la otra parte. La audiencia algunas veces es establecida, para determinar órdenes temporales, tales como quien cuidará de los menores mientras el proceso del divorcio se está llevando a cabo y si es necesaria una manutención temporal. Si la pareja no se ha puesto de acuerdo de los términos el divorcio durante este proceso, se seguirá un juicio en donde el juez decidirá los términos. Si la pareja estipula un acuerdo, el juez da una orden final y el decreto de divorcio será necesario para finalizar el proceso.</p>
      <h3>Divorcio Colaborativo</h3>
      <p>En años recientes, muchas parejas han utilizado esta alternativa de divorcio. En un divorcio colaborativo, la partes acuerdan el no ir a corte. De hecho, acuerdan en despedir a sus abogados y contratar a otros si estos no resuelven la situación fuera de las cortes.</p>
      <p>El divorcio colaborativo se lleva acabo a través de una serie de juntas entre la pareja que se está divorciando, sus abogados y algunas veces con profesionales neutrales. Juntos deciden todos los términos relacionados con el divorcio fuera de la corte.</p>
      <p>El divorcio colaborativo es un proceso que funciona bien para deducir los costos y los conflictos de ciertas situaciones en el divorcio. Sin embargo, el divorcio colaborativo no funciona de igual manera para todos. Es mejor que sea utilizado en situaciones en las cuales no exista ningún tipo de crisis entre la pareja. Además, este no puede ser utilizado en situaciones donde existan alegaciones de violencia doméstica.</p>
      <h3>Conclusión</h3>
      <p>Es enormemente difícil llegar a la decisión de ponerle fin a un matrimonio. Sin embargo, el entender sus opciones bajo las leyes que su estado establece, puede ayudarle a que usted tome la decisión correcta para su familia.  Ya sea que usted proceda con un litigio de divorcio contencioso o trate modos colaborativos, el consultar con un abogado con experiencia California en derecho familiar de la oficina legal de Law Office of Marc E. Grossman le ayudará a lograr los mejores resultados posibles.
	</p>
      <p>Copyright ©2009
FindLaw, a Thomson Business</p>
      <p>DISCLAIMER: This site and any information contained herein are intended for informational purposes only and should not be construed as legal advice. Seek competent legal counsel for advice on any legal matter.</p>
      <p>CLÁUSULA EXONERATIVA: Esta página de Internet y la información contenida en ella son para dar información, no son un substituto para un abogado o un experto en el área jurídica. No está ofreciendo asesoramiento legal y no puede considerarse como tal. Si usted necesita ayuda legal busque a un abogado o una persona competente en la materia.</p>
    </div>
</asp:Content>

