﻿<%@ Page Title="Glendora Attorney, Bankruptcy Attorney and Divorce Law Firm" Language="C#" MasterPageFile="~/MasterPages/SiteMainInternal.master" AutoEventWireup="true" CodeFile="glendora.aspx.cs" Inherits="lawyer_glendora" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
<meta name="description" content="Experienced Glendora Bankruptcy Attorney that can handle your case. Whether you're looking for a divorce lawyer in Glendora or DUI lawyer call on the experts at the Law Offices of Marc Grossman. "/>
<meta name="keywords" content="lawyer, attorney, law firm, law office, legal advice, litigation, civil litigation, criminal defense, lawyer, attorney, bankruptcy, divorce, family law, DUI"/>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainLogo" Runat="Server">
<p style="text-align:center"><a href="http://www.wefight4you.com/" title="Glendora lawyers">
                <img src="http://www.wefight4you.com/images/teamofattorneys.jpg" alt="Glendora Attorney" height="244px" width="800px"></a></p></asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="PageTitleH1" Runat="Server">
<h1 id="pageTitle">Attorneys near Glendora</h1>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="MainContent" Runat="Server">
<p>If you are looking for an attorney near Glendora contact the Law Offices of Marc Grossman. If you think that you have a case contact us for a FREE initial consultation. Our Law Firm is located in Upland between the 10 and 210 freeways. Our main office is a mere 20 minutes away from Glendora, but we are worth the drive.</p>
<h3>Glendora Bankrutpcy Attorney Services</h3>
<p>If you are thinking about filing for bankruptcy in Glendora and don't know where to turn, our initial consultations for bankruptcy are FREE! In order for us to see if you qualify for bankruptcy in Glendora we ask that you come to our office. We go through your financial information to see what type of bankruptcy that you qualify for. There are three main Chapters of bankruptcy that our law firm deals with,</p>
<ul>
<li><a href="http://www.wefight4you.com/bankruptcy/chapter-7.aspx" title="Chapter 7 bankruptcy">Chapter 7 bankruptcy</a></li>
<li><a href="http://www.wefight4you.com/bankruptcy/chapter-11.aspx" title="Chapter 11 bankruptcy">Chapter 11 bankruptcy</a></li>
<li><a href="http://www.wefight4you.com/bankruptcy/chapter-13.aspx" title="Chapter 13 bankruptcy">Chapter 13 bankruptcy</a></li>
</ul>
<p>To see what type of bankruptcy that you qualify for contact us today for a FREE intial bankruptcy evaluation.</p>
<h3>Glendora Divorce Attorney Services</h3>
<p>If you are looking for a Divorce attorney in Glendora contact us for a FREE initial consultation. Our Family Law department is located in our Upland location. Our Law Firm has different Family Law services depending upon the complexity of your case. If you have a simple divorce, divorce mediation or even a complex divorce containing multiple properties our Law Firm has experience to handle it all.</p>
</asp:Content>

