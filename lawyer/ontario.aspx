﻿<%@ Page Title="Ontario Bankruptcy Attorney - Divorce Lawyer - DUI Attorney" Language="C#" MasterPageFile="~/MasterPages/SiteMainInternal.master" AutoEventWireup="true" CodeFile="ontario.aspx.cs" Inherits="lawyer_ontario" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
<meta name="description" content="Experienced Ontario Bankruptcy Attorney that specializes in divorce, bankruptcy and DUI defense. If you are filing a Chapter 7 bankruptcy in Ontario contact us today for a FREE Initial Consultation. "/>
<meta name="keywords" content="lawyer, attorney, law firm, law office, legal advice, litigation, civil litigation, criminal defense, lawyer, attorney, bankruptcy, divorce, chapter 7, chapter 11, chapter 13"/>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainLogo" Runat="Server">
<a href="http://www.wefight4you.com" title="Bankruptcy Attorneys - Divorce Attorneys - DUI - Workers Compensation">
	<p style="text-align:center"><a href="http://www.wefight4you.com/" title="Ontario lawyers">
	                <img src="http://www.wefight4you.com/images/teamofattorneys.jpg" alt="Ontario Attorney" height="244px" width="800px"></a></p> 
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="PageTitleH1" Runat="Server">
<h1 id="pageTitle">Ontario Attorneys that fight for you</h1>
<p style="text-align:center; color:White; font-weight:bold"><a href="#ontario-bankruptcy">Ontario Bankruptcy</a> | <a href="#family-law">Family Law</a> | <a href="#ontario-divorce">Ontario Divorce</a> | <a href="#ontario-dui">Ontario DUI</a></p>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="MainContent" Runat="Server">
<p >Located near Ontario, the Law Offices of Marc Grossman provide legal aid for a wide-array of legal services ranging from <a href="#ontario-divorce">divorce</a>,<a href="#ontario-bankruptcy"> bankruptcy</a>, workers compensation to <a href="#ontario-dui">DUI defense</a>. While you might be wondering whether you should contact us or not, we provide free no obligation initial consultations. This way you will know exactly what our fees are and more information about your case. With our law firm you don't have to worry about the law because we take care of that burden for you. Whether you live in or near Ontario our Offices are only a few minutes away.</p>

<p>If you decide to come in for a Free Initial Consultation, the first thing you'll notice is a relaxed atmosphere. Our goal at the Law Offices of Marc Grossman is to help you feel relaxed while meeting us. We understand that the law might seen scary, but after you meet with us rest assured that you'll feel right at home.</p>

<h2><a name="ontario-bankruptcy">Live Debt Free, file for bankruptcy</a></h2>
<p>Can't afford your bills anymore? Are you having a hard time making ends meet due to the current economy? You're not alone. If you qualify, a Chapter 7 bankruptcy can relinquish all of your debt. With a bankruptcy you can stop the creditor harassment and even save your home. The only way for us to know if you qualify is to come to our office for a FREE Initial Consultation. During the consultation we'll sit you down and answer all of your bankruptcy questions and let you know how much the bankruptcy will cost. Since <a href="/bankruptcy/Default.aspx" title="Bankruptcy Attorney">bankruptcy</a> is complicated our prices vary upon the compexity of the case.</p>

<h3>We provide services for Chapter 7, Chapter 11 and Chapter 13 Bankruptcy</h3>
<p>While a Chapter 7 is geared more towards individuals, we offer Chapter 11 and Chapter 13 bankruptcy services. Chapter 11's and Chapter 13's are made mostly for small business owners and higher income house holds. You'll find that most law firms don't offer Chapter 11 bankruptcy's, but we do. </p>

<h3>Ontario Chapter 7 bankruptcy Attorneys</h3>
<p>We provide Chapter 7 bankruptcy legal service to residents of Ontario. A Chapter 7 or Debt relief is the most common form of bankruptcy if you're an individual. To see if you qualify contact us for a FREE Initial Consultation.</p>

<h3><a name="family-law">Family Law Attorney Services</a></h3>
<p>Whether you need an attorney for divorce, child custody, child visitation or divorce mediation we can help you. We pride ourselves on excellent customer service when it comes to a family law issue. Since 1998 we've helped Ontario residents with their family law case and would like the opportunity to help you with yours. We offer Free Initial Consultations. Our consultations consist of getting to know you and your case.</p>
<h2><a name="ontario-divorce">Ontario Divorce Lawyer Services</a></h2>
<p>Facing a <a href="../divorce/Default.aspx">divorce</a> is hard. Finding an experienced divorce attorney in Ontario might seem impossible. Know that your divorce in good hands when you hire the Law Offices of Marc Grossman to represent you.
</p>
<h3><a name="ontario-dui">DUI Defense Attorneys that serve Ontario - FREE Initial Consultation</a></h3>
<p>You've been charged with a DUI. You're concerned about your license and about your job. Right now isn't the time to panic.... Just because you were charged doesn't mean you've completely lost. At the time of your DUI charge a police officer provided you with a 30 day temporary license (unless you had a suspended or expired license before the DUI charge). Once you're charged with a DUI the DMV immediately holds a meeting that goes over your charges. You have 10 days to file a hearing with the DMV to challenge the suspension or revocation of your license. We have experience preparing people for these hearings. If you or a loved one needs assistance with a DUI charge contact our Law Offices for a FREE initial consultation.</p>

<p>When it comes to legal representation feel confident that our 
    <a href="../Default.aspx">Ontario Lawyers</a> are here by your side. When we take on your case we will make sure to fight for you. </p>
</asp:Content>

