﻿<%@ Page Title="Montclair CA Divorce Attorney, Montclair Bankruptcy Lawyer & Workers Compensation" Language="C#" MasterPageFile="~/MasterPages/SiteMainInternal.master" AutoEventWireup="true" CodeFile="montclair.aspx.cs" Inherits="lawyer_montclair" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
<meta name="description" content="We are experienced Montclair Divorce Attorneys that can handle your case. If you have been looking for a divorce lawyer in Montclair or a bankruptcy attorney in Montclair look no further. Contact us today for a Free initial consultation."/>
<meta name="keywords" content="lawyer, attorney, law firm, law office, legal advice, litigation, civil litigation, criminal defense, lawyer, attorney, California"/>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainLogo" Runat="Server">
<p style="text-align:center"><a href="http://www.wefight4you.com/" title="Montclair lawyers">
                <img src="http://www.wefight4you.com/images/teamofattorneys.jpg" alt="Montclair Attorney" height="244px" width="800px"></a></p></asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="PageTitleH1" Runat="Server">
<h1 id="pageTitle">Experienced Montclair&nbsp; CA Attorneys</h1>
<p style="text-align:center; color:White; font-weight:bold"><a href="#Montclair-bankruptcy-attorneys">Montclair Bankruptcy Attorneys</a> | <a href="#Montclair-divorce-attorneys">Montclair Divorce Attorneys</a> | <a href="#Montclair-DUI-attorney">Montclair DUI Attorney</a></p>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="MainContent" Runat="Server">
<p>If you are looking for a Montclair California Attorney the Law Offices of Marc Grossman have 
    the experience and drive to help you with your case. At the Law Offices of Marc Grossman we handle mostly all legal issues ranging from 
    <a href="../divorce/Default.aspx">divorce</a> to 
    <a href="../bankruptcy/Default.aspx">bankruptcy</a>. Feel confident that the
    experienced Montclair lawyers are here for you.</p>
    <h2><a name="Montclair-bankruptcy-attorneys">Montclair Bankruptcy Attorneys</a></h2>
    <p>If you are considering filing for a bankruptcy in Montclair the Law Offices of 
        Marc Grossman provide ethical bankruptcy services. Our Attorneys have years of 
        experience with Bankruptcy whether you&#39;re interested in filing a
        <a href="../bankruptcy/chapter-7.aspx">Chapter 7 bankruptcy</a>, <a href="../bankruptcy/chapter-11.aspx">Chapter 11 bankruptcy</a> and
        <a href="../bankruptcy/chapter-13.aspx">Chapter 13 bankruptcy</a>. Rather than dealing with 
        the creditors yourself hire us for expert Bankruptcy representation.</p>
    <h2><a name="Montclair-divorce-attorneys">Montclair CA Divorce Lawyers</a></h2>
    <p>Sometimes a marriage does not work. We provide excellent services for those who 
        wish to hire a Montclair divorce lawyer. We have
        <a href="../attorneys/Default.aspx">attorneys</a> on staff to answer an 
        questions that you might have regarding your <a href="../divorce/Default.aspx">
        divorce</a> or <a href="../family-law/Default.aspx">family law</a> issue. We 
        have both men and women divorce people at your disposal to assist you with this 
        hard time in your life.</p>
        
        <h3><a name="Montclair-DUI-attorneys">Montclair DUI Defense Attorney's you can count on</a></h3>
        <p>Charged with a DUI recently? No doubt you've looked around for a DUI defense attorney in or around Montclair to help you. We'd like to thank you for considering the Law Offices of Marc Grossman to defend you in your DUI case. We offer affordable DUI defense services and make sure you understand what you're in for. We don't candy coat the process or our fees to get you into the door. We give you an honest assessment of your case during our FREE initial consultation. During this consultation you'll understand the process of a DUI from an experienced attorney. We have 7 attorneys on staff to help you with your needs. Our main DUI attorney Albert D'Antin has 15 years of police experience along with another 15 as being an attorney. He understands police protocol and has close ties with the community. If you are shopping around for a DUI defense attorney you're in good hands with the Law Offices of Marc Grossman.</p>
        
<p>Rest assured that you have a skilled and aggressive<b> Montclair attorney</b> at your side to not make empty promises about your case but deliver results. We offer free initial consultations to help you with your case.
</p>
</asp:Content>