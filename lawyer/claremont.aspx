<%@ Page Title="Claremont Bankruptcy Attorney, Lawyer - Divorce Attorneys" Language="C#" MasterPageFile="~/MasterPages/SiteMainInternal.master" AutoEventWireup="true" CodeFile="claremont.aspx.cs" Inherits="lawyer_claremont" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <meta name="description" content="Claremont Attorneys that can help you with filing for bankruptcy, divorce, workers compensation and dui cases. Whether you're filing a Chapter 7, Chapter 11 or Chapter 13 bankruptcy in Claremont we can help you."/>
<meta name="keywords" content="lawyer, attorney, law firm, law office, legal advice, litigation, civil litigation, criminal defense, lawyer, attorney, bankruptcy, chapter 7, Chapter 11, Chapter 13, divorce, family law, workers compensation."/>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainLogo" Runat="Server">
<a href="http://www.wefight4you.com/" title="Claremont Attorney">
    <img alt="Claremont Bankruptcy Attorney, Divorce and Workers Compensation" src="../images/claremont-ca-attorneys.jpg" style="width: 802px; height: 228px" /></a>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="PageTitleH1" Runat="Server">
    <h1 id="pageTitle">&nbsp;Claremont Attorneys, that Are on your side</h1>
    <h2 style="text-align:center">Whether you're looking for bankruptcy, divorce, or a DUI attorney in Claremont contact us for FREE</h2>
    <p style="text-align:center; color:White; font-weight:bold"><a href="#claremont-bankruptcy">Claremont Bankruptcy</a> | <a href="#family-law">Family Law</a> | <a href="#divorce">Divorce</a> | <a href="#claremont-dui">Claremont DUI</a></p>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="MainContent" Runat="Server">
    <p>If you are looking for a attorney located near Claremont, we can help. Our Law Firm provides representation for a wide-variety of cases ranging from bankruptcy, divorce to workers compensation. Conveniently located near Claremont California, we offer representation that goes beyond the courtroom. We provide a relaxed atmoshphere, because we feel you should feel at ease when you enter our Law Office.</p>

<h3>Honest Claremont Attorneys</h3>
<p>What sets us apart from other Claremont attorneys is there are no hidden fees or costs to represent you. If you are unsure of whether or not you have a case, we offer Free Initial Consultations to see whether or not you have a  case.</p>

<h2><a name="claremont-bankruptcy">Claremont Bankruptcy Options</a></h2>
<p>If this is your first time <a href="http://www.wefight4you.com/bankruptcy/Default.aspx" title="filing for bankruptcy">filing for bankruptcy</a> know your options. Most of the banruptcy cases involve debt relief from a <a href="http://www.wefight4you.com/bankruptcy/chapter-7.aspx" title="Chapter 7 bankruptcy Claremont">Chapter 7 bankruptcy</a>. If you qualify a Chapter 7 can relieve most if not all of your debt. Unafortunately, restrictions apply depending upon your income and the type of debt you have. If you are considering filing for a Bankruptcy in or near Claremont, rest assured your in good hands with our Law Firm. We've handled hundreds of Bankruptcy cases and would like to the opportunity to represent you.</p>

<h2>Free No Obligation Consultations For Chapter 7, Chapter 11 and Chapter 13 Bankruptcy</h2>
<p>Since every Claremont Bankruptcy case is different, we offer Free face-to-face, no obligation consultations. This Consultation will let you know the type of bankruptcy you're filing along with the fees associated with your bankruptcy. Our Law Office provides bankruptcy services for Chapter 7, <a href="http://www.wefight4you.com/bankruptcy/chapter-11.aspx" title="chapter 11 bankruptcy Claremont">Chapter 11</a> and <a href="http://www.wefight4you.com/bankruptcy/chapter-13.aspx" title="Chapter 13 bankruptcy Claremont" >Chapter 13</a> cases. Even though most of our cases are Chapter 7's, we have experience with Chapter 11 and Chapter 13 bankruptcy's which usually apply to small to medium sized business's.</p>

<h3>You're Not Alone</h3>
<p>Don�t feel that you have to go through bankruptcy alone. We have the right bankruptcy attorney in Claremont to help you through this difficult time. If you don�t know where to start with your bankruptcy your best bet is to come in for a free initial consultation. We are the Chapter 7 and <strong>Chapter 13 lawyers in Claremont</strong> that help you through this.</p>
<h2><a name="family-law">Claremont Divorce Attorney Services</a></h2>
<p>We understand that a <a href="http://www.wefight4you.com/divorce/Default.aspx" title="Divorce">Divorce </a> issue can be a scary time for you. It's important that you choose an attorney that won't charge hidden fees. You might not realize but most law offices provide lower fees to get you into the door. That doesn't happen with the Law Offices of Marc Grossman. We let you you know exactly what you are paying for upfront. We do everything we can so that you can avoid court. Whether you're facing a divorce, child custody, child visitation or would like a divorce mediation we're here for you every step of the way.

<h3><a name="divorce">Divorce Attorneys and how we help you move on</a></h3>
<p>Getting on with your life is the focus of our <a href="http://www.wefight4you.com/divorce/Default.aspx" title="divorce attorneys Claremont">Divorce attorneys</a>. Why drag out the process by going to court? Most of our cases are settled with out going to court because we like to deal with both sides of the divorce. Whether you would like a male or female to represent you in your divorce we have both at the Law Offices of Marc Grossman.</p>

<h3><a name="claremont-dui">Aggressive DUI defense Attorney in Claremont - FREE Initial Consultation</a></h3>
<p>Were you charged with a <a href="http://www.wefight4you.com/criminal-defense/dui.aspx" title="DUI attorney Claremont">DUI in Claremont</a>? If so, our DUI defense attorneys can help. When it comes to DUI we have to act fast on your part. A DUI can hurt you in the long run. A DUI conviction can hold you back from jobs, higher car insurance rates along with high fines in California. To see why the Law Offices of Marc Grossman is a great choice for DUI defense contact us today for a FREE initial DUI consultation. During the consultation we'll let you know your options and whether or not you have a case.</p>

<h4>Free Initial Divorce Consultations</h4>
<p>We offer Free Initial Consultations to educate you on the process and to meet face-to-face. We feel as though once you meet us you will feel right at home. Our Law Offices prides itself on providing a relaxed atmosphere for our Divorce Services.</p>
</asp:Content>

