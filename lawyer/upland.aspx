﻿<%@ Page Title="Upland California Divorce Lawyer - Upland Bankruptcy Lawyer| Law Offices of Marc Grossman" Language="C#" MasterPageFile="~/MasterPages/SiteMainInternal.master" AutoEventWireup="true" CodeFile="upland.aspx.cs" Inherits="lawyer_upland" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <meta name="description" content="Find a qualified Upland Attorney that can handle your case. Whether you're looking for a divorce lawyer in Upland or bankruptcy lawyer call on the experts at the Law Offices of Marc Grossman. "/>
<meta name="keywords" content="lawyer, attorney, law firm, law office, legal advice, litigation, civil litigation, criminal defense, lawyer, attorney,"/>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainLogo" Runat="Server">
<p style="text-align:center"><a href="http://www.wefight4you.com/" title="Upland lawyers">
                <img src="http://www.wefight4you.com/images/teamofattorneys.jpg" alt="Upland Attorney" height="244px" width="800px"></a></p>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="PageTitleH1" Runat="Server">
    <h1 id="pageTitle">Experienced Upland Attorneys</h1>
    <br />
    <p style="text-align:center; color:White; font-weight:bold"><a href="#upland-bankruptcy">Upland Bankruptcy</a> | <a href="#upland-divorce">Divorce</a> | <a href="#upland-workers-compensation">Workers Compensation</a> | <a href="#upland-DUI">DUI</a></p><br />
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="MainContent" Runat="Server">
    <p>If you are in search of an <a href="http://www.wefight4you.com/Default.aspx" title="Upland Attorney">attorney in Upland </a>the Law Offices of Marc Grossman can help. We handle all types of cases and offer free initial consultations, where we discuss your case in great detail. Since 1998 we have helped people from all walks of like get what they are entitled to.</p>
<p>We are aggressive and honest and won’t waste your time with empty promises. We believe in providing you with the best representation possible and have helped many others just like you settle their case.</p>
<h2><a name="upland-divorce">Upland Divorce Lawyer Services</a></h2>
<p>If you are suffering through a <a href="http://www.wefight4you.com/divorce/Default.aspx" title="Upland Divorce">divorce in Upland </a>it’s important to hire a seasoned lawyer in Upland that can help you with your case. Our divorce lawyers can help guide you and provide you with answers to your questions.</p>
<p>Whether you’re going through a Upland divorce or 
   bankruptcy feel confident that the attorneys at the Law Offices of Marc Grossman can help. No matter what the legal issues feel confident you have a lawyer that is on your side and will fight for you.</p>
<h2><a name="upland-bankruptcy">Upland Bankruptcy Attorney Services</a></h2>
<p><strong>Our <a href="http://www.wefight4you.com/bankruptcy/Default.aspx" title="Upland Bankruptcy Attorneys">bankruptcy attorneys in Upland</a> know the law.</strong> It’s important that you hire a lawyer that files the correct paperwork and lead you in the right direction for your bankruptcy. We handle<a 
        href="http://www.wefight4you.com/bankruptcy/chapter-7.aspx" title="Chapter 7"> Chapter 7</a> and 
    <a href="http://www.wefight4you.com/bankruptcy/chapter-13.aspx" title="Chapter 13 bankruptcy">Chapter 13 bankruptcy’s</a> and make sure that we steer you in the right direction. Don’t feel as though you are alone in your bankruptcy. Contact us today for a free initial consultation.</p>
<h2><a name="upland-workers-compensation">Upland Workers Compensation Services</a></h2>
<p>If you are suffering from a <a href="http://www.wefight4you.com/work-injury/Default.aspx" title="Upland Work Injury Attorney">work related injury in Upland</a> the Law Offices of Marc Grossman has over 20 years of experience dealing with <a href="http://www.wefight4you.com/workers-compensation/Default.aspx" title="Upland Workers Compensation Claims">Upland workers compensation claims</a>. It's important that you choose an aggressive attorney. Once you enter our Upland workers compensation office you will be greeted by a friendly staff of attorneys that have what it takes to settle your case.</p>
<h3><a name="upland-DUI">Charged with a DUI? DUI Defense Attorneys in Upland you can count on</a></h3>
<p>If you or someone you know has been charged with a DUI in Upland you have not completely lost. Our DUI defense lawyers have years of experience with the criminal law system. Whether it's your first, second or even third DUI charge we can help. We have two experienced DUI defense lawyers with years of experience. We offer FREE initial consultations for DUI. Call us today to see if you have a DUI case.</p>
<br />
    <hr />
    <h3 style="text-align:center">Directions to our Upland California Office</h3>
    <br />
    <p style="text-align:center"><iframe width="300" height="300" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="http://maps.google.com/maps?f=d&amp;source=s_d&amp;hl=en&amp;geocode=&amp;saddr=&amp;daddr=818+N.+Mountain+Ave.+Upland+CA+91786+suite+111&amp;sll=37.0625,-95.677068&amp;sspn=54.753001,113.642578&amp;ie=UTF8&amp;ll=34.104254,-117.66896&amp;spn=0.00533,0.006437&amp;z=16&amp;output=embed"></iframe><br /><small><a href="http://maps.google.com/maps?f=d&amp;source=embed&amp;hl=en&amp;geocode=&amp;saddr=&amp;daddr=818+N.+Mountain+Ave.+Upland+CA+91786+suite+111&amp;sll=37.0625,-95.677068&amp;sspn=54.753001,113.642578&amp;ie=UTF8&amp;ll=34.104254,-117.66896&amp;spn=0.00533,0.006437&amp;z=16" style="color:#0000FF;text-align:left">Directions to Our Upland, CA</a></small></p>
    <p style="text-align:center"><strong>818 N. Mountain Ave. Upland, CA 91786 Suite 111</strong></p>
</asp:Content>
