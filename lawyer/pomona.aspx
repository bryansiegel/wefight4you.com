﻿<%@ Page Title="Pomona Attorney - Bankruptcy Lawyer in Pomona CA - Divorce Attorneys" Language="C#" MasterPageFile="~/MasterPages/SiteMainInternal.master" AutoEventWireup="true" CodeFile="pomona.aspx.cs" Inherits="lawyer_pomona" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
<meta name="description" content="Experienced Pomona Attorney that can handle your case. Whether you're looking for a divorce lawyer in Pomona or bankruptcy lawyer call on the experts at the Law Offices of Marc Grossman. "/>
<meta name="keywords" content="lawyer, attorney, law firm, law office, legal advice, litigation, civil litigation, criminal defense, lawyer, attorney,"/>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainLogo" Runat="Server">
<p style="text-align:center"><a href="http://www.wefight4you.com/" title="Pomona lawyers">
                <img src="http://www.wefight4you.com/images/teamofattorneys.jpg" alt="Pomona Attorney" height="244px" width="800px"></a></p></asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="PageTitleH1" Runat="Server">
<h1 id="pageTitle">&nbsp;Pomona Attorney that knows the law</h1>
<h2 style="text-align:center">Bankruptcy - Divorce - Workers Compensation</h2>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="MainContent" Runat="Server">
<p>Are you facing a legal issue and don’t know where to turn. We all face a legal matter at one point in our life. If you are looking to hire an 
    <a href="../Default.aspx">attorney in Pomona</a> contact the Law Offices of Marc Grossman for a FREE initial consultation. We handle most types of cases ranging from divorce to bankruptcy and have what it takes to solve your legal problem.</p>
<p>We are the law firm that believes in doing what it takes to solve your case in a timely manner. We have experienced lawyers that know the law and get you what you are entitled to, not empty promises.</p>
<h2>Pomona Bankruptcy Lawyer Services</h2>
<p>Suffering through hard times? Do you think that 
    <a href="../bankruptcy/Default.aspx">bankruptcy</a> is in your future? No matter the reason our attorneys are here by your side. We’ve helped people just like you get through bankruptcy and start over. Don’t waste another minute. Contact us today for a free initial consultation.</p>
<h2>Pomona Divorce Lawyer Services</h2>
<p>So it’s time to get a <a href="../divorce/Default.aspx">divorce</a>. You’ve tried but the best for both parties is that you part ways. It’s important that you hire an experienced divorce attorney that knows the law. Our seasoned attorneys have dealt with divorce and can help you through this difficult time in your life.</p>
<h3>Charged with a DUI in Pomona? FREE Initial DUI Consultation</h3>
<p>You were pulled over for Driving under the influence of Alcohol in Pomona. The officer gave you a 30 day temporary license. You might be wondering if there's anyway to remove the suspension of your license. Contact the Law Offices of Marc Grossman for a FREE DUI Consultation. We understand DUI defense and do what it takes to get you through your DUI charge. A DUI can seriously impact your future, not to mention your auto insurance payments. We have 7 full-time attorneys to assist you with your case. Our Law Firm believes in providing honest services. During your Free initial consultation. we let you know exactly what you're in for and the process of a DUI. </p>

<p>No matter what the legal issue the Law Offices of Marc Grossman has an attorney to assist you with your case. Know that if you hire us we will fight for you.</p>
</asp:Content>