﻿<%@ Page Title="La Verne Bankruptcy Attorney, Divorce Attorney, DUI Attorney" Language="C#" MasterPageFile="~/MasterPages/SiteMainInternal.master" AutoEventWireup="true" CodeFile="la-verne.aspx.cs" Inherits="lawyer_la_verne" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
 <meta name="description" content="If you are in need of an attorney near Chino Hills contact us for a FREE Initial consultation. Whether you need a bankruptcy attorney, divorce attorney our  family law attorneys can help you solve your legal matters. Contact the Law Offices of Marc Grossman today for a free initial consultation. "/>
<meta name="keywords" content="lawyer, attorney, law firm, law office, legal advice, litigation, civil litigation, criminal defense, lawyer, attorney,"/>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainLogo" Runat="Server">
<p style="text-align:center"><a href="http://www.wefight4you.com/" title="La Verne lawyers">
                <img src="http://www.wefight4you.com/images/teamofattorneys.jpg" alt="La Verne Attorney" height="244px" width="800px"></a></p></asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="PageTitleH1" Runat="Server">
<h1 id="pageTitle">La Verne Attorneys<br /> that fight for you</h1>
<h2 style="text-align:center">Our Law Firm Primarily Handles</h2>
 <p style="text-align:center; color:White; font-weight:bold"><a href="#la-verne_bankruptcy">Bankruptcy</a> | <a href="#la-verne_divorce">Family Law</a> | <a href="#la-verne_DUI">DUI</a>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="MainContent" Runat="Server">
<p>Are you a La Verne resident in search of an attorney? Then you have come to the right place. We are attorneys that pride ourselves on providing our clients the bottom line. Whether you decide to hire us as your attorney or not we'll do everything we can, and provide you with an honest answer about your case. </p>
<h2>Our La Verne Primary Attorney Services</h2>
<p>Located only minutes away from La Verne our Upland Law firm primarily handles bankruptcy, divorce, and DUI and workers compensation. We have 7 attorneys available Monday through Friday to hear about your case. Our Law Firm offers Free initial consultations for all matters. Whether or not you believe you have a case call us for a free initial consultation.</p>
<h3><a name="la-verne_bankruptcy">La Verne Bankruptcy Attorney Services</a></h3>
<p>If you are a La Verne resident filing for bankruptcy, or considering filing for bankruptcy contact our Law Firm for free. We offer free initial bankruptcy consultations. Even though we primarily handle Chapter 7 bankruptcy, we have successfully filed Chapter 11 and Chapter 13 bankruptcy.</p>
<h3><a name="la-verne_divorce">La Verne Family Law Attorney Services</a></h3>
<p>Our Law Firm has a Family Law division dedicated to your family law case. Whether you wish to file for divorce or need assistance with child custody/child visitation our law firm will provide you with an honest assessment about your family law case. Our initial consultations for family law are free and we'd love the opportunity to speak with you regarding your case.</p>
<h3><a name="la-verne_DUI">La Verne DUI Attorney Services</a></h3>
<p>If you have been charged with a DUI and wish to hire a La Verne DUI lawyer contact the Law Offices of Marc Grossman for a free initial consultation. Our lead DUI attorney Albert D' Antin has 15 years of police experience and understands police protocol. If you or a loved one has been charged with driving under the influence of drugs or alcohol contact our Law Firm today for a free initial consultation.</p>
<p>No matter what case type you have the Law Offices of Marc Grossman appreciate you taking the time out of your busy schedule to visit our website. Whether or not you decide to hire is solely up to you and we wish you the best of luck with your case.</p>
</asp:Content>

