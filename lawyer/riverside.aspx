﻿<%@ Page Title="Riverside Attorney - Bankruptcy Lawyer in Riverside - Divorce Attorney" Language="C#" MasterPageFile="~/MasterPages/SiteMainInternal.master" AutoEventWireup="true" CodeFile="riverside.aspx.cs" Inherits="lawyer_riverside" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
<meta name="description" content="Find a qualified Riverside Attorney that can handle your case. Whether you're looking for a divorce lawyer in Riverside or bankruptcy lawyer call on the experts at the Law Offices of Marc Grossman. Contact us today for a FREE initial consultation"/>
<meta name="keywords" content="lawyer, attorney, law firm, law office, legal advice, litigation, civil litigation, criminal defense, lawyer, attorney,"/>
<link rel="canonical" href="http://www.wefight4you.com/lawyer/riverside.aspx" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainLogo" Runat="Server">
<p style="text-align:center"><a href="http://www.wefight4you.com/" title="Riverside lawyers">
                <img src="http://www.wefight4you.com/images/teamofattorneys.jpg" alt="Riverside Attorney" height="244px" width="800px"></a></p>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="PageTitleH1" Runat="Server">
<h1 id="pageTitle">Riverside Attorney that fight for your case</h1>
<h2 style="text-align:center">Bankruptcy - Divorce - Workers Compensation</h2>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="MainContent" Runat="Server">
<p>Are you looking for an <a href="../Default.aspx">attorney in Riverside</a>? Sick of searching through countless attorneys and uncertain about whom you should choose? Stop wasting your time and contact the Law Offices of Marc Grossman for a free initial consultation. Our seasoned lawyers have what it takes to guide you through your legal problems. We handle all types of cases ranging from 
    <a href="../divorce/Default.aspx">divorce</a> to 
    <a href="../bankruptcy/Default.aspx">bankruptcy </a>and don’t provide empty promises.</p>
<p>The <a href="../Firm-Overview/Default.aspx">Law Offices of Marc Grossman </a>has a reputation of results. We’ve fought against some of the best legal representation Riverside has to offer and won. Our secret is we are honest with our clients and aggressive with the opposing attorneys.</p>
<p>One you come into our office for a free initial consultation you will feel right at home. We always welcome all of our clients with open arms. We understand that you might be confused about where to turn or what you’re in for as far as your case goes. We guide you through the legal process and make sure that you understand what you are in for.</p>
<h2>Riverside Bankruptcy Lawyer Services</h2>
<p>If you are looking for an experienced <a href="../Default.aspx">bankruptcy attorney in Riverside</a> the Law Offices of Marc Grossman can help. Whether you’re filing a Chapter 7 or Chapter 13 our seasoned lawyers can help you through your bankruptcy.</p>
<h2>Riverside Divorce Lawyer Services</h2>
<p>Not all marriages work. Sometimes divorce is the best option for couples. Our 
    <a href="../Default.aspx">divorce lawyers in Riverside</a> are here to get you through it. Feel confident in the fact that you have a lawyer by your side that not only cares but is aggressive in the courtroom. </p>
<p>No matter what legal issue you face in Riverside the Law Offices of Marc Grossman are here by your side to help you with your legal issues. No matter what the case know that we are here to fight for you.</p>
</asp:Content>