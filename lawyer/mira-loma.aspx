﻿<%@ Page Title="Mira Loma Bankruptcy Attorney, Divorce Lawyer, DUI Defense Attorney" Language="C#" MasterPageFile="~/MasterPages/SiteMainInternal.master" AutoEventWireup="true" CodeFile="mira-loma.aspx.cs" Inherits="lawyer_mira_loma" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
<meta name="description" content="Experienced Mira Loma Bankruptcy Attorney that can handle your case. Whether you're looking for a divorce lawyer in Mira Loma or DUI lawyer call on the attorneys at the Law Offices of Marc Grossman. "/>
<meta name="keywords" content="lawyer, attorney, law firm, law office, legal advice, litigation, civil litigation, criminal defense, lawyer, attorney, bankruptcy, divorce, family law, DUI"/>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainLogo" Runat="Server">
<a href="http://www.wefight4you.com/" title="Bankrutpcy Attorney, Divorce Attorney">
	<p style="text-align:center"><a href="http://www.wefight4you.com/" title="divorce lawyers">
	                <img src="http://www.wefight4you.com/images/teamofattorneys.jpg" alt="Rancho Cucamonga Attorney" height="244px" width="800px"></a></p>
	</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="PageTitleH1" Runat="Server">
<h1 id="pageTitle">Mira Loma Attorneys</h1>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="MainContent" Runat="Server">
<p>Do you think that you have a case? Are you looking for a Mira Loma attorney? Thank you for visiting the Law Offices of Marc Grossman. Our Law Firm can help you whether or not you are in need of legal aid. We have 7 attorneys on staff to assist you with your legal troubles. Whether you wish to file for bankruptcy, divorce or have been charged with a DUI we are the correct Law Firm for you.</p>
<p>Even though we primarily handle bankruptcy cases in Mira Loma we can also represent you in your divorce case. We have a Family Law department here to serve your needs. Since our Initial Consultations are free we can let you know whether or not we will take your case when you meet us.</p>
<h3>Mira Loma Bankruptcy Attorney Services</h3>
<p>If you are having trouble keeping up with your bills and are tired of the harassing creditor phone calls, bankruptcy might be the best solution for you. There are 3 popular bankruptcy chapters our law firm handles. Chapter 7, Chapter 11 and Chapter 13. While Chapter 7 is by far the most utilized we have successfully filed Chapter 11 and Chapter 13 bankruptcy cases. If this is your first time or not contact the Law Offices of Marc Grossman for a free initial consultation.</p>
<h3>Mira Loma Family Law (divorce) Services</h3>
<p>If you are planning on filing for a divorce in Mira Loma contact the Law Offices of Marc Grossman for a free initial consultation. We have a Family Law Department that can help you with your legal issue. Whether it's divorce, child custody or child visitation we can help you.</p>
<h3>Mira Loma DUI Attorney Services</h3>
<p>If you have been charged with a DUI in Mira Loma we can help. If you decide to hire us for your DUI case we immediately get to work filling the necessary paperwork to keep your license. Contact us today for a free initial DUI consultation.</p>
<p>The Law Offices of Marc Grossman would like to thank residents of Mira Loma for taking the time to visit our website.</p>
</asp:Content>

