﻿<%@ Page Title="Diamond Bar Bankruptcy Attorney, Diamond Bar Divorce Lawyer - DUI" Language="C#" MasterPageFile="~/MasterPages/SiteMainInternal.master" AutoEventWireup="true" CodeFile="diamond-bar.aspx.cs" Inherits="lawyer_diamond_bar" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
<meta name="description" content="Experienced Diamond Bar Bankruptcy Attorney that can handle your case. Whether you're looking for a divorce lawyer in Diamond Bar or DUI lawyer call on the experts at the Law Offices of Marc Grossman. "/>
<meta name="keywords" content="lawyer, attorney, law firm, law office, legal advice, litigation, civil litigation, criminal defense, lawyer, attorney, bankruptcy, divorce, family law, DUI"/>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainLogo" Runat="Server">
<p style="text-align:center"><a href="http://www.wefight4you.com/" title="Diamond Bar lawyers">
                <img src="http://www.wefight4you.com/images/teamofattorneys.jpg" alt="Diamond Bar  Attorney" height="244px" width="800px"></a></p></asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="PageTitleH1" Runat="Server">
<h1 id="pageTitle">Attorneys Near Diamond Bar</h1>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="MainContent" Runat="Server">
<p>Do you live in Diamond Bar? Are you in search of an attorney? Are you unsure whether or not that you have a case? Contact the Law Offices of Marc Grossman for a FREE initial consultation. Our Diamond Bar attorneys can help you with some of the best legal aid around California. Our main office is located in Upland, but is well worth the 10 minute drive. Conveniently located between the 10 and 210 fwy's our Law Firm is well worth the drive. </p>
<p>Our attorneys are here to save you money, while providing you with honest representation for Diamond Bar residents. We have 7 passionate attorneys that are here to provide you with honest legal aid. We uphold the motto “We Fight For You” which means, if we go to trial we fight on your side.</p>
<h3>Diamond Bar Bankruptcy Attorney Services</h3>
<p>Don't be embarrassed by filing for bankruptcy. If you are wondering how to file for bankruptcy contact us for a FREE initial bankruptcy consultation. During this consultation we take in account you income, bills and personal property. After we sum up your financial information, we have a better idea of the type of bankruptcy you can file for. Our Law Firm primarily handles,</p>
<ul>
<li><a href="http://www.wefight4you.com/bankruptcy/chapter-7.aspx">Chapter 7 bankruptcy</a></li>
<li><a href="http://www.wefight4you.com/bankruptcy/chapter-11.aspx">Chapter 11 bankruptcy</a></li>
<li><a href="http://www.wefight4you.com/bankruptcy/chapter-13.aspx">Chapter 13 bankruptcy</a></li>
</ul>
<p>If you have questions about bankruptcy in Diamond Bar, there's no better way to understand where you stand than contacting us for a FREE initial bankruptcy consultation.</p>
<h3>Diamond Bar Divorce (Family Law) Attorney Services</h3>
<p>We understand Divorce in Diamond Bar. We have a family law department, that is here to suit your needs. We provide a relaxing environment and offer FREE initial divorce consultations. We treat our consultations as interviews for Divorce representation. If you are shopping around for a Divorce attorney near Diamond Bar, contact us for FREE!</p>
</asp:Content>

