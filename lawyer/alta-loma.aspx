﻿<%@ Page Title="Alta Loma Bankruptcy Attorney, Divorce Lawyer, DUI Attorney" Language="C#" MasterPageFile="~/MasterPages/SiteMainInternal.master" AutoEventWireup="true" CodeFile="alta-loma.aspx.cs" Inherits="lawyer_alta_loma" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
<meta name="description" content="Experienced Alta Loma Bankruptcy Attorney that can handle your case. Whether you're looking for a divorce lawyer in Alta Loma or DUI lawyer call on the experts at the Law Offices of Marc Grossman. "/>
<meta name="keywords" content="lawyer, attorney, law firm, law office, legal advice, litigation, civil litigation, criminal defense, lawyer, attorney, bankruptcy, divorce, family law, DUI"/>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainLogo" Runat="Server">
 <img alt="Alta Loma California Bankruptcy Attorney, Divorce and Workers Compensation" src="../images/i-prac-civillitigation.jpg" style="width: 802px; height: 228px" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="PageTitleH1" Runat="Server">
<h1 id="pageTitle">Alta Loma Attorneys<br /> that fight for you</h1>
<h2 style="text-align:center">Our Law Firm Primarily Handles</h2>
 <p style="text-align:center; color:White; font-weight:bold"><a href="#alta-loma_bankruptcy">Bankruptcy</a> | <a href="#alta-loma_divorce">Family Law</a> | <a href="#alta-loma_DUI">DUI</a>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="MainContent" Runat="Server">
<p>If you are seeking an attorney near Alta Loma you have come to the right place. Our Law Firm primarily handles bankruptcy, divorce and DUI, but we're willing to hear about your case no matter what your legal situation is. We have 7 attorneys on staff with a wide range of experiences able to assist you with your case.</p>
<h3>An Alta Loma Attorney you can count on</h3>
<p>We have experience with Alta Loma Bankruptcy, Divorce and DUI. We offer free initial consultations for all case types and would like the opportunity to speak with you about your legal issue. If you decide to hire an attorney or not hire an attorney we help guide you with your decision (we never make it for you).</p>
<h3><a name="alta-loma_bankruptcy">Alta Loma Bankruptcy Attorney Services</a></h3>
<p>If you live in Alta Loma California and wish to file for bankruptcy contact us today for a free initial consultation. Our Law Firm primarily handles Chapter 7 bankruptcy, but we are willing to take on Chapter 11 and Chapter 13 bankruptcy. You won't know if you qualify for bankruptcy if you don't call and you have nothing to loose, since we offer free initial consultations.</p>
<h3><a name="alta-loma_divorce">Alta Loma Family Law Attorney Services</a></h3>
<p>Whether you wish to file for a divorce in Alta Loma, child custody or child visitation our Alta Loma Family Law lawyers can help. We have experience with a wide array of family law issues. We have a full-time family law staff here to guide you through the family law process. Why wait another day? Contact the Law offices of Marc Grossman for a free divorce consultation.</p>
<h3><a name="alta-loma_DUI">Alta Loma DUI Attorney Services</a></h3>
<p>Have you been charged with a DUI in Alta Loma? DUI's are very time sensitive. You have 10 days to file a hearing with the DMV to keep your license. If you fail to do so, your license may be suspended. Our attorney Albert D' Antin has 15 years of police experience. He knows police protocol and can help you with your DUI case.</p>
<p>Thank you for taking the time to visit our website. Even if you decide not to hire us for your legal issue we wish you the best of luck with your case.</p>
</asp:Content>

