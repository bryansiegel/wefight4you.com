﻿<%@ Page Title="Fontana Bankruptcy Attorney, Fontana Divorce Lawyer - DUI" Language="C#" MasterPageFile="~/MasterPages/SiteMainInternal.master" AutoEventWireup="true" CodeFile="fontana.aspx.cs" Inherits="lawyer_fontana" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
<meta name="description" content="Fontana Attorney that provides FREE initial consultations. Whether you're filing for bankruptcy in Fontana, divorce, or need a DUI or DWI defense attorney contact the Law Offices of Marc Grossman today."/>
<meta name="keywords" content="lawyer, attorney, law firm, law office, legal advice, litigation, civil litigation, criminal defense, lawyer, attorney, divorce, bankruptcy, family law, DUI, child custody, child visitation"/>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainLogo" Runat="Server">
<p style="text-align:center"><a href="http://www.wefight4you.com/" title="divorce lawyers">
                <img src="http://www.wefight4you.com/images/teamofattorneys.jpg" alt="Rancho Cucamonga Attorney" height="244px" width="800px"></a></p>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="PageTitleH1" Runat="Server">
<h1 id="pageTitle">Aggressive Fontana Attorneys</h1>
<p style="text-align:center; color:White; font-weight:bold"><a href="#fontana-bankruptcy">Bankruptcy</a> | <a href="#fontana-divorce">Divorce</a> | <a href="#fontana-dui">DUI</a></p>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="MainContent" Runat="Server">
<p>Located minutes from Fontana, the Law Offices or Marc Grossman are here for your legal needs. Whether you are <a href="#fontana-bankruptcy">filing for bankruptcy in Fontana</a>, <a href="#fontana-divorce">filing for a Fontana divorce</a> or you have been charged with a <a href="#fontana-dui">DUI</a> we have an attorney for you. Since 1998 we've helped those in need of representation. We provide a confortable environment and offer Free initial consultations for all case types. Our Free no obligation consultations consist of getting to know you and your case. After our consultation you'll know where you stand within the letter of the law. So if you or anyone you know in Fontana is in need of an attorney contact the Law Offices of Marc Grossman today!</p>

<h2><a name="fontana-bankruptcy">Filing for bankruptcy in Fontana</a></h2>
<p>Within today's current economy we've noticed a steady increase of people filing for bankruptcy. If you are on of those Fontana residents considering bankruptcy, you're not alone. What you might not know is if you qualify for bankruptcy. Our law firm handles Chapter 7, Chapter 11 and Chapter 13 bankruptcy. There are alot of factors we have to consider in order to know which type of bankruptcy that you qualify for. We've handled hundreds of successful bankruptcy's for our clients and offer FREE initial consultations for bankruptcy. Since the nature of bankruptcy is a complicated one we cannot give you the full price unless you visit our main Upland office.</p>

<h3>What to expect from our Free Bankruptcy Consultation</h3>
<p>When you come to our main office we ask that you bring in all of you paperwork from creditors and your pay stubs. This way we can let you know if you qualify for a Chapter 7, Chapter 11 or Chapter 13 bankruptcy.</p>

<h2><a name="fontana-divorce">Are You Considering Filing for Divorce in Fontana?</a></h2>
<p>We understand that sometimes a marriage doesn't work. We've delt with hundreds of Divorce cases, that range from lengthy divorce battles to divorce mediation that involves no court. Our goal is to process your divorce fast so that you can move on with your life. What sets us apart from most law firms is our ability to handle both sides of the divorce. If you and your spouse agree to the terms of the divorce we have divorce mediations at a reduced cost. Whether your a man or woman considering divorce we can assist you with your case. Our Initial divorce consultations are free. This way, you'll understand the process of a divorce.</p>

<h3><a name="fontana-dui">Fontana DUI Defense from Experienced Attorneys</h3></a>
<p>Charged with a DUI in Fontana? A DUI can impact your future. You face higher auto insurance costs, a suspended license plus many more. We offer FREE initial consultations for DUI cases. Within that consultation we let you know exactly what you're in for. If you have been recently charged with a DUI you have 10 days to request a hearing with the DUI to challenge the suspension or revocation of your license. Just because you made a mistake doesn't mean you have to completely loose. We work with every step of the way to make sure you understand what you're in for. Why wait? Contact us for a FREE Initial no obligation consultation.</p>

<p>Whether you're filing for bankruptcy, divorce or need a DUI defense attorney the Law Offices of Marc Grossman would like to represent you with your legal issues. Contact us today for a FREE Initial Consultation.</p>

</asp:Content>

