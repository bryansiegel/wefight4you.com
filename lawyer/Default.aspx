﻿<%@ Page Title="California Lawyers | Law Offices of Marc Grossman" Language="C#" MasterPageFile="~/MasterPages/SiteMainInternal.master" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="lawyer_Default" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainLogo" Runat="Server">
    <img src="../images/california-lawyers.jpg" alt="California Lawyers" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="PageTitleH1" Runat="Server">
    <h1 id="pageTitle">The California Lawyers at the Law Offices of Marc Grossman</h1>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="MainContent" Runat="Server">
    <p>Over the last decade, the Law Office of Marc E. Grossman has emerged as a unique law firm in the Inland Empire.  Located in 
    <a href="upland.aspx">Upland, California</a> the Law Office of Marc E. Grossman is the only mid-sized law firm in the area that offers clients a full range of legal services including but not limited to – 
    <a href="../bankruptcy/Default.aspx">bankruptcy</a>, 
    <a href="../divorce/Default.aspx">divorce</a>, 
    <a href="../family-law/Default.aspx">family law</a>, 
    <a href="../criminal-defense/Default.aspx">criminal defense</a>,
    <a href="../criminal-defense/dui.aspx">DUI</a>, 
    <a href="../personal-injury/Default.aspx">personal injury</a>, 
    <a href="../wrongful-death/Default.aspx">wrongful death</a>, vehicular accidents, 
    <a href="../immigration/Default.aspx">immigration</a>, 
    <a href="../insurance-bad-faith/Default.aspx">insurance bad faith</a>, special education and appellate work.  There is no legal problem that our California lawyers cannot handle.  Marc Grossman is not a sole practitioner; rather, his firm has seven full-time <a href="http://www.wefight4you.com/attorneys/Default.aspx" title="California Lawyers">California lawyers</a> on staff and a thriving, busy office with locations also in Palm Desert, La Puente and Van Nuys, and <a href="http://www.wefight4you.com/lawyer/chino-hills.aspx" title="Chino Hills">Chino Hills</a>.  At the Law Office of Marc E. Grossman clients receive representation on par with that provided by the best L.A. law firms, all at an Inland Empire price.</p>
 <h2>California Lawyers that are on your side</h2>
<p>Though it is a full service civil and criminal firm, over the last decade Marc Grossman has focused his talents representing clients in divorce.  Former divorce clients describe Marc as a lion in the courtroom and a friend in the office.  Marc has developed a family law reputation to be proud of and he is respected as an advocate by both the legal community and the judiciary.  Marc has taken hundreds of divorce cases to trial and has successfully mediated thousands.</p>
 
<p>The Law Office of Marc E. Grossman is committed to providing the Inland Empire with a full range of legal services at reasonable prices.  Please call and make an appointment today to meet Marc Grossman and have a free legal consultation.</p>

</asp:Content>

