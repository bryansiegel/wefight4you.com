﻿<%@ Page Title="La Puente Workers Compensation Attorney - La Puente Bankruptcy - La Puente Divorce Lawyer" Language="C#" MasterPageFile="~/MasterPages/SiteMainInternal.master" AutoEventWireup="true" CodeFile="la-puente.aspx.cs" Inherits="lawyer_la_puente" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <meta name="description" content="La Puente Workers Compensation Attorney that offers Free initial consultations. See if you have a workers comp claim today by contacting the Law Offices of Marc Grossman La Puente Offices."/>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainLogo" Runat="Server">
<p style="text-align:center"><a href="http://www.wefight4you.com/" title="La Puente lawyers">
                <img src="http://www.wefight4you.com/images/teamofattorneys.jpg" alt="La Puente Attorney" height="244px" width="800px"></a></p></asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="PageTitleH1" Runat="Server">
    <h1 id="pageTitle">La Puente Workers’ Compensation Attorney</h1>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="MainContent" Runat="Server">
<h2 style="text-align:center">La Puente Work Injury Attorney</h2>
    <p>It can be stressful to try to handle an important legal matter with someone who just doesn't understand your language. In Los Angeles County, and especially in the La Puente area, there is a great need for legal professionals who are capable and trained to serve the immigrant community.</p>
<p>At the Law Office of Marc Grossman, we not only have bi-lingual (Spanish/English) staff, we have professionals who are of your same culture. Our hearing representatives and legal assistants empathize with the worries you may have as a result of being a stranger in a strange land, with laws you don’t fully understand. You need to know that your legal representative doesn't merely  “speak Spanish”, you need to know that he (or she) “gets it”.</p>
<p>Immigration laws and Immigration status (legal or undocumented) have no bearing on your ability to enforce your rights without being intimidated with threats. If you worked for wages in the State of California, you are covered by the California Labor Code and you can collect Workers’ Compensation benefits.</p>
<p>Making a false or fraudulent Workers’ Compensation claim is a felony and is punishable by up to five years in state prison or by a fine of up to  $50,000 or double the value of the fraud, (whichever is greater) or by both such imprisonment and fine. </p>
<p>But if you have actually been hurt on the job, whether that injury was the result of your ordinary negligence or the negligence of somebody else, YOU HAVE RIGHTS. AND YOU HAVE A CASE! </p>
<p>Whether you were hurt as a result of an accident which occurred on one particular day or if you may be suffering from an injury which built up little by little, day by day from repetitive motions required by your job, YOU HAVE RIGHTS. AND YOU HAVE A CASE! </p>
<p>Whether your injury is purely physical or also the result of job stress, YOU HAVE RIGHTS. AND YOU HAVE A CASE!</p>
<p>But do you have a lawyer? You need one. And not just any lawyer. You need a lawyer who understands your  “language” A lawyer who understands your worries. You need a lawyer who is unafraid of the system. You need Marc Grossman.</p>
<p>At the Law Office of Marc E. Grossman, WE FIGHT FOR YOU!</p>
<h3>Get A <a href="http://www.wefight4you.com/workers-compensation/Default.aspx" title="workers compensation lawyer">Workers Compensation Lawyer!</a> Insurance Companies Do Not Give Their Money Away!</h3>
    <img alt="Workers Compensation Disclaimer" src="../images/workers-comp-disclaimer.jpg" alt="workers comp disclaimer" />
    <h3>La Puente Bankruptcy Attorney Services</h3>
    <p>The Law Offices of Marc Grossman is taking <a href="http://www.wefight4you.com/bankruptcy/la-puente.aspx" title="La Puente Bankruptcy">La Puente Bankruptcy</a> appointments. If you would like to schedule a FREE Initial Consultation with one of our La Puente bankruptcy lawyers contact us today at 888-407-9068.</p>
    <h3>La Puente Divorce Attorney Services</h3>
    <p>If you are considering a divorce in La Puente and need to hire an attorney contact us for a FREE initial consultation. Whether you need to file a divorce or are looking for general family law legal advice we can help you. We have La Puente child support and child custody services available in our La Puente office. </p>
    <p style="text-align:center; font-weight:bold;">13052 Valley Blvd. La Puente, CA 91746</p>
    <p style="text-align:center"><iframe width="300" height="300" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="http://maps.google.com/maps?f=d&amp;source=s_d&amp;saddr=&amp;daddr=13052+Valley+Blvd,+La+Puente,+CA+91746&amp;hl=en&amp;geocode=&amp;gl=us&amp;mra=ls&amp;sll=34.052167,-118.000834&amp;sspn=0.014738,0.027874&amp;g=13052+Valley+Blvd,+La+Puente,+CA+91746&amp;ie=UTF8&amp;ll=34.052162,-118.000846&amp;spn=0.010667,0.012875&amp;z=15&amp;output=embed"></iframe><br /><small><a href="http://maps.google.com/maps?f=d&amp;source=embed&amp;saddr=&amp;daddr=13052+Valley+Blvd,+La+Puente,+CA+91746&amp;hl=en&amp;geocode=&amp;gl=us&amp;mra=ls&amp;sll=34.052167,-118.000834&amp;sspn=0.014738,0.027874&amp;g=13052+Valley+Blvd,+La+Puente,+CA+91746&amp;ie=UTF8&amp;ll=34.052162,-118.000846&amp;spn=0.010667,0.012875&amp;z=15" style="color:#0000FF;text-align:left">View Larger La Puente Map</a></small></p>
    <p><strong>Surrounding Locations</strong></p>
    <p>The Law Offices of Marc Grossman service the surrounding locations of La Puente, which are </p>
</asp:Content>

