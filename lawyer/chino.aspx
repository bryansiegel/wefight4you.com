﻿<%@ Page Title="Chino Attorney - Bankruptcy Attorney in Chino - Divorce Lawyer - Chino DUI Lawyer" Language="C#" MasterPageFile="~/MasterPages/SiteMainInternal.master" AutoEventWireup="true" CodeFile="chino.aspx.cs" Inherits="lawyer_chino" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
<meta name="description" content="Experienced Chino Bankruptcy Attorney that can handle your case. Whether you're looking for a Chino bankruptcy lawyer or Chino divorce lawyer in call the Law Offices of Marc Grossman for a FREE Initial Consultation. "/>
<meta name="keywords" content="lawyer, attorney, law firm, law office, legal advice, litigation, civil litigation, criminal defense, lawyer, attorney,"/>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainLogo" Runat="Server">
<p style="text-align:center"><a href="http://www.wefight4you.com/" title="Chino lawyers">
                <img src="http://www.wefight4you.com/images/teamofattorneys.jpg" alt="Chino Attorney" height="244px" width="800px"></a></p>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="PageTitleH1" Runat="Server">
<h1 id="pageTitle">Honest Chino Attorneys</h1>
<p style="text-align:center; color:White; font-weight:bold"><a href="#bankruptcy">Bankruptcy</a> | <a href="#divorce">Divorce</a> | <a href="#dui">DUI</a></p>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="MainContent" Runat="Server">
<p>Sometimes life takes a wrong turn in which you have to hire an 
    <a href="../Default.aspx">attorney in Chino</a>. Unfortunately, shopping for a Chino lawyer might seem challenging and you might not know where to turn. Know that the Law Offices of Marc Grossman have the experience and drive to fight for you. We’ve dealt with all types of legal matters ranging from 
    <a href="../divorce/Default.aspx">divorce</a> to 
    <a href="../bankruptcy/Default.aspx">bankruptcy </a>and have an attorney that’s right for your legal situation.</p>
<p>Our honest approach to your case and friendly demeanor will help you feel at ease no matter what legal question that you might have. Why wait any longer? Contact us today for a free initial consultation so that we can go over your case and assist you with your legal matter.</p>
<h2><a name="bankruptcy">Chino Bankruptcy Lawyer Services</a></h2>
<a href="http://www.wefight4you.com/bankruptcy/Default.aspx" title="Filing For Bankruptcy">
<img src="http://www.wefight4you.com/images/chino-bankruptcy.jpg" 
        align="left" alt="Questions about filing Bankruptcy?" style="padding-right: 5px;"/></a>
<p>Bills are unfortunately a way of life that we all have to deal with. Whether you lost your job or you simply can’t afford your debt you should know that there is an experienced bankruptcy attorney in Chino that can help. We handle Chapter 7 and Chapter 13 bankruptcies. Contact us for a free initial consultation.</p>
<h2><a name="divorce">Chino Divorce Lawyer Services</a></h2>
<a href="http://www.wefight4you.com/divorce/Default.aspx" title="Divorce">
<img src="http://www.wefight4you.com/images/chino-divorce.jpg" 
        align="left" alt="Questions about Divorce?" style="padding-right: 5px;"/></a>
<p>Let our Chino Divorce Lawyers assist you with your case. We can assist you with all aspects of divorce and would like to assist you with the painful point in your life. Get your life back and don’t waste another minute.</p>
<br />
<br />
<h3><a name="dui">Need a DUI Defense Attorney in Chino, California?</a></h3>
<a href="http://www.wefight4you.com/criminal-defense/dui.aspx" title="Filing for Bankruptcy">
<img src="http://www.wefight4you.com/images/chino-dui.jpg" 
        align="left" alt="DUI" style="padding-right: 5px;"/></a>
<p>Just because you were charged with a DUI in Chino, doesn't mean that you're convicted of a DUI. If you have been charged you must act now! A DUI is a serious offense and you need to seek council now to know your rights and what you're in for. The Law Offices of Marc Grossman has two experienced Chino DUI defense attorneys to server you. Albert D' Antin has 15 years of experience with the Azusa police department. He is well versed in police protocol. We offer FREE Initial DUI Consultations. This way we can let you know what you're in for and whether you have a case.</p>

<p>No matter what legal challenge that you are facing know that the experts at the Law Offices of Marc Grossman are here for you. Stop searching for a Chino Lawyer and contact us today.</p>
</asp:Content>