﻿<%@ Page Title="Rancho Cucamonga Divorce Attorney | Rancho Cucamonga Bankruptcy Attorney & DUI" Language="C#" MasterPageFile="~/MasterPages/SiteMainInternal.master" AutoEventWireup="true" CodeFile="rancho-cucamonga.aspx.cs" Inherits="lawyer_rancho_cucamonga" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
<meta name="description" content="Minutes away from Rancho Cucamonga lies the Law Offices of Marc Grossman. We can represent you for  bankruptcy, divorce and DUI or DWI charges in Rancho Cucamonga. We offer Free Initial Consultations for bankruptcy, divorce and DUI cases. "/>
<meta name="keywords" content="lawyer, attorney, law firm, law office, legal advice, litigation, civil litigation, criminal defense, lawyer, attorney, bankruptcy, divorce"/>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainLogo" Runat="Server">
<a href="http://www.wefight4you.com" title="Rancho Cucamonga Bankruptcy Attorney - Divorce Attorney">
 	<p style="text-align:center"><a href="http://www.wefight4you.com/" title="divorce lawyers">
	                <img src="http://www.wefight4you.com/images/teamofattorneys.jpg" alt="Rancho Cucamonga Attorney" height="244px" width="800px"></a></p>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="PageTitleH1" Runat="Server">
<h1 id="pageTitle">Top Rated Rancho Cucamonga Attorney</h1>
<h2 style="text-align:center">Providing Legal Services in Rancho Cucamonga</a>
<p style="text-align:center; color:White; font-weight:bold"><a href="#bankruptcy">Rancho Cucamonga Bankruptcy</a> |<a href="#divorce">Rancho Cucamonga Divorce</a> <a href="#dui">Rancho Cucamonga DUI</a></p>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="MainContent" Runat="Server">
<p>Are you a Rancho Cucamonga resident in search of sound legal advice? Located near Rancho Cucamonga the Law Offices of Marc Grossman have what it takes to handle your case. Whether you're filing for bankruptcy, divorce or have been charged with a DUI or DWI our Law Firm has successfully represented people just like you with their case.</p>

<h3>Looking for a Rancho Cucamonga Attorney, Try us we're close by!</h3>
<p>We don't candy coat your case. Nor do we quote low prices to get you in the door. Our Law Firm prides itself on results. When hiring an <a href="http://ranchocucamongacaattorney.com/" title="Rancho Cucamonga Attorney">attorney in Rancho Cucamonga</a> you have to watch out for hidden costs or attorneys that drag your case longer than needs be in order to hike up your legal fee. Our law firm does not do that!</p>

<a name="divorce"><h3>Rancho Cucamonga Divorce Attorney</h3></a>
<p>You're considering a divorce in Rancho Cucamonga and don't know where to turn. You must have millions of questions regarding the divorce process. Contact the Law Offices of Marc Grossman for a FREE initial consultation. We've handled hundreds of divorce cases ranging from simple divorces to complex multi-property, high income divorces in Rancho Cucamonga.</p> 
<h3>We Try To Cut The Stress Out of Divorce in Rancho Cucamonga</h3>
<p>To say that searching for a <a href="http://ranchocucamongacadivorce.com/" title="Rancho Cucamonga divorce">Rancho Cucamonga divorce attorney</a> is stressful is a "no brainer" and obvious. Since 1998 we've realized this and have put a personal touch into everyone of our divorce cases because "no case is the same" and we understand how sensitive a subject divorce is. Sometimes the divorce is mutual, but quite often it is not. This equals to disputes that include:</p>
<ul>
	<li>Child Custody</li>
	<li><a href="http://www.wefight4you.com/family-law/child-visitation.aspx" title="Child Visitation">Child Visitation</a>
	<li>Visitation</li>
	<li>Child Support</li>
	<li>Spousal Support</li>
	<li>Property Division</li>
	<li>Mediation</li>
</ul>

<h3><a name="bankruptcy">Hard Times in Rancho Cucamonga, live debt free with Bankruptcy</a></h3>
<p>In today's economy most are having problems paying their bills. Filing for bankruptcy might be your best option. If this is your first time filing for bankruptcy you might not know where to turn. Our Law Firm primarily handles Chapter 7, Chapter 11 and Chapter 13 bankruptcy. What this means is that we cover bankruptcy from individuals from a small monthly income to business's. Most look to Chapter 7 bankruptcy because in most cases a Chapter 7 eliminates your debt. Sounds good right? Unfortunately, certain restrictions do apply (such as monthly income). The only way for us to know which type of bankruptcy that you qualify for we need you to come to our office with your paperwork. </p>

<h3>FREE Rancho Cucamonga Bankruptcy Consultations</h3>
<p>Our Initial Bankruptcy consultations are FREE. This way we can tell you the type of bankruptcy you can file for along with the fees. Since every bankruptcy is different we need you to bring in all of your paperwork. This paperwork includes your creditors, bills and pay stubs. Call us today for more information at 888-407-9068.</p>

<h3><a name="dui">Charged with a DUI or DWI in Rancho Cucamonga, You have not lost</a></h3>
<p>There's a difference between a conviction and being charged with a DUI. Our DUI defense attorneys have the experience you need for your legal matter. Albert D' Antin has 15 years of experience on the police force and understands police protocol. If you would like to know more information about our DUI defense contact us today to see if you have a case.</p>

<p>Whether you are filing for bankruptcy, divorce or need a DUI defense attorney in Rancho Cucamonga we are here for you. We offer Free initial consultations to see if you have a case. Why dig around for information on the internet when you can get expert advise for FREE!</p>
<h3>Rancho Cucamonga Location</h3>
<p style="text-align:center">
	<iframe width="300" height="300" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="http://maps.google.com/maps?f=d&amp;source=s_d&amp;saddr=&amp;daddr=8282+White+Oak+Ave+%23+112+Rancho+Cucamonga,+CA+91730&amp;hl=en&amp;geocode=&amp;mra=ls&amp;sll=37.0625,-95.677068&amp;sspn=64.792576,50.888672&amp;ie=UTF8&amp;ll=37.0625,-95.677068&amp;spn=64.792576,50.888672&amp;output=embed"></iframe><br /><small><a href="http://maps.google.com/maps?f=d&amp;source=embed&amp;saddr=&amp;daddr=8282+White+Oak+Ave+%23+112+Rancho+Cucamonga,+CA+91730&amp;hl=en&amp;geocode=&amp;mra=ls&amp;sll=37.0625,-95.677068&amp;sspn=64.792576,50.888672&amp;ie=UTF8&amp;ll=37.0625,-95.677068&amp;spn=64.792576,50.888672" style="color:#0000FF;text-align:left">View Larger Rancho Cucamonga Map</a></small>
	</p>
</asp:Content>