﻿<%@ Page Title="San Bernardino County Attorney - Bankruptcy Attorney in San Bernardino - Divorce Lawyer - Law Offices of Marc Grossman" Language="C#" MasterPageFile="~/MasterPages/SiteMainInternal.master" AutoEventWireup="true" CodeFile="san-bernardino.aspx.cs" Inherits="lawyer_san_bernardino" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
<meta name="description" content="Find a qualified San Bernardino County Attorney that can handle your case. Whether you're looking for a divorce lawyer in San Bernardino or bankruptcy lawyer call on the experts at the Law Offices of Marc Grossman. "/>
<meta name="keywords" content="lawyer, attorney, law firm, law office, legal advice, litigation, civil litigation, criminal defense, lawyer, attorney,"/>
<link rel="canonical" href="http://www.wefight4you.com/lawyer/san-bernardino.aspx" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainLogo" Runat="Server">
<p style="text-align:center"><a href="http://www.wefight4you.com/" title="San Bernardino lawyers">
                <img src="http://www.wefight4you.com/images/teamofattorneys.jpg" alt="San Bernardino Attorney" height="244px" width="800px"></a></p>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="PageTitleH1" Runat="Server">
<h1 id="pageTitle">San Bernardino County Attorneys that value your case</h1>
<h2 style="text-align:center">Bankruptcy - Divorce - Workers Compensation</h2>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="MainContent" Runat="Server">
<p>Why waste your time looking through endless listings of <a href="../Default.aspx">attorneys in San Bernardino</a> when all that you really want is someone who has the experience to get you what you’re entitled to. Here at the Law Offices of Marc Grossman we are such lawyers that help you with your case. </p>
<p>No matter what the case we can handle it. We have an army of lawyers that are seasoned in all types of law. As one of the most sought after Law Firms in San Bernardino County we pride ourselves upon integrity and honest representation. We don’t believe in excuses only actual results for your legal issue.</p>
<h2>San Bernardino Bankruptcy Lawyer Services</h2>
<p>Bills are a natural part of life, but sometimes things get in the way such as losing your job. Don’t worry the bankruptcy professionals at the Law Offices of Marc Grossman can help. Whether you’re faced with a Chapter 7 
    bankruptcy or Chapter 13 bankruptcy.</p>
<h2>San Bernardino Divorce Lawyer Services</h2>
<p>Facing a divorce is hard. There is no easy way out, but here at the Law Offices of Marc Grossman we’re here to guide you along the way. We are experienced San Bernardino 
    divorce attorneys that know the law and fight for our clients every step of the way. As far as divorce is concerned you should know that your case is in good hands.</p>
<p>If you have a legal issue in San Bernardino, call on the experts at the Law Offices of Marc Grossman. Our honest approach to the law and our clients is what makes us an asset to any person.</p>
</asp:Content>