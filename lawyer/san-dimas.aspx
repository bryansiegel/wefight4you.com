﻿<%@ Page Title="San Dimas Bankruptcy Attorney, Divorce Lawyer, DUI Defense Attorney" Language="C#" MasterPageFile="~/MasterPages/SiteMainInternal.master" AutoEventWireup="true" CodeFile="san-dimas.aspx.cs" Inherits="lawyer_san_dimas" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
<meta name="description" content="Experienced San Dimas Bankruptcy Attorney that can handle your case. Whether you're looking for a divorce lawyer in Alta Loma or DUI lawyer call on the attorneys at the Law Offices of Marc Grossman. "/>
<meta name="keywords" content="lawyer, attorney, law firm, law office, legal advice, litigation, civil litigation, criminal defense, lawyer, attorney, bankruptcy, divorce, family law, DUI"/>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainLogo" Runat="Server">
<a href="http://www.wefight4you.com/" title="Bankrutpcy Attorney, Divorce Attorney">
	<p style="text-align:center"><a href="http://www.wefight4you.com/" title="San Dimas lawyers">
	                <img src="http://www.wefight4you.com/images/teamofattorneys.jpg" alt="San Dimas Attorney" height="244px" width="800px"></a></p></asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="PageTitleH1" Runat="Server">
<h1 id="pageTitle">San Dimas Attorneys</h1>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="MainContent" Runat="Server">
<p>Are you in search for an attorney near San Dimas? Do you think you have a case but aren't sure? At the Law Offices of Marc Grossman we listen to your case for FREE. Our Law Firm primarily handles bankruptcy, divorce, workers compensation and DUI cases in Southern California. We offer free initial consultations to San Dimas residents no matter what the case type!</p>
<p>The Law Offices of Marc Grossman has 7 San Dimas attorneys on staff to assist you with your legal issue. Even if we can't take on your case, we might be able to refer you to another attorney that can.
</p>
<h3>San Dimas Bankruptcy Attorney Services</h3>
<p>If you are having problems with your bills and need a clean slate Chapter 7 bankruptcy might be your best option. The most popluar types of bankruptcy is Chapter 7, Chapter 11 and Chapter 13 bankruptcy. While chapter 7 bankruptcy is the most common our Law Firm has successfully filed Chapter 11 and Chapter 13 bankruptcy's. If you are unsure of whether or not you qualify for bankruptcy, give us a call. The first consultation is allways free. During this consultation we go over how much it costs, the process and whether or not you qualify.</p>
<h3>
San Dimas Family Law (Divorce) Services</h3>
<p>Our Law Firm has a Family Law Department dedicated towards your needs. We have both a man or woman on staff that has extensive experience with divorce. We understand that it's a tough time for you, which is why we do everything that we can in order to provide a relaxing environment. We can handle everything from a simple San Dimas divorce to a more complicated multiple property ugly dispute. No Matter what type of divorce you face, rest assured the Law Offices of Marc Grossman is on your side. Divorce isn't the only type of case we take on we can assist you with child custody, child visitation and divorce mediation.</p>
<h3>San Dimas DUI Attorney Services</h3>
<p>If you have been charged with a DUI your license is at risk. Since we understand the law we can assist you with your San Dimas DUI. Our Initial DUI consultation is Free in which you will speak with an experienced attorney that has 15 years of police experience. If you or a loved one has been charged with driving under the influence of alcohol or drugs rest assured that you'll have an aggressive DUI defense attorney on your side.</p>
<p>The Law Offices of Marc Grossman would like to thank you for visiting our website. We wish you the best of luck with your case whether you decide to hire us or not.</p>
</asp:Content>

