﻿<%@ Page Title="Chino Hills CA Bankruptcy Attorney, Divorce Lawyer, DUI Lawyer" Language="C#" MasterPageFile="~/MasterPages/SiteMainInternal.master" AutoEventWireup="true" CodeFile="chino-hills.aspx.cs" Inherits="lawyer_chino_hills" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <meta name="description" content="Chino Hills Lawyers who can help you with your legal needs. We can assist you with Chino Hills Bankruptcy, Chino Hills Divorce and Chino Hills DUI. Contact Us today for a FREE Initial Consultation. "/>
<meta name="keywords" content="lawyer, attorney, law firm, law office, legal advice, litigation, civil litigation, criminal defense, lawyer, attorney,"/>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainLogo" Runat="Server">
	<p style="text-align:center"><a href="http://www.wefight4you.com/" title="Chino Hills lawyers">
	                <img src="http://www.wefight4you.com/images/teamofattorneys.jpg" alt="Chino Hills Attorneys" height="244px" width="800px"></a></p>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="PageTitleH1" Runat="Server">
    <h1 id="pageTitle">Chino Hills Attorneys - FREE Initial Consultations</h1>
    <h2>If need legal representation for Bankruptcy, Divorce and a DUI Contact Us today!</h2>
    <p style="text-align:center; color:White; font-weight:bold"><a href="#chino-hills-bankruptcy">Chino Hills Bankruptcy</a> | <a href="#chino-hills-divorce">
        Chino Hills Divorce</a> | <a href="#chino-hills-dui">Chino Hills DUI</a></p>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="MainContent" Runat="Server">
   <p>Our Chino Hills Attorneys have what it takes to represent you. We are a general 
       practice law firm that can handle everything from bankruptcy, family law and workers compensation to DUI and Civil cases. We have 7 
       Chino Hills lawyers available to you, when you need us. We have a reputation of keeping your costs down, 
       whether you&#39;re filing for <a href="#chino-hills-bankruptcy">Chino Hills Bankruptcy</a>, <a href="#chino-hills-divorce">Chino Hills Divorce</a> or a DUI 
       we are the law firm that is on your side. We are not in the practice of taking our clients for a ride with their legal fees. Since 1998 we have served local Chino Hills residents and would like the opportunity to represent you with your legal matter.</p>

<p>Our Initial Consultations are FREE and at no obligation to you. Our consultations usually consist of getting to know you and your case. We feel that face-to-face consultations make our clients feel comfortable with our services. We provide a relaxed environment and never force or budge you to hire us. That's up to you!</p>

<h2><a name="chino-hills-bankruptcy">Chino Hills residents, We help you file for bankruptcy</a></h2><a href="http://www.wefight4you.com/bankruptcy/Default.aspx" title="Filing for Bankruptcy">
<img src="http://www.wefight4you.com/images/chino-hills-bankruptcy.jpg" 
        align="left" alt="Questions about filing Bankruptcy?" style="padding-right: 5px;"/></a>
<p>If this is your first time looking into <a href="http://chinohillsbankruptcyattorney.com/" title="Chino Hills bankruptcy attorney">filing for bankruptcy in Chino Hills</a> there are a few options available to you. Most of our clients qualify for Chapter 7 Debt Relief. A Chapter 7 bankruptcy removes most if not all of your debt. To see if you qualify for Chapter 7 bankruptcy contact us today for a Free Initial Consultation.</p>

<p>Our Law Office can not only assist you with <a href="/bankruptcy/chapter-7.aspx">Chapter 7 bankruptcy</a>, but we have experience with Chapter 11 and Chapter 13 bankruptcy cases. While a Chapter 7 mainly deals with individuals Chapter 11's and 13's focus around small business's and people with larger incomes. If you would like to see what type of bankruptcy you qualify for, don't hesitate to contact us.</p>

<h2><a name="chino-hills-divorce">We are Attorneys that believe in a smooth Family Law Process</a></h2> <a href="http://www.wefight4you.com/divorce/Default.aspx" title="Divorce">
<img src="http://www.wefight4you.com/images/chino-hills-divorce.jpg" 
        align="left" alt="Questions about Divorce?" style="padding-right: 5px;"/></a>
<p>Our main goal of a family law issue is our clients. We go beyond the law and look at the people involved. Whether you need legal assistance with divorce, child custody, child visitation or a divorce mediation we are there for you every step of the way. We go out of our way to make you feel comfortable when you meet us for the first time. We provide Free initial Consultations for <a href="http://chinohillsfamilylawattorney.com/" title="Chino Hills family law">Chino Hills family law</a>. This way we can meet face-to-face in order to inform you about the family law process.</p>

<h3>Divorce - We help you get on with your life</h3>
<p>We understand divorce. Our main goal of a divorce case to help you get on with your life as quick as possible. We can work with both sides of the divorce or just one to make sure the separation goes as smoothly. We offer divorce mediation services if both parties agree to the terms. Divorce mediation is the best course of action if both parties get along. Unfortunately, that's not always the case. If we do have to go to trial over a divorce we are there with you every step of the way.</p>

<h3><a name="chino-hills-dui">Charged with a DUI in Chino Hills?</a></h3> <a href="http://www.wefight4you.com/criminal-defense/dui.aspx" title="Filing for Bankruptcy">
<img src="http://www.wefight4you.com/images/chino-hills-dui.jpg" 
        align="left" alt="DUI" style="padding-right: 5px;"/></a>
<p>A DUI is a serious offense. If you have been charged with a <a href="http://chinohillsduiattorneys.com/" title="Chino Hills DUI Attorneys">DUI in Chino Hills</a>, you have not completely lost. You will have to work fast if you were charged. Immediately upon being charged you've noticed the police officer took your drivers licensee. After the charge the DMV automatically conducts an administrative review. The administrative review includes the officer's report, the suspension and any test results taken. You have 10 days to request a hearing with the DMV regarding the suspension of your license. We work with you so that you're prepared for the DMV hearing.</p>
<p>Whether you legal issue is bankruptcy, family law, workers compensation or a DUI charge we provide FREE initial consultations.</p> 
<h2>Other Chino Hills Websites From the Law Offices of Marc Grossman</h2>
<a href="http://chinohillsbankruptcyattorney.com/" title="Chino Hills Bankruptcy Attorneys">Chino Hills Bankruptcy Attorneys</a><br />
<a href="http://chinohillsdivorceattorneys.com/" title="Chino Hills Divorce Attorneys">Chino Hills Divorce Attorneys</a><br />
<a href="http://chinohillsworkerscompensationattorney.com/" title="Chino Hills Workers Compensation Attorneys">Chino Hills Workers Compensation Attorneys</a><br />
<a href="http://chinohillsfamilylawattorney.com/" title="Chino Hills Family Law Attorney">Chino Hills Family Law Attorney</a><br />
<a href="http://chinohillsduiattorneys.com/" title="Chino Hills DUI Attorneys">Chino Hills DUI Attorneys</a><br /><br /><h3 style="text-align:center">Chino Hills Office location</h3>
<p style="text-align:center">
	<iframe width="300" height="300" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="http://maps.google.com/maps?q=5811+Pine+Avenue,+Chino+Hills,+CA+91709&amp;ie=UTF8&amp;hq=&amp;hnear=5811+Pine+Ave,+Chino+Hills,+San+Bernardino,+California+91709&amp;gl=us&amp;z=14&amp;iwloc=r5&amp;ll=33.946997,-117.677964&amp;output=embed"></iframe><br /><small><a href="http://maps.google.com/maps?q=5811+Pine+Avenue,+Chino+Hills,+CA+91709&amp;ie=UTF8&amp;hq=&amp;hnear=5811+Pine+Ave,+Chino+Hills,+San+Bernardino,+California+91709&amp;gl=us&amp;z=14&amp;iwloc=r5&amp;ll=33.946997,-117.677964&amp;source=embed" style="color:#0000FF;text-align:left">Chino Hills</a></small></p>
</asp:Content>