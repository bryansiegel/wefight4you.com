﻿<%@ Page Title="Corona Bankruptcy Attorney - Corona Divorce Lawyer - DUI" Language="C#" MasterPageFile="~/MasterPages/SiteMainInternal.master" AutoEventWireup="true" CodeFile="corona.aspx.cs" Inherits="lawyer_corona" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
<meta name="description" content="Located new Corona California, the Law Offices of Marc Grossman can help you with your bankruptcy, divorce or workers compensation issue. If you need assistance with filing for bankruptcy contact us today for a Free initial consultation."/>
<meta name="keywords" content="lawyer, attorney, law firm, law office, legal advice, litigation, civil litigation, criminal defense, lawyer, attorney, divorce, bankruptcy, workers compensation"/>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainLogo" Runat="Server">
<p style="text-align:center"><a href="http://www.wefight4you.com/" title="Corona lawyers">
                <img src="http://www.wefight4you.com/images/teamofattorneys.jpg" alt="Corona Attorney" height="244px" width="800px"></a></p>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="PageTitleH1" Runat="Server">
<h1 id="pageTitle">Corona Attorneys That Will Fight For You</h1>
<p style="text-align:center; color:White; font-weight:bold"><a href="#corona-bankruptcy">Corona Bankruptcy</a> | <a href="#corona-divorce">Corona Divorce</a> | <a href="#family-law">Family Law</a> | <a href="#corona-dui">Corona DUI</a></p>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="MainContent" Runat="Server">
<p>Are you a Corona resident facing a legal issue? The Law Offices of Marc Grossman have helped Corona residents with their legal issues for over 11 years. Our Law Firm puts our clients needs first not our own. Our main office is a mere minutes away from Corona and you'll notice we have competitive Corona attorney fees. We understand that you might be shopping around for an Corona lawyer and would like to say that we offer FREE initial consultations for all case types. The first step to any legal issue is contacting us. We offer free initial consultations to go over your case and decide upon the best course of action. We match you up with the best attorney for your case and help you out every step of the way.</p>

<p>We would like to represent you in your case. Whether you're <a href="http://www.wefight4you.com/bankruptcy/Default.aspx">filing for Corona bankruptcy</a>, <a href="http://www.wefight4you.com/divorce/Default.aspx"> Corona divorce</a>, or have a work related injury we'd like the opportunity to speak with you. We have 7 attorneys to assist you with your legal issue. We match you up with the best Corona attorney for your case and help you out every step of the way.</p>

<h2><a name="corona-bankruptcy">Smothered in debt? Consider Corona Bankruptcy</a></h2>
<a href="http://www.wefight4you.com/bankruptcy/Default.aspx" title="Filing For Bankruptcy in Corona">
<img src="http://www.wefight4you.com/images/corona-bankruptcy.jpg" 
        align="left" alt="Questions about filing Bankruptcy?" style="padding-right: 5px;"/></a>
<p>Sometimes unforeseen events arise that cause you to get behind on your bills. Within the last year due to the current economic climate we've seen an increase in bankruptcy. You're not alone if you can't pay your bills, but what you might not know is whether you qualify to file for bankruptcy? Our Law Firm processes <a href="http://www.wefight4you.com/bankruptcy/chapter-7.aspx">Chapter 7</a>, <a href="http://www.wefight4you.com/bankruptcy/chapter-11.aspx">Chapter 11</a> and <a href="http://www.wefight4you.com/bankruptcy/chapter-13.aspx">Chapter 13 bankruptcy</a>. Each Chapter of bankruptcy is different and the exemptions (what you can claim on the bankruptcy) differ. Depending upon your income, depends on the Chapter that you can file. </p>

<h3>Stop searching around for Corona bankruptcy answers, contact us for FREE</h3>
<p>Instead of searching the internet for your answer, we offer FREE no obligation consultations. During this consultation you'll find out if you qualify and the Chapter of bankruptcy you can qualify for. This way you know where you stand and can start down the road of financial freedom.</p>

<h2><a name="corona-divorce">Filing for Divorce in Corona CA</a></h2>
<a href="http://www.wefight4you.com/divorce/Default.aspx" title="Corona Divorce">
<img src="http://www.wefight4you.com/images/corona-divorce.jpg" 
        align="left" alt="Questions about Divorce?" style="padding-right: 5px;"/></a>
<p>Going through a <a href="http://www.wefight4you.com/divorce/Default.aspx"> Corona divorce</a> is one of the hardest things a person can face. It’s crucial that you hire an experienced divorce attorney to help you with your case. Since 1998 the Law Offices of Marc Grossman has processed hundreds of divorces and would like the opportunity to speak with you about yours.  We walk you through the divorce process and let you know exactly what you are in for. Our goal with our divorce services is to stay out of court if possible. This way you save on lawyer fees and the process goes allot faster.</p>

<h3><a name="family-law">Corona Family Law</a></h3>
<p>If you need legal aid for a <a href="http://www.wefight4you.com/family-law/Default.aspx">family law issue</a> ranging from Child Custody, Child Visitation contact us for a free initial consultation. We have experience with all types of family law matters and would like the opportunity to assist you with yours.</p>

<h3><a name="corona-dui">Corona DUI Defense Attorneys - FREE Initial Consultation</a></h3>
<a href="http://www.wefight4you.com/criminal-defense/dui.aspx" title="Corona DUI">
<img src="http://www.wefight4you.com/images/corona-dui.jpg" 
        align="left" alt="DUI" style="padding-right: 5px;"/></a>
<p>Charged with a DUI? If you were pulled over in Corona and charged with a DUI, you'll need to act fast. Once you're charged you have 10 days to request a hearing with the DMV to challenge the suspension or revocation of your license with the DMV. We'll work with you to make sure you're prepared for the hearing, to help you keep your license. The first DUI consultation is free and offered in our main Upland office. If you need an aggressive DUI Attorney in Corona give the Law Offices of Marc Grossman a call.</p>
<p>No matter what the case type, Corona residents should feel confident to have the law offices of Marc Grossman by their side. If you legal issue comes from bankruptcy, divorce
 or even workers compensation we can assist you. Why wait contact us today for a FREE Initial Consultation.</p>
</asp:Content>

