﻿<%@ Page Title="Disclaimer - Law Firm Law Office of Marc E. Grossman Attorneys Upland, California" Language="C#" MasterPageFile="~/MasterPages/SiteMainInternal.master" AutoEventWireup="true" CodeFile="disclaimer.aspx.cs" Inherits="disclaimer" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
<META NAME="ROBOTS" CONTENT="NOINDEX, NOFOLLOW">
<meta name="keywords" content="Adoption Bad Faith Bad Faith Insurance Bankruptcy Law Child Support Criminal Law Custody &amp; Visitation Discrimination Divorce DUI/DWI Employment Law -- Employee Employment Law -- Employer Estate Planning Family Law Fire Claims Immigration &amp; Naturalization Law Insurance Law Labor Law Landlord/Tenant Motor Vehicle Accidents -- Plaintiff Personal Injury -- Defense Personal Injury -- Plaintiff Probate &amp; Estate Administration Real Estate Law Sexual Harassment Traffic Violations White Collar Crimes Wills Workers' Compensation Law">
<meta name="description" content="Law Office of Marc E. Grossman Upland, California">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainLogo" Runat="Server">
    <img src="images/i-default.jpg" style="width: 802px; height: 228px"/>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="PageTitleH1" Runat="Server">
<h1 id="pageTitle">Disclaimer</h1>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="MainContent" Runat="Server">
<p>The information you obtain at this site is not, nor is it intended to be, legal advice. You should consult an attorney for advice regarding your individual situation. We invite you to contact us and welcome your calls, letters and electronic mail. Contacting us does not create an attorney-client relationship. Please do not send any confidential information to us until such time as an attorney-client relationship has been established.</p>
</asp:Content>

