﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="dui-lawyers.aspx.cs" Inherits="duiadd_dui_lawyers" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>

    <link href="~/css/site_working.css" rel="stylesheet" type="text/css"  media="all"/>
    </head>
  
<body class="design home">
    <form id="form1" runat="server">
    
    <img src="../images/printBanner.gif" id="printBanner" class="printElement" alt="" runat="server" />
    <div class="handheldElement"></div>
<div class="lock">
	<div id="bgBar">
	<br />
	<br />
	<h2 style="text-align:center; color:white; font-size: medium;">San Bernardino County - Los Angeles County</h2>
	<h3 style="text-align:center; color:White; font-size: medium;">FREE Initial Consultation</h3>
	<h3 style="text-align:center; color:White; font-size: medium;">(909) 608 - 1204 </h3>
	
	
	
	</div>
</div>

	<div id="banner">
		<div id="containerFlash">
			<img src="../images/lettermark.gif" id="lettermark" width="43" height="87" 
                alt="Law Offices of Marc Grossman Logo" runat="server" />
			<img src="../images/logo-dui.jpg" id="logo" width="333" height="72" 
                alt="Law Office of Marc E. Grossman | Upland, California - Inland Empire Attorney: Riverside, San Bernardino California" 
                style="left: 119px; top: 46px" runat="server" />
		</div>
	<br />
		<br />
		        
	</div>
	

	<div id="header">
		<div id="containerFlash2">
		<br />
			<img src="../images/dui-lawyers.jpg" />
		</div>
	</div>
	
	<div id="containerColumns">
		<div id="columnMain">
			<div id="containerPageTitle">
			 <h1 id="pageTitle">&nbsp;Aggressive DUI & DWI Attorneys</h1>
			</div>
			<div id="content">
				<!-- ### START CONTENT ### -->
             <p style="text-align:left">If you have been charged with driving under the influence of drugs or alcohol, you have not lost. A DUI is a serious matter. You're faced with hefty fines, loss of your drivers license and even the possibility of jail time. California State law states that if you drive with a BAC (blood alcohol level) of .08 or above you will be charged with a DUI. At the Law Offices of Marc Grossman we have experience with DUI cases and uphold a strong reputation of aggressive San Bernardino DUI attorneys. Just because you were "Charged" with a DUI/DWI doesn't mean that you're convicted! 
</p>
<h2>Once You're Charged with a DUI We Must Act Fast</h2>
<p style="text-align:left">When you were charged the arresting officer took your license away. The DMV will grant you a temporary drivers license for 30 days. Did you know that you have up to 10 business days to file an appeal with the DMV in order to keep your license? Once you hire our firm we will act aggressivly on your part and will guide you through the DMV appeal process.</p>

<p style="text-align:center">
    <img src="../images/marc-e-grossman.jpg" /></p>
    <p style="text-align:center"><strong>Marc Grossman Attorney At Law</strong></p>
<p style="text-align:left">Our lead attorney is well-known through out the community and has experience with the criminal system. One of Marc Grossman's most high profile cases to date involve the release of an inmate who served 21 years in prison. You can read more about that case by <a href="http://www.wefight4you.com/wins/Default.aspx" rel="nofollow">clicking here.</a></p>
<p style="text-align:center">
    <img src="../images/albert-dantin.jpg" /></p>
    <p style="text-align:center"><strong>Albert D'Antin Attorney At Law</strong></p>
<p style="text-align:left">Albert D'Antin was a police officer for over 15 years and has extensive experience with police protocol. When it comes to DUI cases Albert knows the law inside and out. You can read more about Al's Bio by <a href="http://www.wefight4you.com/attorneys/albert-d-antin.aspx">clicking here</a>.</p>
<p style="text-align:left">A DUI will give you a criminal record which can prevent you from receiving professional licenses, getting into some schools, and getting certain jobs.  So remember that pleading guilty to a crime does not mean that you pay a fine and the problem will go away, the conviction can follow you for life.  Because convictions are permanent you need to ensure that you are properly represented and advised before you make any decisions that will affect your entire future.</p>
	<!-- ### END CONTENT ### -->
			</div>
		</div>
<div id="columnSide">
			<div id="navigationPractice">
			<br />
				&nbsp;<br />


<h3 style="margin-left:29px; color:White">Office Locations</h3>
<h3 style="text-align:left; margin-left:29px">Upland Office</h3>
<p style="text-align:left;margin-left:29px; color:White">818 N. Mountain Ave.<br> 
  Upland CA 91786<br />
Phone: 909 - 608 - 1204<br />
Office Hours: 9:00 a.m. - 5:30 p.m.<br />
</p>

<h3 style="text-align:left; margin-left:29px">La Puente Office</h3>
<p style="text-align:left;margin-left:29px; color:White">13052 Valley Blvd. <br>
  La Puente, CA 91746<br />
    Phone: 909 - 608 - 1204<br />
Office Hours: 9:00 a.m. - 5:30 p.m.
</p>

<h3 style="text-align:left; margin-left:29px">Palm Desert Office</h3>
<p style="text-align:left;margin-left:29px; color:White">
72960 Fred Waring Drive<br> 
Palm Desert, CA 92260<br />
    Phone: 909 - 608 - 1204<br />
Office Hours: 9:00 a.m. - 5:30 p.m.<br />
</p>

<h3 style="text-align:left; margin-left:29px">Van Nuys Office</h3>
<p style="text-align:left;margin-left:29px; color:White">
16600 Sherman Way<br> 
Van Nuys, CA 91406<br />
    Phone:909 - 608 - 1204<br />
Office Hours: 9:00 a.m. - 5:30 p.m.</p>
			</div>
			<div id="navigationFeatures" class="clearfix">
			</div>
		
		</div>
	</div>
</div>
<div id="footerWrap">
	<div id="lowerWrap">
	</div>
	<div id="footer" class="clearfix">
		<div id="contactInformation">
<p><strong>Law Office of Marc E. Grossman</strong><br />
818 N. Mountain Avenue, Suite 111<br />
Upland, CA 91786<br />
    909 - 608 - 1204<br />
</p>
		</div>
		<div id="geographicalFooter">
		<img src="../images/logoSmall.gif" id="smalllogo" width="289" height="46" alt="MG - Law Office of Marc E. Grossman" runat="server" /><br />
            <p>&nbsp;</p>
		</div>
	</div>
	<div id="finePrintWrap">
		<div id="finePrint">
			<p id="copyright">&#169; 2009 by Law Offices of Marc Grossman. All rights reserved. <a href="/disclaimer.aspx" rel="nofollow">Disclaimer</a> |</p>
			<p id="branding">&nbsp;</p>
		</div>
		<br class="clear">
	</div>
</div>
<script type="text/javascript">
    var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
    document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
</script>
<script type="text/javascript">
    try {
        var pageTracker = _gat._getTracker("UA-1368144-9");
        pageTracker._trackPageview();
    } catch (err) { }</script>
    </form>
</body>
</html>