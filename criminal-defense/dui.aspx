<%@ Page Title="Aggressive San Bernardino County DUI & DWI Attorney, Lawyer | Law Offices of Marc Grossman" Language="C#" MasterPageFile="~/MasterPages/SiteMainInternal.master" AutoEventWireup="true" CodeFile="dui.aspx.cs" Inherits="criminal_defense_dui" %>

<%@ Register Assembly="RssToolkit" Namespace="RssToolkit.Web.WebControls" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainLogo" Runat="Server">
<a href="http://www.wefight4you.com/" title="Free Initial Consultations for DUI">
 <img src="http://www.wefight4you.com/images/teamofattorneys2.jpg" width="800px" height="244px"/></a>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="PageTitleH1" Runat="Server">
<h1 id="pageTitle">Aggressive San Bernardino County <br /> DUI Attorneys that will Fight For You</h1>
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="MainContent" Runat="Server">
<p style="color:white; font-size:large"><strong>If you have been charged with driving under the influence of drugs or alcohol, you have not lost.</strong></p><br />

<p> A DUI is a serious matter. You're faced with hefty fines, loss of your drivers license and even the possibility of jail time. California State law states that if you drive with a BAC (blood alcohol level) of .08 or above you will be charged with a DUI. At the Law Offices of Marc Grossman we have experience with DUI cases and uphold a strong reputation of aggressive San Bernardino DUI attorneys. Just because you were "Charged" with a DUI/DWI doesn't mean that you're convicted! 
</p>
<h2>Once You're Charged with a DUI We Must Act Fast</h2>
<p>When you were charged the arresting officer took your license away. The DMV will grant you a temporary drivers license for 30 days. Did you know that you have up to 10 business days to file an appeal with the DMV in order to keep your license? Once you hire our firm we will act aggressively on your part and will guide you through the DMV appeal process.</p>

<p style="text-align:center">
    <img src="../images/marc-e-grossman.jpg" alt="DUI Attorney Marc Grossman" /></p>
    <p style="text-align:center"><strong><a href="http://www.wefight4you.com/attorneys/marc-grossman.aspx" title="Marc Grossman">Marc Grossman Attorney At Law</a></strong></p>
<p>Our lead attorney is well-known through out the community and has experience with the criminal system. One of Marc Grossman's most high profile cases to date involve the release of an inmate who served 21 years in prison. You can <a href="http://www.wefight4you.com/wins/Default.aspx" rel="nofollow">read more about that case</a> by clicking here.</p>
<p style="text-align:center">
    <img src="http://www.wefight4you.com/images/albert-dantin.jpg" alt="DUI Attorney Albert D'Antin" /></p>
    <p style="text-align:center"><strong><a href="http://www.wefight4you.com/attorneys/albert-d-antin.aspx">Albert D'Antin Attorney At Law</a></strong></p>
<p>Albert D'Antin was a police officer for over 15 years and has extensive experience with police protocol. When it comes to DUI cases Albert knows the law inside and out. You can read more about Al's Bio by <a href="http://www.wefight4you.com/attorneys/albert-d-antin.aspx" title="Albert D'Antin Attorney">clicking here</a>.</p>
<div class="callOut">
  </div>
<p>A DUI will give you a criminal record which can prevent you from receiving professional licenses, getting into some schools, and getting certain jobs.  So remember that pleading guilty to a crime does not mean that you pay a fine and the problem will go away, the conviction can follow you for life.  Because convictions are permanent you need to ensure that you are properly represented and advised before you make any decisions that will affect your entire future.</p>

</asp:Content>

