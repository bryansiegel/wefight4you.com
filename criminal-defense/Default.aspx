﻿<%@ Page Title="California Criminal Defense Attorney, Attorneys, Lawyer, Lawyers" Language="C#" MasterPageFile="~/MasterPages/SiteMainInternal.master" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="criminal_defense_Default" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
 <meta name="description" content="California Criminal Defense Lawyers that provide a FREE initial consultation throughout Southern California." />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainLogo" Runat="Server">
<a href="http://www.wefight4you.com/" title="Criminal Defense Attorney">
    <img src="../images/teamofattorneys2.jpg" style="width: 800px; height: 244px" title="California Criminal Defense Lawyer"/></a>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="PageTitleH1" Runat="Server">
<h1 id="pageTitle">Criminal Defense Lawyers That Fight For You</h1>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="MainContent" Runat="Server">
<p>We understand that sometimes bad things happen to good people. In a criminal law case it's important that your criminal law attorney does everything possible for your case. Every criminal law case is different. Expect us to handle your case with exceptional persistence and expert trial preparation.</p>
<p>You’ve been charged with a crime and wonder what you should do next. It’s time to contact a local criminal defense lawyer. The Law Offices of Marc Grossman are those lawyers. We understand that your livelihood is on the line and no matter what your criminal charge is we would like to hear from you. We offer FREE Initial Criminal Defense Consultations.</p>
<p>We have locations throughout Southern California to assist you. Whether you're in search of a <a href="http://ranchocucamongacaattorney.com/criminal-defense.aspx" title="Rancho Cucamonga Criminal Defense Attorney">Rancho Cucamonga criminal defense attorney</a>, a <a href="http://chinohillscriminalattorney.com/" title="Chino Hills Criminal defense attorney">Chino Hills criminal defense attorney</a> or one of our many other locations the Law Offices of Marc Grossman has your location covered.</p> 
<h3>You've Been Arrested For A Crime; it's Time For An Attorney</h3>
<p>We have represented hundreds of criminal cases since 1998. Don’t feel embarrassed about your crime; chances are we’ve heard about your situation before.  Unsure of whether you or not you have a case.</p>
<h3>Criminal Law Case Types</h3>
<ul>
<li>Misdemeanor</li>
<li>Felony</li>
<li><a href="http://www.wefight4you.com/criminal-defense/dui.aspx" title="DUI defense">DUI</a></li>
<li>Drug Possession</li>
<li>Assault</li>
<li>Battery</li>
<li>Spousal Assault</li>
<li>Probation</li>
<li>Parole</li>
<li>Possession</li>
<li>Hit and Run</li>
<li>Driving on a suspended license</li>
<li>Concealed weapon</li>
<li>False imprisonment</li>
<li>Jail Time</li>
<li>Prison Time</li>
<li>Habeas</li>
<li>Guilty/ Innocent</li>
<li>Plea Bargain</li>
<li>Police Report</li>
<li>RAP Sheet</li>
</ul>

<h3>I'm Sure I Have A Criminal Case, What's Next?</h3>
<ul>
<li>First Call us at the Law Offices of Marc E. Grossman at 888-407-9068, and schedule your free case evaluation.</li>
<li>Next, remember that you are innocent until proven guilty; this means that the prosecution must prove every element of the crime against you beyond a reasonable doubt.  If the prosecution fails to prove each and every element of the crimes with which you are charged beyond a reasonable doubt you are not guilty. </li>
<li>After you are arrested or charged, you will have an initial appearance.  In your initial appearance, you will plead not guilty.  Then the judge will establish if there is probable cause to allow your case to proceed to trial, if there is not probable cause that you committed a crime, the judge will dismiss the case.    </li>
<li>Bail hearings are held to modify the amount of bond that a judge has set to ensure your appearance at trial.  Bail can be modified if the amount is too high.  </li>
</ul>
<p><strong>Evidentiary Hearings</strong><br />Evidentiary hearings are held before your trial begins to determine whether a piece of evidence is admissible against you.</p>
<ul>
<li>Not all evidence is admissible against you in a criminal case.  Evidence which is gathered in violation of the 4th, 5th and 6th Amendments is not admissible.  </li>
</ul>
<p><strong>	Some common examples of Inadmissible Evidence Are:</strong></p>
<ul>
<li>Searches of your home without a warrant;</li>
<li>Illegal pat-downs;</li>
<li>Illegal electronic surveillance;</li>
<li>Statements you make without receiving Miranda Warnings;</li>
<li>Statements you make after asking for a lawyer;</li>
<li>Evidence found after an illegal stop or arrest.</li>
</ul>
<p>	Remember if you are ever being investigated for a crime that the United States Supreme Court held in the landmark case of Miranda v. Arizona, that the Constitution requires that before being interrogated suspects be informed of their right to remain silent and their right to an attorney.  These two rights are critical to a successful defense.  </p>
<p>Anything you say can and will be used against you, so if you have criminal law problems, do not answer any questions from the police and tell the police that you want an attorney.   At the Law Offices of Marc E. Grossman, we are here day and night to protect your rights. </p>
<p>Whether or not evidence is admitted will make your case or break your case; that is why being represented by experienced criminal defense attorneys at your evidentiary hearings is critical.</p>
    </ul>
<h3>Plea Bargains and Agreements</h3>
<p>Over 50% of all criminal cases do not go to trial.  These cases end in plea-bargains.  Here at The Law Offices of Marc Grossman, each of our criminal defense attorneys are skilled and effective negotiators who can get you the best deal if you decide you do not want to go through the time and defense of a lengthy trial.  
</p>
</asp:Content>

