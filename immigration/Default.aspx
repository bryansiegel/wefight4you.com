﻿<%@ Page Title="Upland Immigration Attorney, Lawyer in San Bernardino, CA Citizenship Attorney California Visa" Language="C#" MasterPageFile="~/MasterPages/SiteMainInternal.master" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="immigration_Default" %>

<%@ Register Assembly="RssToolkit" Namespace="RssToolkit.Web.WebControls" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
<meta name="keywords" content="lawyer, attorney, law firm, law office, legal advice, immigration, citizenship, visa, naturalization, green card, PERM, H1-B, deportation, work visa, employment, family visa, lawyer,">
<meta name="description" content="Do you need an experienced attorney to handle your immigration case? Call 1-888-407-9068 for a free consultation with an Upland, California lawyer at the Law Office of Marc E. Grossman.">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainLogo" Runat="Server">
<a href="http://www.wefight4you.com/" title="Immigration Lawyer">
    <img src="../images/teamofattorneys2.jpg" alt="Immigration Attorney" style="width: 800px; height: 244px"/></a>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="PageTitleH1" Runat="Server">
<h1 id="pageTitle">Immigration Attorney</h1>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="MainContent" Runat="Server">
<h2>Upland Immigration Attorney</h2>
<p>United States immigration laws are complex. Often, they can seem like impenetrable barriers. At the Law Office of Marc E. Grossman, we can help you challenge these barriers and work to overcome them. We are dedicated to standing by your side to help you pursue the results you need, whether you are an employer seeking immigrant workers or an immigrant seeking to work or live in this country.</p>
<h3>Free initial consultations are available</h3>
<p><a href="/Forms/contact.aspx" rel="nofollow">E-mail us</a> or call us toll free at 1-888-407-9068 to discuss your case with an experienced immigration lawyer.</p>
<p>We are prepared to spend time with you during your initial consultation. We want to get to know you and start to learn about your needs. We want to help you understand your options so you can get started on the right path with your immigration case.</p>
<h3>Smart, honest immigration representation</h3>
<p>People in Upland, California, and throughout the Inland Empire turn to our law firm. We are committed to providing smart, honest representation. That is the type of representation you deserve.</p>
<p><a href="/Forms/contact.aspx" rel="nofollow">At the Law Office of Marc E. Grossman</a>,  we can handle any matter related to immigration and naturalization, including:</p>

<ul>
   <li>United States citizenship</li>
   <li>Green cards</li>
   <li>Visas</li>
   <li>Business visas</li>
   <li>Employee visas</li>
   <li>Family visas</li>
   <li>Deportation proceedings</li>
   <li>PERM</li>
</ul>

<p>We want to do what is right for you. That means taking the time to find out about your needs. We will carefully weigh the options that are available to you and present you with the one that we believe is most appropriate for reaching your goals. We will make certain that you are involved at every step.</p>
<p>To schedule a free initial consultation about your immigration case, <a href="/Forms/contact.aspx" rel="nofollow">e-mail us</a> or call us toll free at 1-888-407-9068 to speak with an attorney.</p>
    <div class="callOut">
<h3 style="text-align:center">Latest Immigration News</h3><br />
<asp:DataList ID="DataList1" runat="server" DataSourceID="RssDataSource1" 
        Width="404px">
 <ItemTemplate>
 <asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl='<%# Eval("link") %>' Text='<%# Eval("title") %>'></asp:HyperLink>
 </ItemTemplate>
    </asp:DataList>
    <cc1:RssDataSource ID="RssDataSource1" runat="server" MaxItems="3" 
        Url="http://www.opencongress.org/issues/atom/4514_immigration">
    </cc1:RssDataSource>
    </div>
</asp:Content>

