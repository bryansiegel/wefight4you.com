﻿<%@ Page Title="Avalon California Bankruptcy Attorney, Lawyer" Language="C#" MasterPageFile="~/MasterPages/SiteMainInternal.master" AutoEventWireup="true" CodeFile="avalon.aspx.cs" Inherits="los_angeles_avalon" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainLogo" Runat="Server">
<img src="../images/california-lawyers.jpg" title="Avalon Bankruptcy" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="PageTitleH1" Runat="Server">
<h1 id="pageTitle">Avalon Bankruptcy Lawyer</h1>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="MainContent" Runat="Server">
<p>Considering Bankruptcy in Avalon? FREE Initial Consultation</p>
<p>La Puente Office<br />
13052 Valley Blvd. <br />
La Puente, CA 91746<br />
Phone: 888-407-9068<br />
Office Hours: 9:00 a.m. - 5:00 p.m.</p>
</asp:Content>
