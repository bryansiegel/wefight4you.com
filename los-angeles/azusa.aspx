﻿<%@ Page Title="Azusa California Bankruptcy Attorney Lawyer" Language="C#" MasterPageFile="~/MasterPages/SiteMainInternal.master" AutoEventWireup="true" CodeFile="azusa.aspx.cs" Inherits="los_angeles_azusa" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainLogo" Runat="Server">
<img src="../images/california-lawyers.jpg" title="Azusa Bankruptcy" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="PageTitleH1" Runat="Server">
<h1 id="pageTitle">Azusa Bankruptcy Lawyer</h1>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="MainContent" Runat="Server">
<p>Considering Bankruptcy in Azusa? FREE Initial Consultation</p>
<p>La Puente Office<br />
13052 Valley Blvd. <br />
La Puente, CA 91746<br />
Phone: 888-407-9068<br />
Office Hours: 9:00 a.m. - 5:00 p.m.</p>
<iframe width="425" height="350" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="http://maps.google.com/maps?f=d&amp;source=s_d&amp;saddr=azusa+ca&amp;daddr=13052+Valley+Blvd.++La+Puente,+CA+91746&amp;hl=en&amp;geocode=FXPWCAIdld_4-CnjSxVBQunCgDFbdS998xuaJQ%3BFfuXBwIdt3P3-Ck9yPd0JNfCgDEJ6vpDE9XEzg&amp;mra=ls&amp;sll=34.111236,-117.931709&amp;sspn=0.204094,0.363579&amp;ie=UTF8&amp;ll=34.093195,-117.95658&amp;spn=0.08219,0.09804&amp;output=embed"></iframe><br /><small><a href="http://maps.google.com/maps?f=d&amp;source=embed&amp;saddr=azusa+ca&amp;daddr=13052+Valley+Blvd.++La+Puente,+CA+91746&amp;hl=en&amp;geocode=FXPWCAIdld_4-CnjSxVBQunCgDFbdS998xuaJQ%3BFfuXBwIdt3P3-Ck9yPd0JNfCgDEJ6vpDE9XEzg&amp;mra=ls&amp;sll=34.111236,-117.931709&amp;sspn=0.204094,0.363579&amp;ie=UTF8&amp;ll=34.093195,-117.95658&amp;spn=0.08219,0.09804" style="color:#0000FF;text-align:left">View Larger Map</a></small>
</asp:Content>

