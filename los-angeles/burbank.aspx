﻿<%@ Page Title="Burbank Bankruptcy Attorney, Lawyer" Language="C#" MasterPageFile="~/MasterPages/SiteMainInternal.master" AutoEventWireup="true" CodeFile="burbank.aspx.cs" Inherits="los_angeles_burbank" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainLogo" Runat="Server">
<img src="../images/california-lawyers.jpg" title="Burbank Bankruptcy" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="PageTitleH1" Runat="Server">
<h1 id="pageTitle">Burbank Bankruptcy Lawyer</h1>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="MainContent" Runat="Server">
<p>Considering Bankruptcy in Burbank? FREE Initial Consultation</p>
<p>La Puente Office<br />
13052 Valley Blvd. <br />
La Puente, CA 91746<br />
Phone: 888-407-9068<br />
Office Hours: 9:00 a.m. - 5:00 p.m.</p>
<iframe width="425" height="350" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="http://maps.google.com/maps?f=d&amp;source=s_d&amp;saddr=burbank,+California&amp;daddr=13052+Valley+Blvd.++La+Puente,+CA+91746&amp;hl=en&amp;geocode=FeeOCQIdmr_y-CmVxRgoFZXCgDHUih-Qr5F3MA%3BFfuXBwIdt3P3-Ck9yPd0JNfCgDEJ6vpDE9XEzg&amp;mra=ls&amp;sll=34.099525,-117.980595&amp;sspn=0.102061,0.181789&amp;ie=UTF8&amp;ll=34.104465,-118.155135&amp;spn=0.15293,0.30889&amp;output=embed"></iframe><br /><small><a href="http://maps.google.com/maps?f=d&amp;source=embed&amp;saddr=burbank,+California&amp;daddr=13052+Valley+Blvd.++La+Puente,+CA+91746&amp;hl=en&amp;geocode=FeeOCQIdmr_y-CmVxRgoFZXCgDHUih-Qr5F3MA%3BFfuXBwIdt3P3-Ck9yPd0JNfCgDEJ6vpDE9XEzg&amp;mra=ls&amp;sll=34.099525,-117.980595&amp;sspn=0.102061,0.181789&amp;ie=UTF8&amp;ll=34.104465,-118.155135&amp;spn=0.15293,0.30889" style="color:#0000FF;text-align:left">View Larger Map</a></small>
</asp:Content>


